# Tide Symfony Skeleton 

## Download dependencies

composer install

## Create private and pulbic keys to generate JWTs

mkdir -p config/jwt 

openssl genrsa -out config/jwt/private.pem -aes256 4096

openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem

(private phrase must be specified in .env as JWT_PASSPHRASE)

## Configure database
Edit .env file and set the database user, password and name.

DATABASE_URL=mysql://user:password@127.0.0.1:3306/database


## Create and update database
php bin/console doctrine:database:create 

php bin/console doctrine:migrations:migrate

## Seed database
`php bin/console doctrine:fixtures:load`

## Generate and update permissions
php bin/console permissions:update

## Create user
php bin/console user:create username email@example.com password role

example: php bin/console user:create admin admin@admin.com admin SUPER_ADMIN

(default available role: SUPER_ADMIN)

## Run server
`php bin/console server:run`

or you can also run:

`symfony server:start`


## Webhook for connection to SATWS service

### Webhook for download invoices

To chain extractions with their own document download, is needed to set the next URL into SATWS Panel under section Webhooks:

`https://<domain_name>/satws/hooks/extraction_updated`

this URL should have `Extraction Actualizada` in their respective field and should registred with flag `Habilitado` enabled.
The extractions will be fired by a cronjob into the server and it will create every event for updated extraction handled in this webhook.

### Webhook for connecting credentials

Due to every new credential is registred with status pending, is needed to define a Webhook which handle the update event fired for SATWS. To perform that action, is needed to set the next URL into SATWS Panel under section Webhooks;

`https://<domain_name>/satws/hooks/credential_updated`

this URL should have `Credential Actualizada` in their respective field and should registred with flag `Habilitado` enabled.
