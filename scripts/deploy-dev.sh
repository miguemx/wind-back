
SSH_USER="tidi" #tidi
SSH_DOMAIN="tide.company" #tide.company
PROJECT_FULL_PATH="/home/tidi/wind-dev-back" #/home/tidi/example-back

#!/usr/bin/env bash
withComposer=1
withComposerCommand=''
while [ "$1" != "" ]; do
    case $1 in
        -nc | --no_composer)    withComposer=0
                                ;;
    esac
    shift
done

if [ "$withComposer" = "1" ]; then
    withComposerCommand='/usr/local/bin/ea-php81 $(which composer) install'
fi


rsync -azr -v --delete --exclude=.env --exclude=var --exclude=config/jwt --exclude=.git --exclude=vendor --exclude=uploads --exclude=interface --exclude=.idea . $SSH_USER@$SSH_DOMAIN:$PROJECT_FULL_PATH
ssh $SSH_USER@$SSH_DOMAIN "
    cd $PROJECT_FULL_PATH
    $withComposerCommand
    rm -rf var/cache
    php bin/console cache:warmup
    php bin/console --no-interaction doctrine:migrations:migrate
    php bin/console permissions:update
    "
