#!/usr/bin/env bash

databaseUser=${1:-root}
databasePassword=${2:-root}
databaseName=${3:-wind}

REMOTE_DB_USER="wind"
REMOTE_DB_NAME="wind"
SSH_USER="wind"
SSH_DOMAIN="windapp.mx"

ssh $SSH_USER@$SSH_DOMAIN "mysqldump -u$REMOTE_DB_USER -p $REMOTE_DB_NAME| gzip -c" | gunzip > $REMOTE_DB_NAME.sql
mysql -u$databaseUser -p$databasePassword -D $databaseName -e "DROP DATABASE $databaseName"
mysql -u$databaseUser -p$databasePassword -e "CREATE DATABASE $databaseName"
mysql -u$databaseUser -p$databasePassword $databaseName<$REMOTE_DB_NAME.sql

rm $REMOTE_DB_NAME.sql
