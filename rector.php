<?php

declare(strict_types=1);

use Rector\Core\Configuration\Option;
use Rector\Php74\Rector\Property\TypedPropertyRector;
use Rector\Php80\Rector\Class_\AnnotationToAttributeRector;
use Rector\Php80\Rector\Class_\DoctrineAnnotationClassToAttributeRector;
use Rector\Php80\ValueObject\AnnotationToAttribute;
use Rector\Set\ValueObject\LevelSetList;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Rector\Doctrine\Set\DoctrineSetList;
use Rector\Symfony\Set\SymfonySetList;
use Rector\Symfony\Set\SensiolabsSetList;
use Rector\Nette\Set\NetteSetList;

return static function (ContainerConfigurator $containerConfigurator): void {
    // get parameters
    $parameters = $containerConfigurator->parameters();
    $parameters->set(Option::PATHS, [
        __DIR__ . '/src'
    ]);

    // Define what rule sets will be applied
    $containerConfigurator->import(LevelSetList::UP_TO_PHP_80);

    // get services (needed for register a single rule)
    // $services = $containerConfigurator->services();

    // register a single rule
    // $services->set(TypedPropertyRector::class);
	$services = $containerConfigurator->services();

	$services->set(DoctrineAnnotationClassToAttributeRector::class)
	         ->configure([
		         DoctrineAnnotationClassToAttributeRector::REMOVE_ANNOTATIONS => false,
	         ]);
	$services->set(AnnotationToAttributeRector::class)->configure([
		new AnnotationToAttribute('ApiPlatform\Core\Annotation\ApiResource'),
		new AnnotationToAttribute('App\Annotation\TenantAwareAnnotation'),
		new AnnotationToAttribute('App\Annotation\ClientAwareAnnotation'),
		new AnnotationToAttribute('Vich\UploaderBundle\Mapping\Annotation\Uploadable'),
		new AnnotationToAttribute('Vich\UploaderBundle\Mapping\Annotation\UploadableField'),
		new AnnotationToAttribute('Gedmo\Mapping\Annotation\Loggable'),
		new AnnotationToAttribute('Gedmo\Mapping\Annotation\Versioned'),
		new AnnotationToAttribute('Gedmo\Mapping\Annotation\Timestampable'),
		new AnnotationToAttribute('ApiPlatform\Core\Annotation\ApiFilter'),
		new AnnotationToAttribute('Symfony\Component\Validator\Constraint\ValidAppFile'),
	]);
	$containerConfigurator->import(DoctrineSetList::ANNOTATIONS_TO_ATTRIBUTES);
	$containerConfigurator->import(SymfonySetList::ANNOTATIONS_TO_ATTRIBUTES);
	$containerConfigurator->import(NetteSetList::ANNOTATIONS_TO_ATTRIBUTES);
	$containerConfigurator->import(SymfonySetList::SYMFONY_54);
	$containerConfigurator->import(SensiolabsSetList::FRAMEWORK_EXTRA_61);



};


