<?php

namespace App\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
#[\Attribute( \Attribute::TARGET_CLASS )]
final class TenantAwareAnnotation {
	public $tenantFieldName;

    /**
     * @param $tenantFieldName
     */
    public function __construct($tenantFieldName)
    {
        $this->tenantFieldName = $tenantFieldName;
    }


}
