<?php

namespace App\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
#[\Attribute( \Attribute::TARGET_PROPERTY )]
final class AppFileAnnotation {
	public $fileType;
}