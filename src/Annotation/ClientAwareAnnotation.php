<?php

namespace App\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
#[\Attribute( \Attribute::TARGET_CLASS )]
final class ClientAwareAnnotation {
	public $clientFieldName;

    /**
     * @param $clientFieldName
     */
    public function __construct($clientFieldName)
    {
        $this->clientFieldName = $clientFieldName;
    }


}
