<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

trait Commentable {
	/**
	 * @var Conversation
	 */
	#[Assert\Valid]
	#[Groups( [ 'conversation_read', 'conversation_write' ] )]
	#[ORM\OneToOne( targetEntity: Conversation::class, cascade: [ 'persist', 'remove' ] )]
	private $conversation;


	public function getConversation(): ?Conversation {
		return $this->conversation;
	}

	public function setConversation( ?Conversation $conversation ): self {
		$this->conversation = $conversation;

		return $this;
	}
}
