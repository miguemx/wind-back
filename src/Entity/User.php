<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * //Don't add client aware annotation becase users without client relationship (TIDE users) can add
 * comments and other stuff, clients must see those users otherwise some 404 would be shown.
 * Users filter should be added as an extension when requesting the users endpoint.
 */
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ORM\Table( name: 'user' )]
#[ORM\Index( name: 'user_full_name_idx', columns: [ 'full_name' ] )]
#[Gedmo\Loggable]
#[ApiFilter( SearchFilter::class, properties: [
	'username'  => 'partial',
	'email'     => 'partial',
	'role.name' => 'partial'
] )]
#[ApiFilter( BooleanFilter::class, properties: [ 'isActive' ] )]
#[ApiFilter( OrderFilter::class, properties: [ 'username', 'email' ] )]
#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('USER_LIST') or object == user" ],
		'post' => [ 'method' => 'POST', 'access_control' => "is_granted('USER_CREATE')" ]
	], itemOperations: [
	'get'     => [ 'method' => 'GET', 'access_control' => "is_granted('USER_SHOW')" ],
	'put'     => [ 'method' => 'PUT', 'access_control' => "is_granted('USER_UPDATE') or object == user" ],
	'delete'  => [ 'method' => 'DELETE', 'access_control' => "is_granted('USER_DELETE')" ],
	'user_me' => [
		'method'          => 'GET',
		'route_name'      => 'user_me',
		'swagger_context' => [ 'summary' => 'Get logged User', 'parameters' => [] ]
	],
	'logout'  => [
		'method'          => 'GET',
		'route_name'      => 'user_logout',
		'swagger_context' => [
			'summary'    => 'Remove cookie token header',
			'parameters' => []
		]
	]
], attributes: [
	'normalization_context'   => [ 'groups' => [ 'user_read', 'role_read' ] ],
	'denormalization_context' => [
		'groups' => [
			'user_write',
			'user_create',
			'client_write'
		]
	]
] )]
#[ORM\Entity( repositoryClass: UserRepository::class )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[UniqueEntity( fields: [ 'username' ] )]
#[UniqueEntity( fields: [ 'email' ] )]
#[ApiFilter( SearchFilter::class, properties: [ 'client' => 'exact', 'name' => 'partial' ] )]
class User implements UserInterface, \Serializable, PasswordAuthenticatedUserInterface {
	#[ORM\Column( type: 'integer' )]
	#[ORM\Id]
	#[Groups( [ 'user_read', 'user_read_id' ] )]
	#[ORM\GeneratedValue( strategy: 'AUTO' )]
	#[Gedmo\Versioned]
	private int $id;

	#[ORM\Column( type: 'string', length: 60, unique: true )]
	#[Assert\NotBlank]
	#[Assert\Length( max: 60 )]
	#[Groups( [ 'user_create', 'user_read', 'user_read_username', 'client_write' ] )]
	#[Gedmo\Versioned]
	private string $username;

	#[ORM\Column( type: 'string', length: 200 )]
	#[Assert\Length( max: 200, min: 4 )]
	#[Groups( [ 'user_write', 'client_write' ] )]
	#[Gedmo\Versioned]
	private string $password = "----";

	private ?string $plainPassword = null;

	#[ORM\Column( type: 'string', length: 60, unique: true, nullable: true )]
	#[Assert\Length( max: 60 )]
	#[Assert\Email]
	#[Groups( [ 'user_write', 'user_read', 'user_read_email', 'client_write' ] )]
	#[Gedmo\Versioned]
	private string $email;

	#[ORM\Column( name: 'is_active', type: 'boolean' )]
	#[Groups( [ 'user_read', 'user_read_is_active', 'client_write' ] )]
	#[Gedmo\Versioned]
	private bool $isActive = true;

	/**
	 * @var Role
	 */
	#[Groups( [ 'user_write', 'user_read_role' ] )]
	#[ORM\ManyToOne( targetEntity: Role::class, cascade: [ 'persist' ], inversedBy: 'users' )]
	#[ORM\JoinColumn( onDelete: 'SET NULL' )]
	#[Gedmo\Versioned]
	private ?Role $role;

	/**
	 * @Gedmo\Blameable(on="create")
	 */
	#[ORM\ManyToOne( targetEntity: User::class )]
	#[Groups( [ 'user_read_created_by' ] )]
	#[ORM\JoinColumn( name: 'created_by', referencedColumnName: 'id', onDelete: 'SET NULL', nullable: true )]
	private User $createdBy;

	/**
	 * @Gedmo\Blameable(on="update")
	 */
	#[ORM\ManyToOne( targetEntity: User::class )]
	#[Groups( [ 'user_read_updated_by' ] )]
	#[ORM\JoinColumn( name: 'updated_by', referencedColumnName: 'id', onDelete: 'SET NULL', nullable: true )]
	private User $updatedBy;

	#[Groups( [ 'user_read_created_date' ] )]
	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Gedmo\Timestampable( on: 'create' )]
	private \DateTime $createdDate;

	#[Groups( [ 'user_read_updated_date' ] )]
	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Gedmo\Timestampable( on: 'update' )]
	private \DateTime $updatedDate;

	#[Groups( [ 'user_read_notifications' ] )]
	#[ORM\OneToMany( mappedBy: 'user', targetEntity: Notification::class )]
	private Collection|array $notifications;

	/**
	 * @var PermissionGroup[]
	 */
	#[ORM\ManyToMany( targetEntity: PermissionGroup::class, inversedBy: 'users' )]
	#[Groups( [ 'permission_group_read', 'user_write' ] )]
	#[ORM\OrderBy( [ 'name' => 'ASC' ] )]
	#[ORM\JoinTable( name: 'users_permission_groups', joinColumns: [
	   new ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id' ),
	   new ORM\JoinColumn(name: 'permission_group_id', referencedColumnName: 'id')
	] )]
	private Collection|array $permissionGroups;

	/**
	 * @var NotificationTopic []
	 */
	#[Groups( [ 'user_read_disabled_notification_topics' ] )]
	#[ORM\ManyToMany( targetEntity: NotificationTopic::class )]
	private Collection|array $disabledNotificationTopics;

	#[ORM\Column( type: 'string', length: 200, nullable: true )]
	private ?string $recoveryToken;

	#[ORM\Column( type: 'date', nullable: true )]
	private ?\DateTime $recoveryTokenCreationDate;

	#[Groups( [ 'user_read', 'user_write', 'user_read_name', 'client_write' ] )]
	#[ORM\Column( type: 'string', length: 100 )]
	#[Gedmo\Versioned]
	private $name;

	#[Groups( [ 'user_read', 'user_write', 'user_read_last_name', 'client_write' ] )]
	#[ORM\Column( type: 'string', length: 100, nullable: true )]
	#[Gedmo\Versioned]
	private $lastName;

	#[Groups( [ 'user_read', 'user_read_full_name', 'client_write' ] )]
	#[ORM\Column( type: 'string', length: 200 )]
	#[Gedmo\Versioned]
	private $fullName;

	#[Groups( [ 'user_read', 'user_write', 'user_read_phone', 'client_write' ] )]
	#[ORM\Column( type: 'string', length: 20, nullable: true )]
	#[Gedmo\Versioned]
	private $phone;

	#[Groups( [ 'user_write', 'user_read_avatar' ] )]
	#[ORM\OneToOne( targetEntity: 'AppFile', cascade: [ 'persist' ] )]
	#[Gedmo\Versioned]
	private $avatar;

	#[Groups( [ 'user_write', 'user_read_client', 'client_write' ] )]
	#[ORM\ManyToOne( targetEntity: Client::class, inversedBy: 'users' )]
	#[Gedmo\Versioned]
	private $client;

	#[Groups( [ 'user_write', 'user_read_client', 'client_write' ] )]
	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	private $tenant;

	#[ORM\Column( name: 'is_company_owner', type: 'boolean' )]
	#[Groups( [ 'user_read', 'user_write', 'user_read_is_company_owner', 'client_write' ] )]
	#[Gedmo\Versioned]
	private bool $isCompanyOwner = false;

	#[ORM\Column( type: 'string', length: 255, nullable: true )]
	#[Groups( [ 'user_read', 'user_write', 'user_read_position', 'client_write' ] )]
	private $position;

	#[ORM\OneToMany( targetEntity: Message::class, mappedBy: 'user' )]
	private $messages;

	#[ORM\OneToMany( targetEntity: ChannelUser::class, mappedBy: 'user' )]
	private $channelUsers;


	public function __construct() {
		$this->notifications              = new ArrayCollection();
		$this->disabledNotificationTopics = new ArrayCollection();
		$this->permissionGroups           = new ArrayCollection();
		$this->messages                   = new ArrayCollection();
		$this->channelUsers               = new ArrayCollection();
	}

	public function getRole() {
		return $this->role;
	}

	public function setRole( $role ) {
		$this->role = $role;
	}

	public function getRoles(): array {
		$roles = [];
		if ( $this->role ) {
			$roles[] = $this->role->getName();
		}

		return $roles;
	}

	public function setRoles( $roles ) {
		$this->role = $roles[0];
	}

	/**
	 * @ApiProperty(
	 *     attributes={
	 *         "swagger_context"={
	 *             "type"="array",
	 *              "items"={
	 *                  "type"="string"
	 *              }
	 *          }
	 *     }
	 * )
	 */
	#[Groups( [ 'user_read', 'minimal_user_read' ] )]
	public function getPermissionsArray(): array {
		$permissions = [];
		/** @var Permission $permission */
		if ( $this->role ) {
			foreach ( $this->role->getPermissions() as $permission ) {
				$permissions[] = $permission->getName();
			}
		}
		/** @var PermissionGroup $permissionGroup */
		if ( $this->permissionGroups ) {
			foreach ( $this->permissionGroups as $permissionGroup ) {
				/** @var Permission $permission */
				foreach ( $permissionGroup->getPermissions() as $permission ) {
					$permissions[] = $permission->getName();
				}
			}
		}

		return $permissions;
	}

	public function eraseCredentials() {
	}

	/** @see \Serializable::serialize() */
	public function serialize() {
		return serialize( array(
			$this->id,
			$this->username,
			$this->password,
			// see section on salt below
			// $this->salt,
		) );
	}

	/** @see \Serializable::unserialize() */
	public function unserialize( $serialized ) {
		[ $this->id, $this->username, $this->password, ] = unserialize( $serialized );
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail( $email ) {
		$this->email = $email;
	}

	/**
	 * @param mixed $username
	 */
	public function setUsername( $username ) {
		$this->username = $username;
	}

	/**
	 * @param mixed $password
	 */
	public function setPassword( $password ) {
		$this->password = $password;
	}

	public function getUsername() {
		return $this->username;
	}

	public function getSalt() {
		return null;
	}

	public function getPassword(): ?string {
		return $this->password;
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getIsActive(): ?bool {
		return $this->isActive;
	}

	public function isActive(): ?bool {
		return $this->isActive;
	}

	public function setIsActive( bool $isActive ): self {
		$this->isActive = $isActive;

		return $this;
	}

	public function getCreatedDate(): ?\DateTimeInterface {
		return $this->createdDate;
	}

	public function setCreatedDate( ?\DateTimeInterface $createdDate ): self {
		$this->createdDate = $createdDate;

		return $this;
	}

	public function getUpdatedDate(): ?\DateTimeInterface {
		return $this->updatedDate;
	}

	public function setUpdatedDate( ?\DateTimeInterface $updatedDate ): self {
		$this->updatedDate = $updatedDate;

		return $this;
	}

	public function getCreatedBy(): ?self {
		return $this->createdBy;
	}

	public function setCreatedBy( ?self $createdBy ): self {
		$this->createdBy = $createdBy;

		return $this;
	}

	public function getUpdatedBy(): ?self {
		return $this->updatedBy;
	}

	public function setUpdatedBy( ?self $updatedBy ): self {
		$this->updatedBy = $updatedBy;

		return $this;
	}

	/**
	 * @return Collection|Notification[]
	 */
	public function getNotifications(): Collection {
		return $this->notifications;
	}

	public function addNotification( Notification $notification ): self {
		if ( ! $this->notifications->contains( $notification ) ) {
			$this->notifications[] = $notification;
			$notification->setUser( $this );
		}

		return $this;
	}

	public function removeNotification( Notification $notification ): self {
		if ( $this->notifications->contains( $notification ) ) {
			$this->notifications->removeElement( $notification );
			// set the owning side to null (unless already changed)
			if ( $notification->getUser() === $this ) {
				$notification->setUser( null );
			}
		}

		return $this;
	}

	public function getPlainPassword(): string {
		return $this->plainPassword;
	}

	public function setPlainPassword( string $plainPassword ): void {
		$this->plainPassword = $plainPassword;
	}

	/**
	 * @return Collection|NotificationTopic[]
	 */
	public function getDisabledNotificationTopics(): Collection {
		return $this->disabledNotificationTopics;
	}

	public function addDisabledNotificationTopic( NotificationTopic $disabledNotificationTopic ): self {
		if ( ! $this->disabledNotificationTopics->contains( $disabledNotificationTopic ) ) {
			$this->disabledNotificationTopics[] = $disabledNotificationTopic;
		}

		return $this;
	}

	public function removeDisabledNotificationTopic( NotificationTopic $disabledNotificationTopic ): self {
		if ( $this->disabledNotificationTopics->contains( $disabledNotificationTopic ) ) {
			$this->disabledNotificationTopics->removeElement( $disabledNotificationTopic );
		}

		return $this;
	}

	public function getRecoveryToken(): ?string {
		return $this->recoveryToken;
	}

	public function setRecoveryToken( ?string $recoveryToken ): self {
		$this->recoveryToken = $recoveryToken;

		return $this;
	}

	public function getRecoveryTokenCreationDate(): ?\DateTimeInterface {
		return $this->recoveryTokenCreationDate;
	}

	public function setRecoveryTokenCreationDate( ?\DateTimeInterface $recoveryTokenCreationDate ): self {
		$this->recoveryTokenCreationDate = $recoveryTokenCreationDate;

		return $this;
	}

	/**
	 * A visual identifier that represents this user.
	 *
	 * @see UserInterface
	 */
	public function getUserIdentifier(): string {
		return (string) $this->email;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name     = $name;
		$this->fullName = $name ?: '';
		if ( $this->lastName ) {
			$this->fullName .= ' ' . $this->lastName;
		}

		return $this;
	}

	public function getLastName(): ?string {
		return $this->lastName;
	}

	public function setLastName( ?string $lastName ): self {

		$this->lastName = $lastName;
		if ( $this->name ) {
			$this->fullName = $this->name . ' ';
		}
		$this->fullName .= $lastName ?: '';

		return $this;
	}

	public function getFullName(): ?string {
		return $this->fullName;
	}

	public function getPhone(): ?string {
		return $this->phone;
	}

	public function setPhone( ?string $phone ): self {
		$this->phone = $phone;

		return $this;
	}

	public function getAvatar(): ?AppFile {
		return $this->avatar;
	}

	public function setAvatar( ?AppFile $avatar ): self {
		$this->avatar = $avatar;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}

	public function getIsCompanyOwner(): ?bool {
		return $this->isCompanyOwner;
	}

	/**
	 * @param ?boolean $isCompany
	 */
	public function setIsCompanyOwner( bool $isCompanyOwner ): self {
		$this->isCompanyOwner = ( $isCompanyOwner == true );

		return $this;
	}

	public function getPosition(): ?string {
		return $this->position;
	}

	public function setPosition( ?string $position ): self {
		$this->position = $position;

		return $this;
	}

	/**
	 * @return Collection|PermissionGroup[]
	 *
	 */
	#[Groups( [ 'user_read_permission_groups' ] )]
	public function getPermissionGroups(): Collection {
		return $this->permissionGroups;
	}

	public function addPermissionGroup( PermissionGroup $permissionGroup ): self {
		if ( ! $this->permissionGroups->contains( $permissionGroup ) ) {
			$this->permissionGroups[] = $permissionGroup;
		}

		return $this;
	}

	public function removePermissionGroup( PermissionGroup $permissionGroup ): self {
		if ( $this->permissionGroups->contains( $permissionGroup ) ) {
			$this->permissionGroups->removeElement( $permissionGroup );
		}

		return $this;
	}

	public function setFullName( string $fullName ): self {
		$this->fullName = $fullName;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	/**
	 * @return Collection|Message[]
	 */
	public function getMessages(): Collection {
		return $this->messages;
	}

	public function addMessage( Message $message ): self {
		if ( ! $this->messages->contains( $message ) ) {
			$this->messages[] = $message;
			$message->setUser( $this );
		}

		return $this;
	}

	public function removeMessage( Message $message ): self {
		if ( $this->messages->removeElement( $message ) ) {
			// set the owning side to null (unless already changed)
			if ( $message->getUser() === $this ) {
				$message->setUser( null );
			}
		}

		return $this;
	}

	/**
	 * @return Collection|ChannelUser[]
	 */
	public function getChannelUsers(): Collection {
		return $this->channelUsers;
	}

	public function addChannelUser( ChannelUser $channelUser ): self {
		if ( ! $this->channelUsers->contains( $channelUser ) ) {
			$this->channelUsers[] = $channelUser;
			$channelUser->setUser( $this );
		}

		return $this;
	}

	public function removeChannelUser( ChannelUser $channelUser ): self {
		if ( $this->channelUsers->removeElement( $channelUser ) ) {
			// set the owning side to null (unless already changed)
			if ( $channelUser->getUser() === $this ) {
				$channelUser->setUser( null );
			}
		}

		return $this;
	}
}
