<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

trait FoliableTrait {
	#[Groups(['folio_sequential_read'])]
	#[ORM\Column(type: "integer", nullable: false)]
	#[Gedmo\Versioned]
	private $folioSequential = 1;

	public function getFolioSequential(): ?int {
		return $this->folioSequential;
	}

	public function setFolioSequential( ?int $folioSequential ): self {
		$this->folioSequential = $folioSequential;

		return $this;
	}

	/**
	 * @ORM\Column(type="string", length=255, unique=true, nullable=false)
	 * @Gedmo\Versioned
	 * @Groups({ "folio_read" })
	 */
	private $folio;

	public function getFolio(): ?string {
		return $this->folio;
	}

	public function setFolio( string $folio ): self {
		$this->folio = $folio;

		return $this;
	}
}