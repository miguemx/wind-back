<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\QuoteTransactionRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity( repositoryClass: QuoteTransactionRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('QUOTE_TRANSACTION_LIST')" ],
		'post' => [
			'method'         => 'POST',
			'access_control' => "is_granted('QUOTE_TRANSACTION_CREATE')"
		]
	],
	itemOperations: [
		'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('QUOTE_TRANSACTION_UPDATE')" ],
		'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('QUOTE_TRANSACTION_DELETE')" ],
		'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('QUOTE_TRANSACTION_SHOW')" ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [ 'groups' => [ 'quote_transaction_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'quote_transaction_write' ] ]
	] )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[Gedmo\Loggable]
#[ORM\Table( name: 'quote_transaction' )]
class QuoteTransaction {
	use TrackableTrait;

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'quote_transaction_read', 'quote_transaction_write', 'quote_transaction_read_id' ] )]
	private $id;

	#[Groups( [ 'quote_transaction_read', 'quote_transaction_write', 'quote_transaction_read_amount' ] )]
	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Gedmo\Versioned]
	private $amount;

	#[ORM\ManyToOne( targetEntity: Quote::class, inversedBy: 'quoteTransactions' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Groups( [ 'quote_transaction_read', 'quote_transaction_write', 'quote_transaction_read_quote' ] )]
	#[Gedmo\Versioned]
	private $quote;

	#[ORM\ManyToOne( targetEntity: Transaction::class, inversedBy: 'quoteTransactions' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Groups( [ 'quote_transaction_read', 'quote_transaction_read_transaction' ] )]
	#[Gedmo\Versioned]
	private $transaction;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[Groups( [ 'quote_write', 'quote_transaction_write' ] )]
	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private Client $client;

	public function getId(): ?int {
		return $this->id;
	}

	public function getAmount(): ?string {
		return $this->amount;
	}

	public function setAmount( string $amount ): self {
		$this->amount = $amount;

		return $this;
	}

	public function getQuote(): ?Quote {
		return $this->quote;
	}

	public function setQuote( ?Quote $quote ): self {
		$this->quote = $quote;

		return $this;
	}

	public function getTransaction(): ?Transaction {
		return $this->transaction;
	}

	public function setTransaction( ?Transaction $transaction ): self {
		$this->transaction = $transaction;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
