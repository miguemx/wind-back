<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BankTransactionRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity( repositoryClass: BankTransactionRepository::class )]
#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [
	'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('BANK_TRANSACTION_LIST')" ],
	'post' => [ 'method' => 'POST', 'access_control' => "is_granted('BANK_TRANSACTION_CREATE')" ]
], itemOperations: [
	'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('BANK_TRANSACTION_UPDATE')" ],
	'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('BANK_TRANSACTION_DELETE')" ],
	'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('BANK_TRANSACTION_SHOW')" ]
], attributes: [
	'input_formats'           => [ 'json' => [ 'application/json' ] ],
	'output_formats'          => [
		'json' => [ 'application/json' ],
		'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
	],
	'normalization_context'   => [ 'groups' => [ 'bank_transaction_read' ] ],
	'denormalization_context' => [ 'groups' => [ 'bank_transaction_write' ] ]
] )]
class BankTransaction {
	use TrackableTrait;

	public const BANK_TRANSACTION_STATUS_PENDING = 'PENDING';
	public const BANK_TRANSACTION_STATUS_CONFIRMED = 'CONFIRMED';
	public const BANK_TRANSACTION_STATUS_DENIED = 'DENIED';

	#[Groups( [ 'transaction_read', 'transaction_write', 'transaction_read_id' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'bank_transaction_read', 'bank_transaction_write', 'bank_transaction_read_transaction_date' ] )]
	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Gedmo\Versioned]
	private $date;

	#[Groups( [ 'bank_transaction_read', 'bank_transaction_write', 'bank_transaction_read_amount' ] )]
	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Gedmo\Versioned]
	private $amount;

	#[Groups( [ 'bank_transaction_read', 'bank_transaction_write', 'bank_transaction_read_concept' ] )]
	#[ORM\Column( type: 'string', length: 255, nullable: true )]
	#[Gedmo\Versioned]
	private $concept;

	#[Groups( [ 'bank_transaction_read_transaction', 'bank_transaction_write' ] )]
	#[ORM\OneToOne( targetEntity: Transaction::class, inversedBy: 'bankTransaction' )]
	#[ORM\JoinColumn( nullable: true )]
	private $transaction;

	#[Groups( [ 'bank_transaction_read', 'bank_transaction_write', 'bank_transaction_read_status' ] )]
	#[ORM\Column( type: 'string', length: 50 )]
	#[Gedmo\Versioned]
	private string $status = BankTransaction::BANK_TRANSACTION_STATUS_PENDING;

	#[Groups( [ 'bank_transaction_read', 'bank_transaction_write', 'bank_transaction_read_code' ] )]
	#[ORM\Column( type: 'string', length: 255, nullable: true )]
	#[Gedmo\Versioned]
	private $code;

	#[Groups( [ 'bank_transaction_read_account_statement' ] )]
	#[ORM\ManyToOne( targetEntity: AccountStatement::class, inversedBy: 'bankTransactions' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $accountStatement;

	public function getId(): ?int {
		return $this->id;
	}

	public function getDate(): ?\DateTimeInterface {
		return $this->date;
	}

	public function getStatus(): ?string {
		return $this->status;
	}

	public function setStatus( string $status ): self {
		$this->status = $status;

		return $this;
	}

	public function setDate( \DateTimeInterface $date ): self {
		$this->date = $date;

		return $this;
	}

	public function getCode(): ?string {
		return $this->code;
	}

	public function setCode( ?string $code ): self {
		$this->code = $code;

		return $this;
	}

	public function getTransaction(): ?Transaction {
		return $this->transaction;
	}

	public function setTransaction( ?Transaction $transaction ): self {
		$this->transaction = $transaction;

		return $this;
	}

	public function setConcept( string $concept ): self {
		$this->concept = $concept;

		return $this;
	}

	public function getConcept(): ?string {
		return $this->concept;
	}

	public function getAmount(): ?string {
		return $this->amount;
	}

	public function setAmount( string $amount ): self {
		$this->amount = $amount;

		return $this;
	}

	public function getAccountStatement(): ?AccountStatement {
		return $this->accountStatement;
	}

	public function setAccountStatement( ?AccountStatement $accountStatement ): self {
		$this->accountStatement = $accountStatement;

		return $this;
	}
}
