<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\RecurringPaymentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity( repositoryClass: RecurringPaymentRepository::class )]
#[ORM\Table]
#[ORM\Index( name: 'expiration_date_idx', columns: [ 'expiration_date' ] )]
#[ORM\Index( name: 'active_idx', columns: [ 'active' ] )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[Gedmo\Loggable]
#[ApiResource( attributes: [ 'input_formats'           => [ 'json' => [ 'application/json' ] ],
                             'output_formats'          => [
	                             'json' => [ 'application/json' ],
	                             'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
                             ],
                             'normalization_context'   => [ 'groups' => [ 'recurring_payment_read' ] ],
                             'denormalization_context' => [ 'groups' => [ 'recurring_payment_write' ] ]
], collectionOperations: [ 'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('RECURRING_PAYMENT_LIST')" ],
                           'post' => [ 'method'         => 'POST',
                                       'access_control' => "is_granted('RECURRING_PAYMENT_CREATE')"
                           ]
], itemOperations: [ 'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('RECURRING_PAYMENT_UPDATE')" ],
                     'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('RECURRING_PAYMENT_DELETE')" ],
                     'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('RECURRING_PAYMENT_SHOW')" ]
] )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[ApiFilter( SearchFilter::class, properties: [ 'concept' => 'partial', 'recurrence' => 'exact' ] )]
class RecurringPayment {
	use TrackableTrait;

	public const FREQUENCY_TYPE_YEARLY = 'yearly';
	public const FREQUENCY_TYPE_MONTHLY = 'monthly';
	public const FREQUENCY_TYPE_WEEKLY = 'weekly';
	public const FREQUENCY_TYPE_DAILY = 'daily';

	private $dateIncrements = [
		'daily'   => 'days',
		'weekly'  => 'days',
		'monthly' => 'months',
		'yearly'  => 'years',
	];

	#[ORM\Id]
	#[Groups( [ 'recurring_payment_read', 'recurring_payment_write', 'recurring_payment_read_id' ] )]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'recurring_payment_read', 'recurring_payment_write', 'recurring_payment_read_amount' ] )]
	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Gedmo\Versioned]
	private $amount;

	#[Groups( [ 'recurring_payment_read', 'recurring_payment_write', 'recurring_payment_read_concept' ] )]
	#[Assert\Length( max: 300 )]
	#[Assert\NotNull]
	#[ORM\Column( type: 'string', length: 300, nullable: true )]
	#[Gedmo\Versioned]
	private $concept;

	#[Groups( [ 'recurring_payment_read', 'recurring_payment_write', 'recurring_payment_read_active' ] )]
	#[ORM\Column( type: 'boolean' )]
	#[Gedmo\Versioned]
	private bool $active = true;

	/**
	 * @maxDepth(1)
	 */
	#[Groups( [ 'recurring_payment_read', 'recurring_payment_write', 'recurring_payment_read_provider' ] )]
	#[ORM\ManyToOne( targetEntity: Provider::class, inversedBy: 'recurringPayments' )]
	#[Gedmo\Versioned]
	private $provider;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[Groups( [ 'recurring_payment_read', 'recurring_payment_write', 'recurring_payment_read_first_payment_date' ] )]
	#[Assert\NotNull]
	#[ORM\Column( type: 'date' )]
	private $firstPaymentDate;

	#[Groups( [ 'recurring_payment_read', 'recurring_payment_write', 'recurring_payment_read_expiration_date' ] )]
	#[ORM\Column( type: 'date', nullable: true )]
	private $expirationDate;

	/**
	 * @MaxDepth(1)
	 */
	#[Groups( [ 'recurring_payment_read_transactions' ] )]
	#[ORM\OneToMany( targetEntity: Transaction::class, mappedBy: 'recurringPayment' )]
	private $transactions;

	/**
	 *
	 * How often the recurrence will be generated accord of following values: DAY, WEEK, MONTH or YEAR.
	 */
	#[Groups( [ 'recurring_payment_read', 'recurring_payment_write', 'recurring_payment_read_frequency_type' ] )]
	#[Assert\NotNull]
	#[ORM\Column( type: 'string', length: 50 )]
	private $frequencyType;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'recurring_payment_read', 'recurring_payment_write', 'recurring_payment_read_frequency_interval' ] )]
	private $frequencyInterval;

	#[ORM\Column( type: 'string', length: 500, nullable: true )]
	#[Groups( [ 'recurring_payment_read', 'recurring_payment_write', 'recurring_payment_read_repeat_in' ] )]
	private $repeatIn;

	public function __construct() {
		$this->transactions = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getAmount(): ?string {
		return $this->amount;
	}

	public function setAmount( string $amount ): self {
		$this->amount = $amount;

		return $this;
	}

	public function getConcept(): ?string {
		return $this->concept;
	}

	public function setConcept( ?string $concept ): self {
		$this->concept = $concept;

		return $this;
	}

	public function getActive(): ?bool {
		return $this->active;
	}

	public function setActive( bool $active ): self {
		$this->active = $active;

		return $this;
	}

	public function getProvider(): ?Provider {
		return $this->provider;
	}

	public function setProvider( ?Provider $provider ): self {
		$this->provider = $provider;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getFirstPaymentDate(): ?\DateTimeInterface {
		return $this->firstPaymentDate;
	}

	public function setFirstPaymentDate( \DateTimeInterface $firstPaymentDate ): self {
		$this->firstPaymentDate = $firstPaymentDate;

		return $this;
	}

	public function getExpirationDate(): ?\DateTimeInterface {
		return $this->expirationDate;
	}

	public function setExpirationDate( \DateTimeInterface $expirationDate ): self {
		$this->expirationDate = $expirationDate;

		return $this;
	}

	public function getFrequencyType(): ?string {
		return $this->frequencyType;
	}

	public function setFrequencyType( string $frequencyType ): self {
		$this->frequencyType = $frequencyType;

		return $this;
	}

	/**
	 * @return Collection|Transaction[]
	 */
	public function getTransactions(): Collection {
		return $this->transactions;
	}

	public function addTransaction( Transaction $transaction ): self {
		if ( ! $this->transactions->contains( $transaction ) ) {
			$this->transactions[] = $transaction;
			$transaction->setRecurringPayment( $this );
		}

		return $this;
	}

	public function removeTransaction( Transaction $transaction ): self {
		if ( $this->transactions->removeElement( $transaction ) ) {
			// set the owning side to null (unless already changed)
			if ( $transaction->getRecurringPayment() === $this ) {
				$transaction->setRecurringPayment( null );
			}
		}

		return $this;
	}

	#[Groups( [ 'recurring_payment_read_required_payment' ] )]
	public function getRequiredPayment() {
		return $this->getNextPaymentDate();
	}

	/**
	 * getNextPaymentDate
	 *
	 * @return void
	 */
	public function getNextPaymentDate() {
		// if is first payment
		if ( $this->getTransactions()->count() == 0 ) {
			return $this->getFirstPaymentDate()->format( 'Y-m-d' );
		}

		$nextPaymentDate = null;

		$incrementValue = ( $this->getFrequencyInterval() * $this->getTransactions()->count() );
		$incrementType  = $this->dateIncrements[ $this->getFrequencyType() ];

		if ( $this->getFrequencyType() == self::FREQUENCY_TYPE_WEEKLY ) {
			$incrementValue = $incrementValue * 7;
		}

		$nextPaymentDate = $this->getFirstPaymentDate()->modify( sprintf( "+ %s %s", $incrementValue, $incrementType ) );

		switch ( $this->getFrequencyType() ) {
			case RecurringPayment::FREQUENCY_TYPE_YEARLY:
				if ( $this->getRepeatIn() ) {
					$nextPaymentDate = new \DateTime( $nextPaymentDate->format( "Y-{$this->getRepeatIn()}" ) );
				}
				break;

			case RecurringPayment::FREQUENCY_TYPE_MONTHLY:
				if ( $this->getRepeatIn() ) {
					$nextPaymentDate = new \DateTime( $nextPaymentDate->format( "Y-m-{$this->getRepeatIn()}" ) );
				}
				break;

			case RecurringPayment::FREQUENCY_TYPE_WEEKLY:
				if ( $this->getRepeatIn() ) {
					$nextPaymentDate = new \DateTime( date( "Y-m-d", strtotime( "{$this->getRepeatIn()} this week", strtotime( $nextPaymentDate->format( 'Y-m-d' ) ) ) ) );
				}
				break;
		}

		if ( empty( $incrementType ) || empty( $incrementValue ) ) {
			throw new \Exception( "Invalid frequency type" );
		}

		return ( $nextPaymentDate ? $nextPaymentDate->format( 'Y-m-d' ) : null );
	}

	#[Groups( [ 'recurring_payment_read_last_payment' ] )]
	public function getLastPayment() {
		$lastTrx = null;
		if ( ! $this->getTransactions()->last() ) {
			return null;
		}

		return $this->getTransactions()->last();
	}

	public function getFrequencyInterval(): ?int {
		return $this->frequencyInterval;
	}

	public function setFrequencyInterval( int $frequencyInterval ): self {
		$this->frequencyInterval = $frequencyInterval;

		return $this;
	}

	public function getRepeatIn(): ?string {
		return $this->repeatIn;
	}

	public function setRepeatIn( ?string $repeatIn ): self {
		$this->repeatIn = $repeatIn;

		return $this;
	}
}
