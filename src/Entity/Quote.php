<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\ClientAwareAnnotation;
use App\Annotation\TenantAwareAnnotation;
use App\Interfaces\AclEnabledInterface;
use App\Interfaces\FolioInterface;
use App\Repository\QuoteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use App\Filter\SearchAnnotation as Searchable;

#[ORM\Entity( repositoryClass: QuoteRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ClientAwareAnnotation( clientFieldName: 'client_id' )]
#[ORM\Table]
#[ORM\Index( name: 'quote_status_idx', columns: [ 'status' ] )]
#[ORM\Index( name: 'quote_payment_status_idx', columns: [ 'payment_status' ] )]
#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [
        'get'  => [ 'method' => 'GET' ],
        'post' => [ 'method' => 'POST' ]
    ],
	itemOperations: [
        'put'    => [ 'method' => 'PUT' ],
        'delete' => [ 'method' => 'DELETE' ],
        'get'    => [
            'method'         => 'GET',
            'access_control' => "is_granted('QUOTE_SHOW', object) or is_granted('PROJECT_SHOW_ALL_QUOTES', object.project)"
        ]
    ],
	attributes: [
        'input_formats'           => [ 'json' => [ 'application/json' ] ],
        'output_formats'          => [
            'json' => [ 'application/json' ],
            'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
        ],
        'normalization_context'   => [
            'groups' => [ 'quote_read', 'folio_read' ]
        ],
        'denormalization_context' => [
            'groups' => [
                'quote_write',
                'quote_product_write',
                'folio_read'
            ]
        ],
        'order'                   => [ 'updatedDate' => 'DESC' ],
        'filters'                 => [ "search" ]
    ] )]
#[ApiFilter( SearchFilter::class, properties: [
	'project'        => 'exact',
	'project.client' => 'exact',
	'project.name'   => 'partial',
	'status'         => 'exact',
	'paymentStatus'  => 'exact',
	'folio'          => 'partial',
	'title'          => 'partial',
	'client.name'    => 'partial'
] )]
#[ApiFilter( OrderFilter::class, properties: [ 'createdDate' ] )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[Searchable( [ "title", "folio", "project.name", "project.client.name" ] )]
class Quote implements FolioInterface, AclEnabledInterface {
	use TrackableTrait, FoliableTrait;

	public const STATUS_PENDING = "PENDING";
	public const STATUS_APPROVED = "APPROVED";
	public const STATUS_REJECTED = "REJECTED";
	public const STATUS_COMPLETED = "COMPLETED";
	public const STATUS_EXPIRED = "EXPIRED";
	public const PAYMENT_STATUS_PENDING = "PENDING";
	public const PAYMENT_STATUS_PAYING = "PAYING";
	public const PAYMENT_STATUS_PAID = "PAID";

	public static function getApprovedStatuses() {
		return [
			self::STATUS_APPROVED,
			self::STATUS_COMPLETED
		];
	}

	public static function getPendingPaymentStatuses() {
		return [
			self::PAYMENT_STATUS_PENDING,
			self::PAYMENT_STATUS_PAYING
		];
	}

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'quote_read', 'quote_write' ] )]
	private $id;

	#[ORM\ManyToOne( targetEntity: Project::class, inversedBy: 'quotes' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Assert\NotNull]
	#[Groups( [ 'quote_read_project', 'quote_write' ] )]
	#[Gedmo\Versioned]
	#[MaxDepth( 3 )]
	private $project;

	#[ORM\OneToOne( targetEntity: QuotationRequest::class, mappedBy: 'quote', cascade: [ 'persist', 'remove' ] )]
	#[Groups( [ 'quote_read_quotation_request', 'quote_write' ] )]
	#[Gedmo\Versioned]
	private $quotationRequest;

	#[ORM\Column( type: 'string', length: 255 )]
	#[Groups( [ 'quote_read', 'quote_write' ] )]
	#[Gedmo\Versioned]
	private $title;

	#[ORM\Column( type: 'string', length: 255, unique: true )]
	#[Groups( [ 'quote_read', 'quote_write', 'quote_read_folio' ] )]
	#[Gedmo\Versioned]
	private $folio;

	#[ORM\Column( type: 'boolean' )]
	#[Groups( [ 'quote_read', 'quote_write' ] )]
	#[Gedmo\Versioned]
	private $iva;

	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Groups( [ 'quote_read', 'quote_write', 'quote_read_total' ] )]
	#[Gedmo\Versioned]
	private $total;

	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Groups( [ 'quote_read', 'quote_write' ] )]
	#[Gedmo\Versioned]
	private $subtotal;

	#[ORM\Column( type: 'string', length: 50, nullable: false )]
	#[Groups( [ 'quote_read', 'quote_read_status', 'quote_write' ] )]
	#[Gedmo\Versioned]
	private string $status = self::STATUS_PENDING;

	#[ORM\Column( type: 'string', length: 50, nullable: false )]
	#[Groups( [ 'quote_read', 'quote_read_payment_status', 'quote_write' ] )]
	#[Gedmo\Versioned]
	private string $paymentStatus = self::PAYMENT_STATUS_PENDING;

	#[ORM\OneToMany( targetEntity: QuoteProduct::class, mappedBy: 'quote', orphanRemoval: true, cascade: [ 'persist' ] )]
	#[Assert\Valid]
	#[Groups( [ 'quote_read_quote_products', 'quote_write' ] )]
	private $quoteProducts;

	#[ORM\OneToMany( targetEntity: QuoteTransaction::class, mappedBy: 'quote', orphanRemoval: true )]
	#[Assert\Valid]
	#[Groups( [ 'quote_read_quote_transactions', 'quote_write' ] )]
	private $quoteTransactions;

	#[ORM\ManyToOne( targetEntity: User::class )]
	#[Groups( [ 'quote_read_reviewed_by', 'quote_write' ] )]
	#[Gedmo\Versioned]
	private $reviewedBy;

	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Groups( [ 'quote_read', 'quote_read_reviewed_date', 'quote_write' ] )]
	#[Gedmo\Versioned]
	private $reviewedDate;

	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Groups( [ 'quote_read', 'quote_read_expiration_date', 'quote_write' ] )]
	#[Gedmo\Versioned]
	private $expirationDate;

	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Groups( [ 'quote_read', 'quote_read_created_date', 'quote_write' ] )]
	#[Gedmo\Timestampable( on: 'create' )]
	protected $createdDate;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[Groups( [ 'quote_write', 'quote_read_client' ] )]
	#[Assert\NotNull]
	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private Client $client;

	#[ORM\ManyToOne(targetEntity: TaxDocument::class, inversedBy: 'quotes')]
	#[Groups( [ 'quote_write', 'quote_read', 'quote_read_tax_document' ] )]
	#[Gedmo\Versioned]
    private $taxDocument;

	public function __construct() {
		$this->quoteProducts     = new ArrayCollection();
		$this->quoteTransactions = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getProject(): ?Project {
		return $this->project;
	}

	public function setProject( ?Project $project ): self {
		$this->project = $project;

		return $this;
	}

	public function getQuotationRequest(): ?QuotationRequest {
		return $this->quotationRequest;
	}

	public function getTitle(): ?string {
		return $this->title;
	}

	public function setTitle( string $title ): self {
		$this->title = $title;

		return $this;
	}

	public function getFolio(): ?string {
		return $this->folio;
	}

	public function setFolio( string $folio ): self {
		$this->folio = $folio;

		return $this;
	}

	public function getIva(): ?bool {
		return $this->iva;
	}

	public function setIva( bool $iva ): self {
		$this->iva = $iva;

		return $this;
	}

	public function getTotal(): ?string {
		return $this->total;
	}

	public function setTotal( string $total ): self {
		$this->total = $total;

		return $this;
	}

	public function getSubtotal(): ?string {
		return $this->subtotal;
	}

	public function setSubtotal( string $subtotal ): self {
		$this->subtotal = $subtotal;

		return $this;
	}

	public function getStatus(): ?string {
		return $this->status;
	}

	public function setStatus( string $status ): self {
		$this->status = $status;

		return $this;
	}

	/**
	 * @return Collection|QuoteProduct[]
	 */
	public function getQuoteProducts(): Collection {
		return $this->quoteProducts;
	}

	/**
	 * @param Collection|QuoteProduct[] $quoteProducts
	 *
	 * @return $this
	 */
	public function setQuoteProducts( array|\Doctrine\Common\Collections\Collection $quoteProducts ): self {
		$this->quoteProducts = $quoteProducts;

		return $this;
	}

	public function addQuoteProduct( QuoteProduct $quoteProduct ): self {
		if ( ! $this->quoteProducts->contains( $quoteProduct ) ) {
			$this->quoteProducts[] = $quoteProduct;
			$quoteProduct->setQuote( $this );
		}

		return $this;
	}

	public function removeQuoteProduct( QuoteProduct $quoteProduct ): self {
		if ( $this->quoteProducts->removeElement( $quoteProduct ) ) {
			// set the owning side to null (unless already changed)
			if ( $quoteProduct->getQuote() === $this ) {
				$quoteProduct->setQuote( null );
			}
		}

		return $this;
	}

	/**
	 * @return Collection|QuoteTransaction[]
	 */
	public function getQuoteTransactions(): Collection {
		return $this->quoteTransactions;
	}

	public function addQuoteTransaction( QuoteTransaction $quoteTransaction ): self {
		if ( ! $this->quoteTransactions->contains( $quoteTransaction ) ) {
			$this->quoteTransactions[] = $quoteTransaction;
			$quoteTransaction->setQuote( $this );
		}

		return $this;
	}

	public function removeQuoteTransaction( QuoteTransaction $quoteTransaction ): self {
		if ( $this->quoteTransactions->removeElement( $quoteTransaction ) ) {
			// set the owning side to null (unless already changed)
			if ( $quoteTransaction->getQuote() === $this ) {
				$quoteTransaction->setQuote( null );
			}
		}

		return $this;
	}

	public function getApprovedBy(): ?User {
		return $this->approvedBy;
	}

	public function setApprovedBy( ?User $approvedBy ): self {
		$this->approvedBy = $approvedBy;

		return $this;
	}

	public function getApprovedDate(): ?\DateTimeInterface {
		return $this->approvedDate;
	}

	public function setApprovedDate( ?\DateTimeInterface $approvedDate ): self {
		$this->approvedDate = $approvedDate;

		return $this;
	}

	public function getExpirationDate(): ?\DateTimeInterface {
		return $this->expirationDate;
	}

	public function setExpirationDate( \DateTimeInterface $expirationDate ): self {
		$this->expirationDate = $expirationDate;

		return $this;
	}

	public function getPaymentStatus(): ?string {
		return $this->paymentStatus;
	}

	public function setPaymentStatus( string $paymentStatus ): self {
		$this->paymentStatus = $paymentStatus;

		return $this;
	}

	#[Groups( [ 'quote_read_pending_amount' ] )]
	public function getPendingAmount(): ?string {
		$pendingAmount = 0;
		foreach ( $this->quoteTransactions as $quoteTransaction ) {
			$pendingAmount += $quoteTransaction->getAmount();
		}
		$total = $this->getTotal() - $pendingAmount;

		return $total;
	}

	#[Groups( [ 'quote_read_paid_amount' ] )]
	public function getPaidAmount(): ?string {
		return ( $this->getTotal() - $this->getPendingAmount() );
	}

	public function getReviewedDate(): ?\DateTimeInterface {
		return $this->reviewedDate;
	}

	public function setReviewedDate( ?\DateTimeInterface $reviewedDate ): self {
		$this->reviewedDate = $reviewedDate;

		return $this;
	}

	public function getReviewedBy(): ?User {
		return $this->reviewedBy;
	}

	public function setReviewedBy( ?User $reviewedBy ): self {
		$this->reviewedBy = $reviewedBy;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getFolioTypeIdentifier(): string {
		return 'C';
	}

	/**
	 * addDQLFolioSequential
	 *
	 *
	 */
	public function addDQLFolioSequential( \Doctrine\ORM\QueryBuilder $query ): \Doctrine\ORM\QueryBuilder {
		return $query
			->leftJoin( 'e.project', 'p' )
			->andWhere( 'p.client = :client' )
			->setParameter( 'client', $this->getProject()->getClient() );
	}

	/**
	 * getBaseInitials
	 */
	public function getFolioBaseInitials(): string {
		return $this->getProject()->getClient()->getInitials();
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}

	public function setQuotationRequest( ?QuotationRequest $quotationRequest ): self {
		// unset the owning side of the relation if necessary
		if ( $quotationRequest === null && $this->quotationRequest !== null ) {
			$this->quotationRequest->setQuote( null );
		}

		// set the owning side of the relation if necessary
		if ( $quotationRequest !== null && $quotationRequest->getQuote() !== $this ) {
			$quotationRequest->setQuote( $this );
		}

		$this->quotationRequest = $quotationRequest;

		return $this;
	}

	public function getAclDomain(): string {
		return self::class;
	}

	public function getObjectId(): string {
		return (string) $this->getId();
	}

	static function getReadAllEntitiesByParentConfig(): array {

		return [
			[
				'query'          => "SELECT q.id from quote q left join project p on q.project_id=p.id where p.id in (:allowableParentIds)",
				'class'          => Project::class,
				'permissionMask' => Project::ACL_PERMISSION_SHOW_ALL_QUOTES
			]
		];
	}

	public function getTaxDocument(): ?TaxDocument
    {
        return $this->taxDocument;
    }

    public function setTaxDocument(?TaxDocument $taxDocument): self
    {
        $this->taxDocument = $taxDocument;

        return $this;
    }
}
