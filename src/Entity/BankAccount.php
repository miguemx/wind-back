<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\BankAccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Bank;

#[ORM\Entity( repositoryClass: BankAccountRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('BANK_ACCOUNT_LIST')" ],
		'post' => [ 'method' => 'POST', 'access_control' => "is_granted('BANK_ACCOUNT_CREATE')" ]
	],
	itemOperations: [
		'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('BANK_ACCOUNT_UPDATE')" ],
		'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('BANK_ACCOUNT_DELETE')" ],
		'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('BANK_ACCOUNT_SHOW')" ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [ 'groups' => [ 'bank_account_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'bank_account_write', 'bank_write' ] ]
	] )]
#[ApiFilter( SearchFilter::class, properties: [ 'client' => 'exact', 'name' => 'partial' ] )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
class BankAccount {
	use TrackableTrait;

	#[Groups( [ 'bank_account_read', 'bank_account_write', 'bank_account_read_id' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Assert\NotBlank]
	#[Groups( [ 'bank_account_read', 'bank_account_write', 'bank_account_read_name' ] )]
	#[ORM\Column( type: 'string', length: 100 )]
	#[Gedmo\Versioned]
	private $name;

	#[Assert\NotBlank]
	#[Groups( [ 'bank_account_read', 'bank_account_write', 'bank_account_read_number' ] )]
	#[ORM\Column( type: 'string', length: 100, nullable: true )]
	#[Gedmo\Versioned]
	private $number;

	#[Assert\NotBlank]
	#[Groups( [ 'bank_account_read', 'bank_account_write', 'bank_account_read_clabe' ] )]
	#[ORM\Column( type: 'string', length: 18, nullable: true )]
	#[Gedmo\Versioned]
	private $clabe;

	#[Groups( [ 'bank_account_read', 'bank_account_write', 'bank_account_read_balance' ] )]
	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Gedmo\Versioned]
	private int $balance = 0;

	#[Groups( [ 'bank_account_read', 'bank_account_write', 'bank_account_read_belvo_token' ] )]
	#[ORM\Column( type: 'string', length: 64, nullable: true )]
	#[Gedmo\Versioned]
	private $belvoToken;

	#[Groups( [ 'bank_account_write', 'bank_account_read_bank' ] )]
	#[ORM\ManyToOne( targetEntity: Bank::class, inversedBy: 'bankAccounts' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Assert\NotNull]
	#[Gedmo\Versioned]
	private $bank;

	#[Groups( [ 'bank_account_write', 'bank_account_read_transactions' ] )]
	#[ORM\OneToMany( targetEntity: Transaction::class, mappedBy: 'bankAccount' )]
	#[ORM\JoinColumn( nullable: true )]
	private $transactions;

	#[Groups( [ 'bank_account_write', 'bank_account_read_account_statements' ] )]
	#[ORM\OneToMany( targetEntity: AccountStatement::class, mappedBy: 'bankAccount', orphanRemoval: true )]
	#[ORM\JoinColumn( nullable: true )]
	private $accountStatements;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	public function __construct() {
		$this->accountStatements = new ArrayCollection();
		$this->transactions      = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getNumber(): ?string {
		return $this->number;
	}

	public function setNumber( ?string $number ): self {
		$this->number = $number;

		return $this;
	}

	public function getClabe(): ?string {
		return $this->clabe;
	}

	public function setClabe( ?string $clabe ): self {
		$this->clabe = $clabe;

		return $this;
	}

	public function getBelvoToken(): ?string {
		return $this->belvoToken;
	}

	public function setBelvoToken( ?string $belvoToken ): self {
		$this->belvoToken = $belvoToken;

		return $this;
	}

	public function getBalance(): ?string {
		return $this->balance;
	}

	public function setBalance( string $balance ): self {
		$this->balance = $balance;

		return $this;
	}

	public function getBank(): ?Bank {
		return $this->bank;
	}

	public function setBank( ?Bank $bank ): self {
		$this->bank = $bank;

		return $this;
	}

	/**
	 * @return Collection|AccountStatement[]
	 */
	public function getAccountStatements(): Collection {
		return $this->accountStatements;
	}

	public function addAccountStatement( AccountStatement $accountStatement ): self {
		if ( ! $this->accountStatements->contains( $accountStatement ) ) {
			$this->accountStatements[] = $accountStatement;
			$accountStatement->setBankAccount( $this );
		}

		return $this;
	}

	public function removeAccountStatement( AccountStatement $accountStatement ): self {
		if ( $this->accountStatements->removeElement( $accountStatement ) ) {
			// set the owning side to null (unless already changed)
			if ( $accountStatement->getBankAccount() === $this ) {
				$accountStatement->setBankAccount( null );
			}
		}

		return $this;
	}

	/**
	 * @return Collection|Transaction[]
	 */
	public function getTransactions(): Collection {
		return $this->transactions;
	}

	public function addTransaction( Transaction $transaction ): self {
		if ( ! $this->transaction->contains( $transaction ) ) {
			$this->transaction[] = $transaction;
			$transaction->setBankAccountId( $this );
		}

		return $this;
	}

	public function removeTransaction( Transaction $transaction ): self {
		if ( $this->transaction->removeElement( $transaction ) ) {
			// set the owning side to null (unless already changed)
			if ( $transaction->getBankAccountId() === $this ) {
				$transaction->setBankAccountId( null );
			}
		}

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}
}
