<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Filter\SearchFilter;
use App\Repository\CTaxRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table]
#[ORM\Entity(repositoryClass: CTaxRepository::class)]
#[ApiResource( collectionOperations: [ 'get' => [] ], itemOperations: [ 'get' => [] ] )]
#[ApiFilter(SearchFilter::class, properties: ["id", "code", "name","average"])]
#[ApiFilter( OrderFilter::class, properties: [ 'code' ] )]
#[ApiFilter( GroupFilter::class, arguments: [
    'parameterName'         => 'sGroups',
    'overrideDefaultGroups' => true,
    'whitelist'             => [
        'c_tax_read',
        'c_tax_read_code',
        'c_tax_read_name',
        'c_tax_average'
    ]
] )]

class CTax
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', unique: true)]
    #[Groups('c_tax_read','c_tax_read_id')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[Groups('c_tax_read_code')]
    private $code;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups('c_tax_read_name')]
    private $name;

    #[ORM\Column(type: 'float')]
    #[Groups('c_tax_read_average')]
    private $average;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAverage(): ?float
    {
        return $this->average;
    }

    public function setAverage(float $average): self
    {
        $this->average = $average;

        return $this;
    }
}
