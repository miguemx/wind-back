<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;


trait TrackableTrait {

	/**
	 * @var User
	 * @Gedmo\Blameable(on="create")
	 * @MaxDepth(1)
	 */
	#[Groups( [ 'blame_creation', 'read_created_by' ] )]
	#[ORM\ManyToOne( targetEntity: User::class )]
	#[ORM\JoinColumn( name: 'created_by', referencedColumnName: 'id', nullable: true, onDelete: 'SET NULL' )]
	protected $createdBy;

	/**
	 * @var User
	 * @Gedmo\Blameable(on="update")
	 * @MaxDepth(1)
	 */
	#[Groups( [ 'blame_update', 'read_updated_by' ] )]
	#[ORM\ManyToOne( targetEntity: User::class )]
	#[ORM\JoinColumn( name: 'updated_by', referencedColumnName: 'id', nullable: true, onDelete: 'SET NULL' )]
	protected $updatedBy;

	/**
	 * @var \DateTime $created
	 * @MaxDepth(1)
	 */
	#[Groups( [ 'creation_date', 'read_created_date' ] )]
	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Gedmo\Timestampable( on: 'create' )]
	protected $createdDate;

	/**
	 * @var \DateTime $updated
	 * @MaxDepth(1)
	 */
	#[Groups( [ 'update_date', 'read_updated_date' ] )]
	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Gedmo\Timestampable( on: 'update' )]
	protected $updatedDate;

	/**
	 * @return User
	 */
	public function getCreatedBy() {
		return $this->createdBy;
	}

	/**
	 * @param User $createdBy
	 */
	public function setCreatedBy( $createdBy ) {
		$this->createdBy = $createdBy;
	}

	/**
	 * @return User
	 */
	public function getUpdatedBy() {
		return $this->updatedBy;
	}

	/**
	 * @param User $updatedBy
	 */
	public function setUpdatedBy( $updatedBy ) {
		$this->updatedBy = $updatedBy;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedDate() {
		return $this->createdDate;
	}

	/**
	 * @param \DateTime $createdDate
	 */
	public function setCreatedDate( $createdDate ) {
		$this->createdDate = $createdDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getUpdatedDate() {
		return $this->updatedDate;
	}

	/**
	 * @param \DateTime $updatedDate
	 */
	public function setUpdatedDate( $updatedDate ) {
		$this->updatedDate = $updatedDate;
	}
}
