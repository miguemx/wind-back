<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\ClientAwareAnnotation;
use App\Annotation\TenantAwareAnnotation;
use App\Interfaces\FolioInterface;
use App\Repository\MemorandumRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('MEMORANDUM_LIST')" ],
		'post' => [ 'method' => 'POST', 'access_control' => "is_granted('MEMORANDUM_CREATE')" ]
	],
	itemOperations: [
		'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('MEMORANDUM_UPDATE')" ],
		'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('MEMORANDUM_DELETE')" ],
		'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('MEMORANDUM_SHOW')" ]
	],
	attributes: [
		'normalization_context'   => [ 'groups' => [ 'memorandum_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'memorandum_write' ] ],
        'order'                   => [ 'createdDate' => 'DESC' ]
	] )]
#[ORM\Entity( repositoryClass: MemorandumRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ClientAwareAnnotation( clientFieldName: 'client_id' )]
#[Gedmo\Loggable]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[ApiFilter( SearchFilter::class, properties: [
	'project'      => 'exact',
	'title'        => 'partial',
	'project.name' => 'partial',
	'client.name'  => 'partial'
] )]
class Memorandum implements FolioInterface {
	use TrackableTrait, FoliableTrait;

	public $typeFolio = 'Memorandum';

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'memorandum_read', 'memorandum_read_id' ] )]
	#[Gedmo\Versioned]
	private $id;

	#[ORM\Column( type: 'string', length: 255 )]
	#[Groups( [ 'memorandum_read', 'memorandum_read_title', 'memorandum_write' ] )]
	#[Assert\NotBlank]
	#[Gedmo\Versioned]
	private $title;

	#[ORM\ManyToOne( targetEntity: Project::class, inversedBy: 'memorandums' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Groups( [ 'memorandum_read', 'memorandum_read_project', 'memorandum_write' ] )]
	#[Assert\NotBlank]
	private $project;

	#[ORM\Column( type: 'text' )]
	#[Groups( [ 'memorandum_read', 'memorandum_read_content', 'memorandum_write' ] )]
	#[Assert\NotBlank]
	#[Gedmo\Versioned]
	private $content;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[Assert\NotNull]
	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Groups( [ 'memorandum_write' ] )]
	#[Gedmo\Versioned]
	private Client $client;

	#[ORM\Column( type: 'string', length: 255, unique: true, nullable: false )]
	#[Groups( [ 'memorandum_read_folio' ] )]
	#[Gedmo\Versioned]
	private $folio;

	public function getId(): ?int {
		return $this->id;
	}

	public function getTitle(): ?string {
		return $this->title;
	}

	public function setTitle( string $title ): self {
		$this->title = $title;

		return $this;
	}

	public function getProject(): ?Project {
		return $this->project;
	}

	public function setProject( ?Project $project ): self {
		$this->project = $project;

		return $this;
	}

	public function getContent(): ?string {
		return $this->content;
	}

	public function setContent( string $content ): self {
		$this->content = $content;

		return $this;
	}

	public function getFolioTypeIdentifier(): string {
		return 'M';
	}

	/**
	 * addDQLFolioSequential
	 */
	public function addDQLFolioSequential( \Doctrine\ORM\QueryBuilder $query ): \Doctrine\ORM\QueryBuilder {
		return $query
			->leftJoin( 'e.project', 'p' )
			->andWhere( 'p.client = :client' )
			->setParameter( 'client', $this->getProject()->getClient() );
	}

	/**
	 * getBaseInitials
	 */
	public function getFolioBaseInitials(): string {
		return $this->getProject()->getClient()->getInitials();
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
