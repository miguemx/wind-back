<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Repository\MunicipalityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table]
#[UniqueConstraint( name: 'municipality_unique', columns: [ 'name', 'state_id' ] )]
#[ApiResource( collectionOperations: [ 'get' => [] ], itemOperations: [ 'get' => [] ] )]
#[ORM\Entity( repositoryClass: MunicipalityRepository::class )]
#[ApiFilter( SearchFilter::class, properties: [ 'id', 'name', 'state' ] )]
#[ApiFilter( OrderFilter::class, properties: [ 'name' ] )]
#[ApiFilter( GroupFilter::class, arguments: [
	'parameterName'         => 'sGroups',
	'overrideDefaultGroups' => true,
	'whitelist'             => [
		'municipality_read',
		'municipality_read_state',
		'municipality_read_neighbourhoods'
	]
] )]
class Municipality {
	#[ORM\Id]
	#[Groups( [ 'municipality_read', 'municipality_read_id' ] )]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[Groups( [ 'municipality_read' ] )]
	#[ORM\Column( type: 'string' )]
	private string $name;

	#[Assert\NotNull]
	#[Groups( [ 'municipality_read_state' ] )]
	#[ORM\ManyToOne( targetEntity: State::class, inversedBy: 'municipalities' )]
	#[ORM\JoinColumn( nullable: false )]
	private State $state;

	/**
	 * @var Neighbourhood[]
	 */
	#[Groups( [ 'municipality_read_neighbourhoods' ] )]
	#[ORM\OneToMany( targetEntity: Neighbourhood::class, mappedBy: 'municipality' )]
	private $neighbourhoods;

	public function __construct() {
		$this->neighbourhoods = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection|Neighbourhood[]
	 */
	public function getNeighbourhoods(): Collection {
		return $this->neighbourhoods;
	}

	public function addNeighbourhood( Neighbourhood $neighbourhood ): self {
		if ( ! $this->neighbourhoods->contains( $neighbourhood ) ) {
			$this->neighbourhoods[] = $neighbourhood;
			$neighbourhood->setMunicipality( $this );
		}

		return $this;
	}

	public function removeNeighbourhood( Neighbourhood $neighbourhood ): self {
		if ( $this->neighbourhoods->removeElement( $neighbourhood ) ) {
			// set the owning side to null (unless already changed)
			if ( $neighbourhood->getMunicipality() === $this ) {
				$neighbourhood->setMunicipality( null );
			}
		}

		return $this;
	}

	public function getState(): State {
		return $this->state;
	}

	public function setState( State $state ): self {
		$this->state = $state;

		return $this;
	}
}
