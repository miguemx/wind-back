<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Repository\ChannelUserRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET' ],
		'post' => [ 'method' => 'POST' ]
	], itemOperations: [
		'get'    => [ 'method' => 'GET' ],
		'put'    => [ 'method' => 'PUT' ],
		'delete' => [ 'method' => 'DELETE' ]
], attributes: [
	'normalization_context'   => [ 'groups' => [ 'channel_user_read' ] ],
	'denormalization_context' => [ 'groups' => [ 'channel_user_write', 'channel_write' ] ]
] )]
#[Gedmo\Loggable]
#[ORM\Entity( repositoryClass: ChannelUserRepository::class )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
class ChannelUser {
	use TrackableTrait;

	#[ORM\Id]
	#[Groups( [ 'channel_user_read', 'channel_user_write', 'channel_user_read_id' ] )]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[Groups( [ 'channel_user_read', 'channel_user_write', 'channel_user_read_user', 'channel_write' ] )]
	#[ORM\ManyToOne( targetEntity: User::class, inversedBy: 'channelUsers' )]
	private $user;

	#[Groups( [ 'channel_user_read', 'channel_user_write', 'channel_user_read_channel', 'channel_write' ] )]
	#[ORM\ManyToOne( targetEntity: Channel::class, inversedBy: 'channelUsers' )]
	private $channel;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	private $tenant;

	#[Groups( [ 'channel_user_read', 'channel_user_read_client', 'channel_write' ] )]
	#[ORM\ManyToOne( targetEntity: Client::class, inversedBy: 'channelUsers' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $client;

	public function getId(): ?int {
		return $this->id;
	}

	public function getUser(): ?User {
		return $this->user;
	}

	public function setUser( ?User $user ): self {
		$this->user = $user;

		return $this;
	}

	public function getChannel(): ?Channel {
		return $this->channel;
	}

	public function setChannel( ?Channel $channel ): self {
		$this->channel = $channel;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
