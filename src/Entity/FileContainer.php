<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\AppFileAnnotation;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\FileContainerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This is a generic container to have a standard on how to attach files to different entities like Project, Quote, etc.
 * With this entity we create a homogeneous interface to which the front can query, reate or delete the files.
 */
#[ApiResource( collectionOperations: [ 'get' => [ 'method' => 'GET' ] ], itemOperations: [
	'put'    => [ 'method' => 'PUT' ],
	'delete' => [ 'method' => 'DELETE' ],
	'get'    => [ 'method' => 'GET' ]
], attributes: [
	'input_formats'           => [ 'json' => [ 'application/json' ] ],
	'output_formats'          => [ 'json' => [ 'application/json' ] ],
	'normalization_context'   => [ 'groups' => [ 'file_container_read' ] ],
	'denormalization_context' => [ 'groups' => [ 'file_container_write' ] ]
] )]
#[Gedmo\Loggable]
#[ORM\Entity( repositoryClass: FileContainerRepository::class )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
class FileContainer {
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'file_container_read', 'file_container_read_id' ] )]
	private $id;

	/**
	 * @var AppFile[]
     */
    #[AppFileAnnotation(fileType: AppFile::GENERAL_FILE)]
	#[Groups( [ 'file_container_write', 'file_container_read_app_files' ] )]
	#[ORM\ManyToMany( targetEntity: AppFile::class, cascade: [ 'persist', 'remove' ], orphanRemoval: 'cascade' )]
	#[ORM\JoinTable( name: 'file_container_app_file', joinColumns: [
	   new ORM\JoinColumn(name: 'file_container_id', referencedColumnName: 'id', onDelete: 'CASCADE' ),
	   new ORM\JoinColumn(name: 'app_file_id', referencedColumnName: 'id')
	] )]
	private $appFiles;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[Groups( [ 'file_container_read_client' ] )]
	#[Assert\NotNull]
	#[ORM\JoinColumn( nullable: false )]
	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[Gedmo\Versioned]
	private Client $client;

	public function __construct() {
		$this->appFiles = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	/**
	 * @return Collection|AppFile[]
	 */
	public function getAppFiles(): Collection {
		return $this->appFiles;
	}

	public function addAppFile( AppFile $appFile ): self {
		if ( ! $this->appFiles->contains( $appFile ) ) {
			$this->appFiles[] = $appFile;
		}

		return $this;
	}

	public function removeAppFile( AppFile $appFile ): self {
		$this->appFiles->removeElement( $appFile );

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
