<?php

namespace App\Entity;

use App\Annotation\TenantAwareAnnotation;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity( repositoryClass: \App\Repository\NotificationUserEntityRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[Gedmo\Loggable]
#[ORM\Table( name: 'notification_user_entity' )]
#[UniqueConstraint( name: 'notification_user_unique', columns: [
	'notification_topic_id',
	'entity_class',
	'user_id',
	'entity_id'
] )]
class NotificationUserEntity {
	use TrackableTrait;

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[ORM\Column( type: 'string' )]
	#[Gedmo\Versioned]
	private string $entityClass;

	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private int $entityId;

	#[ORM\ManyToOne( targetEntity: NotificationTopic::class )]
	#[Gedmo\Versioned]
	private NotificationTopic $notificationTopic;

	#[ORM\ManyToOne( targetEntity: User::class )]
	#[Gedmo\Versioned]
	private User $user;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[Gedmo\Versioned]
	private Client $client;

	public function getId(): ?int {
		return $this->id;
	}

	public function getEntityClass(): ?string {
		return $this->entityClass;
	}

	public function setEntityClass( string $entityClass ): self {
		$this->entityClass = $entityClass;

		return $this;
	}

	public function getEntityId(): ?int {
		return $this->entityId;
	}

	public function setEntityId( int $entityId ): self {
		$this->entityId = $entityId;

		return $this;
	}

	public function getNotificationTopic(): ?NotificationTopic {
		return $this->notificationTopic;
	}

	public function setNotificationTopic( ?NotificationTopic $notificationTopic ): self {
		$this->notificationTopic = $notificationTopic;

		return $this;
	}

	public function getUser(): ?User {
		return $this->user;
	}

	public function setUser( ?User $user ): self {
		$this->user = $user;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
