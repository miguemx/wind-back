<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TaxDocumentItemRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\ClientAwareAnnotation;
use App\Annotation\TenantAwareAnnotation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TaxDocumentItemRepository::class)]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[Gedmo\Loggable]
#[ApiResource(
    collectionOperations: [
        'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('TAX_DOCUMENT_ITEM_LIST')" ],
        'post' => [ 'method' => 'POST', 'access_control' => "is_granted('TAX_DOCUMENT_ITEM_CREATE')" ]
    ],
    itemOperations: [
        'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('TAX_DOCUMENT_ITEM_UPDATE')" ],
        'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('TAX_DOCUMENT_ITEM_DELETE')" ],
        'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('TAX_DOCUMENT_ITEM_SHOW')" ]
    ],
    attributes: [
        'input_formats'           => [ 'json' => [ 'application/json' ] ],
        'output_formats'          => [
            'json' => [ 'application/json' ],
        ],
        'normalization_context'   => [
            'groups' => [ 'tax_document_item_read' ]
        ],
        'denormalization_context' => [
            'groups' => [
                'tax_document_item_write'
            ]
        ]
    ])]
#[ApiFilter(GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ])]
class TaxDocumentItem
{
    use TrackableTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['tax_document_item_read', 'tax_document_item_write','tax_document_item_read_id'])]
    private $id;

    #[ORM\ManyToOne(targetEntity: TaxDocument::class, inversedBy: 'taxDocumentItems')]
    #[Groups(['tax_document_item_write','tax_document_item_read_tax_document'])]
    private $taxDocument;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['tax_document_item_read','tax_document_item_write','tax_document_item_read_product_identification'])]
    #[Gedmo\Versioned]
    private $productIdentification;

    #[ORM\Column(type: 'string', length: 500, nullable: true)]
    #[Groups(['tax_document_item_read','tax_document_item_write','tax_document_item_read_description'])]
    #[Gedmo\Versioned]
    private $description;

    #[ORM\Column(type: 'decimal', precision: 14, scale: 4)]
    #[Groups(['tax_document_item_read','tax_document_item_write','tax_document_item_read_unit_amount'])]
    #[Gedmo\Versioned]
    private $unitAmount;

    #[ORM\Column(type: 'string', length: 10, nullable: true)]
    #[Groups(['tax_document_item_read','tax_document_item_write','tax_document_item_read_unit_code'])]
    #[Gedmo\Versioned]
    private $unitCode;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 4)]
    #[Groups(['tax_document_item_read','tax_document_item_write','tax_document_item_read_discount_amount'])]
    #[Gedmo\Versioned]
    private $discountAmount;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 4)]
    #[Groups(['tax_document_item_read','tax_document_item_write','tax_document_item_read_tax_amount'])]
    #[Gedmo\Versioned]
    private $taxAmount;

    #[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 4)]
    #[Groups(['tax_document_item_read','tax_document_item_write','tax_document_item_read_price'])]
    #[Gedmo\Versioned]
    private $price;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 4)]
    #[Groups(['tax_document_item_read','tax_document_item_write','tax_document_item_read_iva'])]
    #[Gedmo\Versioned]
    private $iva = 0;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 4)]
    #[Groups(['tax_document_item_read','tax_document_item_write','tax_document_item_read_ieps'])]
    #[Gedmo\Versioned]
    private $ieps = 0;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 4)]
    #[Groups(['tax_document_item_read','tax_document_item_write','tax_document_item_read_isr'])]
    #[Gedmo\Versioned]
    private $isr = 0;

    public function __construct()
    {
        $this->taxes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTaxDocument(): ?TaxDocument
    {
        return $this->taxDocument;
    }

    public function setTaxDocument(?TaxDocument $taxDocument): self
    {
        $this->taxDocument = $taxDocument;

        return $this;
    }

    public function getProductIdentification(): ?string
    {
        return $this->productIdentification;
    }

    public function setProductIdentification(?string $productIdentification): self
    {
        $this->productIdentification = $productIdentification;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUnitAmount(): ?string
    {
        return $this->unitAmount;
    }

    public function setUnitAmount(string $unitAmount): self
    {
        $this->unitAmount = $unitAmount;

        return $this;
    }

    public function getUnitCode(): ?string
    {
        return $this->unitCode;
    }

    public function setUnitCode(?string $unitCode): self
    {
        $this->unitCode = $unitCode;

        return $this;
    }

    public function getDiscountAmount(): ?string
    {
        return $this->discountAmount;
    }

    public function setDiscountAmount(string $discountAmount): self
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    public function getTaxAmount(): ?string
    {
        return $this->taxAmount;
    }

    public function setTaxAmount(string $taxAmount): self
    {
        $this->taxAmount = $taxAmount;

        return $this;
    }

    public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
                                   $this->tenant = $tenant;
                               
                                   return $this;
                               }

    
    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIva(): ?string
    {
        return $this->iva;
    }

    public function setIva(string $iva): self
    {
        $this->iva = $iva;

        return $this;
    }

    public function getIeps(): ?string
    {
        return $this->ieps;
    }

    public function setIeps(string $ieps): self
    {
        $this->ieps = $ieps;

        return $this;
    }

    public function getIsr(): ?string
    {
        return $this->isr;
    }

    public function setIsr(string $ieps): self
    {
        $this->isr = $isr;

        return $this;
    }
}
