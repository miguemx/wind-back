<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\ProviderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity( repositoryClass: ProviderRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('PROVIDER_LIST')" ],
		'post' => [ 'method' => 'POST', 'access_control' => "is_granted('PROVIDER_CREATE')" ]
	],
	itemOperations: [
		'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('PROVIDER_UPDATE')" ],
		'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('PROVIDER_DELETE')" ],
		'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('PROVIDER_SHOW')" ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [ 'groups' => [ 'provider_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'provider_write' ] ]
	] )]
class Provider {
	use TrackableTrait;

	#[Groups( [ 'provider_read', 'provider_write', 'provider_read_id' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'provider_read', 'provider_write', 'provider_read_name' ] )]
	#[ORM\Column( type: 'string', length: 255 )]
	#[Gedmo\Versioned]
	private $name;

	#[Groups( [ 'provider_read', 'provider_write', 'provider_read_phone' ] )]
	#[ORM\Column( type: 'string', length: 50 )]
	#[Gedmo\Versioned]
	private $phone;

	#[Groups( [ 'provider_read', 'provider_write', 'provider_read_description' ] )]
	#[ORM\Column( type: 'string', length: 300, nullable: true )]
	#[Gedmo\Versioned]
	private $description;

	#[Groups( [ 'provider_read_recurring_payments' ] )]
	#[ORM\OneToMany( targetEntity: RecurringPayment::class, mappedBy: 'provider' )]
	private $recurringPayments;

	#[Groups( [ 'provider_read', 'provider_write', 'provider_read_transactions' ] )]
	#[ORM\OneToMany( targetEntity: Transaction::class, mappedBy: 'provider' )]
	private $transactions;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	public function __construct() {
		$this->transactions      = new ArrayCollection();
		$this->recurringPayments = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getPhone(): ?string {
		return $this->phone;
	}

	public function setPhone( string $phone ): self {
		$this->phone = $phone;

		return $this;
	}

	public function getDescription(): ?string {
		return $this->description;
	}

	public function setDescription( ?string $description ): self {
		$this->description = $description;

		return $this;
	}

	/**
	 * @return Collection|RecurringPayment[]
	 */
	public function getRecurringPayments(): Collection {
		return $this->RecurringPayments;
	}

	public function addRecurringPayment( RecurringPayment $recurringPayment ): self {
		if ( ! $this->recurringPayment->contains( $recurringPayment ) ) {
			$this->recurringPayments[] = $recurringPayment;
			$recurringPayment->setProviderId( $this );
		}

		return $this;
	}

	public function removeRecurringPayment( RecurringPayment $recurringPayment ): self {
		if ( $this->recurringPayments->removeElement( $recurringPayment ) ) {
			// set the owning side to null (unless already changed)
			if ( $recurringPayment->getProviderId() === $this ) {
				$recurringPayment->setProviderId( null );
			}
		}

		return $this;
	}

	/**
	 * @return Collection|Transaction[]
	 */
	public function getTransactions(): Collection {
		return $this->transactions;
	}

	public function addTransaction( Transaction $transaction ): self {
		if ( ! $this->transactions->contains( $transaction ) ) {
			$this->transactions[] = $transaction;
			$transaction->setProviderId( $this );
		}

		return $this;
	}

	public function removeTransaction( Transaction $transaction ): self {
		if ( $this->transactions->removeElement( $transaction ) ) {
			// set the owning side to null (unless already changed)
			if ( $transaction->getProviderId() === $this ) {
				$transaction->setProviderId( null );
			}
		}

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}
}
