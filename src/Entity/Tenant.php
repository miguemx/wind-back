<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\TenantAwareAnnotation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[TenantAwareAnnotation( tenantFieldName: 'id' )]
#[ORM\Table]
#[ApiResource(
    collectionOperations: [
        'get' => []
    ],
    itemOperations: [
        'get' => [],
        'put' => [ 'method' => 'PUT', 'access_control' => "is_granted('TENANT_UPDATE')" ],
    ],
    attributes: [
		'normalization_context'   => [
            'groups' => [ 'tenant_read' ]
        ],
            'denormalization_context' => [
            'groups' => [
                'tenant_write',
                'fiscal_data_write',
                'tenant_rfc_write'
            ]
        ]
    ]
)]
#[ORM\Entity]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
class Tenant {
	#[ORM\Id]
    #[Groups( [ 'tenant_read', 'tenant_read_id' ] )]
    #[ORM\GeneratedValue]
    #[ORM\Column( type: 'integer' )]
    private $id;

	#[Groups( [ 'tenant_read', 'tenant_read_name' ] )]
    #[ORM\Column( type: 'string' )]
    private string $alias;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: TenantRfc::class)]
    private $tenantRfcs;

    public function __construct()
    {
        $this->fiscalDatas = new ArrayCollection();
        $this->tenantRfcs = new ArrayCollection();
    }

	public function getId(): ?int {
                       return $this->id;
                   }

	public function setId( int $id ): Tenant {
        $this->id = $id;

        return $this;
    }

	public function getAlias(): ?string {
        return $this->alias;
    }

	public function setAlias( string $alias ): self {
        $this->alias = $alias;

        return $this;
    }

    /**
	 * @param Collection|FiscalData[] $fiscalDatas
	 *
	 * @return $this
	 */
	public function setFiscalDatas( $fiscalDatas ): self {
            $this->fiscalDatas = $fiscalDatas;

        return $this;
    }

    /**
     * @return Collection<int, TenantRfc>
     */
    public function getTenantRfcs(): Collection
    {
        return $this->tenantRfcs;
    }

    public function addTenantRfc(TenantRfc $tenantRfc): self
    {
        if (!$this->tenantRfcs->contains($tenantRfc)) {
            $this->tenantRfcs[] = $tenantRfc;
            $tenantRfc->setTenant($this);
        }

        return $this;
    }

    public function removeTenantRfc(TenantRfc $tenantRfc): self
    {
        if ($this->tenantRfcs->removeElement($tenantRfc)) {
            // set the owning side to null (unless already changed)
            if ($tenantRfc->getTenant() === $this) {
                $tenantRfc->setTenant(null);
            }
        }

        return $this;
    }
}
