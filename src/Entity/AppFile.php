<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\ClientAwareAnnotation;
use App\Annotation\TenantAwareAnnotation;
use App\Validators as AppAssert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * AppFile
 */
#[AppAssert\ValidAppFile]
#[ORM\Table( name: 'app_file' )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ClientAwareAnnotation( clientFieldName: 'client_id' )]
#[Vich\Uploadable]
#[Gedmo\Loggable]
#[ORM\Entity]
#[ApiResource(
	collectionOperations: [
		'get' => [ 'method' => 'GET' ]
	],
	itemOperations: [
		'get' => [ 'method' => 'GET' ]
	],
	attributes: [
		'normalization_context' => [
			'groups'          => [ 'app_file_read' ],
			'datetime_format' => 'Y-m-d\TH:i'
		]
	] )]
class AppFile {
	use TrackableTrait;

	public const IMAGE_MIME_TYPES = [ 'image/jpg', 'image/jpeg', 'image/png' ];
	public const GENERAL_PURPOSE_FILE_TYPES = [
		'application/pdf',
		'application/x-pdf',
		'image/jpg',
		'image/jpeg',
		'image/png',
		'application/xml',
		'text/xml',
		'application/vnd.ms-excel',
		'application/vnd.ms-office'
	];
	public const GENERAL_FILE = 'general_file';

	static function getTypes() {
		return [ self::IMAGE_MIME_TYPES, self::GENERAL_PURPOSE_FILE_TYPES ];
	}

	#[ORM\Column( name: 'id', type: 'integer' )]
	#[ORM\Id]
	#[Groups( [ 'read_file', 'app_file_read' ] )]
	#[ORM\GeneratedValue( strategy: 'AUTO' )]
	private int $id;



	#[Vich\UploadableField(mapping:"file", fileNameProperty:"name", size:"size", mimeType:"mimeType")]
	#[Assert\File( maxSize: '19531k', maxSizeMessage: 'El archivo no debe pesar más de 20 MB' )]
	#[Gedmo\Versioned]
	private ?\Symfony\Component\HttpFoundation\File\File $file = null;

	#[Assert\Choice( callback: 'getTypes' )]
	#[ORM\Column( type: 'string' )]
	#[Gedmo\Versioned]
	private string $type;

	#[ORM\Column( type: 'string', length: 255 )]
	#[Groups( [ 'read_file', 'app_file_read' ] )]
	#[Gedmo\Versioned]
	private ?string $name;

	#[ORM\Column( name: 'original_name', type: 'string', length: 255, nullable: true )]
	#[Groups( [ 'read_file', 'app_file_read' ] )]
	#[Assert\Length( max: 255 )]
	#[Gedmo\Versioned]
	private ?string $originalName;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'read_file', 'app_file_read' ] )]
	#[Gedmo\Versioned]
	private ?int $size;

	#[ORM\Column( type: 'string', length: 255 )]
	#[Groups( [ 'read_file', 'app_file_read' ] )]
	#[Gedmo\Versioned]
	private ?string $mimeType;

	#[ORM\Column( type: 'boolean' )]
	#[Gedmo\Versioned]
	private bool $isPublic = false;

	/**
	 * @var \DateTime $created
	 */
	#[Groups( [ 'app_file_read', 'app_file_read_created_date' ] )]
	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Gedmo\Timestampable( on: 'create' )]
	protected $createdDate;

	/**
	 * @var \DateTime $updated
	 */
	#[Groups( [ 'app_file_read', 'app_file_read_updated_date' ] )]
	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Gedmo\Timestampable( on: 'update' )]
	protected $updatedDate;

	/**
	 * @var User
	 * @Gedmo\Blameable(on="create")
	 * @MaxDepth(1)
	 */
	#[Groups( [ 'app_file_read_created_by' ] )]
	#[ORM\ManyToOne( targetEntity: User::class )]
	#[ORM\JoinColumn( name: 'created_by', referencedColumnName: 'id', onDelete: 'SET NULL', nullable: true )]
	protected $createdBy;

	/**
	 * @var User
	 * @Gedmo\Blameable(on="update")
	 * @MaxDepth(1)
	 */
	#[Groups( [ 'app_file_read_updated_by' ] )]
	#[ORM\ManyToOne( targetEntity: User::class )]
	#[ORM\JoinColumn( name: 'updated_by', referencedColumnName: 'id', onDelete: 'SET NULL', nullable: true )]
	protected $updatedBy;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[ORM\JoinColumn( nullable: true )]
	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[Gedmo\Versioned]
	private $client;


	/**
	 * The owner domain refers to a type of objects these rules applies.
	 * Usually it is an Object's class, but it is not constrained to be a class.
	 * Example: $appFile->setDomain(Project::class);
     */
	 #[ORM\Column(type:"string", length:255, nullable:true)]
     #[Groups(["app_file_read"])]
     #[Gedmo\Versioned]
	private $ownerDomain;// "Project"=>"App/Entity/Project"

	/**
	 * The identifier of the object these rules will apply. It's usually the id field of the object from the "domain"
	 * Example: $appFile->setObjectId($project->getId());
     */
    #[ORM\Column(type:"string", length:40, nullable:true)]
    #[Groups(["app_file_read"])]
    #[Gedmo\Versioned]
	private $objectId;// "1"

	public function __construct() {
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
	 * of 'UploadedFile' is injected into this setter to trigger the  update. If this
	 * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
	 * must be able to accept an instance of 'File' as the bundle will inject one here
	 * during Doctrine hydration.
	 *
	 * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
	 *
	 * @return AppFile
	 */
	public function setFile( \Symfony\Component\HttpFoundation\File\File|\Symfony\Component\HttpFoundation\File\UploadedFile $file = null ) {
		$this->file = $file;

		if ( $file ) {
			if ( $file::class == UploadedFile::class ) {
				$this->setOriginalName( $file->getClientOriginalName() );
			}

			// It is required that at least one field changes if you are using doctrine
			// otherwise the event listeners won't be called and the file is lost
			$this->updatedDate = new \DateTimeImmutable();
		}

		return $this;
	}

	/**
	 * @return File|null
	 */
	public function getFile() {
		return $this->file;
	}

	/**
	 * @param string $name
	 *
	 * @return AppFile
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param integer $size
	 *
	 * @return AppFile
	 */
	public function setSize( $size ) {
		$this->size = $size;

		return $this;
	}

	/**
	 * @return integer|null
	 */
	public function getSize() {
		return $this->size;
	}

	/**
	 * Set mimeType
	 *
	 * @param string $mimeType
	 *
	 * @return AppFile
	 */
	public function setMimeType( $mimeType ) {
		$this->mimeType = $mimeType;

		return $this;
	}

	/**
	 * Get mimeType
	 *
	 * @return string
	 */
	public function getMimeType() {
		return $this->mimeType;
	}

	/**
	 * Set isPublic
	 *
	 * @param boolean $isPublic
	 *
	 * @return AppFile
	 */
	public function setIsPublic( $isPublic ) {
		$this->isPublic = $isPublic;

		return $this;
	}

	/**
	 * Get isPublic
	 *
	 * @return boolean
	 */
	public function getIsPublic() {
		return $this->isPublic;
	}

	/**
	 * Set originalName
	 *
	 * @param string $originalName
	 *
	 * @return AppFile
	 */
	public function setOriginalName( $originalName ) {
		$this->originalName = $originalName;

		return $this;
	}

	/**
	 * Get originalName
	 *
	 * @return string
	 */
	public function getOriginalName() {
		return $this->originalName;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType( $type ) {
		$this->type = $type;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

        return $this;
    }

    public function getOwnerDomain(): ?string
    {
        return $this->ownerDomain;
    }

    public function setOwnerDomain(?string $ownerDomain): self
    {
        $this->ownerDomain = $ownerDomain;

        return $this;
    }

    public function getObjectId(): ?string
    {
        return $this->objectId;
    }

    public function setObjectId(?string $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }
}
