<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Permission
 */
#[ORM\Table( name: 'permission' )]
#[ORM\Index( name: 'permission_name_idx', columns: [ 'name' ] )]
#[ORM\Entity]
#[UniqueEntity( fields: [ 'name' ] )]
#[ApiResource(
	collectionOperations: [
		'get' => [
			'method'         => 'GET',
			'access_control' => "is_granted('PERMISSION_LIST')"
		]
	],
	itemOperations: [
		'get' => [
			'method'         => 'GET',
			'access_control' => "is_granted('PERMISSION_SHOW')"
		]
	], attributes: [
	'normalization_context'   => [ 'groups' => [ 'permission_read' ] ],
	'denormalization_context' => [ 'groups' => [ 'permission_write' ] ]
] )]
#[UniqueEntity( fields: [ 'name' ] )]
class Permission {
	use TrackableTrait;

	#[Groups( [ 'permission_read' ] )]
	#[ORM\Column( name: 'id', type: 'integer', options: [ 'unsigned' => true ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue( strategy: 'AUTO' )]
	private int $id;

	#[Groups( [ 'permission_read', 'permission_write' ] )]
	#[ORM\Column( type: 'string' )]
	#[Assert\NotBlank]
	private string $name;

	/**
	 * @var Role[]
	 */
	#[Groups( [ 'permission_read', 'permission_write' ] )]
	#[ORM\ManyToMany( targetEntity: Role::class, inversedBy: 'permissions', cascade: [ 'persist' ] )]
	private $roles;

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->roles = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Permission
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Add role
	 *
	 *
	 * @return Permission
	 */
	public function addRole( Role $role ) {
		if ( ! $this->roles->contains( $role ) ) {
			$this->roles[] = $role;
			$this->addRole( $role );
		}

		return $this;
	}

	/**
	 * Remove role
	 */
	public function removeRole( Role $role ) {
		$this->roles->removeElement( $role );
	}

	/**
	 * Get roles
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getRoles() {
		return $this->roles;
	}

	/**
	 * Get rolesAsArray
	 *
	 * @return array
	 */
	public function getRolesAsArray() {
		/**
		 * @var $roles Role
		 */
		$roles = $this->roles;
		$array = [];
		foreach ( $roles as $role ) {
			$array[] = $role->getName();
		}

		return $array;
	}
}
