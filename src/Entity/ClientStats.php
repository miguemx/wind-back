<?php

namespace App\Entity;

use App\Annotation\ClientAwareAnnotation;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\ClientStatsRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[Gedmo\Loggable]
#[ORM\Entity( repositoryClass: ClientStatsRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ClientAwareAnnotation( clientFieldName: 'client_id' )]
class ClientStats {
	use TrackableTrait;

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'client_stats_read', 'client_stats_read_id' ] )]
	private $id;

	#[Groups( [ 'client_stats_read_client' ] )]
	#[ORM\OneToOne( targetEntity: Client::class, inversedBy: 'clientStats' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $client;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'client_stats_read', 'client_stats_read_projects_count' ] )]
	private int $projectsCount = 0;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'client_stats_read', 'client_stats_read_approved_projects_count' ] )]
	private int $approvedProjectsCount = 0;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'client_stats_read', 'client_stats_read_quotes_count' ] )]
	private int $quotesCount = 0;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'client_stats_read', 'client_stats_read_approved_quotes_count' ] )]
	private int $approvedQuotesCount = 0;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'client_stats_read', 'client_stats_read_pending_quotes_count' ] )]
	private int $pendingQuotesCount = 0;

	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Groups( [ 'client_stats_read', 'client_stats_read_all_quotes_sum' ] )]
	private int $allQuotesSum = 0;

	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Groups( [ 'client_stats_read', 'client_stats_read_approved_quotes_sum' ] )]
	private int $approvedQuotesSum = 0;

	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Groups( [ 'client_stats_read', 'client_stats_read_payments_sum' ] )]
	private int $paymentsSum = 0;

	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Groups( [ 'client_stats_read', 'client_stats_read_pending_payments_sum' ] )]
	private int $pendingPaymentsSum = 0;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	public function getId(): ?int {
		return $this->id;
	}

	public function getClient(): Client {
		return $this->client;
	}

	public function setClient( Client $client ): self {
		$this->client = $client;

		return $this;
	}

	public function getProjectsCount(): ?int {
		return $this->projectsCount;
	}

	public function setProjectsCount( int $projectsCount ): self {
		$this->projectsCount = $projectsCount;

		return $this;
	}

	public function getApprovedProjectsCount(): ?int {
		return $this->approvedProjectsCount;
	}

	public function setApprovedProjectsCount( int $approvedProjectsCount ): self {
		$this->approvedProjectsCount = $approvedProjectsCount;

		return $this;
	}

	public function getQuotesCount(): ?int {
		return $this->quotesCount;
	}

	public function setQuotesCount( int $quotesCount ): self {
		$this->quotesCount = $quotesCount;

		return $this;
	}

	public function getApprovedQuotesCount(): ?int {
		return $this->approvedQuotesCount;
	}

	public function setApprovedQuotesCount( int $approvedQuotesCount ): self {
		$this->approvedQuotesCount = $approvedQuotesCount;

		return $this;
	}

	public function getAllQuotesSum(): ?float {
		return $this->allQuotesSum;
	}

	public function setAllQuotesSum( float $allQuotesSum ): self {
		$this->allQuotesSum = $allQuotesSum;

		return $this;
	}

	public function getApprovedQuotesSum(): ?float {
		return $this->approvedQuotesSum;
	}

	public function setApprovedQuotesSum( float $approvedQuotesSum ): self {
		$this->approvedQuotesSum = $approvedQuotesSum;

		return $this;
	}

	public function getPaymentsSum(): ?float {
		return $this->paymentsSum;
	}

	public function setPaymentsSum( float $paymentsSum ): self {
		$this->paymentsSum = $paymentsSum;

		return $this;
	}

	public function getPendingPaymentsSum(): ?float {
		return $this->pendingPaymentsSum;
	}

	public function setPendingPaymentsSum( float $pendingPaymentsSum ): self {
		$this->pendingPaymentsSum = $pendingPaymentsSum;

		return $this;
	}

	public function getPendingQuotesCount(): ?int {
		return $this->pendingQuotesCount;
	}

	public function setPendingQuotesCount( int $pendingQuotesCount ): self {
		$this->pendingQuotesCount = $pendingQuotesCount;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}
}
