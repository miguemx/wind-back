<?php

namespace App\Entity;

use App\Repository\TideAclRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity( repositoryClass: TideAclRepository::class )]
#[ORM\Table]
#[Gedmo\Loggable]
#[ORM\UniqueConstraint( columns: [ 'domain', 'object_id', 'security_identity' ] )]
#[ORM\Index( columns: [ 'domain', 'object_id' ], name: 'domain_object_idx' )]
#[ORM\Index( columns: [ 'security_identity' ], name: 'domain_object_idx' )]
class TideAcl {
	public const ACL_PERMISSION_SHOW = 1;
	public const ACL_PERMISSION_UPDATE = 1 << 2 | self::ACL_PERMISSION_SHOW;
	public const ACL_PERMISSION_DELETE = 1 << 3 | self::ACL_PERMISSION_SHOW;
	public const ACL_PERMISSION_UPDATE_ACLS = 1 << 4 | self::ACL_PERMISSION_SHOW;
	use TrackableTrait;

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	public function __construct(
		/**
		 * The domain refers to a type of objects these rules applies.
		 * Usually it is an Object's class, but it is not constrained to be a class.
		 * Example: $acl->setDomain(Project::class);
		 */
		#[ORM\Column( type: 'string', length: 255 )] #[Groups( [ 'tide_acl_read' ] )] #[Gedmo\Versioned] private $domain,
		// "Project"=>"App/Entity/Project"
		/**
		 * The identifier of the object these rules will apply. It's usually the id field of the object from the "domain"
		 * Example: $acl->setObjectId($project->getId());
		 */
		#[ORM\Column( type: 'string', length: 40 )] #[Groups( [ 'tide_acl_read' ] )] #[Gedmo\Versioned] private $objectId,
		// "1"
		/**
		 * This is the identifier of the actor who wants to perform an action on the object.
		 * It's usually the user's id, but more advanced rules could be applied to "Groups" of users. In that case this
		 * would be the Group's id.
		 * To support more than one type of actors, the identifier should differentiate them with a prefix. For example
		 * prefix all users' ids with "U:" and groups' ids with "G:"
		 * Example: $acl->setSecurityIdentity( "U:".$user->getId() );//The concatenation should be made by a helper class
		 */
		#[ORM\Column( type: 'string', length: 255 )] #[Groups( [ 'tide_acl_read' ] )] #[Gedmo\Versioned] private $securityIdentity,
		// "U:1"
		/**
		 * A bitmask saying which permissions do the actor has. It's a bitmask, so we can force some permissions to be available before others.
		 * For example, view permission could be 1 and edit permission could be 3. In this case giving edit permission to something
		 * would force the view permission since it makes no sense without it.
		 * The permissions available and their names should be defined by the domain.
		 */
		#[ORM\Column( type: 'integer' )] #[Groups( [ 'tide_acl_read' ] )] #[Gedmo\Versioned] private $permissionMask
	) {
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getDomain(): ?string {
		return $this->domain;
	}

	public function setDomain( string $domain ): self {
		$this->domain = $domain;

		return $this;
	}

	public function getObjectId(): ?string {
		return $this->objectId;
	}

	public function setObjectId( string $objectId ): self {
		$this->objectId = $objectId;

		return $this;
	}

	public function getSecurityIdentity(): ?string {
		return $this->securityIdentity;
	}

	public function setSecurityIdentity( string $securityIdentity ): self {
		$this->securityIdentity = $securityIdentity;

		return $this;
	}

	public function getPermissionMask(): ?int {
		return $this->permissionMask;
	}

	public function setPermissionMask( int $permissionMask ): self {
		$this->permissionMask = $permissionMask;

		return $this;
	}
}
