<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\TenantAwareAnnotation;
use App\Interfaces\FolioInterface;
use App\Repository\QuotationRequestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource( collectionOperations: [
	'get'  => [ 'method' => 'GET' ],
	'post' => [ 'method' => 'POST' ]
],
	itemOperations: [
		'put'    => [ 'method' => 'PUT' ],
		'delete' => [ 'method' => 'DELETE' ],
		'get'    => [ 'method' => 'GET' ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [ 'groups' => [ 'quotation_request_read' ] ],
		'denormalization_context' => [
			'groups' => [
				'quotation_request_write',
				'quotation_request_product_write'
			]
		]
	] )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ORM\Entity( repositoryClass: QuotationRequestRepository::class )]
#[Gedmo\Loggable]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[ApiFilter( SearchFilter::class, properties: [
	'project' => 'exact',
	'quote'   => 'exact',
	'title'   => 'partial',
	'folio'   => 'partial'
] )]
#[ApiFilter( ExistsFilter::class, properties: [ 'quote' ] )]
class QuotationRequest implements FolioInterface {
	use TrackableTrait, FoliableTrait;

	public const STATUS_PENDING = "PENDING";
	public const STATUS_APPROVED = "APPROVED";
	public const STATUS_REJECTED = "REJECTED";
	use TrackableTrait;

	#[Groups( [ 'quotation_request_write', 'quotation_request_read', 'quotation_request_read_id' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'quotation_request_write', 'quotation_request_read', 'quotation_request_read_title' ] )]
	#[ORM\Column( type: 'string', length: 300 )]
	#[Gedmo\Versioned]
	private $title;

	#[ORM\Column( type: 'text', nullable: true )]
	#[Groups( [ 'quotation_request_write', 'quotation_request_read', 'quotation_request_read_description' ] )]
	#[Gedmo\Versioned]
	private $description;

	#[ORM\OneToOne( targetEntity: Quote::class, inversedBy: 'quotationRequest', cascade: [ 'persist', 'remove' ] )]
	#[Groups( [ 'quotation_request_write', 'quotation_request_read', 'quotation_request_read_quote' ] )]
	#[ORM\JoinColumn( nullable: true )]
	#[Gedmo\Versioned]
	private $quote;

	#[ORM\OneToMany( targetEntity: QuotationRequestProduct::class, mappedBy: 'quotationRequest', orphanRemoval: true, cascade: [ 'persist' ] )]
	#[Groups( [
		'quotation_request_read_quotation_request_products',
		'quotation_request_write',
		'quotation_request_read'
	] )]
	private $quotationRequestProducts;

	#[Groups( [ 'quotation_request_read', 'quotation_request_read_project', 'quotation_request_write' ] )]
	#[ORM\ManyToOne( targetEntity: Project::class, inversedBy: 'quotationRequests', cascade: [ 'persist' ] )]
	#[ORM\JoinColumn( nullable: false )]
	#[Assert\NotNull]
	private $project;

	#[Groups( [ 'quotation_request_read', 'quotation_request_read_status', 'quotation_request_write' ] )]
	#[ORM\Column( type: 'string', length: 255, nullable: true )]
	#[Gedmo\Versioned]
	private $status;

	#[ORM\Column( type: 'boolean', nullable: true )]
	#[Groups( [ 'quotation_request_read', 'quotation_request_read_is_archived', 'quotation_request_write' ] )]
	#[Gedmo\Versioned]
	private bool $isArchived;

	#[Groups( [ 'quotation_request_read', 'quotation_request_read_folio', 'quotation_request_write' ] )]
	#[ORM\Column( type: 'string', length: 255, nullable: false )]
	#[Gedmo\Versioned]
	private $folio;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[Groups( [ 'quotation_request_write' ] )]
	#[ORM\JoinColumn( nullable: false )]
	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[Gedmo\Versioned]
	private Client $client;

	public function __construct() {
		$this->quotationRequestProducts = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getTitle(): ?string {
		return $this->title;
	}

	public function setTitle( string $title ): self {
		$this->title = $title;

		return $this;
	}

	public function getDescription(): ?string {
		return $this->description;
	}

	public function setDescription( ?string $description ): self {
		$this->description = $description;

		return $this;
	}

	public function getIsArchived(): ?bool {
		return $this->isArchived;
	}

	public function setIsArchived( bool $isArchived ): self {
		$this->isArchived = $isArchived;

		return $this;
	}

	/**
	 * @return Collection|QuotationRequestProduct[]
	 */
	public function getQuotationRequestProducts(): Collection {
		return $this->quotationRequestProducts;
	}

	/**
	 * @param Collection|QuotationRequestProduct[] $quotationRequestProducts
	 *
	 * @return $this
	 */
	public function setQuotationRequestProducts( array|\Doctrine\Common\Collections\Collection $quotationRequestProducts ): self {
		$this->quotationRequestProducts = $quotationRequestProducts;

		return $this;
	}

	public function addQuotationRequestProduct( QuotationRequestProduct $quotationRequestProducts ): self {
		if ( ! $this->quotationRequestProducts->contains( $quotationRequestProducts ) ) {
			$this->quotationRequestProducts[] = $quotationRequestProducts;
			$quotationRequestProducts->setQuotationRequest( $this );
		}

		return $this;
	}

	public function removeQuotationRequestProduct( QuotationRequestProduct $quotationRequestProducts ): self {
		if ( $this->quotationRequestProducts->removeElement( $quotationRequestProducts ) ) {
			// set the owning side to null (unless already changed)
			if ( $quotationRequestProducts->getQuotationRequest() === $this ) {
				$quotationRequestProducts->setQuotationRequest( null );
			}
		}

		return $this;
	}

	public function getProject(): ?Project {
		return $this->project;
	}

	public function setProject( ?Project $project ): self {
		$this->project = $project;

		return $this;
	}

	public function getStatus(): ?string {
		return $this->status;
	}

	public function setStatus( ?string $status ): self {
		$this->status = $status;

		return $this;
	}

	public function getFolio(): ?string {
		return $this->folio;
	}

	public function setFolio( ?string $folio ): self {
		$this->folio = $folio;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getFolioTypeIdentifier(): string {
		return 'S';
	}

	/**
	 * addDQLFolioSequential
	 */
	public function addDQLFolioSequential( \Doctrine\ORM\QueryBuilder $query ): \Doctrine\ORM\QueryBuilder {
		return $query
			->leftJoin( 'e.project', 'p' )
			->andWhere( 'p.client = :client' )
			->setParameter( 'client', $this->getProject()->getClient() );
	}

	/**
	 * getBaseInitials
	 */
	public function getFolioBaseInitials(): string {
		return $this->getProject()->getClient()->getInitials();
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}

	public function getQuote(): ?Quote {
		return $this->quote;
	}

	public function setQuote( ?Quote $quote ): self {
		$this->quote = $quote;

		return $this;
	}
}
