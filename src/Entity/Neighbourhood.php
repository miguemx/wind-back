<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Repository\NeighbourhoodRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table]
#[UniqueConstraint( name: 'neighbourhood_unique', columns: [ 'name', 'postal_code', 'municipality_id' ] )]
#[ApiResource( collectionOperations: [
	'get'        => [ 'method' => 'GET' ],
	'get_public' => [
		'method'     => 'GET',
		'route_name' => 'get_public_neighbourhoods'
	]
], itemOperations: [ 'get' => [ 'method' => 'GET' ] ] )]
#[ORM\Entity( repositoryClass: NeighbourhoodRepository::class )]
#[ApiFilter( SearchFilter::class, properties: [ 'id', 'name', 'municipality', 'postalCode', 'shippingZone' ] )]
#[ApiFilter( OrderFilter::class, properties: [ 'name' ] )]
#[ApiFilter( GroupFilter::class, arguments: [
	'parameterName'         => 'sGroups',
	'overrideDefaultGroups' => true,
	'whitelist'             => [
		'neighbourhood_read',
		'neighbourhood_read_shipping_zone',
		'neighbourhood_read_municipality',
		'municipality_read',
		'municipality_read_state',
		'state_read'
	]
] )]
class Neighbourhood {
	#[ORM\Id]
	#[Groups( [ 'neighbourhood_read', 'neighbourhood_read_id' ] )]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[Groups( [ 'neighbourhood_read', 'neighbourhood_read_name' ] )]
	#[ORM\Column( type: 'string' )]
	private string $name;

	#[Groups( [ 'neighbourhood_read', 'neighbourhood_read_postal_code' ] )]
	#[ORM\Column( type: 'string' )]
	private string $postalCode;

	#[ORM\ManyToOne( targetEntity: Municipality::class, inversedBy: 'neighbourhoods', cascade: [ 'persist' ] )]
	#[ORM\JoinColumn( nullable: false )]
	#[Groups( [ 'neighbourhood_read_municipality' ] )]
	private Municipality $municipality;

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getPostalCode(): ?string {
		return $this->postalCode;
	}

	public function setPostalCode( string $postalCode ): self {
		$this->postalCode = $postalCode;

		return $this;
	}

	public function getMunicipality(): Municipality {
		return $this->municipality;
	}

	public function setMunicipality( Municipality $municipality ): self {
		$this->municipality = $municipality;

		return $this;
	}
}
