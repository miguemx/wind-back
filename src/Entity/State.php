<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Repository\StateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table]
#[UniqueConstraint( name: 'state_unique', columns: [ 'name' ] )]
#[ApiResource( collectionOperations: [ 'get' => [] ], itemOperations: [ 'get' => [] ] )]
#[ORM\Entity( repositoryClass: StateRepository::class )]
#[ApiFilter( SearchFilter::class, properties: [ 'id', 'name' ] )]
#[ApiFilter( OrderFilter::class, properties: [ 'name' ] )]
#[ApiFilter( GroupFilter::class, arguments: [
	'parameterName'         => 'sGroups',
	'overrideDefaultGroups' => true,
	'whitelist'             => [ 'state_read' ]
] )]
class State {
	#[ORM\Id]
	#[Groups( [ 'state_read', 'state_read_id' ] )]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[Groups( [ 'state_read', 'state_read_name' ] )]
	#[ORM\Column( type: 'string' )]
	private string $name;

	#[ORM\Column( type: 'string' )]
	private string $code;

	/**
	 * @var Municipality[]
	 */
	#[Groups( [ 'state_read_municipalities' ] )]
	#[ORM\OneToMany( targetEntity: Municipality::class, mappedBy: 'state' )]
	private $municipalities;

	public function __construct() {
		$this->municipalities = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection|Municipality[]
	 */
	public function getMunicipalities(): Collection {
		return $this->municipalities;
	}

	public function addMunicipality( Municipality $municipality ): self {
		if ( ! $this->municipalities->contains( $municipality ) ) {
			$this->municipalities[] = $municipality;
			$municipality->setState( $this );
		}

		return $this;
	}

	public function removeMunicipality( Municipality $municipality ): self {
		if ( $this->municipalities->removeElement( $municipality ) ) {
			// set the owning side to null (unless already changed)
			if ( $municipality->getState() === $this ) {
				$municipality->setState( null );
			}
		}

		return $this;
	}

	public function getCode(): ?string {
		return $this->code;
	}

	public function setCode( string $code ): self {
		$this->code = $code;

		return $this;
	}
}
