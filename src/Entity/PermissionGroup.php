<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\PermissionGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [
			'groups' => [
				'permission_group_read',
				'permission_read',
				'permission_group_read_permission'
			]
		],
		'denormalization_context' => [ 'groups' => [ 'permission_group_write' ] ]
	] )]
#[ApiFilter( SearchFilter::class, properties: [
	'name'        => 'partial',
	'description' => 'partial',
	'systemName'  => 'partial'
] )]
#[ApiFilter( OrderFilter::class, properties: [ 'name', 'description', 'systemName' ] )]
#[ORM\Entity( repositoryClass: PermissionGroupRepository::class )]
class PermissionGroup {
	/**
	 * @ApiProperty(writable=false)
	 */
	#[ORM\Id]
	#[Groups( [ 'permission_group_read', 'permission_group_write' ] )]
	#[ORM\Column( name: 'id', type: 'guid' )]
	private $id;

	#[ORM\Column( type: 'string', length: 255 )]
	#[Groups( [ 'permission_group_read', 'permission_group_write', 'permission_group_read_name' ] )]
	private $name;

	#[ORM\Column( type: 'string', length: 255, unique: true )]
	#[Groups( [ 'permission_group_read', 'permission_group_write', 'permission_group_read_code' ] )]
	private $code;

	#[ORM\Column( type: 'string', length: 512 )]
	#[Groups( [ 'permission_group_read', 'permission_group_write', 'permission_group_read_description' ] )]
	private $description;

	/**
	 * @var Permission[]
	 */
	#[Groups( [ 'permission_group_write', 'permission_group_read_permissions' ] )]
	#[ORM\ManyToMany( targetEntity: Permission::class )]
	#[ORM\JoinTable( name: 'permission_groups_permissions', joinColumns: [
	   new ORM\JoinColumn(name: 'permission_group_id', referencedColumnName: 'id', onDelete: 'CASCADE' ),
	   new ORM\JoinColumn(name: 'permission_id', referencedColumnName: 'id')
	] )]
	private $permissions;

	/**
	 * @var User[]
	 */
	#[Groups( [ 'permission_group_write' ] )]
	#[ORM\ManyToMany( targetEntity: User::class, mappedBy: 'permissionGroups' )]
	private $users;

	public function __construct() {
		$this->permissions = new ArrayCollection();
		$this->users       = new ArrayCollection();
	}

	public function getId(): ?string {
		return $this->id;
	}

	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return Collection|Permission[]
	 */
	public function getPermissions(): Collection {
		return $this->permissions;
	}

	public function getPermissionsArray(): array {
		$permissionsArray = [];
		foreach ( $this->permissions as $permission ) {
			$permissionsArray[] = $permission->getName();
		}

		return $permissionsArray;
	}

	public function addPermission( Permission $permission ): self {
		if ( ! $this->permissions->contains( $permission ) ) {
			$this->permissions[] = $permission;
		}

		return $this;
	}

	public function removePermission( Permission $permission ): self {
		if ( $this->permissions->contains( $permission ) ) {
			$this->permissions->removeElement( $permission );
		}

		return $this;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getCode(): ?string {
		return $this->code;
	}

	public function setCode( string $code ): self {
		$this->code = $code;

		return $this;
	}

	public function getDescription(): ?string {
		return $this->description;
	}

	public function setDescription( string $description ): self {
		$this->description = $description;

		return $this;
	}

	/**
	 * @return Collection|User[]
	 */
	public function getUsers(): Collection {
		return $this->users;
	}

	public function addUser( User $user ): self {
		if ( ! $this->users->contains( $user ) ) {
			$this->users[] = $user;
		}

		return $this;
	}

	public function removeUser( User $user ): self {
		if ( $this->users->contains( $user ) ) {
			$this->users->removeElement( $user );
		}

		return $this;
	}
}
