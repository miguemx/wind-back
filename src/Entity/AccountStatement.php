<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\AppFileAnnotation;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\AccountStatementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity( repositoryClass: AccountStatementRepository::class )]
#[Gedmo\Loggable]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ApiResource( collectionOperations: [
	'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('ACCOUNT_STATEMENT_LIST')" ],
	'post' => [
		'method'         => 'POST',
		'access_control' => "is_granted('ACCOUNT_STATEMENT_CREATE')"
	]
], itemOperations: [
	'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('ACCOUNT_STATEMENT_UPDATE')" ],
	'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('ACCOUNT_STATEMENT_DELETE')" ],
	'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('ACCOUNT_STATEMENT_SHOW')" ]
], attributes: [
	'input_formats'           => [ 'json' => [ 'application/json' ] ],
	'output_formats'          => [
		'json' => [ 'application/json' ],
		'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
	],
	'normalization_context'   => [ 'groups' => [ 'account_statement_read' ] ],
	'denormalization_context' => [
		'groups' => [
			'account_statement_write',
			'bank_transaction_write'
		]
	]
] )]
class AccountStatement {
	use TrackableTrait;

	public const ACCOUNT_STATEMENT_STATUS_PENDING = 'PENDING';
	public const ACCOUNT_STATEMENT_STATUS_CONFIRMED = 'CONFIRMED';
	public const ACCOUNT_STATEMENT_STATUS_DENIED = 'DENIED';

	#[ORM\Id]
	#[Groups( [ 'account_statement_read', 'account_statement_write', 'account_statement_read_id' ] )]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'account_statement_read', 'account_statement_write', 'account_statement_read_status' ] )]
	#[ORM\Column( type: 'string', length: 50 )]
	#[Gedmo\Versioned]
	private string $status = AccountStatement::ACCOUNT_STATEMENT_STATUS_PENDING;

	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Groups( [ 'account_statement_read', 'account_statement_read_period_start_date', 'account_statement_write' ] )]
	#[Gedmo\Versioned]
	private $periodStartDate;

	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Groups( [ 'account_statement_read', 'account_statement_read_period_end_date', 'account_statement_write' ] )]
	#[Gedmo\Versioned]
	private $periodEndDate;

	#[Groups( [ 'account_statement_read', 'account_statement_write', 'account_statement_read_bank_account' ] )]
	#[ORM\ManyToOne( targetEntity: BankAccount::class, inversedBy: 'accountStatements' )]
	#[ORM\JoinColumn( nullable: true )]
	#[Gedmo\Versioned]
	private $bankAccount;

	/**
	 * @AppFileAnnotation(fileType=AppFile::GENERAL_FILE)
	 */
	#[Groups( [ 'account_statement_write', 'account_statement_read_app_file' ] )]
	#[ORM\OneToOne( targetEntity: 'AppFile', cascade: [ 'persist' ] )]
	#[Gedmo\Versioned]
	private $document;

	#[Groups( [ 'account_statement_read', 'account_statement_write', 'account_statement_read_average_balance' ] )]
	#[ORM\Column( type: 'decimal', precision: 14, scale: 2, nullable: true )]
	#[Gedmo\Versioned]
	private $averageBalance;

	#[Groups( [ 'account_statement_read', 'account_statement_write', 'account_statement_read_total_income' ] )]
	#[ORM\Column( type: 'decimal', precision: 14, scale: 2, nullable: true )]
	#[Gedmo\Versioned]
	private $totalIncome;

	#[Groups( [ 'account_statement_read', 'account_statement_write', 'account_statement_read_total_expense' ] )]
	#[ORM\Column( type: 'decimal', precision: 14, scale: 2, nullable: true )]
	#[Gedmo\Versioned]
	private $totalExpense;

	#[Groups( [ 'account_statement_write', 'account_statement_read_bank_transactions' ] )]
	#[ORM\OneToMany( targetEntity: BankTransaction::class, mappedBy: 'accountStatement' )]
	private $bankTransactions;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	public function __construct() {
		$this->transactions = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getStatus(): ?string {
		return $this->status;
	}

	public function setStatus( string $status ): self {
		$this->status = $status;

		return $this;
	}

	public function getPeriodStartDate(): ?\DateTimeInterface {
		return $this->periodStartDate;
	}

	public function setPeriodStartDate( ?\DateTimeInterface $periodStartDate ): self {
		$this->periodStartDate = $periodStartDate;

		return $this;
	}

	public function getPeriodEndDate(): ?\DateTimeInterface {
		return $this->periodEndDate;
	}

	public function setPeriodEndDate( ?\DateTimeInterface $periodEndDate ): self {
		$this->periodEndDate = $periodEndDate;

		return $this;
	}

	public function getBankAccount(): ?BankAccount {
		return $this->bankAccount;
	}

	public function setBankAccount( ?BankAccount $bankAccount ): self {
		$this->bankAccount = $bankAccount;

		return $this;
	}

	public function getDocument(): ?AppFile {
		return $this->document;
	}

	public function setDocument( ?AppFile $document ): self {
		$this->document = $document;

		return $this;
	}

	public function getAverageBalance(): ?string {
		return $this->averageBalance;
	}

	public function setAverageBalance( string $averageBalance ): self {
		$this->averageBalance = $averageBalance;

		return $this;
	}

	public function getTotalIncome(): ?string {
		return $this->totalIncome;
	}

	public function setTotalIncome( string $totalIncome ): self {
		$this->totalIncome = $totalIncome;

		return $this;
	}

	public function getTotalExpense(): ?string {
		return $this->totalExpense;
	}

	public function setTotalExpense( string $totalExpense ): self {
		$this->totalExpense = $totalExpense;

		return $this;
	}

	/**
	 * @return Collection|BankTransaction[]
	 */
	public function getBankTransactions(): Collection {
		return $this->bankTransactions;
	}

	public function addBankTransaction( BankTransaction $bankTransaction ): self {
		if ( ! $this->bankTransactions->contains( $bankTransaction ) ) {
			$this->bankTransactions[] = $bankTransaction;
			$bankTransaction->setAccountStatement( $this );
		}

		return $this;
	}

	public function removeBankTransaction( BankTransaction $bankTransaction ): self {
		if ( $this->bankTransactions->contains( $bankTransaction ) ) {
			$this->bankTransactions->removeElement( $bankTransaction );
			// set the owning side to null (unless already changed)
			if ( $bankTransaction->getAccountStatement() === $this ) {
				$bankTransaction->setAccountStatement( null );
			}
		}

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	#[Groups( [ 'account_statement_read_total_transactions' ] )]
	public function getTotalTransactions(): int {
		return count( $this->getBankTransactions() );
	}
}
