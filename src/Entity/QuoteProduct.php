<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\QuoteProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity( repositoryClass: QuoteProductRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('QUOTE_PRODUCT_LIST')" ],
		'post' => [ 'method' => 'POST', 'access_control' => "is_granted('QUOTE_PRODUCT_CREATE')" ]
	],
	itemOperations: [
		'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('QUOTE_PRODUCT_UPDATE')" ],
		'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('QUOTE_PRODUCT_DELETE')" ],
		'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('QUOTE_PRODUCT_SHOW')" ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [ 'groups' => [ 'quote_product_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'quote_product_write' ] ]
	] )]
class QuoteProduct {
	use TrackableTrait;

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'quote_product_read', 'quote_product_read_id' ] )]
	private $id;

	#[ORM\Column( type: 'string', length: 255 )]
	#[Groups( [ 'quote_product_write', 'quote_product_read', 'quote_product_read_name' ] )]
	#[Gedmo\Versioned]
	private $name;

	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Groups( [ 'quote_product_write', 'quote_product_read', 'quote_product_read_price' ] )]
	#[Gedmo\Versioned]
	private $price;

	#[ORM\ManyToOne( targetEntity: Quote::class, inversedBy: 'quoteProducts' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Groups( [ 'quote_product_read_quote' ] )]
	#[Gedmo\Versioned]
	private $quote;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'quote_product_write', 'quote_product_read', 'quote_product_read_quantity' ] )]
	#[Gedmo\Versioned]
	private $quantity;

	#[ORM\Column( type: 'boolean' )]
	#[Groups( [ 'quote_product_write', 'quote_product_read', 'quote_product_read_is_optional' ] )]
	#[Gedmo\Versioned]
	private $isOptional;

	#[ORM\Column( type: 'text', nullable: true )]
	#[Groups( [ 'quote_product_write', 'quote_product_read', 'quote_product_read_description' ] )]
	#[Gedmo\Versioned]
	private $description;

	#[ORM\Column( type: 'boolean', nullable: true )]
	#[Groups( [ 'quote_product_write', 'quote_product_read', 'quote_product_read_was_approved' ] )]
	#[Gedmo\Versioned]
	private $wasApproved;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[Groups( [ 'quote_write' ] )]
	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[Assert\NotNull]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private Client $client;

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getPrice(): ?string {
		return $this->price;
	}

	public function setPrice( string $price ): self {
		$this->price = $price;

		return $this;
	}

	public function getQuantity(): ?int {
		return $this->quantity;
	}

	public function setQuantity( int $quantity ): self {
		$this->quantity = $quantity;

		return $this;
	}

	public function getQuote(): ?Quote {
		return $this->quote;
	}

	public function setQuote( ?Quote $quote ): self {
		$this->quote = $quote;

		return $this;
	}

	public function getIsOptional(): ?bool {
		return $this->isOptional;
	}

	public function setIsOptional( bool $isOptional ): self {
		$this->isOptional = $isOptional;

		return $this;
	}

	public function getDescription(): ?string {
		return $this->description;
	}

	public function setDescription( string $description ): self {
		$this->description = $description;

		return $this;
	}

	public function getWasApproved(): ?bool {
		return $this->wasApproved;
	}

	public function setWasApproved( ?bool $wasApproved ): self {
		$this->wasApproved = $wasApproved;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
