<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\ConfigurationRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ApiResource]
#[Gedmo\Loggable]
#[ORM\Entity( repositoryClass: ConfigurationRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
class Configuration {
	use TrackableTrait;

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[ORM\Column( type: 'text' )]
	#[Gedmo\Versioned]
	private $value;

	#[ORM\Column( type: 'string', length: 255, nullable: true )]
	#[Gedmo\Versioned]
	private $description;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	public function getId(): ?int {
		return $this->id;
	}

	public function getValue(): ?string {
		return $this->value;
	}

	public function setValue( string $value ): self {
		$this->value = $value;

		return $this;
	}

	public function getDescription(): ?string {
		return $this->description;
	}

	public function setDescription( ?string $description ): self {
		$this->description = $description;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}
}
