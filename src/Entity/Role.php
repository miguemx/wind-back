<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Role
 */
#[ORM\Table( name: 'role' )]
#[ORM\Entity]
#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [
		'get' => [
			'method'         => 'GET',
			'access_control' => "is_granted('ROLES_LIST')"
		]
	],
	itemOperations: [ 'get' => [ 'method' => 'GET', 'access_control' => "is_granted('ROLES_SHOW')" ] ],
	attributes: [
		'normalization_context'   => [ 'groups' => [ 'role_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'role_write' ] ]
	] )]
#[UniqueEntity( fields: [ 'name' ] )]
class Role {
	use TrackableTrait;

	#[ORM\Column( name: 'id', type: 'integer', options: [ 'unsigned' => true ] )]
	#[Groups( [ 'role_read' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue( strategy: 'AUTO' )]
	#[Gedmo\Versioned]
	private int $id;

	#[Groups( [ 'role_read' ] )]
	#[ORM\Column( type: 'string' )]
	#[Assert\NotBlank]
	#[Gedmo\Versioned]
	private string $name;

	#[Groups( [ 'role_read' ] )]
	#[ORM\Column( type: 'string' )]
	#[Assert\NotBlank]
	#[Gedmo\Versioned]
	private string $title;

	/**
	 * @var Permission[]
	 */
	#[ORM\ManyToMany( targetEntity: Permission::class, mappedBy: 'roles' )]
	private $permissions;

	/**
	 * @var User[]
	 */
	#[ORM\OneToMany( mappedBy: 'role', targetEntity: User::class )]
	private $users;

	#[Groups( [ 'role_read' ] )]
	#[ORM\Column( type: 'boolean' )]
	#[Gedmo\Versioned]
	private bool $assignable = true;

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->permissions = new \Doctrine\Common\Collections\ArrayCollection();
		$this->users       = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Role
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Add permission
	 *
	 *
	 * @return Role
	 */
	public function addPermission( Permission $permission ) {
		if ( ! $permission->getRoles()->contains( $this ) ) {
			$this->permissions[] = $permission;
			$permission->addRole( $this );
		}

		return $this;
	}

	/**
	 * Remove permission
	 */
	public function removePermission( Permission $permission ) {
		$permission->removeRole( $this );
		$this->permissions->removeElement( $permission );
	}

	/**
	 * Get permissions
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPermissions() {
		return $this->permissions;
	}

	/**
	 * Add user
	 *
	 *
	 * @return Role
	 */
	public function addUser( User $user ) {
		$this->users[] = $user;

		return $this;
	}

	/**
	 * Remove user
	 */
	public function removeUser( User $user ) {
		$this->users->removeElement( $user );
	}

	/**
	 * Get users
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getUsers() {
		return $this->users;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 *
	 * @return Role
	 */
	public function setTitle( $title ) {
		$this->title = $title;

		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return bool
	 */
	public function isAssignable() {
		return $this->assignable;
	}

	/**
	 * @param bool $assignable
	 */
	public function setAssignable( $assignable ) {
		$this->assignable = $assignable;
	}

	public function getAssignable(): ?bool {
		return $this->assignable;
	}
}
