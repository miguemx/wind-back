<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity( repositoryClass: \App\Repository\NotificationTopicRepository::class )]
#[ApiResource(
	collectionOperations: [
		'get'                                  => [ 'method' => 'GET' ],
		'notification_topics_user_can_receive' => [
			'method'     => 'GET',
			'route_name' => 'notification_topics_user_can_receive'
		]
	],
	itemOperations: [ 'get' => [ 'method' => 'GET' ] ], attributes: [
	'normalization_context'   => [ 'groups' => [ 'notification_topic_read' ] ],
	'denormalization_context' => [ 'groups' => [ 'notification_topic_write' ] ]
] )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
class NotificationTopic {
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[Groups( [ 'notification_read', 'notification_topic_read', 'notification_topic_read_id' ] )]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[Groups( [ 'notification_read', 'notification_topic_read', 'notification_topic_read_name' ] )]
	#[ORM\Column( type: 'string', unique: true )]
	private string $name;

	#[Groups( [ 'notification_read', 'notification_topic_read', 'notification_topic_read_description' ] )]
	#[ORM\Column( type: 'string', length: 1020 )]
	private string $description;

	/**
	 * @var Permission[]
	 */
	#[Groups( [ 'notification_topic_read_permissions' ] )]
	#[ORM\ManyToMany( targetEntity: Permission::class, cascade: [ 'persist' ] )]
	private $permissions;

	public function __construct() {
		$this->permissions = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection|Permission[]
	 */
	public function getPermissions(): Collection {
		return $this->permissions;
	}

	public function addPermission( Permission $permission ): self {
		if ( ! $this->permissions->contains( $permission ) ) {
			$this->permissions[] = $permission;
		}

		return $this;
	}

	public function removePermission( Permission $permission ): self {
		if ( $this->permissions->contains( $permission ) ) {
			$this->permissions->removeElement( $permission );
		}

		return $this;
	}

	public function getDescription(): ?string {
		return $this->description;
	}

	public function setDescription( string $description ): self {
		$this->description = $description;

		return $this;
	}
}
