<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\ClientAwareAnnotation;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\TaxDocumentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: TaxDocumentRepository::class)]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id')]
#[UniqueEntity(
    fields: ['uuidFolioFiscal'],
    errorPath: 'Error',
    message: 'El uuid del folio fiscal ya existe',
)]
#[Gedmo\Loggable]
#[ApiResource(
    collectionOperations: [
        'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('TAX_DOCUMENT_LIST')" ],
        'post' => [ 'method' => 'POST', 'access_control' => "is_granted('TAX_DOCUMENT_CREATE')" ]
    ],
    itemOperations: [
        'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('TAX_DOCUMENT_UPDATE')" ],
        'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('TAX_DOCUMENT_DELETE')" ],
        'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('TAX_DOCUMENT_SHOW')" ]
    ],
    attributes: [
        'input_formats'           => [ 'json' => [ 'application/json' ] ],
        'output_formats'          => [
            'json' => [ 'application/json' ],
            'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
        ],
        'normalization_context'   => [
            'groups' => [ 'tax_document_read' ]
        ],
        'denormalization_context' => [
            'groups' => [
                'tax_document_write',
                'tax_document_item_write'
            ]
        ]
    ],
    order: ['issuedAt' => 'DESC']
)]
#[ApiFilter(GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ])]
class TaxDocument
{
    use TrackableTrait;

    public const STATUS_RECEIVED = "RECEIVED";
	public const STATUS_CANCELLED = "CANCELLED";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_id'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_document_type'])]
    #[Gedmo\Versioned]
    private $documentType;

    #[ORM\Column(type: 'string', length: 255, nullable: true, unique: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_uuid'])]
    #[Gedmo\Versioned]
    private $uuidFolioFiscal;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_folio'])]
    #[Gedmo\Versioned]
    private $folio;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_status'])]
    #[Gedmo\Versioned]
    private $status = self::STATUS_RECEIVED;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'taxDocument')]
    #[ORM\JoinColumn( nullable: true )]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_client'])]
    #[Gedmo\Versioned]
    private $client = null;

    #[ORM\ManyToOne(targetEntity: Tenant::class)]
    #[ORM\JoinColumn( nullable: false )]
    #[Gedmo\Versioned]
    private $tenant;

    #[ORM\OneToMany(mappedBy: 'taxDocument', targetEntity: TaxDocumentItem::class, cascade: [ 'persist', 'remove' ], orphanRemoval: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_tax_document_items'])]
    private $taxDocumentItems;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_payment_type'])]
    #[Gedmo\Versioned]
    private $paymentType;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_payment_method'])]
    #[Gedmo\Versioned]
    private $paymentMethod;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_is_issuer'])]
    #[Gedmo\Versioned]
    private $isIssuer;

    #[ORM\Column(type: 'string', length: 5)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_currency'])]
    #[Gedmo\Versioned]
    private $currency;

    #[ORM\Column(type: 'decimal', precision: 14, scale: 4, nullable: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_discount'])]
    #[Gedmo\Versioned]
    private $discount;

    #[ORM\Column(type: 'decimal', precision: 14, scale: 4)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_tax'])]
    #[Gedmo\Versioned]
    private $tax;

    #[ORM\Column(type: 'decimal', precision: 14, scale: 4)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_retained_taxes'])]
    #[Gedmo\Versioned]
    private $retainedTaxes;

    #[ORM\Column(type: 'decimal', precision: 14, scale: 4)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_transferred_taxes'])]
    #[Gedmo\Versioned]
    private $transferredTaxes;

    #[ORM\Column(type: 'decimal', precision: 14, scale: 4)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_subtotal'])]
    #[Gedmo\Versioned]
    private $subtotal;

    #[ORM\Column(type: 'decimal', precision: 14, scale: 4)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_total'])]
    #[Gedmo\Versioned]
    private $total;

    #[ORM\Column(type: 'decimal', precision: 14, scale: 4)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_paid_amount'])]
    #[Gedmo\Versioned]
    private $paidAmount;

    #[ORM\Column(type: 'decimal', precision: 14, scale: 4)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_due_amount'])]
    #[Gedmo\Versioned]
    private $dueAmount;

    #[ORM\Column(type: 'datetime', nullable: true)] 
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_due_date'])]
    #[Gedmo\Versioned]
    private $lastPaymentDate;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_fully_paid_at'])]
    #[Gedmo\Versioned]
    private $fullyPaidAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_canceled_at'])]
    #[Gedmo\Versioned]
    private $canceledAt;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_issued_at'])]
    #[Gedmo\Versioned]
    private $issuedAt;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_rfc'])]
    #[Gedmo\Versioned]
    private $rfc;

    #[ORM\OneToOne(targetEntity: AppFile::class, cascade: ['persist', 'remove'])]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_xml_file'])]
    #[Gedmo\Versioned]
    private $xmlFile;

    #[ORM\OneToMany(mappedBy: 'taxDocument', targetEntity: Quote::class)]
    #[Groups(['tax_document_read','tax_document_write','tax_document_quotes'])]
    private $quotes;

    #[ORM\OneToOne(targetEntity: AppFile::class, cascade: ['persist', 'remove'])]
    #[Groups(['tax_document_read','tax_document_write','tax_document_read_pdf_file'])]
    #[Gedmo\Versioned]
    private $pdfFile;

    public function __construct()
    {
        $this->taxDocumentItems = new ArrayCollection();
        $this->quotes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocumentType(): ?string
    {
        return $this->documentType;
    }

    public function setDocumentType(?string $documentType): self
    {
        $this->documentType = $documentType;

        return $this;
    }

    public function getUuidFolioFiscal(): ?string
    {
        return $this->uuidFolioFiscal;
    }

    public function setUuidFolioFiscal(?string $uuidFolioFiscal): self
    {
        $this->uuidFolioFiscal = $uuidFolioFiscal;

        return $this;
    }

    public function getFolio(): ?string
    {
        return $this->folio;
    }

    public function setFolio(?string $folio): self
    {
        $this->folio = $folio;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    public function setUpdatedDate(?\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    /**
     * @return Collection<int, TaxDocumentItem>
     */
    public function getTaxDocumentItems(): Collection
    {
        return $this->taxDocumentItems;
    }

    public function addTaxDocumentItem(TaxDocumentItem $taxDocumentItem): self
    {
        if (!$this->taxDocumentItems->contains($taxDocumentItem)) {
            $this->taxDocumentItems[] = $taxDocumentItem;
            $taxDocumentItem->setTaxDocument($this);
        }

        return $this;
    }

    public function removeTaxDocumentItem(TaxDocumentItem $taxDocumentItem): self
    {
        if ($this->taxDocumentItems->removeElement($taxDocumentItem)) {
            // set the owning side to null (unless already changed)
            if ($taxDocumentItem->getTaxDocument() === $this) {
                $taxDocumentItem->setTaxDocument(null);
            }
        }

        return $this;
    }

    public function getPaymentType(): ?string
    {
        return $this->paymentType;
    }

    public function setPaymentType(?string $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function isIsIssuer(): ?bool
    {
        return $this->isIssuer;
    }

    public function setIsIssuer(bool $isIssuer): self
    {
        $this->isIssuer = $isIssuer;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getDiscount(): ?string
    {
        return $this->discount;
    }

    public function setDiscount(?string $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getTax(): ?string
    {
        return $this->tax;
    }

    public function setTax(string $tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    public function getRetainedTaxes(): ?string
    {
        return $this->retainedTaxes;
    }

    public function setRetainedTaxes(string $retainedTaxes): self
    {
        $this->retainedTaxes = $retainedTaxes;

        return $this;
    }

    public function getTransferredTaxes(): ?string
    {
        return $this->transferredTaxes;
    }

    public function setTransferredTaxes(string $transferredTaxes): self
    {
        $this->transferredTaxes = $transferredTaxes;

        return $this;
    }

    public function getSubtotal(): ?string
    {
        return $this->subtotal;
    }

    public function setSubtotal(string $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getPaidAmount(): ?string
    {
        return $this->paidAmount;
    }

    public function setPaidAmount(string $paidAmount): self
    {
        $this->paidAmount = $paidAmount;

        return $this;
    }

    public function getDueAmount(): ?string
    {
        return $this->dueAmount;
    }

    public function setDueAmount(string $dueAmount): self
    {
        $this->dueAmount = $dueAmount;

        return $this;
    }

    public function getLastPaymentDate(): ?\DateTimeInterface
    {
        return $this->lastPaymentDate;
    }

    public function setLastPaymentDate(?\DateTimeInterface $lastPaymentDate): self
    {
        $this->lastPaymentDate = $lastPaymentDate;

        return $this;
    }

    public function getFullyPaidAt(): ?\DateTimeInterface
    {
        return $this->fullyPaidAt;
    }

    public function setFullyPaidAt(?\DateTimeInterface $fullyPaidAt): self
    {
        $this->fullyPaidAt = $fullyPaidAt;

        return $this;
    }

    public function getCanceledAt(): ?\DateTimeInterface
    {
        return $this->canceledAt;
    }

    public function setCanceledAt(?\DateTimeInterface $canceledAt): self
    {
        $this->canceledAt = $canceledAt;

        return $this;
    }

    public function getIssuedAt(): ?\DateTimeInterface
    {
        return $this->issuedAt;
    }

    public function setIssuedAt(\DateTimeInterface $issuedAt): self
    {
        $this->issuedAt = $issuedAt;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(?string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getXmlFile(): ?AppFile
    {
        return $this->xmlFile;
    }

    public function setXmlFile(?AppFile $xmlFile): self
    {
        $this->xmlFile = $xmlFile;

        return $this;
    }

    /**
     * @return Collection<int, Quote>
     */
    public function getQuotes(): Collection
    {
        return $this->quotes;
    }

    public function addQuote(Quote $quote): self
    {
        if (!$this->quotes->contains($quote)) {
            $this->quotes[] = $quote;
            $quote->setTaxDocument($this);
        }

        return $this;
    }

    public function removeQuote(Quote $quote): self
    {
        if ($this->quotes->removeElement($quote)) {
            // set the owning side to null (unless already changed)
            if ($quote->getTaxDocument() === $this) {
                $quote->setTaxDocument(null);
            }
        }

        return $this;
    }

    public function getPdfFile(): ?AppFile
    {
        return $this->pdfFile;
    }

    public function setPdfFile(?AppFile $pdfFile): self
    {
        $this->pdfFile = $pdfFile;

        return $this;
    }
}
