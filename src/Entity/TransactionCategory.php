<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Repository\TransactionCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity( repositoryClass: TransactionCategoryRepository::class )]
#[ApiResource(
	collectionOperations: [
		'get'  => [
			'method'         => 'GET',
			'access_control' => "is_granted('TRANSACTION_CATEGORY_LIST')"
		],
		'post' => [
			'method'         => 'POST',
			'access_control' => "is_granted('TRANSACTION_CATEGORY_CREATE')"
		]
	],
	itemOperations: [
		'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('TRANSACTION_CATEGORY_UPDATE')" ],
		'delete' => [
			'method'         => 'DELETE',
			'access_control' => "is_granted('TRANSACTION_CATEGORY_DELETE')"
		],
		'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('TRANSACTION_CATEGORY_SHOW')" ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [ 'groups' => [ 'transaction_category_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'transaction_category_write' ] ]
	] )]
#[Gedmo\Loggable]
#[ApiFilter( SearchFilter::class, properties: [ null ] )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
class TransactionCategory {
	#[Groups( [ 'transaction_category_read', 'transaction_category_write', 'transaction_category_read_id' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'transaction_category_read', 'transaction_category_write', 'transaction_category_read_name' ] )]
	#[ORM\Column( type: 'string', length: 255 )]
	#[Gedmo\Versioned]
	private $name;

	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[ORM\JoinColumn( nullable: false )]
	private $client;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	private $tenant;

	#[Groups( [ 'transaction_category_read', 'transaction_category_write', 'transaction_category_read_transactions' ] )]
	#[ORM\OneToMany( targetEntity: Transaction::class, mappedBy: 'transactionCategory' )]
	private $transactions;

	public function __construct() {
		$this->transactions = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	/**
	 * @return Collection<int, Transaction>
	 */
	public function getTransactions(): Collection {
		return $this->transactions;
	}

	public function addTransaction( Transaction $transaction ): self {
		if ( ! $this->transactions->contains( $transaction ) ) {
			$this->transactions[] = $transaction;
			$transaction->setTransactionCategory( $this );
		}

		return $this;
	}

	public function removeTransaction( Transaction $transaction ): self {
		if ( $this->transactions->removeElement( $transaction ) ) {
			// set the owning side to null (unless already changed)
			if ( $transaction->getTransactionCategory() === $this ) {
				$transaction->setTransactionCategory( null );
			}
		}

		return $this;
	}
}
