<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\BankRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity( repositoryClass: BankRepository::class )]
#[Gedmo\Loggable]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ApiResource(
	collectionOperations: [
	'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('BANK_LIST')" ],
	'post' => [ 'method' => 'POST', 'access_control' => "is_granted('BANK_CREATE')" ]
	],
	itemOperations: [
	'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('BANK_UPDATE')" ],
	'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('BANK_DELETE')" ],
	'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('BANK_SHOW')" ]
	],
	attributes: [
	'input_formats'           => [ 'json' => [ 'application/json' ] ],
	'output_formats'          => [
		'json' => [ 'application/json' ],
		'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
	],
	'normalization_context'   => [ 'groups' => [ 'bank_read' ] ],
	'denormalization_context' => [ 'groups' => [ 'bank_write' ] ]
] )]
class Bank {
	use TrackableTrait;

	#[Groups( [ 'bank_read', 'bank_write', 'bank_read_id' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'bank_read', 'bank_write', 'bank_read_name' ] )]
	#[ORM\Column( type: 'string', length: 150 )]
	#[Gedmo\Versioned]
	private $name;

	#[ORM\Column( type: 'string', nullable: false )]
	private string $code;

	#[Groups( [ 'bank_read', 'bank_write', 'bank_read_phone' ] )]
	#[ORM\Column( type: 'string', length: 20, nullable: true )]
	#[Gedmo\Versioned]
	private $phone;

	#[ORM\OneToMany( targetEntity: BankAccount::class, mappedBy: 'bank', orphanRemoval: true )]
	private $bankAccounts;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: true )]
	#[Gedmo\Versioned]
	private $tenant;

	public function __construct() {
		$this->bankAccounts = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getPhone(): ?string {
		return $this->phone;
	}

	public function setPhone( ?string $phone ): self {
		$this->phone = $phone;

		return $this;
	}

	/**
	 * @return Collection|BankAccount[]
	 */
	public function getBankAccounts(): Collection {
		return $this->bankAccounts;
	}

	public function addBankAccount( BankAccount $bankAccount ): self {
		if ( ! $this->bankAccounts->contains( $bankAccount ) ) {
			$this->bankAccounts[] = $bankAccount;
			$bankAccount->setBank( $this );
		}

		return $this;
	}

	public function removeBankAccount( BankAccount $bankAccount ): self {
		if ( $this->bankAccounts->removeElement( $bankAccount ) ) {
			// set the owning side to null (unless already changed)
			if ( $bankAccount->getBank() === $this ) {
				$bankAccount->setBank( null );
			}
		}

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getCode(): ?string {
		return $this->code;
	}

	public function setCode( string $code ): self {
		$this->code = $code;

		return $this;
	}
}
