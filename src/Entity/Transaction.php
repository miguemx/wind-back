<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\TransactionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity( repositoryClass: TransactionRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[ORM\Table]
#[ORM\Index( name: 'transaction_date_idx', columns: [ 'transaction_date' ] )]
#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('TRANSACTION_LIST')" ],
		'post' => [ 'method' => 'POST', 'access_control' => "is_granted('TRANSACTION_CREATE')" ]
	],
	itemOperations: [
		'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('TRANSACTION_UPDATE')" ],
		'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('TRANSACTION_DELETE')" ],
		'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('TRANSACTION_SHOW')" ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
        'order'                   => ["transactionDate"=>"DESC"],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [ 'groups' => [ 'transaction_read' ] ],
		'denormalization_context' => [
			'groups' => [
				'transaction_write',
				'quote_transaction_write'
			]
		]
	] )]
#[ApiFilter( SearchFilter::class, properties: [
	'bankAccount.id' => 'exact',
	'bankAccount.name' => 'partial',
	'concept'          => 'partial',
	'invoice.folio'    => 'partial',
	'quoteTransactions.client.id'  => 'exact',
] )]
#[ApiFilter( DateFilter::class, properties: [ 'transactionDate' ] )]
#[ApiFilter( OrderFilter::class, properties: [ 'amount' ] )]
class Transaction {
	use TrackableTrait;

	public const TRANSACTION_TYPE_INCOME = "income";
	public const TRANSACTION_TYPE_EXPENSE = "expense";
	public const TRANSACTION_STATUS_PENDING = 'PENDING';
	public const TRANSACTION_STATUS_APPROVED = 'APPROVED';
	public const TRANSACTION_STATUS_CANCELLED = 'CANCELLED';

	#[Groups( [ 'transaction_read', 'transaction_write', 'transaction_read_id' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'transaction_read', 'transaction_write', 'transaction_read_amount' ] )]
	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Gedmo\Versioned]
	private $amount;

	#[Groups( [ 'transaction_read', 'transaction_write', 'transaction_read_concept' ] )]
	#[ORM\Column( type: 'string', length: 255, nullable: true )]
	#[Gedmo\Versioned]
	private $concept;

	#[Groups( [ 'transaction_read', 'transaction_write', 'transaction_read_folio' ] )]
	#[ORM\Column( type: 'string', length: 20, nullable: true )]
	#[Gedmo\Versioned]
	private $folio;

	#[ORM\Column( type: 'string', length: 255, nullable: false )]
	#[Groups( [ 'transaction_read', 'transaction_read_transaction_type', 'transaction_write' ] )]
	#[Assert\NotBlank]
	#[Gedmo\Versioned]
	private $transactionType;

	#[Groups( [ 'transaction_write', 'transaction_read_provider' ] )]
	#[ORM\ManyToOne( targetEntity: Provider::class, inversedBy: 'transactions2' )]
	#[ORM\JoinColumn( nullable: true )]
	#[Gedmo\Versioned]
	private $provider;

	#[Groups( [ 'transaction_read', 'transaction_write', 'transaction_read_transaction_date' ] )]
	#[ORM\Column( type: 'datetime', nullable: false )]
	#[Gedmo\Versioned]
	private $transactionDate;

	#[ORM\OneToMany( targetEntity: QuoteTransaction::class, mappedBy: 'transaction', cascade: [
		'persist',
		'remove'
	], orphanRemoval: true )]
	#[Groups( [ 'transaction_read_quote_transactions', 'transaction_write' ] )]
	private $quoteTransactions;

	#[Groups( [ 'transaction_read_bank_account', 'transaction_write' ] )]
	#[ORM\ManyToOne( targetEntity: BankAccount::class, inversedBy: 'transactions' )]
	#[ORM\JoinColumn( nullable: true )]
	#[Gedmo\Versioned]
	private $bankAccount;

	#[Groups( [ 'transaction_read_comments', 'transaction_write' ] )]
	#[ORM\Column( type: 'string', length: 4096, nullable: true )]
	#[Gedmo\Versioned]
	private $comments;

	#[Groups( [ 'transaction_read', 'transaction_write', 'transaction_read_operation' ] )]
	#[ORM\Column( type: 'decimal', precision: 10, scale: 2, nullable: true )]
	#[Gedmo\Versioned]
	private $operation;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: true )]
	#[Gedmo\Versioned]
	private $tenant;

	#[Groups( [ 'transaction_read_bank_transaction' ] )]
	#[ORM\OneToOne( targetEntity: BankTransaction::class, mappedBy: 'transaction' )]
	#[ORM\JoinColumn( nullable: true )]
	#[Gedmo\Versioned]
	private $bankTransaction;

	#[Groups( [ 'transaction_read', 'transaction_write', 'transaction_read_status' ] )]
	#[ORM\Column( type: 'string', length: 50 )]
	#[Gedmo\Versioned]
	private string $status = Transaction::TRANSACTION_STATUS_PENDING;

	#[Groups( [ 'transaction_write', 'transaction_read_invoice' ] )]
	#[ORM\ManyToOne( targetEntity: Invoice::class, inversedBy: 'transaction' )]
	private $invoice;

	#[Groups( [ 'transaction_write', 'transaction_read_recurring_payment' ] )]
	#[ORM\ManyToOne( targetEntity: RecurringPayment::class, inversedBy: 'transactions' )]
	private $recurringPayment;

	#[Groups( [ 'transaction_read', 'transaction_write', 'transaction_read_original_amount' ] )]
	#[ORM\Column( type: 'decimal', precision: 14, scale: 2, nullable: true )]
	private $originalAmount;

	#[Groups( [ 'transaction_write', 'transaction_read_transaction_category' ] )]
	#[ORM\ManyToOne( targetEntity: TransactionCategory::class, inversedBy: 'transactions' )]
	private $transactionCategory;

	public function __construct() {
		$this->quoteTransactions = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getAmount(): ?string {
		return $this->amount;
	}

	public function setAmount( string $amount ): self {
		$this->amount = $amount;

		return $this;
	}

	public function getConcept(): ?string {
		return $this->concept;
	}

	#[Groups( [ 'transaction_read_quotes_folios' ] )]
	public function getQuotesFolios(): ?string {
		$folios = [];
		foreach ( $this->quoteTransactions as $quoteTransaction ) {
			$folios[] = $quoteTransaction->getQuote()->getFolio();
		}

		return implode( ', ', $folios );
	}

	public function setConcept( string $concept ): self {
		$this->concept = $concept;

		return $this;
	}

	public function getFolio(): ?string {
		return $this->folio;
	}

	public function setFolio( string $folio ): self {
		$this->folio = $folio;

		return $this;
	}

	public function getTransactionType(): ?string {
		return $this->transactionType;
	}

	public function setTransactionType( string $transactionType ): self {
		$this->transactionType = $transactionType;

		return $this;
	}

	public function getProvider(): ?Provider {
		return $this->provider;
	}

	public function setProvider( ?Provider $provider ): self {
		$this->provider = $provider;

		return $this;
	}

	public function getComments(): ?string {
		return $this->comments;
	}

	public function setComments( string $comments ): self {
		$this->comments = $comments;

		return $this;
	}

	public function getTransactionDate(): ?\DateTimeInterface {
		return $this->transactionDate;
	}

	public function setTransactionDate( \DateTimeInterface $transactionDate ): self {
		$this->transactionDate = $transactionDate;

		return $this;
	}

	public function getOperation(): ?string {
		return $this->operation;
	}

	public function setOperation( string $operation ): self {
		$this->operation = $operation;

		return $this;
	}

	public function getBankAccount(): ?BankAccount {
		return $this->bankAccount;
	}

	public function setBankAccount( ?BankAccount $bankAccount ): self {
		$this->bankAccount = $bankAccount;

		return $this;
	}

	/**
	 * @return Collection|QuoteTransaction[]
	 */
	public function getQuoteTransactions(): Collection {
		return $this->quoteTransactions;
	}

	public function addQuoteTransaction( QuoteTransaction $quoteTransaction ): self {
		if ( ! $this->quoteTransactions->contains( $quoteTransaction ) ) {
			$this->quoteTransactions[] = $quoteTransaction;
			$quoteTransaction->setTransaction( $this );
		}

		return $this;
	}

	public function removeQuoteTransaction( QuoteTransaction $quoteTransaction ): self {
		if ( $this->quoteTransactions->contains( $quoteTransaction ) ) {
			$this->quoteTransactions->removeElement( $quoteTransaction );
			// set the owning side to null (unless already changed)
			if ( $quoteTransaction->getTransaction() === $this ) {
				$quoteTransaction->setTransaction( null );
			}
		}

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getStatus(): ?string {
		return $this->status;
	}

	public function setStatus( string $status ): self {
		$this->status = $status;

		return $this;
	}

	public function getInvoice(): ?Invoice {
		return $this->invoice;
	}

	public function setInvoice( ?Invoice $invoice ): self {
		$this->invoice = $invoice;

		return $this;
	}

	public function getBankTransaction(): ?BankTransaction {
		return $this->bankTransaction;
	}

	public function setBankTransaction( ?BankTransaction $bankTransaction ): self {
		$this->bankTransaction = $bankTransaction;

		return $this;
	}

	public function getRecurringPayment(): ?RecurringPayment {
		return $this->recurringPayment;
	}

	public function setRecurringPayment( ?RecurringPayment $recurringPayment ): self {
		$this->recurringPayment = $recurringPayment;

		return $this;
	}

	public function getOriginalAmount(): ?string {
		return $this->originalAmount;
	}

	public function setOriginalAmount( ?string $originalAmount ): self {
		$this->originalAmount = $originalAmount;

		return $this;
	}

	public function getTransactionCategory(): ?TransactionCategory {
		return $this->transactionCategory;
	}

	public function setTransactionCategory( ?TransactionCategory $transactionCategory ): self {
		$this->transactionCategory = $transactionCategory;

		return $this;
	}
}
