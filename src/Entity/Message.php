<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\AppFileAnnotation;
use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET' ],
		'post' => [ 'method' => 'POST' ]
	],
	itemOperations: [
		'get'    => [ 'method' => 'GET' ],
		'put'    => [ 'method' => 'PUT' ],
		'delete' => [ 'method' => 'DELETE' ]
	],
	attributes: [
		'normalization_context'   => [ 'groups' => [ 'message_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'message_write' ] ],
		'order'                   => [ 'createdDate' => 'DESC' ]
	] )]
#[ORM\Entity( repositoryClass: MessageRepository::class )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
class Message {
	#[ORM\Id]
	#[Groups( [ 'message_read', 'message_write', 'message_read_id' ] )]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[Assert\NotNull]
	#[Assert\NotBlank]
	#[Groups( [ 'message_read', 'message_write', 'message_read_channel' ] )]
	#[ORM\ManyToOne( targetEntity: Channel::class, inversedBy: 'messages' )]
	private $channel;

	#[Groups( [ 'message_read', 'message_read_user' ] )]
	#[ORM\ManyToOne( targetEntity: User::class, inversedBy: 'messages' )]
	private $user;

	/**
	 * @AppFileAnnotation(fileType=AppFile::GENERAL_FILE)
	 */
	#[Groups( [ 'message_read', 'message_write', 'message_read_app_file' ] )]
	#[ORM\OneToOne( targetEntity: AppFile::class, cascade: [ 'persist', 'remove' ] )]
	private $appFile;

	#[Groups( [ 'message_read', 'message_write', 'message_read_message' ] )]
	#[ORM\Column( type: 'text', nullable: true )]
	private $message;

	#[Groups( [ 'message_read', 'message_write', 'message_read_created_date' ] )]
	#[ORM\Column( type: 'datetime' )]
	#[Gedmo\Timestampable( on: 'create' )]
	private $createdDate;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	private $tenant;

	#[ORM\ManyToOne( targetEntity: Client::class, inversedBy: 'messages' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $client;

	public function __construct() {
		//$this->setAppFile(new AppFile());
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getChannel(): ?Channel {
		return $this->channel;
	}

	public function setChannel( ?Channel $channel ): self {
		$this->channel = $channel;

		return $this;
	}

	public function getUser(): ?User {
		return $this->user;
	}

	public function setUser( ?User $user ): self {
		$this->user = $user;

		return $this;
	}

	public function getAppFile(): ?AppFile {
		return $this->appFile;
	}

	public function setAppFile( ?AppFile $appFile ): self {
		$this->appFile = $appFile;

		return $this;
	}

	public function getMessage(): ?string {
		return $this->message;
	}

	public function setMessage( ?string $message ): self {
		$this->message = $message;

		return $this;
	}

	public function getCreatedDate(): ?\DateTimeInterface {
		return $this->createdDate;
	}

	public function setCreatedDate( \DateTimeInterface $createdDate ): self {
		$this->createdDate = $createdDate;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
