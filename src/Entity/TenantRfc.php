<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Repository\TenantRfcRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Filter\SearchFilter;
use App\Annotation\TenantAwareAnnotation;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validators as AppAssert;

#[ORM\Table]
#[Gedmo\Loggable]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id')]
#[ApiResource(
    collectionOperations: [
        'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('FISCAL_DATA_LIST')" ],
        'post' => [ 'method' => 'POST', 'access_control' => "is_granted('FISCAL_DATA_CREATE')" ]
    ],
    itemOperations: [
        'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('FISCAL_DATA_UPDATE')" ],
        'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('FISCAL_DATA_DELETE')" ],
        'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('FISCAL_DATA_SHOW')" ]
    ],
    attributes: [
        'input_formats'           => [ 'json' => [ 'application/json' ] ],
        'output_formats'          => [
            'json' => [ 'application/json' ],
        ],
        'normalization_context'   => [
            'groups' => [ 
                'tenant_rfc_read',
                'tenant_rfc_write' 
            ]
        ],
        'denormalization_context' => [
            'groups' => [
                'tenant_rfc_write',
                'tenant_write'
            ]
        ]
    ])]
#[ApiFilter(GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ])]
#[ApiFilter(SearchFilter::class, properties: ["id", "rfc", "tenant"])]
#[ORM\Entity(repositoryClass: TenantRfcRepository::class)]
#[UniqueConstraint(name: "uniquerfc", columns: ["rfc"])]
class TenantRfc
{
    use TrackableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['tenant_rfc_read','tenant_rfc_write','tenant_rfc_read_id'])]
    private $id;

    #[ORM\ManyToOne(targetEntity: Tenant::class, inversedBy: 'tenantRfcs')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['tenant_rfc_write','tenant_rfc_read_tenant'])]
    private $tenant;

    #[ORM\Column(type: 'string', length: 14)]
    #[Assert\NotBlank]
    #[AppAssert\ValidRfc]
    #[Groups(['tenant_rfc_read','tenant_rfc_write','tenant_rfc_read_rfc'])]
    private $rfc;

    #[ORM\Column(type: 'string', length: 64, nullable: true)]
    #[Groups(['tenant_rfc_read','tenant_rfc_write','tenant_rfc_read_sat_ws_id'])]
    private $satWsId;

    #[ORM\Column(type: 'string', length: 12, nullable: true)]
    #[Groups(['tenant_rfc_read','tenant_rfc_write','tenant_rfc_read_sat_ws_status'])]
    private $satWsStatus;

    #[ORM\Column(type: 'boolean')]
    #[Groups( [ 'fiscal_data_read_is_sat_ws_configured', 'fiscal_data_write' ] )]
    private $isSatWsConfigured = false;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_sat_ws_password'])]
    #[Gedmo\Versioned]
    private $satWsExtractionId; // sheduler ID for SAT WS, for delete it or update it

    #[ORM\Column(type: 'boolean')]
    #[Groups( [ 'fiscal_data_read_is_first_data_extracted', 'fiscal_data_write' ] )]
    private $isFirstDataExtracted = false;

    #[ORM\Column( type: 'datetime', nullable: true )]
	#[Groups( [ 'fiscal_data_write', 'fiscal_data_read_next_extraction_date', 'fiscal_data_read' ] )]
	#[Gedmo\Versioned]
	private $lastExtractionDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = mb_strtoupper($rfc);

        return $this;
    }

    public function getSatWsId(): ?string
    {
        return $this->satWsId;
    }

    public function setSatWsId(?string $satWsId): self
    {
        $this->satWsId = $satWsId;

        return $this;
    }

    public function getSatWsStatus(): ?string
    {
        return $this->satWsStatus;
    }

    public function setSatWsStatus(?string $satWsStatus): self
    {
        $this->satWsStatus = $satWsStatus;

        return $this;
    }

    public function isIsSatWsConfigured(): ?bool
    {
        return $this->isSatWsConfigured;
    }

    public function setIsSatWsConfigured(bool $isSatWsConfigured): self
    {
        $this->isSatWsConfigured = $isSatWsConfigured;

        return $this;
    }

    public function getSatWsExtractionId(): ?string
    {
        return $this->satWsExtractionId;
    }

    public function setSatWsExtractionId(?string $satWsExtractionId): self
    {
        $this->satWsExtractionId = $satWsExtractionId;

        return $this;
    }

    public function getIsFirstDataExtracted(): ?bool
    {
        return $this->isFirstDataExtracted;
    }

    public function setIsFirstDataExtracted(bool $isFirstDataExtracted): self
    {
        $this->isFirstDataExtracted = $isFirstDataExtracted;

        return $this;
    }

    public function getLastExtractionDate(): ?\DateTimeInterface
    {
        return $this->lastExtractionDate;
    }

    public function setLastExtractionDate(?\DateTimeInterface $lastExtractionDate): self
    {
        $this->lastExtractionDate = $lastExtractionDate;

        return $this;
    }

}
