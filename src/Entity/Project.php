<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\ClientAwareAnnotation;
use App\Annotation\TenantAwareAnnotation;
use App\Interfaces\AclEnabledInterface;
use App\Interfaces\FolioInterface;
use App\Repository\ProjectRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use App\Filter\SearchAnnotation as Searchable;

#[ORM\Entity( repositoryClass: ProjectRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ClientAwareAnnotation( clientFieldName: 'client_id' )]
#[ORM\Table]
#[ORM\Index( name: 'project_status_idx', columns: [ 'status' ] )]
#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET' ],
		'post' => [ 'method' => 'POST', 'access_control' => "is_granted('PROJECT_CREATE')" ]
	],
	itemOperations: [
		'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('PROJECT_UPDATE', object)" ],
		'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('PROJECT_DELETE', object)" ],
		'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('PROJECT_SHOW', object)" ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [
			'groups' => [ 'project_read', 'folio_read' ],
			'enable_max_depth' => 'true',
			],
		'denormalization_context' => [ 'groups' => [ 'project_write', 'folio_read' ] ],
		'order'                   => [ 'updatedDate' => 'DESC' ],
		'filters'                 => [ "search" ]
	] )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[ApiFilter( SearchFilter::class, properties: [
	'client'      => 'exact',
	'client.name' => 'partial',
	'status'      => 'exact',
    'folio'      => 'partial',
	'name'        => 'partial'
] )]
#[ApiFilter( OrderFilter::class, properties: [ 'name', 'createdDate', 'updatedDate' ] )]
#[Searchable( [ "name", "client.name" ] )]
class Project implements FolioInterface, AclEnabledInterface {
	use TrackableTrait, FoliableTrait, Commentable;

	public const ACL_PERMISSION_SHOW_ALL_QUOTES = 1 << 9 | TideAcl::ACL_PERMISSION_SHOW;
	public const ACL_PERMISSION_SHOW_ALL_QUOTATION_REQUESTS = 1 << 10 | TideAcl::ACL_PERMISSION_SHOW;
	public const ACL_PERMISSION_SHOW_ALL_MEMORANDUM = 1 << 11 | TideAcl::ACL_PERMISSION_SHOW;
	//New project pending to ba accepted or rejected
	public const PROJECT_STATUS_PENDING = 'PENDING';
	//Canceled project may be rejected by the client, or he never answered
	public const PROJECT_STATUS_CANCELED = 'CANCELED';
	//A project with NO pending development but may have other pending stuff, like pending payments, or it's maybe a recursive payment
	// like the rent of a server or system operations
	public const PROJECT_STATUS_OPEN = 'OPEN';
	// Is being developed right now
	public const PROJECT_STATUS_IN_DEVELOPMENT = 'IN_DEVELOPMENT';
	// Project finished, nothing pending within
	public const PROJECT_STATUS_CLOSED = 'CLOSED';

	#[Groups( [ 'project_read', 'project_write', 'project_read_id' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'project_read', 'project_write', 'project_read_name' ] )]
	#[ORM\Column( type: 'string', length: 255 )]
	#[Gedmo\Versioned]
	private $name;

	#[Groups( [ 'project_read', 'project_write', 'project_read_description' ] )]
	#[ORM\Column( type: 'string', length: 4096, nullable: true )]
	#[Gedmo\Versioned]
	private $description;

	#[Groups( [ 'project_read', 'project_write', 'project_read_status' ] )]
	#[ORM\Column( type: 'string', length: 50 )]
	#[Gedmo\Versioned]
	private string $status = Project::PROJECT_STATUS_PENDING;

	#[Groups( [ 'project_write', 'project_read_client' ] )]
	#[ORM\ManyToOne( targetEntity: Client::class, inversedBy: 'projects' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Assert\NotNull( message: 'Debes seleccionar un cliente' )]
	#[Gedmo\Versioned]
	private $client;

	#[Groups( [ 'project_write', 'project_read_quotes' ] )]
	#[ORM\OneToMany( targetEntity: Quote::class, mappedBy: 'project' )]
	#[MaxDepth(3)]
	private $quotes;

	/**
	 * @var DateTime $startDate
	 */
	#[Groups( [ 'project_write', 'project_read', 'project_read_start_date' ] )]
	#[ORM\Column( type: 'date', nullable: true )]
	protected $startDate;

	/**
	 * @var DateTime $endDate
	 */
	#[Groups( [ 'project_write', 'project_read', 'project_read_end_date' ] )]
	#[ORM\Column( type: 'date', nullable: true )]
	protected $endDate;

	#[Groups( [ 'project_read_project_stats' ] )]
	#[ORM\OneToOne( targetEntity: ProjectStats::class, mappedBy: 'project', cascade: [ 'persist', 'remove' ] )]
	#[Gedmo\Versioned]
	private $projectStats;

	#[Groups( [ 'project_read_quotation_requests' ] )]
	#[ORM\OneToMany( targetEntity: QuotationRequest::class, mappedBy: 'project' )]
	private $quotationRequests;

	#[ORM\OneToOne( targetEntity: 'FileContainer', cascade: [ 'persist', 'remove' ] )]
	#[ORM\JoinColumn( nullable: false )]
	#[Groups( [ 'project_read_file_container' ] )]
	private $fileContainer;

	/**
	 * @var \DateTime $updated
	 */
	#[Groups( [ 'project_read', 'project_read_updated_date' ] )]
	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Gedmo\Timestampable( on: 'update' )]
	protected $updatedDate;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[Groups( [ 'project_read_memorandums' ] )]
	#[ORM\OneToMany( targetEntity: Memorandum::class, mappedBy: 'project' )]
	private $memorandums;

	public function __construct() {
		$this->memorandums       = new ArrayCollection();
		$this->quotes            = new ArrayCollection();
		$this->quotationRequests = new ArrayCollection();
		$this->setProjectStats( new ProjectStats() );
		$this->setFileContainer( new FileContainer() );

		//$this->conversation = new Conversation();
		//$this->conversation->setClient( $this->getClient() );
		//$this->conversation->setParentClass(Project::class);
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getStatus(): ?string {
		return $this->status;
	}

	public function setStatus( string $status ): self {
		$this->status = $status;

		return $this;
	}

	/**
	 * @return Collection|Quote[]
	 */
	public function getQuotes(): Collection {
		return $this->quotes;
	}

	public function addQuote( Quote $quote ): self {
		if ( ! $this->quotes->contains( $quote ) ) {
			$this->quotes[] = $quote;
			$quote->setProject( $this );
		}

		return $this;
	}

	public function removeQuote( Quote $quote ): self {
		if ( $this->quotes->removeElement( $quote ) ) {
			// set the owning side to null (unless already changed)
			if ( $quote->getProject() === $this ) {
				$quote->setProject( null );
			}
		}

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription( $description ): Project {
		$this->description = $description;

		return $this;
	}

	public function getStartDate(): ?DateTime {
		return $this->startDate;
	}

	public function setStartDate( ?DateTime $startDate ): void {
		$this->startDate = $startDate;
	}

	public function getEndDate(): ?DateTime {
		return $this->endDate;
	}

	public function setEndDate( ?DateTime $endDate ): void {
		$this->endDate = $endDate;
	}

	/**
	 * @return Collection|QuotationRequest[]
	 */
	public function getQuotationRequests(): Collection {
		return $this->quotationRequests;
	}

	public function addQuotationRequest( QuotationRequest $quotationRequest ): self {
		if ( ! $this->quotationRequests->contains( $quotationRequest ) ) {
			$this->quotationRequests[] = $quotationRequest;
			$quotationRequest->setProject( $this );
		}

		return $this;
	}

	public function removeQuotationRequest( QuotationRequest $quotationRequest ): self {
		if ( $this->quotationRequests->removeElement( $quotationRequest ) ) {
			// set the owning side to null (unless already changed)
			if ( $quotationRequest->getProject() === $this ) {
				$quotationRequest->setProject( null );
			}
		}

		return $this;
	}

	public function getProjectStats(): ?ProjectStats {
		return $this->projectStats;
	}

	public function setProjectStats( ?ProjectStats $projectStats ): self {
		// unset the owning side of the relation if necessary
		if ( $projectStats === null && $this->projectStats !== null ) {
			$this->projectStats->setProject( null );
		}

		// set the owning side of the relation if necessary
		if ( $projectStats !== null && $projectStats->getProject() !== $this ) {
			$projectStats->setProject( $this );
		}

		if ( $projectStats && $this->getClient() ) {
			$projectStats->setClient( $this->getClient() );
		}
		if ( $projectStats && $this->getTenant() ) {
			$projectStats->setTenant( $this->getTenant() );
		}

		$this->projectStats = $projectStats;

		return $this;
	}

	public function getFileContainer(): ?FileContainer {
		return $this->fileContainer;
	}

	public function setFileContainer( ?FileContainer $fileContainer ): self {
		$this->fileContainer = $fileContainer;

		if ( $fileContainer && $this->getClient() ) {
			$fileContainer->setClient( $this->getClient() );
		}
		if ( $fileContainer && $this->getTenant() ) {
			$fileContainer->setTenant( $this->getTenant() );
		}

		return $this;
	}

	public function getFolioTypeIdentifier(): string {
		return 'P';
	}

	/**
	 * addDQLFolioSequential
	 *
	 *
	 */
	public function addDQLFolioSequential( \Doctrine\ORM\QueryBuilder $query ): \Doctrine\ORM\QueryBuilder {
		return $query->andWhere( 'e.client = :client' )
		             ->setParameter( 'client', $this->getClient() );
	}

	/**
	 * getBaseInitials
	 */
	public function getFolioBaseInitials(): string {
		return $this->getClient()->getInitials();
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;
		if ( $this->getProjectStats() ) {
			$this->getProjectStats()->setTenant( $tenant );
		}
		if ( $this->getFileContainer() ) {
			$this->getFileContainer()->setTenant( $tenant );
		}

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;
		if ( $this->getProjectStats() ) {
			$this->getProjectStats()->setClient( $client );
		}
		if ( $this->getFileContainer() ) {
			$this->getFileContainer()->setClient( $client );
		}

		return $this;
	}

	#[ORM\Column( type: 'string', length: 255, unique: true )]
	#[Groups( [ 'project_read', 'project_read_folio' ] )]
	#[Gedmo\Versioned]
	private $folio;

	/**
	 * @link AclEnabledInterface::getAclDomain()
	 */
	public function getAclDomain(): string {
		return self::class;
	}

	/**
	 * @link AclEnabledInterface::getObjectId()
	 */
	public function getObjectId(): string {
		return (string) $this->getId();
	}

	static function getReadAllEntitiesByParentConfig(): array {
		return [];
	}

	/**
	 * @return Collection|Memorandum[]
	 */
	public function getMemorandums(): Collection {
		return $this->memorandums;
	}

	public function addMemorandum( Memorandum $memorandum ): self {
		if ( ! $this->memorandums->contains( $memorandum ) ) {
			$this->memorandums[] = $memorandum;
			$memorandum->setProject( $this );
		}

		return $this;
	}

	public function removeMemorandum( Memorandum $memorandum ): self {
		if ( $this->memorandums->removeElement( $memorandum ) ) {
			// set the owning side to null (unless already changed)
			if ( $memorandum->getProject() === $this ) {
				$memorandum->setProject( null );
			}
		}

		return $this;
	}
}
