<?php

namespace App\Entity;

use App\Annotation\TenantAwareAnnotation;
use App\Repository\ProjectStatsRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity( repositoryClass: ProjectStatsRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[Gedmo\Loggable]
class ProjectStats {
	use TrackableTrait;

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'project_stats_read', 'project_stats_read_id' ] )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'project_stats_read_project' ] )]
	#[ORM\OneToOne( targetEntity: Project::class, inversedBy: 'projectStats' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $project;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'project_stats_read', 'project_stats_read_quotes_count' ] )]
	#[Gedmo\Versioned]
	private int $quotesCount = 0;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'project_stats_read', 'project_stats_read_approved_quotes_count' ] )]
	#[Gedmo\Versioned]
	private int $approvedQuotesCount = 0;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [ 'project_stats_read', 'project_stats_read_approved_quotes_count' ] )]
	#[Gedmo\Versioned]
	private int $pendingQuotesCount = 0;

	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Groups( [ 'project_stats_read', 'project_stats_read_all_quotes_sum' ] )]
	#[Gedmo\Versioned]
	private int $allQuotesSum = 0;

	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Groups( [ 'project_stats_read', 'project_stats_read_approved_quotes_sum' ] )]
	#[Gedmo\Versioned]
	private int $approvedQuotesSum = 0;

	#[ORM\Column( type: 'decimal', precision: 14, scale: 2 )]
	#[Groups( [ 'project_stats_read', 'project_stats_read_payments_sum' ] )]
	#[Gedmo\Versioned]
	private int $paymentsSum = 0;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[ORM\JoinColumn( nullable: false )]
	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[Gedmo\Versioned]
	private Client $client;

	public function getId(): ?int {
		return $this->id;
	}

	public function getQuotesCount(): ?int {
		return $this->quotesCount;
	}

	public function setQuotesCount( int $quotesCount ): self {
		$this->quotesCount = $quotesCount;

		return $this;
	}

	public function getApprovedQuotesCount(): ?int {
		return $this->approvedQuotesCount;
	}

	public function setApprovedQuotesCount( int $approvedQuotesCount ): self {
		$this->approvedQuotesCount = $approvedQuotesCount;

		return $this;
	}

	public function getAllQuotesSum(): ?string {
		return $this->allQuotesSum;
	}

	public function setAllQuotesSum( string $allQuotesSum ): self {
		$this->allQuotesSum = $allQuotesSum;

		return $this;
	}

	public function getApprovedQuotesSum(): ?string {
		return $this->approvedQuotesSum;
	}

	public function setApprovedQuotesSum( string $approvedQuotesSum ): self {
		$this->approvedQuotesSum = $approvedQuotesSum;

		return $this;
	}

	public function getPaymentsSum(): ?string {
		return $this->paymentsSum;
	}

	public function setPaymentsSum( string $paymentsSum ): self {
		$this->paymentsSum = $paymentsSum;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getProject() {
		return $this->project;
	}

	/**
	 * @param mixed $project
	 *
	 * @return ProjectStats
	 */
	public function setProject( $project ) {
		$this->project = $project;

		return $this;
	}

	public function getPendingQuotesCount(): ?int {
		return $this->pendingQuotesCount;
	}

	public function setPendingQuotesCount( int $pendingQuotesCount ): self {
		$this->pendingQuotesCount = $pendingQuotesCount;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
