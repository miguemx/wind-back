<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Repository\ChannelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET' ],
		'post' => [ 'method' => 'POST' ]
	],
	itemOperations: [
		'get'    => [ 'method' => 'GET' ],
		'put'    => [ 'method' => 'PUT' ],
		'delete' => [ 'method' => 'DELETE' ]
	],
	attributes: [
		'normalization_context'   => [ 'groups' => [ 'channel_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'channel_write' ] ]
	] )]
#[Gedmo\Loggable]
#[ORM\Entity( repositoryClass: ChannelRepository::class )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
class Channel {
	use TrackableTrait;

	#[ORM\Id]
	#[Groups( [ 'channel_read', 'channel_write', 'channel_read_id', 'conversation_read' ] )]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[Groups( [ 'channel_write', 'channel_read_conversation' ] )]
	#[ORM\ManyToOne( targetEntity: Conversation::class, inversedBy: 'channels' )]
	#[ORM\JoinColumn( nullable: false )]
	private $conversation;

	#[Groups( [ 'channel_read', 'channel_write', 'channel_read_name' ] )]
	#[ORM\Column( type: 'string', length: 255 )]
	private $name;

	#[Groups( [ 'channel_read', 'channel_write', 'channel_read_enable_internal_users' ] )]
	#[ORM\Column( type: 'boolean', nullable: true )]
	private $enableInternalUsers;

	#[Groups( [ 'channel_read', 'channel_write', 'channel_read_enable_external_users' ] )]
	#[ORM\Column( type: 'boolean', nullable: true )]
	private $enableExternalUsers;

	#[Groups( [ 'channel_read', 'channel_read_messages' ] )]
	#[ORM\OneToMany( targetEntity: Message::class, mappedBy: 'channel', cascade: [ 'persist', 'remove' ] )]
	private $messages;

	#[Groups( [ 'channel_read', 'channel_write', 'channel_read_channel_users' ] )]
	#[ORM\OneToMany( targetEntity: ChannelUser::class, mappedBy: 'channel', cascade: [ 'persist', 'remove' ] )]
	private $channelUsers;

	#[Groups( [ 'channel_write' ] )]
	#[ORM\ManyToOne( targetEntity: Client::class, inversedBy: 'projects' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $client;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	public function __construct() {
		$this->messages     = new ArrayCollection();
		$this->channelUsers = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getConversation(): ?Conversation {
		return $this->conversation;
	}

	public function setConversation( ?Conversation $conversation ): self {
		$this->conversation = $conversation;

		return $this;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getEnableInternalUsers(): ?bool {
		return $this->enableInternalUsers;
	}

	public function setEnableInternalUsers( ?bool $enableInternalUsers ): self {
		$this->enableInternalUsers = $enableInternalUsers;

		return $this;
	}

	public function getEnableExternalUsers(): ?bool {
		return $this->enableExternalUsers;
	}

	public function setEnableExternalUsers( ?bool $enableExternalUsers ): self {
		$this->enableExternalUsers = $enableExternalUsers;

		return $this;
	}

	/**
	 * @return Collection|Message[]
	 */
	public function getMessages(): Collection {
		return $this->messages;
	}

	public function addMessage( Message $message ): self {
		if ( ! $this->messages->contains( $message ) ) {
			$this->messages[] = $message;
			$message->setChannel( $this );
		}

		return $this;
	}

	public function removeMessage( Message $message ): self {
		if ( $this->messages->removeElement( $message ) ) {
			// set the owning side to null (unless already changed)
			if ( $message->getChannel() === $this ) {
				$message->setChannel( null );
			}
		}

		return $this;
	}

	/**
	 * @return Collection|ChannelUser[]
	 */
	public function getChannelUsers(): Collection {
		return $this->channelUsers;
	}

	public function addChannelUser( ChannelUser $channelUser ): self {
		if ( ! $this->channelUsers->contains( $channelUser ) ) {
			$this->channelUsers[] = $channelUser;
			$channelUser->setChannel( $this );
		}

		return $this;
	}

	public function removeChannelUser( ChannelUser $channelUser ): self {
		if ( $this->channelUsers->removeElement( $channelUser ) ) {
			// set the owning side to null (unless already changed)
			if ( $channelUser->getChannel() === $this ) {
				$channelUser->setChannel( null );
			}
		}

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
