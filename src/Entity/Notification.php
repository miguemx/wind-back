<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\TenantAwareAnnotation;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Notification
 *
 * )
 */
#[ORM\Table( name: 'notification' )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[Gedmo\Loggable]
#[ORM\Entity( repositoryClass: \App\Repository\NotificationRepository::class )]
#[ApiResource(
	collectionOperations: [
		'get'                      => [ 'method' => 'GET' ],
		'get_header_notifications' => [
			'method'          => 'GET',
			'route_name'      => 'get_header_notifications',
			'swagger_context' => [ 'summary' => 'Mark notification as read' ]
		]
	],
	itemOperations: [
		'mark_as_read'     => [
			'method'          => 'PUT',
			'route_name'      => 'notifications_mark_as_read',
			'swagger_context' => [ 'summary' => 'Mark notification as read' ]
		],
		'mark_all_as_read' => [
			'method'          => 'PUT',
			'route_name'      => 'notifications_mark_all_as_read',
			'swagger_context' => [
				'summary'    => 'Mark all notifications as read',
				'parameters' => []
			]
		]
	],
	attributes: [
		'normalization_context'   => [ 'groups' => [ 'notification_read', 'update_date' ] ],
		'denormalization_context' => [ 'groups' => [ 'notification_write' ] ],
		'order'                   => [ 'updatedDate' => 'DESC' ]
	] )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[ApiFilter( SearchFilter::class, properties: [
	'relatedUser.id'         => 'exact',
	'notificationTopic.code' => 'partial',
	'relatedUser.role.name'  => 'partial',
	'relatedUser.username'   => 'partial'
] )]
#[ApiFilter( OrderFilter::class, properties: [ 'createdDate', 'readDate', 'updatedDate' ] )]
#[ApiFilter( ExistsFilter::class, properties: [ 'readDate' ] )]
class Notification {
	use TrackableTrait;

	public const DELIVER_BY_PUSH = 'push';
	public const DELIVER_BY_MAIL = 'mail';

	#[Groups( [ 'notification_read' ] )]
	#[ORM\Column( name: 'id', type: 'integer' )]
	#[ORM\Id]
	#[ORM\GeneratedValue( strategy: 'AUTO' )]
	#[Gedmo\Versioned]
	private int $id;

	#[Groups( [ 'notification_read' ] )]
	#[ORM\Column( name: 'html', type: 'text', nullable: true )]
	#[Gedmo\Versioned]
	private string $html;

	#[Groups( [ 'notification_read' ] )]
	#[ORM\Column( name: 'link', type: 'string', length: 1024, nullable: true )]
	#[Gedmo\Versioned]
	private string $link;

	#[ORM\ManyToOne( targetEntity: User::class, cascade: [ 'persist' ], inversedBy: 'notifications' )]
	#[Gedmo\Versioned]
	private User $user;

	#[Groups( [ 'notification_read' ] )]
	#[ORM\Column( name: 'read_date', type: 'datetime', nullable: true )]
	#[Gedmo\Versioned]
	private \DateTime $readDate;

	/**
	 * @var \DateTime $created
	 */
	#[Groups( [ 'notification_read' ] )]
	#[ORM\Column( type: 'datetime', nullable: true )]
	#[Gedmo\Timestampable( on: 'create' )]
	#[Gedmo\Versioned]
	protected $createdDate;

	#[Groups( [ 'notification_read' ] )]
	#[ORM\Column( type: 'string', nullable: true )]
	#[Gedmo\Versioned]
	private string $groupName;

	#[Groups( [ 'notification_read' ] )]
	#[ORM\Column( type: 'integer', nullable: true )]
	#[Gedmo\Versioned]
	private int $quantity = 1;

	#[Groups( [ 'notification_read' ] )]
	#[ORM\Column( type: 'json', nullable: true )]
	#[Gedmo\Versioned]
	private $config;

	#[Groups( [ 'notification_read' ] )]
	#[ORM\ManyToOne( targetEntity: NotificationTopic::class )]
	#[Gedmo\Versioned]
	private NotificationTopic $notificationTopic;

	#[Groups( [ 'notification_read' ] )]
	#[ORM\Column( type: 'json', nullable: true )]
	#[Gedmo\Versioned]
	private $metadata;

	#[Groups( [ 'notification_read', 'notification_read_user' ] )]
	#[ORM\ManyToOne( targetEntity: User::class )]
	#[Gedmo\Versioned]
	private User $relatedUser;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[Gedmo\Versioned]
	private Client $client;

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return User
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser( $user ) {
		$this->user = $user;
	}

	/**
	 * @return \DateTime
	 */
	public function getReadDate() {
		return $this->readDate;
	}

	/**
	 * @param \DateTime $readDate
	 */
	public function setReadDate( $readDate ) {
		$this->readDate = $readDate;
	}

	public function getHtml(): ?string {
		return $this->html;
	}

	public function setHtml( ?string $html ): self {
		$this->html = $html;

		return $this;
	}

	public function getLink(): ?string {
		return $this->link;
	}

	public function setLink( ?string $link ): self {
		$this->link = $link;

		return $this;
	}

	public function getQuantity(): ?int {
		return $this->quantity;
	}

	public function setQuantity( ?int $quantity ): self {
		$this->quantity = $quantity;

		return $this;
	}

	public function getConfig(): ?array {
		return $this->config;
	}

	public function setConfig( ?array $config ): self {
		$this->config = $config;

		return $this;
	}

	public function getNotificationTopic(): ?NotificationTopic {
		return $this->notificationTopic;
	}

	public function setNotificationTopic( ?NotificationTopic $notificationTopic ): self {
		$this->notificationTopic = $notificationTopic;

		return $this;
	}

	public function getGroupName(): ?string {
		return $this->groupName;
	}

	public function setGroupName( ?string $groupName ): self {
		$this->groupName = $groupName;

		return $this;
	}

	public function getMetadata(): ?array {
		return $this->metadata;
	}

	public function setMetadata( ?array $metadata ): self {
		$this->metadata = $metadata;

		return $this;
	}

	public function getRelatedUser(): ?User {
		return $this->relatedUser;
	}

	public function setRelatedUser( ?User $relatedUser ): self {
		$this->relatedUser = $relatedUser;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
