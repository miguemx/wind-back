<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\InvoiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[Gedmo\Loggable]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ApiResource(
	collectionOperations: [
		'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('INVOICE_LIST')" ],
		'post' => [ 'method' => 'POST', 'access_control' => "is_granted('INVOICE_CREATE')" ]
	],
	itemOperations: [
		'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('INVOICE_UPDATE')" ],
		'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('INVOICE_DELETE')" ],
		'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('INVOICE_SHOW')" ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [ 'groups' => [ 'invoice_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'invoice_write' ] ]
	] )]
#[ORM\Entity( repositoryClass: InvoiceRepository::class )]
class Invoice {
	use TrackableTrait;

	#[Groups( [ 'invoice_read', 'invoice_read_id' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[Groups( [ 'invoice_write', 'invoice_read', 'invoice_read_folio' ] )]
	#[ORM\Column( type: 'string', length: 50, nullable: true )]
	#[Gedmo\Versioned]
	private $folio;

	#[Groups( [ 'invoice_write', 'invoice_read_transaction' ] )]
	#[ORM\OneToMany( mappedBy: 'invoice', targetEntity: Transaction::class, cascade: [
		'persist',
		'remove'
	], orphanRemoval: true )]
	private $transaction;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	public function __construct() {
		$this->transaction = new ArrayCollection();
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getFolio(): ?string {
		return $this->folio;
	}

	public function setFolio( ?string $folio ): self {
		$this->folio = $folio;

		return $this;
	}

	/**
	 * @return Collection|Transaction[]
	 */
	public function getTransaction(): Collection {
		return $this->transaction;
	}

	public function addTransaction( Transaction $transaction ): self {
		if ( ! $this->transaction->contains( $transaction ) ) {
			$this->transaction[] = $transaction;
			$transaction->setInvoice( $this );
		}

		return $this;
	}

	public function removeTransaction( Transaction $transaction ): self {
		if ( $this->transaction->removeElement( $transaction ) ) {
			// set the owning side to null (unless already changed)
			if ( $transaction->getInvoice() === $this ) {
				$transaction->setInvoice( null );
			}
		}

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}
}
