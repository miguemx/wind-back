<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\ClientAwareAnnotation;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[ORM\Entity( repositoryClass: ClientRepository::class )]
#[ORM\Table]
#[ORM\Index( name: 'client_name_idx', columns: [ 'name' ] )]
#[Gedmo\Loggable]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[ClientAwareAnnotation( clientFieldName: 'id' )]
#[ApiResource(
	collectionOperations: [
		'get'  => [
			'method'         => 'GET',
			'access_control' => "is_granted('CLIENT_LIST')"
		],
		'post' => [
			'method'         => 'POST',
			'access_control' => "is_granted('CLIENT_CREATE')"
		]
	],
	itemOperations: [
		'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('CLIENT_UPDATE')", "route_name" => "client_put" ],
		'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('CLIENT_DELETE')" ],
		'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('CLIENT_SHOW')" ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [ 'groups' => [ 'client_read' ] ],
         		'denormalization_context' => [ 
         			'groups' => [ 
         				'client_write', 
         				'client_write_user', 
         				'fiscal_data_write' 
         			] 
         		]
         	],
	order: [ 'name' ]
)]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
#[ApiFilter( OrderFilter::class, properties: [ 'name' ] )]
#[ApiFilter( SearchFilter::class, properties: [
	'name'           => 'partial',
	'users.fullName' => 'partial',
	'users.name'     => 'partial'
] )]
class Client {
	use TrackableTrait;

	#[Groups( [ 'client_read', 'client_write', 'client_read_id' ] )]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	#[Gedmo\Versioned]
	private $id;

	#[Groups( [ 'client_read', 'client_write', 'client_read_name' ] )]
	#[Assert\Length( max: 150 )]
	#[ORM\Column( type: 'string', length: 150 )]
	#[Gedmo\Versioned]
	private $name;

	#[Groups( [ 'client_read', 'client_write', 'client_read_initials' ] )]
	#[Assert\Length( max: 8 )]
	#[ORM\Column( type: 'string', length: 8, nullable: true, unique: true )]
	#[Gedmo\Versioned]
	private $initials;

	#[Groups( [ 'client_read', 'client_write', 'client_read_zip' ] )]
	#[Assert\Length( max: 5 )]
	#[ORM\Column( type: 'string', length: 5, nullable: true )]
	#[Gedmo\Versioned]
	private $zip;

	#[Groups( [ 'client_read', 'client_write', 'client_read_picture' ] )]
	#[ORM\Column( type: 'string', length: 255, nullable: true )]
	#[Gedmo\Versioned]
	private $picture;

	#[Groups( [ 'client_read', 'client_write', 'client_read_phone' ] )]
	#[Assert\Length( max: 20 )]
	#[ORM\Column( type: 'string', length: 20, nullable: true )]
	#[Gedmo\Versioned]
	private $phone;

	#[Groups( [ 'client_write_user', 'client_read_users' ] )]
	#[Assert\Valid]
	#[ORM\OneToMany( targetEntity: User::class, mappedBy: 'client', cascade: [ 'persist' ] )]
	private $users;

	#[Groups( [ 'client_write', 'client_read_projects' ] )]
	#[ORM\OneToMany( targetEntity: Project::class, mappedBy: 'client', orphanRemoval: true )]
	private $projects;

	#[Groups( [ 'client_read', 'client_write', 'client_read_company_name' ] )]
	#[Assert\Length( max: 255 )]
	#[ORM\Column( type: 'string', length: 255, nullable: true )]
	#[Gedmo\Versioned]
	private $companyName;

	#[Groups( [ 'client_read', 'client_write', 'client_read_address' ] )]
	#[Assert\Length( max: 255 )]
	#[ORM\Column( type: 'string', length: 255, nullable: true )]
	#[Gedmo\Versioned]
	private $address;

	#[Groups( [ 'client_read_client_stats' ] )]
	#[ORM\OneToOne( targetEntity: ClientStats::class, mappedBy: 'client', cascade: [ 'persist', 'remove' ] )]
	#[Gedmo\Versioned]
	private $clientStats;

	#[Groups( [ 'client_read', 'client_write', 'client_read_is_legal_person' ] )]
	#[ORM\Column( type: 'boolean', nullable: true )]
	#[Gedmo\Versioned]
	private $isLegalPerson;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups( [ 'client_read', 'client_write', 'client_read_last_name' ] )]
    #[Assert\Length( max: 255 )]
    #[Gedmo\Versioned]
    private $lastName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups( [ 'client_read', 'client_write', 'client_read_second_last_name' ] )]
    #[Gedmo\Versioned]
    private $secondLastName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups( [ 'client_read', 'client_write', 'client_read_curp' ] )]
    #[Gedmo\Versioned]
    private $curp;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups( [ 'client_read', 'client_write', 'client_read_birthday' ] )]
    #[Gedmo\Versioned]
    private $birthday;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: TaxDocument::class)]
    #[Groups( [ 'client_read', 'client_write', 'client_read_tax_document' ] )]
    private $taxDocument;

    #[ORM\OneToOne(inversedBy: 'client', targetEntity: FiscalData::class, cascade: ['persist', 'remove'])]
	#[Groups( [ 'client_read', 'client_write', 'client_read_fiscal_data' ] )]
    private $fiscalData;

	public function __construct() {
		$this->projects = new ArrayCollection();
		$this->users    = new ArrayCollection();
		$this->setClientStats( new ClientStats() );
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( ?string $name ): self {
		$this->name = $name;

		return $this;
	}

    public function getLastName(): ?string {
        return $this->lastName;
    }

    public function setLastName( ?string $lastName ): self {
        $this->lastName = $lastName;

        return $this;
    }

    public function getSecondLastName(): ?string {
        return $this->secondLastName;
    }

    public function setSecondLastName( ?string $secondLastName ): self {
        $this->secondLastName = $secondLastName;

        return $this;
    }

	public function getZip(): ?string {
		return $this->zip;
	}

	public function setZip( ?string $zip ): self {
		$this->zip = $zip;

		return $this;
	}

    public function getCurp(): ?string {
        return $this->curp;
    }

    public function setCurp( ?string $curp ): self {
        $this->curp = $curp;

        return $this;
    }
	public function getPicture(): ?string {
		return $this->picture;
	}

	public function setPicture( ?string $picture ): self {
		$this->picture = $picture;

		return $this;
	}

	public function getPhone(): ?string {
		return $this->phone;
	}

	public function setPhone( ?string $phone ): self {
		$this->phone = $phone;

		return $this;
	}

	/**
	 * @return Collection|User[]
	 */
	public function getUsers(): Collection {
		return $this->users;
	}

	public function addUser( User $user ): self {
		if ( ! $this->users->contains( $user ) ) {
			$this->users[] = $user;
			$user->setClient( $this );
		}

		return $this;
	}

	public function removeUser( User $user ): self {
		if ( $this->users->removeElement( $user ) ) {
			// set the owning side to null (unless already changed)
			if ( $user->getClient() === $this ) {
				$user->setClient( null );
			}
		}

		return $this;
	}

	/**
	 * @return Collection|Project[]
	 */
	public function getProjects(): Collection {
		return $this->projects;
	}

	public function addProject( Project $project ): self {
		if ( ! $this->projects->contains( $project ) ) {
			$this->projects[] = $project;
			$project->setClient( $this );
		}

		return $this;
	}

	public function removeProject( Project $project ): self {
		if ( $this->projects->removeElement( $project ) ) {
			// set the owning side to null (unless already changed)
			if ( $project->getClient() === $this ) {
				$project->setClient( null );
			}
		}

		return $this;
	}

	public function getCompanyName(): ?string {
		return $this->companyName;
	}

	public function setCompanyName( ?string $companyName ): self {
		$this->companyName = $companyName;

		return $this;
	}

	public function getAddress(): ?string {
		return $this->address;
	}

	public function setAddress( ?string $address ): self {
		$this->address = $address;

		return $this;
	}

	/**
	 * @return ?string
	 */
	public function getInitials() {
		return $this->initials;
	}

	/**
	 * @param ?string $initials
	 */
	public function setInitials( $initials ): Client {
		$this->initials = $initials;

		return $this;
	}

	public function getClientStats(): ?ClientStats {
		return $this->clientStats;
	}

	public function setClientStats( ?ClientStats $clientStats ): self {
		// unset the owning side of the relation if necessary
		if ( $clientStats === null && $this->clientStats !== null ) {
			$this->clientStats->setClient( null );
		}

		// set the owning side of the relation if necessary
		if ( $clientStats !== null ) {
			$clientStats->setClient( $this );
			$clientStats->setTenant( $this->getTenant() );
		}

		$this->clientStats = $clientStats;

		return $this;
	}

	public function getIsLegalPerson(): ?bool {
		return $this->isLegalPerson;
	}

	public function setIsLegalPerson( ?bool $isLegalPerson ): self {
		$this->isLegalPerson = $isLegalPerson;

		return $this;
	}

	#[Groups( [ 'client_read_company_owner' ] )]
	public function getCompanyOwner(): ?User {
		foreach ( $this->getUsers() as $user ) {
			if ( $user->getIsCompanyOwner() ) {
				return $user;
			}
		}

		return null;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;
		if ( $this->getClientStats() ) {
			$this->getClientStats()->setTenant( $tenant );
		}

		return $this;
	}

    public function getFiscalData(): ?FiscalData
    {
        return $this->fiscalData;
    }

    public function setFiscalData(?FiscalData $fiscalData): self
    {
        $this->fiscalData = $fiscalData;

        return $this;
    }
}
