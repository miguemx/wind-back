<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\TenantAwareAnnotation;
use App\Repository\QuotationRequestProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity( repositoryClass: QuotationRequestProductRepository::class )]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id' )]
#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [
		'get'  => [
			'method'         => 'GET',
			'access_control' => "is_granted('QUOTATION_REQUEST_PRODUCT_LIST')"
		],
		'post' => [
			'method'         => 'POST',
			'access_control' => "is_granted('QUOTATION_REQUEST_PRODUCT_CREATE')"
		]
	],
	itemOperations: [
		'put'    => [
			'method'         => 'PUT',
			'access_control' => "is_granted('QUOTATION_REQUEST_PRODUCT_UPDATE')"
		],
		'delete' => [
			'method'         => 'DELETE',
			'access_control' => "is_granted('QUOTATION_REQUEST_PRODUCT_DELETE')"
		],
		'get'    => [
			'method'         => 'GET',
			'access_control' => "is_granted('QUOTATION_REQUEST_PRODUCT_SHOW')"
		]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [
			'json' => [ 'application/json' ],
			'xlsx' => [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ]
		],
		'normalization_context'   => [ 'groups' => [ 'quotation_request_product_read' ] ],
		'denormalization_context' => [
			'groups' => [
				'quotation_request_product_write',
				'quotation_request_write'
			]
		]
	] )]
class QuotationRequestProduct {
	use TrackableTrait;

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[ORM\Column( type: 'string', length: 255 )]
	#[Groups( [
		'quotation_request_product_write',
		'quotation_request_product_read',
		'quotation_request_product_read_name',
		'quotation_request_write'
	] )]
	#[Gedmo\Versioned]
	private $name;

	#[ORM\ManyToOne( targetEntity: QuotationRequest::class, inversedBy: 'quotationRequestProducts' )]
	#[ORM\JoinColumn( nullable: false )]
	#[Groups( [ 'quotation_request_product_read_quotation_request' ] )]
	#[Gedmo\Versioned]
	private $quotationRequest;

	#[ORM\Column( type: 'integer' )]
	#[Groups( [
		'quotation_request_product_write',
		'quotation_request_product_read',
		'quotation_request_product_read_quantity',
		'quotation_request_write'
	] )]
	#[Gedmo\Versioned]
	private $quantity;

	#[ORM\Column( type: 'boolean' )]
	#[Groups( [
		'quotation_request_product_write',
		'quotation_request_product_read',
		'quotation_request_product_read_is_optional',
		'quotation_request_write'
	] )]
	#[Gedmo\Versioned]
	private $isOptional;

	#[ORM\Column( type: 'text', nullable: true )]
	#[Groups( [
		'quotation_request_product_write',
		'quotation_request_product_read',
		'quotation_request_product_read_description',
		'quotation_request_write'
	] )]
	#[Gedmo\Versioned]
	private $description;

	#[ORM\Column( type: 'boolean', nullable: true )]
	#[Groups( [
		'quotation_request_product_write',
		'quotation_request_product_read',
		'quotation_request_product_read_was_approved',
		'quotation_request_write'
	] )]
	#[Gedmo\Versioned]
	private $wasApproved;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $tenant;

	#[Groups( [ 'quotation_request_product_write', 'quotation_request_write' ] )]
	#[ORM\JoinColumn( nullable: false )]
	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[Gedmo\Versioned]
	private Client $client;

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getPrice(): ?string {
		return $this->price;
	}

	public function setPrice( string $price ): self {
		$this->price = $price;

		return $this;
	}

	public function getQuantity(): ?int {
		return $this->quantity;
	}

	public function setQuantity( int $quantity ): self {
		$this->quantity = $quantity;

		return $this;
	}

	public function getQuotationRequest(): ?QuotationRequest {
		return $this->quotationRequest;
	}

	public function setQuotationRequest( ?QuotationRequest $quotationRequest ): self {
		$this->quotationRequest = $quotationRequest;

		return $this;
	}

	public function getIsOptional(): ?bool {
		return $this->isOptional;
	}

	public function setIsOptional( bool $isOptional ): self {
		$this->isOptional = $isOptional;

		return $this;
	}

	public function getDescription(): ?string {
		return $this->description;
	}

	public function setDescription( string $description ): self {
		$this->description = $description;

		return $this;
	}

	public function getWasApproved(): ?bool {
		return $this->wasApproved;
	}

	public function setWasApproved( ?bool $wasApproved ): self {
		$this->wasApproved = $wasApproved;

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
