<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Repository\ConversationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[Gedmo\Loggable]
#[ApiResource(
	collectionOperations: [ 'get' => [ 'method' => 'GET' ] ],
	itemOperations: [
		'put'    => [ 'method' => 'PUT' ],
		'delete' => [ 'method' => 'DELETE' ],
		'get'    => [ 'method' => 'GET' ]
	],
	attributes: [
		'input_formats'           => [ 'json' => [ 'application/json' ] ],
		'output_formats'          => [ 'json' => [ 'application/json' ] ],
		'normalization_context'   => [ 'groups' => [ 'file_container_read' ] ],
		'denormalization_context' => [ 'groups' => [ 'file_container_write' ] ]
	] )]
#[ORM\Entity( repositoryClass: ConversationRepository::class )]
#[ApiFilter( GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ] )]
class Conversation {
	use TrackableTrait;

	#[ORM\Id]
	#[Groups( [ 'conversation_read', 'conversation_write', 'conversation_read_id' ] )]
	#[ORM\GeneratedValue]
	#[ORM\Column( type: 'integer' )]
	private $id;

	#[Groups( [ 'conversation_read', 'conversation_write', 'conversation_read_parent_class' ] )]
	#[ORM\Column( type: 'string', length: 255 )]
	private $parentClass;

	#[Groups( [ 'conversation_read', 'conversation_write', 'conversation_read_channels' ] )]
	#[ORM\OneToMany( mappedBy: 'conversation', targetEntity: Channel::class, cascade: [
		'persist',
		'remove'
	], orphanRemoval: true )]
	private $channels;

	#[ORM\ManyToOne( targetEntity: Tenant::class )]
	#[ORM\JoinColumn( nullable: false )]
	private $tenant;

	#[Groups( [ 'conversation_read', 'conversation_read_client' ] )]
	#[ORM\ManyToOne( targetEntity: Client::class )]
	#[ORM\JoinColumn( nullable: false )]
	#[Gedmo\Versioned]
	private $client;

	public function __construct() {
		$this->channels = new ArrayCollection();
		//$channel = $this->getNewChannel();
		//$this->channels[0] = $channel;
	}

	private function getNewChannel() {
		$channel = null;

		return $channel;
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getParentClass(): ?string {
		return $this->parentClass;
	}

	public function setParentClass( string $parentClass ): self {
		$this->parentClass = $parentClass;

		return $this;
	}

	/**
	 * @return Collection|Channel[]
	 */
	public function getChannels(): Collection {
		return $this->channels;
	}

	public function addChannel( Channel $channel ): self {
		if ( ! $this->channels->contains( $channel ) ) {
			$this->channels[] = $channel;
			$channel->setConversation( $this );
		}

		return $this;
	}

	public function removeChannel( Channel $channel ): self {
		if ( $this->channels->removeElement( $channel ) ) {
			// set the owning side to null (unless already changed)
			if ( $channel->getConversation() === $this ) {
				$channel->setConversation( null );
			}
		}

		return $this;
	}

	public function getTenant(): ?Tenant {
		return $this->tenant;
	}

	public function setTenant( ?Tenant $tenant ): self {
		$this->tenant = $tenant;

		return $this;
	}

	public function getClient(): ?Client {
		return $this->client;
	}

	public function setClient( ?Client $client ): self {
		$this->client = $client;

		return $this;
	}
}
