<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use App\Annotation\ClientAwareAnnotation;
use App\Annotation\TenantAwareAnnotation;
use App\Filter\SearchFilter;
use App\Repository\FiscalDataRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\UniqueConstraint;
use App\Validators as AppAssert;

#[ORM\Entity(repositoryClass: FiscalDataRepository::class)]
#[TenantAwareAnnotation( tenantFieldName: 'tenant_id')]
#[ClientAwareAnnotation( clientFieldName: 'client_id')]
#[ORM\table]
#[Gedmo\Loggable]
#[ApiResource(
    collectionOperations: [
        'get'  => [ 'method' => 'GET', 'access_control' => "is_granted('FISCAL_DATA_LIST')" ],
        'post' => [ 'method' => 'POST', 'access_control' => "is_granted('FISCAL_DATA_CREATE')" ]
    ],
    itemOperations: [
        'put'    => [ 'method' => 'PUT', 'access_control' => "is_granted('FISCAL_DATA_UPDATE')" ],
        'delete' => [ 'method' => 'DELETE', 'access_control' => "is_granted('FISCAL_DATA_DELETE')" ],
        'get'    => [ 'method' => 'GET', 'access_control' => "is_granted('FISCAL_DATA_SHOW')" ]
    ],
    attributes: [
        'input_formats'           => [ 'json' => [ 'application/json' ] ],
        'output_formats'          => [
            'json' => [ 'application/json' ],
        ],
        'normalization_context'   => [
            'groups' => [
                'fiscal_data_read',
                'fiscal_data_write'
            ]
        ],
        'denormalization_context' => [
            'groups' => [
                'fiscal_data_write',
                'tenant_write'
            ]
        ]
    ])]
#[ApiFilter(GroupFilter::class, arguments: [ 'parameterName' => 'sGroups', 'overrideDefaultGroups' => true ])]
#[ApiFilter(SearchFilter::class, properties: ["id", "rfc", "businessName"])]
class FiscalData
{
    use TrackableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_id'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[AppAssert\ValidRfc]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_rfc'])]
    #[Gedmo\Versioned]
    private $rfc = '';

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_business_name'])]
    #[Gedmo\Versioned]
    private $businessName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Email]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_email'])]
    #[Gedmo\Versioned]
    private $email = '';

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_comercial_name'])]
    #[Gedmo\Versioned]
    private $comercialName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_street'])]
    #[Gedmo\Versioned]
    private $street;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_external_number'])]
    #[Gedmo\Versioned]
    private $externalNumber;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_internal_number'])]
    #[Gedmo\Versioned]
    private $internalNumber;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_neighborhood'])]
    #[Gedmo\Versioned]
    private $neighborhood;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_delegation'])]
    #[Gedmo\Versioned]
    private $delegation;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_city'])]
    #[Gedmo\Versioned]
    private $city;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_state'])]
    #[Gedmo\Versioned]
    private $state;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_country'])]
    #[Gedmo\Versioned]
    private $country;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['fiscal_data_read','fiscal_data_write','fiscal_data_read_zip'])]
    #[Gedmo\Versioned]
    private $zip;

    #[ORM\ManyToOne( targetEntity: Tenant::class, inversedBy: 'fiscalData' )]
    #[ORM\JoinColumn( nullable: false )]
    #[Groups(['fiscal_data_write','fiscal_data_read_tenant'])]
    #[Gedmo\Versioned]
    private $tenant;

    #[ORM\OneToOne(mappedBy: 'fiscalData', targetEntity: Client::class, cascade: ['persist', 'remove'])]
    #[Groups(['fiscal_data_read_client','fiscal_data_write','fiscal_data_read'])]
    private $client;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = mb_strtoupper($rfc);

        return $this;
    }

    public function getBusinessName(): ?string
    {
        return $this->businessName;
    }

    public function setBusinessName(?string $businessName): self
    {
        $this->businessName = $businessName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getComercialName(): ?string
    {
        return $this->comercialName;
    }

    public function setComercialName(?string $comercialName): self
    {
        $this->comercialName = $comercialName;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getExternalNumber(): ?string
    {
        return $this->externalNumber;
    }

    public function setExternalNumber(?string $externalNumber): self
    {
        $this->externalNumber = $externalNumber;

        return $this;
    }

    public function getInternalNumber(): ?string
    {
        return $this->internalNumber;
    }

    public function setInternalNumber(?string $internalNumber): self
    {
        $this->internalNumber = $internalNumber;

        return $this;
    }

    public function getNeighborhood(): ?string
    {
        return $this->neighborhood;
    }

    public function setNeighborhood(?string $neighborhood): self
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    public function getDelegation(): ?string
    {
        return $this->delegation;
    }

    public function setDelegation(?string $delegation): self
    {
        $this->delegation = $delegation;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    public function setUpdatedDate(\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        // unset the owning side of the relation if necessary
        if ($client === null && $this->client !== null) {
            $this->client->setFiscalData(null);
        }

        // set the owning side of the relation if necessary
        if ($client !== null && $client->getFiscalData() !== $this) {
            $client->setFiscalData($this);
        }

        $this->client = $client;

        return $this;
    }

}
