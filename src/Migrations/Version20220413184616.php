<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220413184616 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX tenant_initials ON client (tenant_id, initials)');
        $this->addSql('DROP INDEX UNIQ_C74404555DD9566F ON client');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX tenant_initials ON client');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C74404555DD9566F ON client (initials)');
    }
}
