<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230109183708 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fiscal_data ADD tenant_fiscal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fiscal_data ADD CONSTRAINT FK_D5D66703A6E2BEB FOREIGN KEY (tenant_fiscal_id) REFERENCES tenant (id)');
        $this->addSql('CREATE INDEX IDX_D5D66703A6E2BEB ON fiscal_data (tenant_fiscal_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fiscal_data DROP FOREIGN KEY FK_D5D66703A6E2BEB');
        $this->addSql('DROP INDEX IDX_D5D66703A6E2BEB ON fiscal_data');
        $this->addSql('ALTER TABLE fiscal_data DROP tenant_fiscal_id');
    }
}
