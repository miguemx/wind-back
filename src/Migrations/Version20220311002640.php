<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311002640 extends AbstractMigration {
	public function getDescription(): string {
		return 'Change average balance to be null in account statement';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE account_statement CHANGE average_balance average_balance NUMERIC(14, 2) DEFAULT NULL' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE account_statement CHANGE average_balance average_balance NUMERIC(14, 2) NOT NULL' );
	}
}
