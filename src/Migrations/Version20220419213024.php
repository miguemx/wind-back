<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220419213024 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_file DROP owner_domain, DROP object_id');
        $this->addSql('DROP INDEX tenant_initials ON client');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C74404555DD9566F ON client (initials)');
        $this->addSql('ALTER TABLE file_container_app_file DROP INDEX UNIQ_DFDF2D0C7E96F921, ADD INDEX IDX_DFDF2D0C7E96F921 (app_file_id)');
        $this->addSql('ALTER TABLE file_container_app_file DROP FOREIGN KEY FK_DFDF2D0C7E96F921');
        $this->addSql('ALTER TABLE file_container_app_file ADD CONSTRAINT FK_DFDF2D0C7E96F921 FOREIGN KEY (app_file_id) REFERENCES app_file (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE permission_groups_permissions DROP FOREIGN KEY FK_BC46418FED90CCA');
        $this->addSql('ALTER TABLE permission_groups_permissions ADD CONSTRAINT FK_BC46418FED90CCA FOREIGN KEY (permission_id) REFERENCES permission (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transaction CHANGE tenant_id tenant_id INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_file ADD owner_domain VARCHAR(255) DEFAULT NULL, ADD object_id VARCHAR(40) DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_C74404555DD9566F ON client');
        $this->addSql('CREATE UNIQUE INDEX tenant_initials ON client (tenant_id, initials)');
        $this->addSql('ALTER TABLE file_container_app_file DROP INDEX IDX_DFDF2D0C7E96F921, ADD UNIQUE INDEX UNIQ_DFDF2D0C7E96F921 (app_file_id)');
        $this->addSql('ALTER TABLE file_container_app_file DROP FOREIGN KEY FK_DFDF2D0C7E96F921');
        $this->addSql('ALTER TABLE file_container_app_file ADD CONSTRAINT FK_DFDF2D0C7E96F921 FOREIGN KEY (app_file_id) REFERENCES app_file (id)');
        $this->addSql('ALTER TABLE permission_groups_permissions DROP FOREIGN KEY FK_BC46418FED90CCA');
        $this->addSql('ALTER TABLE permission_groups_permissions ADD CONSTRAINT FK_BC46418FED90CCA FOREIGN KEY (permission_id) REFERENCES permission (id)');
        $this->addSql('ALTER TABLE transaction CHANGE tenant_id tenant_id INT NOT NULL');
    }
}
