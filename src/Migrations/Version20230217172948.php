<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230217172948 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fiscal_data DROP is_sat_ws_configured, DROP sat_ws_id, DROP sat_ws_extraction_id, DROP is_first_data_extracted, DROP next_extraction_date');
        $this->addSql('ALTER TABLE tenant_rfc ADD is_sat_ws_configured TINYINT(1) NOT NULL, ADD sat_ws_extraction_id VARCHAR(255) DEFAULT NULL, ADD is_first_data_extracted TINYINT(1) NOT NULL, ADD next_extraction_date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fiscal_data ADD is_sat_ws_configured TINYINT(1) NOT NULL, ADD sat_ws_id VARCHAR(255) DEFAULT NULL, ADD sat_ws_extraction_id VARCHAR(255) DEFAULT NULL, ADD is_first_data_extracted TINYINT(1) NOT NULL, ADD next_extraction_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE tenant_rfc DROP is_sat_ws_configured, DROP sat_ws_extraction_id, DROP is_first_data_extracted, DROP next_extraction_date');
    }
}
