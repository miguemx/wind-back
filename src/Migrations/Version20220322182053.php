<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220322182053 extends AbstractMigration {
	public function getDescription(): string {
		return '';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE bank_transaction ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD created_date DATETIME DEFAULT NULL, ADD updated_date DATETIME DEFAULT NULL' );
		$this->addSql( 'ALTER TABLE bank_transaction ADD CONSTRAINT FK_50BCB3AEDE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON DELETE SET NULL' );
		$this->addSql( 'ALTER TABLE bank_transaction ADD CONSTRAINT FK_50BCB3AE16FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON DELETE SET NULL' );
		$this->addSql( 'CREATE INDEX IDX_50BCB3AEDE12AB56 ON bank_transaction (created_by)' );
		$this->addSql( 'CREATE INDEX IDX_50BCB3AE16FE72E1 ON bank_transaction (updated_by)' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE bank_transaction DROP FOREIGN KEY FK_50BCB3AEDE12AB56' );
		$this->addSql( 'ALTER TABLE bank_transaction DROP FOREIGN KEY FK_50BCB3AE16FE72E1' );
		$this->addSql( 'DROP INDEX IDX_50BCB3AEDE12AB56 ON bank_transaction' );
		$this->addSql( 'DROP INDEX IDX_50BCB3AE16FE72E1 ON bank_transaction' );
		$this->addSql( 'ALTER TABLE bank_transaction DROP created_by, DROP updated_by, DROP created_date, DROP updated_date' );
	}
}
