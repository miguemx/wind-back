<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220223190155 extends AbstractMigration {
	public function getDescription(): string {
		return '';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE transaction ADD status VARCHAR(50) NOT NULL, DROP validated' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE transaction ADD validated TINYINT(1) DEFAULT NULL, DROP status, CHANGE concept concept VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE transaction_type transaction_type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE comments comments VARCHAR(4096) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
	}
}
