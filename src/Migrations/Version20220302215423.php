<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use App\Entity\Conversation;
use App\Entity\Project;
use App\Helpers\createConversation;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220302215423 extends AbstractMigration implements ContainerAwareInterface {
	private $container;

	public function setContainer( ContainerInterface $container = null ) {
		$this->container = $container;
	}

	public function getDescription(): string {
		return '';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$em = $this->container->get( 'doctrine.orm.entity_manager' );

		// ... create conversations and update the projects table
		$query  = "SELECT * FROM project WHERE conversation_id IS NULL";
		$stmt   = $this->connection->prepare( $query );
		$result = $stmt->execute();

		while ( $row = $result->fetch() ) {
			$projectId = $row['id'];

			$project = $em->getRepository( Project::class )->find( $projectId );

			$conversation = new Conversation();
			$conversation->setClient( $project->getClient() );
			$conversation->setTenant( $project->getTenant() );
			$conversation->setParentClass( Project::class );

			$project->setConversation( $conversation );

			$em->persist( $conversation );
			$em->persist( $project );
			$em->flush();
		}
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs

	}
}
