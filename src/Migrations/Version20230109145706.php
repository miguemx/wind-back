<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230109145706 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE fiscal_data (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, tenant_id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, is_default TINYINT(1) DEFAULT NULL, rfc VARCHAR(255) NOT NULL, business_name VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, type_person VARCHAR(50) NOT NULL, comercial_name VARCHAR(255) DEFAULT NULL, street VARCHAR(255) NOT NULL, external_number VARCHAR(255) DEFAULT NULL, internal_number VARCHAR(255) DEFAULT NULL, neighborhood VARCHAR(255) DEFAULT NULL, delegation VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, state VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, zip VARCHAR(255) DEFAULT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_D5D667019EB6921 (client_id), INDEX IDX_D5D66709033212A (tenant_id), INDEX IDX_D5D6670DE12AB56 (created_by), INDEX IDX_D5D667016FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fiscal_data ADD CONSTRAINT FK_D5D667019EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE fiscal_data ADD CONSTRAINT FK_D5D66709033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)');
        $this->addSql('ALTER TABLE fiscal_data ADD CONSTRAINT FK_D5D6670DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE fiscal_data ADD CONSTRAINT FK_D5D667016FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE fiscal_data');
    }
}
