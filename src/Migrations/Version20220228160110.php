<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220228160110 extends AbstractMigration {
	public function getDescription(): string {
		return '';
	}

	public function up( Schema $schema ): void {
		$this->addSql( 'ALTER TABLE bank ADD code VARCHAR(255) NOT NULL' );
		$this->addSql( 'ALTER TABLE bank CHANGE tenant_id tenant_id INT DEFAULT NULL' );

		$this->addSql( 'INSERT into bank (code, name) values ("40138","ABC CAPITAL");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40133","ACTINVER");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40062","AFIRME");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90638","AKALA");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90706","ARCUS");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90659","ASP INTEGRA OPC");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40128","AUTOFIN");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40127","AZTECA");' );
		$this->addSql( 'INSERT into bank (code, name) values ("37166","BaBien");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40030","BAJIO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40002","BANAMEX");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40154","BANCO FINTERRA");' );
		$this->addSql( 'INSERT into bank (code, name) values ("37006","BANCOMEXT");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40137","BANCOPPEL");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40160","BANCO S3");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40152","BANCREA");' );
		$this->addSql( 'INSERT into bank (code, name) values ("37019","BANJERCITO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40147","BANKAOOL");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40106","BANK OF AMERICA");' );
		$this->addSql( 'INSERT into bank (code, name) values ("37009","BANOBRAS");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40072","BANORTE");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40058","BANREGIO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40060","BANSI");' );
		$this->addSql( 'INSERT into bank (code, name) values ("2001","BANXICO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40129","BARCLAYS");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40145","BBASE");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40012","BBVA MEXICO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40112","BMONEX");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90677","CAJA POP MEXICA");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90683","CAJA TELEFONIST");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90648","CB Evercore");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90630","CB INTERCAM");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40143","CIBANCO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90631","CI BOLSA");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90901","CLS");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90903","CoDi Valida");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40130","COMPARTAMOS");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40140","CONSUBANCO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90652","CREDICAPITAL");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40126","CREDIT SUISSE");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90680","CRISTOBAL COLON");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40151","DONDE");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90616","FINAMEX");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90634","FINCOMUN");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90689","FOMPED");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90685","FONDO (FIRA)");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90601","GBM");' );
		$this->addSql( 'INSERT into bank (code, name) values ("37168","HIPOTECARIA FED");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40021","HSBC");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40155","ICBC");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40036","INBURSA");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90902","INDEVAL");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40150","INMOBILIARIO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40136","INTERCAM BANCO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90686","INVERCAP");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40059","INVEX");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40110","JP MORGAN");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90653","KUSPIT");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90670","LIBERTAD");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90602","MASARI");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40042","MIFEL");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40158","MIZUHO BANK");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90600","MONEXCB");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40108","MUFG");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40132","MULTIVA BANCO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90613","MULTIVA CBOLSA");' );
		$this->addSql( 'INSERT into bank (code, name) values ("37135","NAFIN");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90684","OPM");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40148","PAGATODO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90620","PROFUTURO");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40156","SABADELL");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40014","SANTANDER");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40044","SCOTIABANK");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40157","SHINHAN");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90646","STP");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90656","UNAGRA");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90617","VALMEX");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90605","VALUE");' );
		$this->addSql( 'INSERT into bank (code, name) values ("90608","VECTOR");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40113","VE POR MAS");' );
		$this->addSql( 'INSERT into bank (code, name) values ("40141","VOLKSWAGEN");' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs

	}
}
