<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220221194432 extends AbstractMigration {
	public function getDescription(): string {
		return 'Add permission name index';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql( 'CREATE INDEX permission_name_idx ON permission (name)' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql( 'DROP INDEX permission_name_idx' );
	}
}
