<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230216155450 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fiscal_data DROP FOREIGN KEY FK_D5D66703A6E2BEB');
        $this->addSql('DROP INDEX IDX_D5D66703A6E2BEB ON fiscal_data');
        $this->addSql('DROP INDEX tenantFiscal ON fiscal_data');
        $this->addSql('ALTER TABLE fiscal_data DROP tenant_fiscal_id');
        $this->addSql('ALTER TABLE tenant_rfc ADD CONSTRAINT FK_FFD70B8BDE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tenant_rfc ADD CONSTRAINT FK_FFD70B8B16FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_FFD70B8BDE12AB56 ON tenant_rfc (created_by)');
        $this->addSql('CREATE INDEX IDX_FFD70B8B16FE72E1 ON tenant_rfc (updated_by)');
        $this->addSql('ALTER TABLE tenant_rfc RENAME INDEX tenant_id TO IDX_FFD70B8B9033212A');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fiscal_data ADD tenant_fiscal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fiscal_data ADD CONSTRAINT FK_D5D66703A6E2BEB FOREIGN KEY (tenant_fiscal_id) REFERENCES tenant (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_D5D66703A6E2BEB ON fiscal_data (tenant_fiscal_id)');
        $this->addSql('CREATE UNIQUE INDEX tenantFiscal ON fiscal_data (rfc, tenant_fiscal_id)');
        $this->addSql('ALTER TABLE tenant_rfc DROP FOREIGN KEY FK_FFD70B8BDE12AB56');
        $this->addSql('ALTER TABLE tenant_rfc DROP FOREIGN KEY FK_FFD70B8B16FE72E1');
        $this->addSql('DROP INDEX IDX_FFD70B8BDE12AB56 ON tenant_rfc');
        $this->addSql('DROP INDEX IDX_FFD70B8B16FE72E1 ON tenant_rfc');
        $this->addSql('ALTER TABLE tenant_rfc RENAME INDEX idx_ffd70b8b9033212a TO tenant_id');
    }
}
