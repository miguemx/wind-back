<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230109160448 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD fiscal_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C744045514CD4C05 FOREIGN KEY (fiscal_data_id) REFERENCES fiscal_data (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C744045514CD4C05 ON client (fiscal_data_id)');
        $this->addSql('ALTER TABLE fiscal_data DROP FOREIGN KEY FK_D5D667019EB6921');
        $this->addSql('DROP INDEX IDX_D5D667019EB6921 ON fiscal_data');
        $this->addSql('ALTER TABLE fiscal_data DROP client_id, DROP is_default');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C744045514CD4C05');
        $this->addSql('DROP INDEX UNIQ_C744045514CD4C05 ON client');
        $this->addSql('ALTER TABLE client DROP fiscal_data_id');
        $this->addSql('ALTER TABLE fiscal_data ADD client_id INT NOT NULL, ADD is_default TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE fiscal_data ADD CONSTRAINT FK_D5D667019EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_D5D667019EB6921 ON fiscal_data (client_id)');
    }
}
