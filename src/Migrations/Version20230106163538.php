<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230106163538 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tax_document ADD pdf_file_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tax_document ADD CONSTRAINT FK_38FD2BC8E071F843 FOREIGN KEY (pdf_file_id) REFERENCES app_file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_38FD2BC8E071F843 ON tax_document (pdf_file_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tax_document DROP FOREIGN KEY FK_38FD2BC8E071F843');
        $this->addSql('DROP INDEX UNIQ_38FD2BC8E071F843 ON tax_document');
        $this->addSql('ALTER TABLE tax_document DROP pdf_file_id');
    }
}
