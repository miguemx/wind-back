<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309165413 extends AbstractMigration {
	public function getDescription(): string {
		return '';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql( 'CREATE TABLE bank_transaction (id INT AUTO_INCREMENT NOT NULL, transaction_id INT DEFAULT NULL, account_statement_id INT DEFAULT NULL, date DATETIME DEFAULT NULL, amount NUMERIC(14, 2) NOT NULL, concept VARCHAR(255) DEFAULT NULL, code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_50BCB3AE2FC0CB0F (transaction_id), INDEX IDX_50BCB3AE1D44A01D (account_statement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB' );
		$this->addSql( 'ALTER TABLE bank_transaction ADD CONSTRAINT FK_50BCB3AE2FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id)' );
		$this->addSql( 'ALTER TABLE bank_transaction ADD CONSTRAINT FK_50BCB3AE1D44A01D FOREIGN KEY (account_statement_id) REFERENCES account_statement (id)' );
		$this->addSql( 'ALTER TABLE account_statement ADD document_id INT DEFAULT NULL, ADD period_start_date DATETIME DEFAULT NULL, ADD period_end_date DATETIME DEFAULT NULL, DROP month, DROP year, DROP file, DROP period, CHANGE bank_account_id bank_account_id INT DEFAULT NULL, CHANGE average_balance average_balance NUMERIC(14, 2) NOT NULL' );
		$this->addSql( 'ALTER TABLE account_statement ADD CONSTRAINT FK_E2A1892DC33F7837 FOREIGN KEY (document_id) REFERENCES app_file (id)' );
		$this->addSql( 'CREATE UNIQUE INDEX UNIQ_E2A1892DC33F7837 ON account_statement (document_id)' );
		$this->addSql( 'ALTER TABLE project CHANGE file_container_id file_container_id INT NOT NULL' );
		$this->addSql( 'ALTER TABLE transaction ADD bank_transaction_id INT DEFAULT NULL' );
		$this->addSql( 'ALTER TABLE transaction ADD CONSTRAINT FK_723705D1B898B7D6 FOREIGN KEY (bank_transaction_id) REFERENCES bank_transaction (id)' );
		$this->addSql( 'CREATE UNIQUE INDEX UNIQ_723705D1B898B7D6 ON transaction (bank_transaction_id)' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1B898B7D6' );
		$this->addSql( 'DROP TABLE bank_transaction' );
		$this->addSql( 'ALTER TABLE account_statement DROP FOREIGN KEY FK_E2A1892DC33F7837' );
		$this->addSql( 'DROP INDEX UNIQ_E2A1892DC33F7837 ON account_statement' );
		$this->addSql( 'ALTER TABLE account_statement ADD month INT NOT NULL, ADD year INT NOT NULL, ADD file VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD period VARCHAR(50) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP document_id, DROP period_start_date, DROP period_end_date, CHANGE bank_account_id bank_account_id INT NOT NULL, CHANGE average_balance average_balance NUMERIC(10, 2) NOT NULL' );
		$this->addSql( 'ALTER TABLE app_file CHANGE type type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE original_name original_name VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE mime_type mime_type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE bank CHANGE name name VARCHAR(150) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE code code VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE bank_account CHANGE name name VARCHAR(100) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE number number VARCHAR(100) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE clabe clabe VARCHAR(18) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE channel CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE client CHANGE name name VARCHAR(150) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE initials initials VARCHAR(8) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE zip zip VARCHAR(5) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE picture picture VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE company_name company_name VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE address address VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE configuration CHANGE value value LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE conversation CHANGE parent_class parent_class VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE ext_log_entries CHANGE action action VARCHAR(8) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE object_id object_id VARCHAR(64) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE object_class object_class VARCHAR(191) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE data data LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\', CHANGE username username VARCHAR(191) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE invoice CHANGE folio folio VARCHAR(50) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE memorandum CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE content content LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE message CHANGE message message LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE monthly_transaction CHANGE concept concept VARCHAR(300) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE municipality CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE neighbourhood CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE postal_code postal_code VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE notification CHANGE html html LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE link link VARCHAR(1024) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE group_name group_name VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE notification_topic CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(1020) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE notification_user_entity CHANGE entity_class entity_class VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE permission CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE permission_group CHANGE id id CHAR(36) NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:guid)\', CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE code code VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(512) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE permission_groups_permissions CHANGE permission_group_id permission_group_id CHAR(36) NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:guid)\'' );
		$this->addSql( 'ALTER TABLE project CHANGE file_container_id file_container_id INT DEFAULT NULL, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(4096) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE provider CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(300) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quotation_request CHANGE title title VARCHAR(300) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quotation_request_product CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quote CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE payment_status payment_status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quote_product CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE role CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE state CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE code code VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE tenant CHANGE alias alias VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE tide_acl CHANGE domain domain VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE object_id object_id VARCHAR(40) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE security_identity security_identity VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'DROP INDEX UNIQ_723705D1B898B7D6 ON transaction' );
		$this->addSql( 'ALTER TABLE transaction DROP bank_transaction_id, CHANGE concept concept VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE transaction_type transaction_type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE comments comments VARCHAR(4096) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE user CHANGE username username VARCHAR(60) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(200) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE email email VARCHAR(60) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE recovery_token recovery_token VARCHAR(200) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(100) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE last_name last_name VARCHAR(100) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE full_name full_name VARCHAR(200) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE position position VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE users_permission_groups CHANGE permission_group_id permission_group_id CHAR(36) NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:guid)\'' );
	}
}
