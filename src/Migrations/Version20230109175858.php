<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230109175858 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE applied_taxes (id INT AUTO_INCREMENT NOT NULL, tax_document_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, amount NUMERIC(10, 4) NOT NULL, factor_type VARCHAR(255) NOT NULL, factor_amount NUMERIC(10, 4) NOT NULL, INDEX IDX_23E469B14C817138 (tax_document_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transferred_taxes (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE applied_taxes ADD CONSTRAINT FK_23E469B14C817138 FOREIGN KEY (tax_document_id) REFERENCES tax_document (id)');
        $this->addSql('ALTER TABLE fiscal_data ADD tenant_fiscal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fiscal_data ADD CONSTRAINT FK_D5D66703A6E2BEB FOREIGN KEY (tenant_fiscal_id) REFERENCES tenant (id)');
        $this->addSql('CREATE INDEX IDX_D5D66703A6E2BEB ON fiscal_data (tenant_fiscal_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE applied_taxes');
        $this->addSql('DROP TABLE transferred_taxes');
        $this->addSql('ALTER TABLE fiscal_data DROP FOREIGN KEY FK_D5D66703A6E2BEB');
        $this->addSql('DROP INDEX IDX_D5D66703A6E2BEB ON fiscal_data');
        $this->addSql('ALTER TABLE fiscal_data DROP tenant_fiscal_id');
    }
}
