<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230214101712 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tenant_rfc (id INT NOT NULL AUTO_INCREMENT , tenant_id INT NOT NULL , rfc VARCHAR(14) NOT NULL , sat_ws_id VARCHAR(64) NULL , sat_ws_status VARCHAR(12) NULL , created_date DATETIME NULL , updated_date DATETIME NULL , created_by INT NULL, updated_by INT NULL, PRIMARY KEY (id), FOREIGN KEY (tenant_id) REFERENCES tenant(id));');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE tenant_rfc;');
    }
}
