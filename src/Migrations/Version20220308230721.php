<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220308230721 extends AbstractMigration {
	public function getDescription(): string {
		return '';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql( 'CREATE TABLE transaction_category (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, tenant_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_483E30A919EB6921 (client_id), INDEX IDX_483E30A99033212A (tenant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB' );
		$this->addSql( 'ALTER TABLE transaction_category ADD CONSTRAINT FK_483E30A919EB6921 FOREIGN KEY (client_id) REFERENCES client (id)' );
		$this->addSql( 'ALTER TABLE transaction_category ADD CONSTRAINT FK_483E30A99033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)' );
		$this->addSql( 'ALTER TABLE transaction ADD transaction_category_id INT DEFAULT NULL, ADD original_amount NUMERIC(14, 2) DEFAULT NULL' );
		$this->addSql( 'ALTER TABLE transaction ADD CONSTRAINT FK_723705D1AECF88CF FOREIGN KEY (transaction_category_id) REFERENCES transaction_category (id)' );
		$this->addSql( 'CREATE INDEX IDX_723705D1AECF88CF ON transaction (transaction_category_id)' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1AECF88CF' );
		$this->addSql( 'DROP TABLE transaction_category' );
		$this->addSql( 'DROP INDEX IDX_723705D1AECF88CF ON transaction' );
		$this->addSql( 'ALTER TABLE transaction DROP transaction_category_id, DROP original_amount' );
	}
}
