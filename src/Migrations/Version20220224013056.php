<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220224013056 extends AbstractMigration {
	public function getDescription(): string {
		return '';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE cassette_user DROP FOREIGN KEY FK_1D60BA03A7F23671' );
		$this->addSql( 'ALTER TABLE comment DROP FOREIGN KEY FK_9474526CA7F23671' );
		$this->addSql( 'ALTER TABLE app_file_comment DROP FOREIGN KEY FK_D9CF1D6F8697D13' );
		$this->addSql( 'DROP TABLE app_file_comment' );
		$this->addSql( 'DROP TABLE cassette' );
		$this->addSql( 'DROP TABLE cassette_user' );
		$this->addSql( 'DROP TABLE comment' );
		$this->addSql( 'ALTER TABLE conversation CHANGE client_id client_id INT NOT NULL' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql( 'CREATE TABLE app_file_comment (id INT AUTO_INCREMENT NOT NULL, app_file_id INT NOT NULL, comment_id INT NOT NULL, tenant_id INT NOT NULL, client_id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_D9CF1D67E96F921 (app_file_id), INDEX IDX_D9CF1D69033212A (tenant_id), INDEX IDX_D9CF1D6DE12AB56 (created_by), INDEX IDX_D9CF1D6F8697D13 (comment_id), INDEX IDX_D9CF1D619EB6921 (client_id), INDEX IDX_D9CF1D616FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ' );
		$this->addSql( 'CREATE TABLE cassette (id INT AUTO_INCREMENT NOT NULL, tenant_id INT NOT NULL, client_id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, entity VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_CC834889033212A (tenant_id), INDEX IDX_CC83488DE12AB56 (created_by), INDEX IDX_CC8348819EB6921 (client_id), INDEX IDX_CC8348816FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ' );
		$this->addSql( 'CREATE TABLE cassette_user (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, cassette_id INT NOT NULL, tenant_id INT NOT NULL, client_id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, notifications_enabled TINYINT(1) DEFAULT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_1D60BA03A7F23671 (cassette_id), INDEX IDX_1D60BA0319EB6921 (client_id), INDEX IDX_1D60BA0316FE72E1 (updated_by), INDEX IDX_1D60BA039033212A (tenant_id), INDEX IDX_1D60BA03DE12AB56 (created_by), INDEX IDX_1D60BA03A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ' );
		$this->addSql( 'CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, cassette_id INT NOT NULL, tenant_id INT NOT NULL, client_id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, is_removed TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_9474526C9033212A (tenant_id), INDEX IDX_9474526CDE12AB56 (created_by), INDEX IDX_9474526C19EB6921 (client_id), INDEX IDX_9474526C16FE72E1 (updated_by), INDEX IDX_9474526CA7F23671 (cassette_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ' );
		$this->addSql( 'ALTER TABLE app_file_comment ADD CONSTRAINT FK_D9CF1D67E96F921 FOREIGN KEY (app_file_id) REFERENCES app_file (id)' );
		$this->addSql( 'ALTER TABLE app_file_comment ADD CONSTRAINT FK_D9CF1D6DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON DELETE SET NULL' );
		$this->addSql( 'ALTER TABLE app_file_comment ADD CONSTRAINT FK_D9CF1D619EB6921 FOREIGN KEY (client_id) REFERENCES client (id)' );
		$this->addSql( 'ALTER TABLE app_file_comment ADD CONSTRAINT FK_D9CF1D69033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)' );
		$this->addSql( 'ALTER TABLE app_file_comment ADD CONSTRAINT FK_D9CF1D6F8697D13 FOREIGN KEY (comment_id) REFERENCES comment (id)' );
		$this->addSql( 'ALTER TABLE app_file_comment ADD CONSTRAINT FK_D9CF1D616FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON DELETE SET NULL' );
		$this->addSql( 'ALTER TABLE cassette ADD CONSTRAINT FK_CC8348816FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON DELETE SET NULL' );
		$this->addSql( 'ALTER TABLE cassette ADD CONSTRAINT FK_CC834889033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)' );
		$this->addSql( 'ALTER TABLE cassette ADD CONSTRAINT FK_CC8348819EB6921 FOREIGN KEY (client_id) REFERENCES client (id)' );
		$this->addSql( 'ALTER TABLE cassette ADD CONSTRAINT FK_CC83488DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON DELETE SET NULL' );
		$this->addSql( 'ALTER TABLE cassette_user ADD CONSTRAINT FK_1D60BA03A7F23671 FOREIGN KEY (cassette_id) REFERENCES cassette (id)' );
		$this->addSql( 'ALTER TABLE cassette_user ADD CONSTRAINT FK_1D60BA0319EB6921 FOREIGN KEY (client_id) REFERENCES client (id)' );
		$this->addSql( 'ALTER TABLE cassette_user ADD CONSTRAINT FK_1D60BA03A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)' );
		$this->addSql( 'ALTER TABLE cassette_user ADD CONSTRAINT FK_1D60BA03DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON DELETE SET NULL' );
		$this->addSql( 'ALTER TABLE cassette_user ADD CONSTRAINT FK_1D60BA0316FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON DELETE SET NULL' );
		$this->addSql( 'ALTER TABLE cassette_user ADD CONSTRAINT FK_1D60BA039033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)' );
		$this->addSql( 'ALTER TABLE comment ADD CONSTRAINT FK_9474526C9033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)' );
		$this->addSql( 'ALTER TABLE comment ADD CONSTRAINT FK_9474526CDE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON DELETE SET NULL' );
		$this->addSql( 'ALTER TABLE comment ADD CONSTRAINT FK_9474526C19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)' );
		$this->addSql( 'ALTER TABLE comment ADD CONSTRAINT FK_9474526CA7F23671 FOREIGN KEY (cassette_id) REFERENCES cassette (id)' );
		$this->addSql( 'ALTER TABLE comment ADD CONSTRAINT FK_9474526C16FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON DELETE SET NULL' );
		$this->addSql( 'ALTER TABLE account_statement CHANGE file file VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE period period VARCHAR(50) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE app_file CHANGE type type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE original_name original_name VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE mime_type mime_type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE bank CHANGE name name VARCHAR(150) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE bank_account CHANGE name name VARCHAR(100) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE number number VARCHAR(100) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE clabe clabe VARCHAR(18) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE channel CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE client CHANGE name name VARCHAR(150) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE initials initials VARCHAR(8) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE zip zip VARCHAR(5) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE picture picture VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE company_name company_name VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE address address VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE configuration CHANGE value value LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE conversation CHANGE client_id client_id INT DEFAULT NULL, CHANGE parent_class parent_class VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE ext_log_entries CHANGE action action VARCHAR(8) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE object_id object_id VARCHAR(64) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE object_class object_class VARCHAR(191) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE data data LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\', CHANGE username username VARCHAR(191) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE memorandum CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE content content LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE message CHANGE message message LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE monthly_transaction CHANGE concept concept VARCHAR(300) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE neighbourhood CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE postal_code postal_code VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE notification CHANGE html html LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE link link VARCHAR(1024) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE group_name group_name VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE notification_topic CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(1020) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE notification_user_entity CHANGE entity_class entity_class VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE permission CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE permission_group CHANGE id id CHAR(36) NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:guid)\', CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE code code VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(512) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE permission_groups_permissions CHANGE permission_group_id permission_group_id CHAR(36) NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:guid)\'' );
		$this->addSql( 'ALTER TABLE project CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(4096) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE provider CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(300) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quotation_request CHANGE title title VARCHAR(300) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quotation_request_product CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quote CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE payment_status payment_status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quote_product CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE role CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE state CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE code code VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE tenant CHANGE alias alias VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE tide_acl CHANGE domain domain VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE object_id object_id VARCHAR(40) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE security_identity security_identity VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE transaction CHANGE concept concept VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE transaction_type transaction_type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE comments comments VARCHAR(4096) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE user CHANGE username username VARCHAR(60) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(200) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE email email VARCHAR(60) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE recovery_token recovery_token VARCHAR(200) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(100) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE last_name last_name VARCHAR(100) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE full_name full_name VARCHAR(200) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE position position VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE users_permission_groups CHANGE permission_group_id permission_group_id CHAR(36) NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:guid)\'' );
	}
}
