<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220303214135 extends AbstractMigration {
	public function getDescription(): string {
		return '';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE recurring_payment ADD repeat_in VARCHAR(500) DEFAULT NULL, ADD next_payment DATE NOT NULL, CHANGE period frequency_type VARCHAR(50) NOT NULL, CHANGE frequency frequency_interval INT NOT NULL' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE recurring_payment DROP repeat_in, DROP next_payment, CHANGE frequency_type period VARCHAR(50) NOT NULL, CHANGE frequency_interval frequency INT NOT NULL' );
	}
}
