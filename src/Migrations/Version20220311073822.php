<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311073822 extends AbstractMigration {
	public function getDescription(): string {
		return 'Add total income and total expense to account statement';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE account_statement ADD total_income NUMERIC(14, 2) DEFAULT NULL, ADD total_expense NUMERIC(14, 2) DEFAULT NULL' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE account_statement DROP total_income, DROP total_expense' );
	}
}
