<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220223204421 extends AbstractMigration {
	public function getDescription(): string {
		return '';
	}

	public function up( Schema $schema ): void {
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql( 'CREATE TABLE invoice (id INT AUTO_INCREMENT NOT NULL, tenant_id INT NOT NULL, folio VARCHAR(50) DEFAULT NULL, INDEX IDX_906517449033212A (tenant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB' );
		$this->addSql( 'ALTER TABLE invoice ADD CONSTRAINT FK_906517449033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)' );
		$this->addSql( 'ALTER TABLE transaction ADD invoice_id INT DEFAULT NULL' );
		$this->addSql( 'ALTER TABLE transaction ADD CONSTRAINT FK_723705D12989F1FD FOREIGN KEY (invoice_id) REFERENCES invoice (id)' );
		$this->addSql( 'CREATE INDEX IDX_723705D12989F1FD ON transaction (invoice_id)' );
	}

	public function down( Schema $schema ): void {
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql( 'ALTER TABLE transaction DROP FOREIGN KEY FK_723705D12989F1FD' );
		$this->addSql( 'DROP TABLE invoice' );
		$this->addSql( 'ALTER TABLE account_statement CHANGE file file VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE period period VARCHAR(50) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE app_file CHANGE type type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE original_name original_name VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE mime_type mime_type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE bank CHANGE name name VARCHAR(150) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE bank_account CHANGE name name VARCHAR(100) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE number number VARCHAR(100) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE clabe clabe VARCHAR(18) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE cassette CHANGE entity entity VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE client CHANGE name name VARCHAR(150) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE initials initials VARCHAR(8) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE zip zip VARCHAR(5) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE picture picture VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE company_name company_name VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE address address VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE configuration CHANGE value value LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE ext_log_entries CHANGE action action VARCHAR(8) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE object_id object_id VARCHAR(64) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE object_class object_class VARCHAR(191) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE data data LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\', CHANGE username username VARCHAR(191) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE memorandum CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE content content LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE monthly_transaction CHANGE concept concept VARCHAR(300) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE municipality CHANGE state_id state_id INT DEFAULT NULL, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE neighbourhood CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE postal_code postal_code VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE notification CHANGE html html LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE link link VARCHAR(1024) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE group_name group_name VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE notification_topic CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(1020) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE notification_user_entity CHANGE entity_class entity_class VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE permission CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE permission_group CHANGE id id CHAR(36) NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:guid)\', CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE code code VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(512) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE permission_groups_permissions CHANGE permission_group_id permission_group_id CHAR(36) NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:guid)\'' );
		$this->addSql( 'ALTER TABLE project CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(4096) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE provider CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(300) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quotation_request CHANGE title title VARCHAR(300) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quotation_request_product CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quote CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE payment_status payment_status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE quote_product CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE role CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE state CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE code code VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE tenant CHANGE alias alias VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE tide_acl CHANGE domain domain VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE object_id object_id VARCHAR(40) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE security_identity security_identity VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'DROP INDEX IDX_723705D12989F1FD ON transaction' );
		$this->addSql( 'ALTER TABLE transaction DROP invoice_id, CHANGE concept concept VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE folio folio VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE comments comments VARCHAR(4096) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE user CHANGE username username VARCHAR(60) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(200) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE email email VARCHAR(60) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE recovery_token recovery_token VARCHAR(200) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(100) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE last_name last_name VARCHAR(100) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE full_name full_name VARCHAR(200) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE position position VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`' );
		$this->addSql( 'ALTER TABLE users_permission_groups CHANGE permission_group_id permission_group_id CHAR(36) NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:guid)\'' );
	}
}
