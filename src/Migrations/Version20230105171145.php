<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230105171145 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ctax (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, average DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_7917E49577153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tax_document (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, tenant_id INT NOT NULL, xml_file_id INT DEFAULT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, document_type VARCHAR(255) DEFAULT NULL, uuid_folio_fiscal VARCHAR(255) DEFAULT NULL, folio VARCHAR(255) DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, payment_type VARCHAR(255) DEFAULT NULL, payment_method VARCHAR(255) DEFAULT NULL, is_issuer TINYINT(1) NOT NULL, currency VARCHAR(5) NOT NULL, discount NUMERIC(14, 4) DEFAULT NULL, tax NUMERIC(14, 4) NOT NULL, retained_taxes NUMERIC(14, 4) NOT NULL, transferred_taxes NUMERIC(14, 4) NOT NULL, subtotal NUMERIC(14, 4) NOT NULL, total NUMERIC(14, 4) NOT NULL, paid_amount NUMERIC(14, 4) NOT NULL, due_amount NUMERIC(14, 4) NOT NULL, last_payment_date DATETIME DEFAULT NULL, fully_paid_at DATETIME DEFAULT NULL, canceled_at DATETIME DEFAULT NULL, issued_at DATETIME NOT NULL, rfc VARCHAR(255) DEFAULT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_38FD2BC8D175AA0C (uuid_folio_fiscal), INDEX IDX_38FD2BC819EB6921 (client_id), INDEX IDX_38FD2BC89033212A (tenant_id), UNIQUE INDEX UNIQ_38FD2BC83389B852 (xml_file_id), INDEX IDX_38FD2BC8DE12AB56 (created_by), INDEX IDX_38FD2BC816FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tax_document_item (id INT AUTO_INCREMENT NOT NULL, tax_document_id INT DEFAULT NULL, tenant_id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, product_identification VARCHAR(255) DEFAULT NULL, description VARCHAR(500) DEFAULT NULL, unit_amount NUMERIC(14, 4) NOT NULL, unit_code VARCHAR(10) DEFAULT NULL, discount_amount NUMERIC(10, 4) NOT NULL, tax_amount NUMERIC(10, 4) NOT NULL, price NUMERIC(10, 4) NOT NULL, iva NUMERIC(10, 4) NOT NULL, ieps NUMERIC(10, 4) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_46EB82584C817138 (tax_document_id), INDEX IDX_46EB82589033212A (tenant_id), INDEX IDX_46EB8258DE12AB56 (created_by), INDEX IDX_46EB825816FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tax_document ADD CONSTRAINT FK_38FD2BC819EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE tax_document ADD CONSTRAINT FK_38FD2BC89033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)');
        $this->addSql('ALTER TABLE tax_document ADD CONSTRAINT FK_38FD2BC83389B852 FOREIGN KEY (xml_file_id) REFERENCES app_file (id)');
        $this->addSql('ALTER TABLE tax_document ADD CONSTRAINT FK_38FD2BC8DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tax_document ADD CONSTRAINT FK_38FD2BC816FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tax_document_item ADD CONSTRAINT FK_46EB82584C817138 FOREIGN KEY (tax_document_id) REFERENCES tax_document (id)');
        $this->addSql('ALTER TABLE tax_document_item ADD CONSTRAINT FK_46EB82589033212A FOREIGN KEY (tenant_id) REFERENCES tenant (id)');
        $this->addSql('ALTER TABLE tax_document_item ADD CONSTRAINT FK_46EB8258DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tax_document_item ADD CONSTRAINT FK_46EB825816FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE client ADD last_name VARCHAR(255) DEFAULT NULL, ADD second_last_name VARCHAR(255) DEFAULT NULL, ADD curp VARCHAR(255) DEFAULT NULL, ADD birthday VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE quote ADD tax_document_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE quote ADD CONSTRAINT FK_6B71CBF44C817138 FOREIGN KEY (tax_document_id) REFERENCES tax_document (id)');
        $this->addSql('CREATE INDEX IDX_6B71CBF44C817138 ON quote (tax_document_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE quote DROP FOREIGN KEY FK_6B71CBF44C817138');
        $this->addSql('ALTER TABLE tax_document_item DROP FOREIGN KEY FK_46EB82584C817138');
        $this->addSql('DROP TABLE ctax');
        $this->addSql('DROP TABLE tax_document');
        $this->addSql('DROP TABLE tax_document_item');
        $this->addSql('ALTER TABLE client DROP last_name, DROP second_last_name, DROP curp, DROP birthday');
        $this->addSql('DROP INDEX IDX_6B71CBF44C817138 ON quote');
        $this->addSql('ALTER TABLE quote DROP tax_document_id');
    }
}
