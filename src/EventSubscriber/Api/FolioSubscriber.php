<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Helpers\FolioHelper;
use App\Interfaces\FolioInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class FolioSubscriber implements EventSubscriberInterface {
	public function __construct( private FolioHelper $folioHelper, private EntityManagerInterface $em ) {
	}

	public static function getSubscribedEvents(): array {
		return [
			KernelEvents::VIEW => [
				[ 'preWrite', EventPriorities::PRE_WRITE ]
			],
		];
	}

	public function preWrite( ViewEvent $event ) {
		$entity = $event->getControllerResult();
		$method = $event->getRequest()->getMethod();

		if ( ! $entity instanceof FolioInterface ) {
			return;
		}

		if ( $method === Request::METHOD_POST ) {
			$this->folioHelper->createAndSetNextFolio( $entity );
		}
	}
}
