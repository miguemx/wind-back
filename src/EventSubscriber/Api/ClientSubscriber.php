<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Client;
use App\Entity\Tenant;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Helpers\{UserManager, InitialsHelper};
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ClientSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface  $entityManager
     */
    private $em;
	private $userManager;
	private LoggerInterface $logger;
	private $initialsHelper;
	private $tokenStorage;

    public function __construct(EntityManagerInterface $entityManager, UserManager $userManager, LoggerInterface $logger, InitialsHelper $initialsHelper, TokenStorageInterface $tokeInterface)
	{
		$this->em = $entityManager;
		$this->userManager = $userManager;
		$this->logger = $logger;
		$this->initialsHelper = $initialsHelper;
		$this->tokenStorage = $tokeInterface;
    }

	public static function getSubscribedEvents(): array {
		return [
			KernelEvents::VIEW => [
				[ 'preWrite', EventPriorities::PRE_WRITE ],
				[ 'postWrite', EventPriorities::POST_WRITE ]
			],
		];
	}

	public function preWrite( ViewEvent $event ) {
		$client            = $event->getControllerResult();
		$method            = $event->getRequest()->getMethod();
		$requestParameters = json_decode( $event->getRequest()->getContent(), null );

		if (!$client instanceof Client )
			return;

		if ($method === Request::METHOD_POST){
			// Set initials if not set
			if (!$client->getInitials()){
				$tenant = null;

				if($this->tokenStorage->getToken()->getUser()->getTenant()) {
					$tenant = $this->tokenStorage->getToken()->getUser()->getTenant();
				} else {
					$tenant = $client->getTenant();
				}

				if(!$tenant)
					throw new HttpException(400, 'Se debe enviar el tenant para crear el cliente');

				$initials = $this->initialsHelper->generateInitials($client->getName(), $tenant);
				$client->setInitials( $initials );
			}
		}
	}

	public function postWrite( ViewEvent $event ) {
		$client            = $event->getControllerResult();
		$method            = $event->getRequest()->getMethod();
		$requestParameters = json_decode( $event->getRequest()->getContent(), null );

		if ( ! $client instanceof Client ) {
			return;
		}

		$requestParameters = json_decode($event->getRequest()->getContent());

		if ( Request::METHOD_POST == $method ) {  // create user
			if ( $requestParameters->sendAccess ) {
				$user = $client->getUsers()[0];

				try {
					$this->userManager->sendCredentials( $user );
				} catch ( \Exception $e ) {
					$this->logger->error( "Error sending credentials to client {$user->getEmail()}: {$e->getMessage()}" );
				}
			}
		}
	}
}
