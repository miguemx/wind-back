<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use App\Helpers\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final class UserSubscriber implements EventSubscriberInterface {

	public function __construct(
		private UserManager $userManager,
		private EntityManagerInterface $entityManager,
		private AuthorizationCheckerInterface $authorizationChecker,
		private TokenStorageInterface $tokenStorage
	) {
	}

	public static function getSubscribedEvents() {
		return [
			KernelEvents::VIEW => [
				[ 'preWrite', EventPriorities::PRE_WRITE ],
				[ 'postWrite', EventPriorities::POST_WRITE ]
			]
		];
	}

	public function preWrite( ViewEvent $event ) {
		$user              = $event->getControllerResult();
		$method            = $event->getRequest()->getMethod();
		$requestParameters = json_decode( $event->getRequest()->getContent(), null );

		$currentUser = $this->tokenStorage->getToken()->getUser();

		if ( Request::METHOD_PUT === $method ) {
			if ( $currentUser->getClient() ) {
				if ( isset( $requestParameters->client ) ) {
					throw new HttpException( 403, 'No tienes permisos para actualizar el cliente.' );
				}
			}
		}

		if ( Request::METHOD_PUT === $method && @$requestParameters->permissionGroups ) {
			if ( ! $this->authorizationChecker->isGranted( 'USER_PERMISSION_GROUP_UPDATE' ) ) {
				throw new HttpException( 403, 'No tienes permisos para actualizar los permisos de un usuario' );
			}
		}

		if ( ! $user instanceof User || Request::METHOD_DELETE == $method ) {
			return;
		}

		if ( @$requestParameters->password ) {
			$user->setPlainPassword( $user->getPassword() );
			$user->setPassword( $this->userManager->encodePassword( $user, $user->getPassword() ) );
		}
	}

	public function postWrite( ViewEvent $event ) {
		$user   = $event->getControllerResult();
		$method = $event->getRequest()->getMethod();

		if ( ! $user instanceof User || Request::METHOD_DELETE == $method ) {
			return;
		}

		$requestParameters = json_decode( $event->getRequest()->getContent(), null );

		if ( Request::METHOD_POST == $method ) {  // create user
			if ( $requestParameters->sendAccess ) {
				$this->userManager->sendCredentials( $user );
			}
		}
	}
}
