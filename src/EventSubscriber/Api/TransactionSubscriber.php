<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Transaction;
use App\Helpers\TransactionsHelper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class TransactionSubscriber implements EventSubscriberInterface {
	public function __construct( private TransactionsHelper $transactionsHelper, TokenStorageInterface $token, LoggerInterface $logger, EntityManagerInterface $entityManager ) {
		$this->logger = $logger;
		$this->em     = $entityManager;

		if ( $token->getToken() && $token->getToken()->getUser() instanceof User ) {
			$this->user = $token->getToken()->getUser();
		}
	}

	public static function getSubscribedEvents(): array {
		return [
			KernelEvents::VIEW => [
				[ 'preWrite', EventPriorities::PRE_WRITE ],
				[ 'postWrite', EventPriorities::POST_WRITE ]
			],
		];
	}

	public function preWrite( ViewEvent $event ) {
		$transaction = $event->getControllerResult();
		$method      = $event->getRequest()->getMethod();
      $uow = $this->em->getUnitOfWork();

		if ( ! $transaction instanceof Transaction ) {
			return;
		}

      if($method === Request::METHOD_POST){
        $this->transactionsHelper->updateTransactionQuotesStatus($transaction);
      }

      // set initial originalAmount, update balance in accounts for new transaction
      if($method === Request::METHOD_POST && $transaction->getBankAccount() && Transaction::TRANSACTION_STATUS_APPROVED === $transaction->getStatus()){
        $this->updateAccountAmount($transaction);
      }


      // set initial originalAmount, update balance in accounts for updated transactions if new status is approved
      if($method === Request::METHOD_PUT && $transaction->getBankAccount() && Transaction::TRANSACTION_STATUS_APPROVED === $transaction->getStatus()){
        $oldTransaction = $uow->getOriginalEntityData($transaction);

        if($oldTransaction["status"]!= $transaction->getStatus()){
          $this->updateAccountAmount($transaction);
        }
      }

      // return amount to account if transaction is rejected and previous status was approved
      if($method === Request::METHOD_PUT && $transaction->getBankAccount() && Transaction::TRANSACTION_STATUS_APPROVED !== $transaction->getStatus()){
        $oldTransaction = $uow->getOriginalEntityData($transaction);

        // if transaction was approved before, return amount to account
        if($oldTransaction["status"]===Transaction::TRANSACTION_STATUS_APPROVED && $oldTransaction["status"]!=$transaction->getStatus()){
          // fix amount for expenses
          if($transaction->getTransactionType() === Transaction::TRANSACTION_TYPE_EXPENSE && $transaction->getAmount()>0){
            $amount = $amount * -1;
            $transaction->setAmount($amount);
          }

          $transaction->getBankAccount()->setBalance($transaction->getBankAccount()->getBalance() - $transaction->getAmount());
        }
      }
    }

    private function updateAccountAmount($transaction)
    {
      // update originalAmount
      $accountOriginalAmount = $transaction->getBankAccount()->getBalance();
      $transaction->setOriginalAmount($accountOriginalAmount);

      // update account balance
      $amount = $transaction->getAmount();

      // fix amount for expenses
      if($transaction->getTransactionType() === Transaction::TRANSACTION_TYPE_EXPENSE && $transaction->getAmount()>0){
        $amount = $amount * -1;
        $transaction->setAmount($amount);
      }

      $transaction->getBankAccount()->setBalance($accountOriginalAmount + $amount);
    }

    public function postWrite(ViewEvent $event){
		$transaction = $event->getControllerResult();
		$method      = $event->getRequest()->getMethod();

		if ( ! $transaction instanceof Transaction ) {
			return;
		}

		if ( $method === Request::METHOD_POST || $method === Request::METHOD_PUT || $method === Request::METHOD_DELETE ) {
			try {
				$this->transactionsHelper->updateProjectsStatsByTransaction( $transaction );
			} catch ( \Exception $e ) {
				$this->logger->error( 'TransactionSubscriber - Could not compute stats for quotes: ' . $e->getMessage() );
			}
		}
	}
}
