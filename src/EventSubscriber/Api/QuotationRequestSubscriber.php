<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\QuotationRequest;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class QuotationRequestSubscriber implements EventSubscriberInterface {

	public function __construct(
		TokenStorageInterface $token,
		LoggerInterface $logger,
		EntityManagerInterface $entityManager
	) {
		$this->logger = $logger;
		$this->em     = $entityManager;

		if ( $token->getToken() && $token->getToken()->getUser() instanceof User ) {
			$this->user = $token->getToken()->getUser();
		}
	}

	public static function getSubscribedEvents() {
		return [ KernelEvents::VIEW => [ 'preWrite', EventPriorities::PRE_WRITE ] ];
	}

	public function preWrite( ViewEvent $event ) {
		$quotationRequest = $event->getControllerResult();
		$method           = $event->getRequest()->getMethod();

		if ( ! $quotationRequest instanceof QuotationRequest ) {
			return;
		}

		// Update quotation request project updatedDate if method is POST OR Put
		if ( $method === Request::METHOD_PUT || $method === Request::METHOD_POST ) {
			$project = $quotationRequest->getProject();
			$project->setUpdatedDate( new \DateTime() );
		}

	}
}
