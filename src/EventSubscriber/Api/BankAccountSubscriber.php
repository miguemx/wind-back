<?php
namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Entity\BankAccount;
use App\Helpers\BankAccountHelper;
use Symfony\Component\HttpKernel\Exception\HttpException;

final class BankAccountSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $em;
    private BankAccountHelper $bankAccountHelper;

    public function __construct(EntityManagerInterface $entityManager, BankAccountHelper $bankAccountHelper)
    {
        $this->em = $entityManager;
        $this->bankAccountHelper = $bankAccountHelper;
    }

	public static function getSubscribedEvents(): array
    {
		return [
			KernelEvents::VIEW => [
                ['preWrite', EventPriorities::PRE_WRITE]
            ],
		];
	}

	public function preWrite(ViewEvent $event)
	{
		$entity = $event->getControllerResult();
		$method = $event->getRequest()->getMethod();

		if (!$entity instanceof BankAccount)
            return;

        if($method === Request::METHOD_POST || $method === Request::METHOD_PUT){
            $entity->setClabe($this->bankAccountHelper->cleanString($entity->getClabe()));

            if(!$this->bankAccountHelper->isValidCLABE($entity->getClabe())){
                throw new HttpException(400, "La CLABE no es válida");
            }
        
            $entity->setNumber($this->bankAccountHelper->cleanString($entity->getNumber()));
            
            if(!$this->bankAccountHelper->isValidAccountNumber($entity->getNumber())){
                throw new HttpException(400, "El número de cuenta no es válido");
            }
        }
	}
}
