<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Message;
use App\Entity\User;
use App\Helpers\ConversationHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


final class MessageSubscriber implements EventSubscriberInterface {
	private EntityManagerInterface $entityManager;

	private User $user;

	public function __construct( EntityManagerInterface $entityManager, TokenStorageInterface $token, private ConversationHelper $conversationHelper ) {
		$this->em = $entityManager;

		if ( $token->getToken() && $token->getToken()->getUser() instanceof User ) {
			$this->user = $token->getToken()->getUser();
		}
	}

	public static function getSubscribedEvents(): array {
		return [
			KernelEvents::VIEW => [
				[ 'preWrite', EventPriorities::PRE_WRITE ]
			],
		];
	}

	public function preWrite( ViewEvent $event ) {
		$message = $event->getControllerResult();
		$method  = $event->getRequest()->getMethod();

		if ( ! $message instanceof Message ) {
			return;
		}

		if ( $method === Request::METHOD_POST ) {
			// Validate if the user have permission to send messages to this conversation
			$conversation = $message->getChannel()->getConversation();

			if ( $this->conversationHelper->canSendMessagesToConversation( $this->user, $message->getChannel() ) ) {
				$message->setUser( $this->user );

				$client = $this->em->getRepository( $message->getChannel()->getConversation()->getParentClass() )
				                   ->findBy( [ 'conversation' => $message->getChannel()->getConversation() ] )[0]
					->getClient();
				$message->setClient( $client );
			} else {
				throw new \Exception( 'You have not permissions send messages to this conversation' );
			}
		}
	}
}
