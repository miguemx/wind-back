<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Project;
use App\Helpers\ProjectHelper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ProjectSubscriber implements EventSubscriberInterface {

	public function __construct( private ProjectHelper $projectHelper, private LoggerInterface $logger, private EntityManagerInterface $em ) {
	}

	public static function getSubscribedEvents(): array {
		return [
			KernelEvents::VIEW => [
				[ 'preWrite', EventPriorities::PRE_WRITE ],
				[ 'postWrite', EventPriorities::POST_WRITE ]
			]
		];
	}

	public function preWrite( ViewEvent $event ) {
		$project = $event->getControllerResult();
		$method  = $event->getRequest()->getMethod();

		if ( ! $project instanceof Project ) {
			return;
		}

		if ( $method === Request::METHOD_POST ) {
			$this->projectHelper->createConversation( $project );
		}
	}

	public function postWrite( ViewEvent $event ) {
		$project = $event->getControllerResult();
		$method  = $event->getRequest()->getMethod();

		if ( ! $project instanceof Project ) {
			return;
		}

		if ( $method === Request::METHOD_POST || $method === Request::METHOD_PUT || $method === Request::METHOD_DELETE ) {
			try {
				$this->projectHelper->computeStatsAfterChange( $project, true );
			} catch ( \Exception $e ) {
				$this->logger->error( 'Could not compute stats after project change: ' . $e->getMessage() );
			}
		}
	}
}
