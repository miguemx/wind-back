<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\AccountStatement;
use App\Helpers\AccountStatementsHelper;
use App\Helpers\PdfReader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class AccountStatementSubscriber implements EventSubscriberInterface {
	public function __construct( private EntityManagerInterface $em, private LoggerInterface $logger, private PdfReader $pdfReader, private AccountStatementsHelper $accountStatementsHelper ) {
	}

	public static function getSubscribedEvents(): array {
		return [
			KernelEvents::VIEW => [
				[ 'preWrite', EventPriorities::PRE_WRITE ]
			],
		];
	}

	public function preWrite( ViewEvent $event ) {
		$accountStatement  = $event->getControllerResult();
		$method            = $event->getRequest()->getMethod();
		$requestParameters = json_decode( $event->getRequest()->getContent(), null );

		if ( ! $accountStatement instanceof AccountStatement ) {
			return;
		}

		if ( $method === Request::METHOD_POST ) {
			$document = $event->getRequest()->files->get( 'files' )[0];

			$this->accountStatementsHelper->buildAccountStatementFromPDF( $accountStatement, $document );
		}
	}
}
