<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Memorandum;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class MemorandumSubscriber implements EventSubscriberInterface {

	public function __construct(
		TokenStorageInterface $token,
		LoggerInterface $logger,
		EntityManagerInterface $entityManager
	) {
		$this->logger = $logger;
		$this->em     = $entityManager;

		if ( $token->getToken() && $token->getToken()->getUser() instanceof User ) {
			$this->user = $token->getToken()->getUser();
		}
	}

	public static function getSubscribedEvents() {
		return [ KernelEvents::VIEW => [ 'preWrite', EventPriorities::PRE_WRITE ] ];
	}

	public function preWrite( ViewEvent $event ) {
		$memorandum = $event->getControllerResult();
		$method     = $event->getRequest()->getMethod();

		if ( ! $memorandum instanceof Memorandum ) {
			return;
		}

		// Update memorandum project updatedDate if method is POST or PUT
		if ( $method === Request::METHOD_PUT || $method === Request::METHOD_POST ) {
			$project = $memorandum->getProject();
			$project->setUpdatedDate( new \DateTime() );
		}

	}
}

