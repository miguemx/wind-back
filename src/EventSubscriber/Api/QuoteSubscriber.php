<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Quote;
use App\Entity\User;
use App\Helpers\ProjectHelper;
use App\Helpers\QuoteHelper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class QuoteSubscriber implements EventSubscriberInterface {

	private User $user;

	private FolioHelper $folioHelper;

	public function __construct(
		private ProjectHelper $projectHelper,
		private QuoteHelper $quoteHelper,
		TokenStorageInterface $token,
		private LoggerInterface $logger,
		EntityManagerInterface $entityManager
	) {
		$this->em = $entityManager;

		if ( $token->getToken() && $token->getToken()->getUser() instanceof User ) {
			$this->user = $token->getToken()->getUser();
		}
	}

	public static function getSubscribedEvents(): array {
		return [
			KernelEvents::VIEW => [
				[ 'preWrite', EventPriorities::PRE_WRITE ],
				[ 'postWrite', EventPriorities::POST_WRITE ]
			],
		];
	}

	public function preWrite( ViewEvent $event ) {
		$quote  = $event->getControllerResult();
		$method = $event->getRequest()->getMethod();
		$uow    = $this->em->getUnitOfWork();

		if ( ! $quote instanceof Quote ) {
			return;
		}

		//Set reviewed by and date if the new quote is already approved
		if ( $method === Request::METHOD_PUT ) {
			$oldQuote = $uow->getOriginalEntityData( $quote );

			if ( $oldQuote['status'] !== $quote->getStatus() ) {
				$this->quoteHelper->updateQuoteReviewFields( $quote, $this->user );
			}
		}

		//Update quote project updatedDate if method put or post
		if ( $method === Request::METHOD_PUT || $method === Request::METHOD_POST ) {
			$project = $quote->getProject();
			$project->setUpdatedDate( new \DateTime() );
		}

	}

	public function postWrite( ViewEvent $event ) {
		$quote  = $event->getControllerResult();
		$method = $event->getRequest()->getMethod();

		if ( ! $quote instanceof Quote ) {
			return;
		}

		if ( $method === Request::METHOD_POST || $method === Request::METHOD_PUT || $method === Request::METHOD_DELETE ) {
			try {
				$this->projectHelper->computeStatsAfterChange( $quote->getProject(), true );
			} catch ( \Exception $e ) {
				$this->logger->error( 'Could not compute stats after project change: ' . $e->getMessage() );
			}
		}
	}
}
