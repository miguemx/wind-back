<?php
namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Entity\Tenant;
use App\Entity\FiscalData;
use App\Helpers\SatWsHelper;
use Symfony\Component\HttpKernel\Exception\HttpException;

final class TenantSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $em;
    private SatWsHelper $satWsHelper;

    public function __construct(EntityManagerInterface $entityManager, SatWsHelper $satWsHelper)
    {
        $this->em = $entityManager;
        $this->satWsHelper = $satWsHelper;
    }

	public static function getSubscribedEvents(): array
    {
		return [
			KernelEvents::VIEW => [
                ['postWrite', EventPriorities::POST_WRITE]
            ],
		];
	}

	public function postWrite(ViewEvent $event)
	{
		$entity = $event->getControllerResult();
		$method = $event->getRequest()->getMethod();

		if (!$entity instanceof Tenant)
            return;
            
        if($method === Request::METHOD_PUT || $method === Request::METHOD_POST){
            $requestFiscalDatas = json_decode($event->getRequest()->getContent(), true);
            
            if(isset($requestFiscalDatas["fiscalDatas"])) {
                $this->satWsHelper->configureRfcsInSatWs($requestFiscalDatas["fiscalDatas"], $entity);
            }
        }
	}
}
