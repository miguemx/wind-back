<?php

namespace App\EventSubscriber\Doctrine;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

// for Doctrine < 2.4: use Doctrine\ORM\Event\LifecycleEventArgs;

class TenantBindingSubscriber implements EventSubscriber {

	public function __construct( private TokenStorageInterface $tokenStorage ) {
	}

	public function getSubscribedEvents() {
		return [
			Events::prePersist
		];
	}

	public function prePersist( LifecycleEventArgs $args ) {
		if ( ! $this->tokenStorage->getToken() ) {
			return;
		}

		$user   = $this->tokenStorage->getToken()->getUser();
		$entity = $args->getObject();

		if ( ! method_exists( $entity::class, 'setTenant' ) ) {
			return;
		}

		if ( $user->getTenant() ) {
			$entity->setTenant( $user->getTenant() );

			return;
		}

		throw new \Exception( "Could not obtain the tenant for user " . $user->getUserIdentifier() );
	}

}