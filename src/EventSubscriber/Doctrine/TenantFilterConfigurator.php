<?php

namespace App\EventSubscriber\Doctrine;

use App\Entity\User;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class TenantFilterConfigurator {
	public function __construct( private EntityManagerInterface $em, private TokenStorageInterface $tokenStorage, private Reader $reader ) {
	}

	public function onKernelRequest(): void {
		/**
		 * @var User $user
		 */
		if ( ! $user = $this->getUser() ) {
			return;
		}

		if ( $user->getRole() && $user->getRole()->getName() === "SUPER_ADMIN" ) {
			return;
		}

		if ( ! $user->getTenant() ) {
			throw new \Exception( 'Usuario con id ' . $user->getId() . ' no tiene tenant y no es administrador' );
		}

		$filter = $this->em->getFilters()->enable( 'tenant_filter' );
		$filter->setParameter( 'id', $user->getTenant()->getId() );
		$filter->setAnnotationReader( $this->reader );
	}

	private function getUser(): ?UserInterface {
		if ( ! $token = $this->tokenStorage->getToken() ) {
			return null;
		}
		$user = $token->getUser();

		return $user instanceof UserInterface ? $user : null;
	}
}