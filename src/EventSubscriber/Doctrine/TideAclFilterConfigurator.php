<?php

namespace App\EventSubscriber\Doctrine;

use App\Doctrine\Filter\TideAclFilter;
use App\Entity\User;
use App\Helpers\TideAclHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class TideAclFilterConfigurator {
	public function __construct( private EntityManagerInterface $em, private TokenStorageInterface $tokenStorage, private TideAclHelper $tideAclHelper ) {
	}

	public function onKernelRequest( RequestEvent $event ): void {
		/**
		 * @var User $user
		 */
		if ( ! $user = $this->getUser() ) {
			return;
		}

		if ( $user->getRole() && $user->getRole()->getName() === "SUPER_ADMIN" ) {
			return;
		}

		//Disable filter on api platform item operations
		if ( $event->getRequest()->attributes->get( '_api_item_operation_name' ) ) {
			return;
		}

		/**
		 * @var TideAclFilter $filter
		 */
		$filter = $this->em->getFilters()->enable( 'tide_acl_filter' );
		$filter->setTideAclHelper( $this->tideAclHelper );
		$filter->setUser( $user );
		$filter->setEm( $this->em );

	}

	private function getUser(): ?UserInterface {
		if ( ! $token = $this->tokenStorage->getToken() ) {
			return null;
		}
		$user = $token->getUser();

		return $user instanceof UserInterface ? $user : null;
	}
}