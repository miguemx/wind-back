<?php

namespace App\EventSubscriber\Security;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTCreatedSubscriber implements EventSubscriberInterface
{
    const REMEMBER_ME_EXPIRATION_DAYS = 30;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }
    
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            Events::JWT_CREATED => 'onJWTCreated',
        ];
    }

    /**
     * @param JWTCreatedEvent $event
     * @throws \Exception
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();

        if( $request->request->get( '_remember_me' ) ) {
            $rememberMe = $request->request->get( '_remember_me' );

            if( $rememberMe === 'true' ) {
                $expiration = new \DateTime('+' . self::REMEMBER_ME_EXPIRATION_DAYS . ' days');

                $payload = $event->getData();
                $payload['exp'] = $expiration->getTimestamp();

                $event->setData($payload);
            }
        }
    }
}