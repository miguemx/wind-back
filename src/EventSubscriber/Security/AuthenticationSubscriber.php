<?php

namespace App\EventSubscriber\Security;

use App\Helpers\UserManager;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;

final class AuthenticationSubscriber implements EventSubscriberInterface {
	public function __construct( private UserManager $userManager, private RequestStack $requestStack ) {
	}

	public static function getSubscribedEvents() {
		return [
			Events::AUTHENTICATION_SUCCESS => [ 'onJWTAuthenticationSuccess', 3 ],
			Events::AUTHENTICATION_FAILURE => [ 'onJWTAuthenticationFailure', 10 ]
		];
	}

	public function onJWTAuthenticationSuccess( AuthenticationSuccessEvent $event ) {
		$response   = $event->getResponse();
		$data       = $event->getData();
		$expiration = new \DateTime();
		$expiration->add( new \DateInterval( 'P2D' ) );
		$response->headers->setCookie( new Cookie( 'token', $data['token'], $expiration ) );
		$response->headers->set( 'Access-Control-Allow-Credentials', 'true' );
	}

	public function onJWTAuthenticationFailure( AuthenticationFailureEvent $event ) {
		$response = $event->getResponse();
		$response->headers->set( 'Access-Control-Allow-Credentials', 'true' );
	}


}