<?php

namespace App\Doctrine\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\TideAcl;
use App\Entity\User;
use App\Interfaces\AclEnabledInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use ReflectionClass;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class AclExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface {
	public function __construct( private TokenStorageInterface $tokenStorage, private EntityManagerInterface $em ) {
	}

	/**
	 * {@inheritdoc}
	 */
	public function applyToCollection( QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null ) {
		$this->addWhere( $queryBuilder, $resourceClass );
	}

	public function applyToItem( QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = [] ) {
		return;
	}


	private function addWhere( QueryBuilder $qb, string $resourceClass ) {
		return;

		$reflectionClass = new ReflectionClass( $resourceClass );
		if ( ! $reflectionClass->implementsInterface( AclEnabledInterface::class ) ) {
			return;
		}


		/**
		 * @var User $user
		 */
		$user = $this->tokenStorage->getToken()->getUser();

		$rootAlias          = $qb->getRootAliases()[0];
		$innerJoinCondition = sprintf( '%s.id= acl.objectId', $rootAlias );
		$qb->innerJoin( TideAcl::class, 'acl', 'WITH', $innerJoinCondition );
		$qb->where( 'acl.securityIdentity = :identity' )
		   ->setParameter( 'identity', 'U:' . $user->getId() )
		   ->andWhere( 'BIT_AND(acl.permissionMask,1)>0' );

	}


}