<?php

namespace App\Doctrine\Filter;

use App\Annotation\TenantAwareAnnotation;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

final class TenantFilter extends SQLFilter {
	private $reader;

	public function addFilterConstraint( ClassMetadata $targetEntity, $targetTableAlias ): string {
		if ( null === $this->reader ) {
			throw new \RuntimeException( sprintf( 'An annotation reader must be provided. Be sure to call "%s::setAnnotationReader()".', self::class ) );
		}

		// The Doctrine filter is called for any query on any entity
		// Check if the current entity is "user aware" (marked with an annotation)
		$tenantAware = null;

		$reflection = $targetEntity->getReflectionClass();		
		$classAttributes = $reflection->getAttributes();
		
		foreach ( $classAttributes as $attribute ) {

			if ( $attribute->getName() === 'App\Annotation\TenantAwareAnnotation' ) {
				$tenantAware = $attribute->getArguments();
			}
		}

		if ( ! $tenantAware ) {
			return '';
		}		

		$fieldName = $tenantAware["tenantFieldName"];
		try {
			// Don't worry, getParameter automatically escapes parameters
			$tenantId = $this->getParameter( 'id' );
		} catch ( \InvalidArgumentException ) {
			// No user id has been defined
			return '';
		}

		if ( empty( $fieldName ) || empty( $tenantId ) ) {
			return '';
		}

		return sprintf( '%s.%s = %s', $targetTableAlias, $fieldName, $tenantId );
	}

	public function setAnnotationReader( Reader $reader ): void {
		$this->reader = $reader;
	}
}