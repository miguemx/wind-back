<?php

namespace App\Doctrine\Filter;

use App\Entity\TideAcl;
use App\Entity\User;
use App\Helpers\TideAclHelper;
use App\Interfaces\AclEnabledInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use ReflectionClass;

final class TideAclFilter extends SQLFilter {

	private ?\App\Helpers\TideAclHelper $tideAclHelper = null;

	private ?\App\Entity\User $user = null;

	/**
	 * @var EntityManagerInterface $em
	 */
	private $em;

	private $sqlConditions;


	public function addFilterConstraint( ClassMetadata $targetEntity, $targetTableAlias ): string {
		$reflectionClass = $targetEntity->getReflectionClass();
		if ( ! $reflectionClass->implementsInterface( AclEnabledInterface::class ) ) {
			return '';
		}

		$conditions = [];
		if ( $this->em && $this->user ) {
			//Prevents filter recursive calls
			$this->em->getFilters()->disable( 'tide_acl_filter' );
			if ( ! isset( $this->sqlConditions[ $reflectionClass->getName() ] ) ) {
				$this->sqlConditions[ $reflectionClass->getName() ] = $this->getSqlConditionsForEntity( $this->user, $reflectionClass );
			}
			$sqlConditions = $this->sqlConditions[ $reflectionClass->getName() ];
			$this->em->getFilters()->enable( 'tide_acl_filter' );

			foreach ( $sqlConditions as $sqlCondition ) {
				$conditions[] = sprintf( '%s.id IN (%s)', $targetTableAlias, $sqlCondition );
			}

			return implode( ' OR ', $conditions );
		}

		return '';
	}


	private function getSqlConditionsForEntity( User $user, ReflectionClass $reflectionClass ) {

		//Check if user has permission to read all the entities
		if ( $this->userCanReadAllEntitiesOfClass( $user, $reflectionClass ) ) {
			return [];
		}
		$sqlConditions = [];
		//Generate sub-queries ruled by parent classes permissions
		$instance = $reflectionClass->newInstanceWithoutConstructor();
		$configs  = $instance::getReadAllEntitiesByParentConfig();
		foreach ( $configs as $config ) {
			$class          = $config['class'];
			$permissionMask = $config['permissionMask'];
			$entitiesIds    = $this->tideAclHelper->getAllowedUserEntitiesByPermissionMask( $user, $class, $permissionMask, true );
			if ( ( is_countable( $entitiesIds ) ? count( $entitiesIds ) : 0 ) > 0 ) {
				$entitiesStrings = implode( ',', $entitiesIds );
			} else {
				$entitiesStrings = "0";
			}
			$sqlConditions[] = str_replace( ':allowableParentIds', $entitiesStrings, $config['query'] );
		}

		//Get read allowed id's array
		$entitiesIds = $this->tideAclHelper->getAllowedUserEntitiesByPermissionMask( $user, $reflectionClass->getName(), TideAcl::ACL_PERMISSION_SHOW, true );
		if ( ( is_countable( $entitiesIds ) ? count( $entitiesIds ) : 0 ) > 0 ) {
			$entitiesStrings = implode( ',', $entitiesIds );
		} else {
			$entitiesStrings = "0";
		}
		$sqlConditions[] = $entitiesStrings;

		return $sqlConditions;
	}

	private function userCanReadAllEntitiesOfClass( User $user, ReflectionClass $class ) {

		//example: PROJECT_LIST or QUOTE_LIST
		return in_array( strtoupper($class->getShortName()) . '_' . 'LIST', $user->getPermissionsArray() );
	}

	public function setTideAclHelper( TideAclHelper $tideAclHelper ): void {
		$this->tideAclHelper = $tideAclHelper;
	}

	public function setUser( User $user ): void {
		$this->user = $user;
	}

	public function setEm( EntityManagerInterface $em ): void {
		$this->em = $em;
	}


}