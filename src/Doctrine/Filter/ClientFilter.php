<?php

namespace App\Doctrine\Filter;

use App\Annotation\ClientAwareAnnotation;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

final class ClientFilter extends SQLFilter {
	private $reader;

	public function addFilterConstraint( ClassMetadata $targetEntity, $targetTableAlias ): string {

		if ( null === $this->reader ) {
			throw new \RuntimeException( sprintf( 'An annotation reader must be provided. Be sure to call "%s::setAnnotationReader()".', self::class ) );
		}

		// The Doctrine filter is called for any query on any entity
		$clientAware = null;

		$reflection = $targetEntity->getReflectionClass();		
		$classAttributes = $reflection->getAttributes();
		
		foreach ( $classAttributes as $attribute ) {

			if ( $attribute->getName() === 'App\Annotation\ClientAwareAnnotation' ) {
				$clientAware = $attribute->getArguments();
			}
		}

		if ( ! $clientAware ) {
			return '';
		}

		$fieldName = $clientAware["clientFieldName"];
		try {
			// Don't worry, getParameter automatically escapes parameters
			$clientId = $this->getParameter( 'id' );
		} catch ( \InvalidArgumentException ) {
			// No user id has been defined
			return '';
		}

		if ( empty( $fieldName ) || empty( $clientId ) ) {
			return '';
		}

		return sprintf( '%s.%s = %s', $targetTableAlias, $fieldName, $clientId );
	}

	public function setAnnotationReader( Reader $reader ): void {
		$this->reader = $reader;
	}
}