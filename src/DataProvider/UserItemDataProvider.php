<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

//Solution to avoid api platform forcing getting id parameter https://github.com/api-platform/api-platform/issues/337, they said it is a bad practice :(
final class UserItemDataProvider implements ItemDataProviderInterface {
	private \App\Repository\UserRepository $repository;

	/**
	 * UserMe constructor.
	 */
	public function __construct( EntityManagerInterface $entityManager, private TokenStorageInterface $tokenStorage ) {
		$this->repository = $entityManager->getRepository( User::class );
	}

	/**
	 * @param int|string $id
	 * @param string|null $operationName
	 *
	 * @return User|null
	 * @throws ResourceClassNotSupportedException
	 */
	public function getItem( string $resourceClass, $id, string $operationName = null, array $context = [] ) {
		if ( User::class !== $resourceClass ) {
			throw new ResourceClassNotSupportedException();
		}

		// retrieves User from the security when hitting the route 'api_users_me' (no id needed)
		if ( $operationName === 'user_me' ) {
			return $this->tokenStorage->getToken()->getUser();
		}

		return $this->repository->find( $id ); // Retrieves User normally for other actions
	}
}
