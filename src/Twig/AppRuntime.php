<?php

namespace App\Twig;

use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface {

	public function priceFilter( $number, $currency = 'MXN' ) {
		$price_format = number_format( $number, 2, '.', ',' );

		return $price_format . " " . $currency;
	}
}