<?php

namespace App\Interfaces;

interface FolioInterface {
	/**
	 * This method returns the initials to identify de client
	 */
	public function getFolioBaseInitials(): string;

	/**
	 * This method returns the initial letter to identify the folio type
	 */
	public function getFolioTypeIdentifier(): string;

	/**
	 * This method modify the DQL to complete de query to get the next folio sequential
	 */
	public function addDQLFolioSequential( \Doctrine\ORM\QueryBuilder $query ): \Doctrine\ORM\QueryBuilder;

	public function setFolioSequential( ?int $folioSequential );

	public function getFolioSequential(): ?int;

	public function setFolio( string $folioSequential );
}
