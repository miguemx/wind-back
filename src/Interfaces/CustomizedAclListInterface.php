<?php

namespace App\Interfaces;

interface CustomizedAclListInterface {

	/**
	 * This method modify the DQL to complete de query to get the next folio sequential
	 */
	public function getListDql( \Doctrine\ORM\QueryBuilder $query ): \Doctrine\ORM\QueryBuilder;

}
