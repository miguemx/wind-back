<?php

namespace App\Interfaces;

interface AclEnabledInterface {
	/**
	 * Returns a string representing the domain the acl will give permission to.
	 * This is usually the class name.
	 */
	public function getAclDomain(): string;

	/**
	 * Returns a string representing the id of the object that belongs to the "domain"
	 * This is usually the object primary key.
	 */
	public function getObjectId(): string;

	/**
	 * Should return an empty array or an array with the following structure
	 * [
	 * 'query'          => "SELECT q.id from quote q left join project p on q.project_id=p.id where p.id in (:allowableParentIds)", //query to filter by parent properties
	 * 'class'          => Project::class, //Parent class holding the perimssion
	 * 'permissionMask' => Project::ACL_PERMISSION_SHOW_ALL_QUOTES //Permission that allow read all entities belonging to that entity
	 * ]
	 */
	static function getReadAllEntitiesByParentConfig(): array;

}
