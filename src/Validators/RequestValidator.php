<?php

namespace App\Validators;

use JsonException;
use Symfony\Component\HttpFoundation\Request;

class RequestValidator {

	public const VALIDATION_EXISTS = "VALIDATION_EXISTS";
	public const VALIDATION_EMAIL = "VALIDATION_EMAIL";

	/**
	 * True to parse and validate the request body formatted aws json
	 * False to validate the query string
	 */
	private bool $parseBody = true;

	private $parsedData;

	public function __construct( private Request $request, private $fields = [], $options = [] ) {

		if ( $options ) {
			if ( isset( $options["parseBody"] ) ) {
				$this->parseBody = $options["parseBody"];
			}
		}
	}

	/**
	 * @throws JsonException
	 */
	private function parseData() {
		if ( $this->parseBody ) {
			$this->parsedData = json_decode( $this->request->getContent(), true );
		} else {
			$this->parsedData = $this->request->query->all();
		}
	}

	public function validateRequest() {

		try {
			$this->parseData();
		} catch ( JsonException ) {
			return [ "No se envió ningún parámetro" ];
		}

		$errors = [];
		foreach ( $this->getFields() as $field => $validation ) {
			if ( ! isset( $this->parsedData[ $field ] ) ) {
				return [ "El campo $field es obligatorio" ];
			}
			$value = $this->parsedData[ $field ];
			if ( ( $validation === self::VALIDATION_EMAIL ) &&
			     ! preg_match( '/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i', $value ) ) {
				$errors[] = "El campo $field de contener un email válido";
			}
		}

		return [ "valid" => count( $errors ) === 0, "errors" => $errors ];

	}

	public function isParseBody(): bool {
		return $this->parseBody;
	}

	public function setParseBody( bool $parseBody ): void {
		$this->parseBody = $parseBody;
	}

	/**
	 * @return array|mixed
	 */
	public function getFields() {
		return $this->fields;
	}

	/**
	 * @param array|mixed $fields
	 */
	public function setFields( $fields ): void {
		$this->fields = $fields;
	}

	/**
	 * @return mixed
	 */
	public function getParsedData() {
		return $this->parsedData;
	}

	/**
	 * @param mixed $parsedData
	 */
	public function setParsedData( $parsedData ): void {
		$this->parsedData = $parsedData;
	}
}
