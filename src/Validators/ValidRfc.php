<?php
namespace App\Validators;
use Symfony\Component\Validator\Constraint;
/**
 * @Annotation
 */
#[\Attribute]
class ValidRfc extends Constraint {
    public string $message = 'Por favor proporcione un RFC correcto.';
    public string $mode = 'strict';
    
	/**
	 * @return class 
	 */
	public function validatedBy() {
		return static::class.'Validator';
	}
}