<?php

namespace App\DataFixtures\ORM;

use App\Helpers\ClientStatsHelper;
use App\Helpers\ProjectStatsHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Nelmio\Alice\Loader\NativeLoader;
use Symfony\Component\Finder\Finder;

class LoadFixtures extends Fixture {

	public function __construct( private ClientStatsHelper $clientStatsHelper, private ProjectStatsHelper $projectStatsHelper ) {
	}

	public array $fixtureFiles = [
		'permissions'         => __DIR__ . '/fixtures/auto_permissions.yml',
		'tenant'              => __DIR__ . '/fixtures/tenants.yml',
		'user'                => __DIR__ . '/fixtures/users.yml',
		'client'              => __DIR__ . '/fixtures/clients.yml',
		'project'             => __DIR__ . '/fixtures/projects.yml',
		'quotation_request'   => __DIR__ . '/fixtures/quotation_requests.yml',
		'bank'                => __DIR__ . '/fixtures/banks.yml',
		'bankAccount'         => __DIR__ . '/fixtures/bankAccounts.yml',
		'transactionCategory' => __DIR__ . '/fixtures/transactionCategories.yml',
		'transaction'         => __DIR__ . '/fixtures/transactions.yml',
		'quotes'              => __DIR__ . '/fixtures/quotes.yml',
		'recurringPayment'    => __DIR__ . '/fixtures/recurringPayments.yml',
	];

	public function load( ObjectManager $manager ) {
		$this->loadCatalogues( $manager );
		$this->loadFixtures( $manager, $this->fixtureFiles );
		$manager->flush();

		$this->clientStatsHelper->computeAllStatsForAllClients();
		$this->projectStatsHelper->computeAllStatsForAllProjects();
	}

	public function loadCatalogues( ObjectManager $manager ) {
		$finder = new Finder();
		$finder->in(__DIR__.'/catalogues')
			->name('*.sql')->sort(function ($a, $b) { return strcmp($b->getBasename(), $a->getBasename()); })
				->reverseSorting();

		foreach ( $finder as $file ) {
			print "Importing: {$file->getBasename()} " . PHP_EOL;
			$sql = $file->getContents();
			$manager->getConnection()->exec( $sql );  // Execute native SQL
		}
	}

	public function loadFixtures( ObjectManager $manager, $files = null ) {
		$loader    = new NativeLoader();
		$objectSet = $loader->loadFiles( $files );
		foreach ( $objectSet->getObjects() as $key => $object ) {
			$className = $object::class;
			print "Importing: {$className} " . PHP_EOL;

			$this->addReference( $key, $object );
			$manager->persist( $object );
		}
	}
}
