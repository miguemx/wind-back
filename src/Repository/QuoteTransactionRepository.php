<?php

namespace App\Repository;

use App\Entity\QuoteTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuoteTransaction|null find( $id, $lockMode = null, $lockVersion = null )
 * @method QuoteTransaction|null findOneBy( array $criteria, array $orderBy = null )
 * @method QuoteTransaction[]    findAll()
 * @method QuoteTransaction[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class QuoteTransactionRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, QuoteTransaction::class );
	}

	// /**
	//  * @return QuoteTransaction[] Returns an array of QuoteTransaction objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('q')
			->andWhere('q.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('q.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?QuoteTransaction
	{
		return $this->createQueryBuilder('q')
			->andWhere('q.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
