<?php

namespace App\Repository;

use App\Entity\TideAcl;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TideAcl|null find( $id, $lockMode = null, $lockVersion = null )
 * @method TideAcl|null findOneBy( array $criteria, array $orderBy = null )
 * @method TideAcl[]    findAll()
 * @method TideAcl[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class TideAclRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, TideAcl::class );
	}

	// /**
	//  * @return TideAcl[] Returns an array of TideAcl objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('t')
			->andWhere('t.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('t.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?TideAcl
	{
		return $this->createQueryBuilder('t')
			->andWhere('t.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/

	public function getAllowedUserEntitiesByPermissionMask( string $securityIdentity, string $entityClass, int $permissionMask ) {

		return $this->createQueryBuilder( 'acl' )
		            ->select( 'e' )
		            ->innerJoin( $entityClass, 'e', 'WITH', 'acl.objectId = e.id' )
		            ->where( 'acl.securityIdentity = :securityIdentity' )->setParameter( 'securityIdentity', $securityIdentity )
		            ->andWhere( 'BIT_AND(acl.permissionMask, :permissionMask) = :permissionMask' )->setParameter( 'permissionMask', $permissionMask )
		            ->andWhere( 'acl.domain = :entityClass' )->setParameter( 'entityClass', $entityClass )
		            ->getQuery()
		            ->getResult();
	}
}
