<?php

namespace App\Repository;

use App\Entity\ProjectStats;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProjectStats|null find( $id, $lockMode = null, $lockVersion = null )
 * @method ProjectStats|null findOneBy( array $criteria, array $orderBy = null )
 * @method ProjectStats[]    findAll()
 * @method ProjectStats[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class ProjectStatsRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, ProjectStats::class );
	}

	// /**
	//  * @return ProjectStats[] Returns an array of ProjectStats objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('p')
			->andWhere('p.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('p.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?ProjectStats
	{
		return $this->createQueryBuilder('p')
			->andWhere('p.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
