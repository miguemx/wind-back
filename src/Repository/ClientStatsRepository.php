<?php

namespace App\Repository;

use App\Entity\ClientStats;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClientStats|null find( $id, $lockMode = null, $lockVersion = null )
 * @method ClientStats|null findOneBy( array $criteria, array $orderBy = null )
 * @method ClientStats[]    findAll()
 * @method ClientStats[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class ClientStatsRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, ClientStats::class );
	}

	// /**
	//  * @return ClientStats[] Returns an array of ClientStats objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('c.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?ClientStats
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
