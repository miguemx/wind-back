<?php

namespace App\Repository;

use App\Entity\Transaction;
use App\Entity\TransactionCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TransactionCategory|null find( $id, $lockMode = null, $lockVersion = null )
 * @method TransactionCategory|null findOneBy( array $criteria, array $orderBy = null )
 * @method TransactionCategory[]    findAll()
 * @method TransactionCategory[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class TransactionCategoryRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, TransactionCategory::class );
	}

	// /**
	//  * @return TransactionCategory[] Returns an array of TransactionCategory objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('t')
			->andWhere('t.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('t.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?TransactionCategory
	{
		return $this->createQueryBuilder('t')
			->andWhere('t.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/

	public function getExpensesByCategoriesBetweenDates( $from, $to ) {
		return $this->createQueryBuilder( 't' )
		            ->select( 't.id, t.name, SUM(trx.amount) as total' )
		            ->join( 't.transactions', 'trx' )
		            ->where( 'trx.transactionDate BETWEEN :from AND :to' )
		            ->andWhere( 'trx.transactionType = :type' )
					->andWhere( 'trx.status = :status' )
		            ->setParameter( 'from', $from )
		            ->setParameter( 'to', $to )
		            ->setParameter( 'type', Transaction::TRANSACTION_TYPE_EXPENSE )
					->setParameter( 'status', Transaction::TRANSACTION_STATUS_APPROVED )
		            ->groupBy( 't.id' )
		            ->getQuery()
		            ->getResult();
	}

	public function getIncomesByCategoriesBetweenDates( $from, $to, $client ) {
		$query = $this->createQueryBuilder( 't' )
					->select( 't.id, t.name, SUM(trx.amount) as total' )
					->join( 't.transactions', 'trx' )
					->where( 'trx.transactionDate BETWEEN :from AND :to' )
					->andWhere( 'trx.transactionType = :type' )
					->andWhere( 'trx.status = :status' )
					->setParameter( 'from', $from )
					->setParameter( 'to', $to )
					->setParameter( 'type', Transaction::TRANSACTION_TYPE_INCOME )
					->setParameter( 'status', Transaction::TRANSACTION_STATUS_APPROVED )
					->groupBy( 't.id' );

		if($client)
			$query->leftJoin('trx.quoteTransactions', 'qt')
					->andWhere('qt.client = :client')
					->setParameter( 'client', $client );

		return $query->getQuery()->getResult();
	}
}
