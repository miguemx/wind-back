<?php

namespace App\Repository;

use App\Entity\FileContainer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FileContainer|null find( $id, $lockMode = null, $lockVersion = null )
 * @method FileContainer|null findOneBy( array $criteria, array $orderBy = null )
 * @method FileContainer[]    findAll()
 * @method FileContainer[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class FileContainerRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, FileContainer::class );
	}

	// /**
	//  * @return FileContainer[] Returns an array of FileContainer objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('f')
			->andWhere('f.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('f.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?FileContainer
	{
		return $this->createQueryBuilder('f')
			->andWhere('f.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
