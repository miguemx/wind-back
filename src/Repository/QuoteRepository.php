<?php

namespace App\Repository;

use App\Entity\Quote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Quote|null find( $id, $lockMode = null, $lockVersion = null )
 * @method Quote|null findOneBy( array $criteria, array $orderBy = null )
 * @method Quote[]    findAll()
 * @method Quote[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class QuoteRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, Quote::class );
	}


	public function getPendingQuotesCount() {
		return (int) $this->createQueryBuilder( 'q' )
		                  ->select( 'COUNT( q.id )' )
		                  ->where( "q.status = :status" )
		                  ->getQuery()
		                  ->setParameter( 'status', Quote::STATUS_PENDING )
		                  ->getSingleScalarResult() ?: 0;
	}

	public function getPendingPaymentQuotesCount() {
		return $this->createQueryBuilder( 'q' )
		            ->select( 'COUNT( q.id )' )
		            ->where( "q.status IN (:approvedStatuses)" )
		            ->andWhere( "q.paymentStatus IN (:paymentStatus)" )
		            ->getQuery()
		            ->setParameter( 'approvedStatuses', Quote::getApprovedStatuses() )
		            ->setParameter( 'paymentStatus', [ Quote::PAYMENT_STATUS_PAYING, Quote::PAYMENT_STATUS_PENDING ] )
		            ->getSingleScalarResult() ?: 0;
	}


    /**
     * Find all expired quotes and change them
     */
    public function setExpireStatusOfQuotesBefore( $date ){

        $query = $this->createQueryBuilder('q')
            ->update(Quote::class, 'q')
            ->set('q.status', ':expiredStatus')
            ->where('q.expirationDate < :date')
            ->andWhere('q.status = :pendingStatus')
            ->setParameter('date', $date)
            ->setParameter('expiredStatus', Quote::STATUS_EXPIRED)
            ->setParameter('pendingStatus', Quote::STATUS_PENDING)
            ->getQuery();

            return $query->execute();
    }
}
