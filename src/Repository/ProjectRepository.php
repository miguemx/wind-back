<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\Quote;
use App\Entity\QuoteTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find( $id, $lockMode = null, $lockVersion = null )
 * @method Project|null findOneBy( array $criteria, array $orderBy = null )
 * @method Project[]    findAll()
 * @method Project[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class ProjectRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, Project::class );
	}

	///////// For Single Project Stats //////////

	public function getQuotesCount( Project $project ) {
		return (int) $this->createQueryBuilder( 'p' )
		                  ->select( 'COUNT( q.id )' )
		                  ->leftJoin( 'p.quotes', 'q' )
		                  ->where( 'p = :project' )
		                  ->getQuery()
		                  ->setParameter( 'project', $project )
		                  ->getSingleScalarResult();
	}

	public function getApprovedQuotesCount( Project $project ) {
		return (int) $this->createQueryBuilder( 'p' )
		                  ->select( 'COUNT( q.id )' )
		                  ->leftJoin( 'p.quotes', 'q' )
		                  ->where( 'p = :project' )
		                  ->andWhere( "q.status IN (:approvedStatuses)" )
		                  ->getQuery()
		                  ->setParameter( 'project', $project )
		                  ->setParameter( 'approvedStatuses', Quote::getApprovedStatuses() )
		                  ->getSingleScalarResult();
	}

	public function getPendingQuotesCount( Project $project ) {

		return (int) $this->createQueryBuilder( 'p' )
		                  ->select( 'COUNT( q.id )' )
		                  ->leftJoin( 'p.quotes', 'q' )
		                  ->where( 'p = :project' )
		                  ->andWhere( "q.status = :status" )
		                  ->getQuery()
		                  ->setParameter( 'project', $project )
		                  ->setParameter( 'status', Quote::STATUS_PENDING )
		                  ->getSingleScalarResult();
	}

	public function getAllQuotesSum( Project $project ) {
		return (float) $this->createQueryBuilder( 'p' )
		                    ->select( 'SUM( q.total )' )
		                    ->leftJoin( 'p.quotes', 'q' )
		                    ->where( 'p = :project' )
		                    ->getQuery()
		                    ->setParameter( 'project', $project )
		                    ->getSingleScalarResult() ?: 0;
	}

	public function getApprovedQuotesSum( Project $project ) {
		$query = $this->createQueryBuilder( 'p' )
		              ->select( 'SUM( q.total )' )
		              ->leftJoin( 'p.quotes', 'q' )
		              ->where( 'p = :project' )
		              ->andWhere( "q.status IN (:approvedStatuses)" )
		              ->getQuery();

		return (float) $query
			->setParameter( 'project', $project )
			->setParameter( 'approvedStatuses', Quote::getApprovedStatuses() )
			->getSingleScalarResult() ?: 0;
	}

	public function getPaymentsSum( Project $project ) {
		return (float) $this->getEntityManager()->createQueryBuilder()
		                    ->select( 'SUM( qt.amount )' )
		                    ->from( QuoteTransaction::class, 'qt' )
		                    ->leftJoin( 'qt.quote', 'q' )
		                    ->leftJoin( 'q.project', 'p' )
		                    ->where( 'p = :project' )
		                    ->getQuery()
		                    ->setParameter( 'project', $project )
		                    ->getSingleScalarResult() ?: 0;
	}

	///////// General Project Stats //////////

	public function getActiveProjectsCount() {
		return (int) $this->createQueryBuilder( 'p' )
		                  ->select( 'COUNT( p.id )' )
		                  ->where( "p.status in (:status)" )
		                  ->getQuery()
		                  ->setParameter( 'status', [
			                  Project::PROJECT_STATUS_IN_DEVELOPMENT,
			                  Project::PROJECT_STATUS_OPEN
		                  ] )
		                  ->getSingleScalarResult() ?: 0;
	}

}
