<?php

namespace App\Repository;

use App\Entity\NotificationTopic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NotificationTopic|null find( $id, $lockMode = null, $lockVersion = null )
 * @method NotificationTopic|null findOneBy( array $criteria, array $orderBy = null )
 * @method NotificationTopic[]    findAll()
 * @method NotificationTopic[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class NotificationTopicRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, NotificationTopic::class );
	}


	public function findTopicsByPermissionsArray( array $permissionsArray ) {
		return $this->createQueryBuilder( 'n' )
		            ->addSelect( 'p.name as HIDDEN pname' )
		            ->addSelect( 'p.id as HIDDEN pid' )
		            ->leftJoin( 'n.permissions', 'p' )
		            ->having( 'p.name in (:permissionsArray) or p.id is null' )->setParameter( 'permissionsArray', $permissionsArray )
		            ->orderBy( 'n.systemSection', 'ASC' )
		            ->addOrderBy( 'n.description', 'ASC' )
		            ->getQuery()
		            ->getResult();
	}

	/*
	public function findOneBySomeField($value): ?NotificationTopic
	{
		return $this->createQueryBuilder('n')
			->andWhere('n.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
