<?php

namespace App\Repository;

use App\Entity\Client;
use App\Entity\Project;
use App\Entity\Quote;
use App\Entity\QuoteTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Client|null find( $id, $lockMode = null, $lockVersion = null )
 * @method Client|null findOneBy( array $criteria, array $orderBy = null )
 * @method Client[]    findAll()
 * @method Client[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class ClientRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, Client::class );
	}

	public function getProjectsCount( Client $client ) {
		return (int) $this->createQueryBuilder( 'c' )
		                  ->select( 'COUNT( p.id )' )
		                  ->leftJoin( 'c.projects', 'p' )
		                  ->where( 'c = :client' )
		                  ->getQuery()
		                  ->setParameter( 'client', $client )
		                  ->getSingleScalarResult();
	}

	public function getApprovedProjectsCount( Client $client ) {
		return (int) $this->createQueryBuilder( 'c' )
		                  ->select( 'COUNT( p.id )' )
		                  ->leftJoin( 'c.projects', 'p' )
		                  ->where( 'c = :client' )
		                  ->andWhere( 'p.status NOT IN (:approvedStatus)' )
		                  ->getQuery()
		                  ->setParameter( 'client', $client )
		                  ->setParameter( 'approvedStatus', [
			                  Project::PROJECT_STATUS_PENDING,
			                  Project::PROJECT_STATUS_CANCELED
		                  ] )
		                  ->getSingleScalarResult();
	}

	public function getQuotesCount( Client $client ) {
		return (int) $this->createQueryBuilder( 'c' )
		                  ->select( 'COUNT( q.id )' )
		                  ->leftJoin( 'c.projects', 'p' )
		                  ->leftJoin( 'p.quotes', 'q' )
		                  ->where( 'c = :client' )
		                  ->getQuery()
		                  ->setParameter( 'client', $client )
		                  ->getSingleScalarResult();
	}

	public function getApprovedQuotesCount( Client $client ) {
		return (int) $this->createQueryBuilder( 'c' )
		                  ->select( 'COUNT( q.id )' )
		                  ->leftJoin( 'c.projects', 'p' )
		                  ->leftJoin( 'p.quotes', 'q' )
		                  ->where( 'c = :client' )
		                  ->andWhere( 'q.status IN (:approvedStatuses)' )
		                  ->getQuery()
		                  ->setParameter( 'client', $client )
		                  ->setParameter( 'approvedStatuses', Quote::getApprovedStatuses() )
		                  ->getSingleScalarResult();
	}

	public function getPendingQuotesCount( Client $client ) {
		return (int) $this->createQueryBuilder( 'c' )
		                  ->select( 'COUNT( q.id )' )
		                  ->leftJoin( 'c.projects', 'p' )
		                  ->leftJoin( 'p.quotes', 'q' )
		                  ->where( 'c = :client' )
		                  ->andWhere( 'q.status= :pendingStatus' )
		                  ->getQuery()
		                  ->setParameter( 'client', $client )
		                  ->setParameter( 'pendingStatus', Quote::STATUS_PENDING )
		                  ->getSingleScalarResult();
	}

	public function getAllQuotesSum( Client $client ) {
		return (float) $this->createQueryBuilder( 'c' )
		                    ->select( 'SUM( q.total )' )
		                    ->leftJoin( 'c.projects', 'p' )
		                    ->leftJoin( 'p.quotes', 'q' )
		                    ->where( 'c = :client' )
		                    ->getQuery()
		                    ->setParameter( 'client', $client )
		                    ->getSingleScalarResult() ?: 0;
	}

	public function getAllPendingQuotesSum( Client $client ) {
		return (float) $this->createQueryBuilder( 'c' )
		                    ->select( 'SUM( q.total )' )
		                    ->leftJoin( 'c.projects', 'p' )
		                    ->leftJoin( 'p.quotes', 'q' )
		                    ->where( 'c = :client' )
							->andWhere( 'q.status IN (:status)' )
							->andWhere( 'q.paymentStatus IN (:paymentStatus)' )
		                    ->getQuery()
		                    ->setParameter( 'client', $client )
							->setParameter( 'status', Quote::getApprovedStatuses() )
							->setParameter( 'paymentStatus', Quote::getPendingPaymentStatuses() )
		                    ->getSingleScalarResult() ?: 0;
	}


	public function getApprovedQuotesSum( Client $client ) {
		return (float) $this->createQueryBuilder( 'c' )
		                    ->select( 'SUM( q.total )' )
		                    ->leftJoin( 'c.projects', 'p' )
		                    ->leftJoin( 'p.quotes', 'q' )
		                    ->where( 'c = :client' )
		                    ->andWhere( 'q.status IN (:approvedStatuses)' )
		                    ->getQuery()
		                    ->setParameter( 'client', $client )
		                    ->setParameter( 'approvedStatuses', Quote::getApprovedStatuses() )
		                    ->getSingleScalarResult() ?: 0;
	}

	/**
	 * Returns the sum of all quote transactions for the given client
	 *
	 * @return float
	 */
	public function getPaymentsSum( Client $client ) {
		return (float) $this->getEntityManager()->createQueryBuilder()
		                    ->select( 'SUM( qt.amount )' )
		                    ->from( QuoteTransaction::class, 'qt' )
		                    ->leftJoin( 'qt.quote', 'q' )
		                    ->leftJoin( 'q.project', 'p' )
		                    ->leftJoin( 'p.client', 'c' )							
		                    ->where( 'c = :client' )
							->andWhere( 'q.status IN (:status)' )
							->andWhere( 'q.paymentStatus IN (:paymentStatus)' )
		                    ->getQuery()
		                    ->setParameter( 'client', $client )
							->setParameter( 'status', Quote::getApprovedStatuses() )
							->setParameter( 'paymentStatus', Quote::getPendingPaymentStatuses() )
		                    ->getSingleScalarResult() ?: 0;
	}
}
