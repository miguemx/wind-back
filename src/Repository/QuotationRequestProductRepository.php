<?php

namespace App\Repository;

use App\Entity\QuotationRequestProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuotationRequestProduct|null find( $id, $lockMode = null, $lockVersion = null )
 * @method QuotationRequestProduct|null findOneBy( array $criteria, array $orderBy = null )
 * @method QuotationRequestProduct[]    findAll()
 * @method QuotationRequestProduct[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class QuotationRequestProductRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, QuotationRequestProduct::class );
	}

	// /**
	//  * @return QuotationRequestProduct[] Returns an array of QuotationRequestProduct objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('q')
			->andWhere('q.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('q.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?QuotationRequestProduct
	{
		return $this->createQueryBuilder('q')
			->andWhere('q.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
