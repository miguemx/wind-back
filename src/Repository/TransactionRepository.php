<?php

namespace App\Repository;

use App\Entity\Transaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transaction|null find( $id, $lockMode = null, $lockVersion = null )
 * @method Transaction|null findOneBy( array $criteria, array $orderBy = null )
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class TransactionRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, Transaction::class );
	}

	// /**
	//  * @return Transaction[] Returns an array of Transaction objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('t')
			->andWhere('t.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('t.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?Transaction
	{
		return $this->createQueryBuilder('t')
			->andWhere('t.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/

	public function getLastTransaction( $recurringPayment ) {
		return $this->createQueryBuilder( 't' )
		            ->select( 't.transactionDate' )
		            ->where( 't.recurringPayment = :recurringPayment' )->setParameter( 'recurringPayment', $recurringPayment )
		            ->orderBy( 't.id', 'DESC' )
		            ->setMaxResults( 1 )
		            ->getQuery()
		            ->getResult();
	}

	public function getIncomesByCategoriesBetweenDates( $from, $to, $client ) {
		$query = $this->createQueryBuilder( 't' )
					->select( 'IDENTITY(t.transactionCategory) as trxCatId, SUM(t.amount) as total' )
					->where( 't.transactionDate BETWEEN :from AND :to' )
					->andWhere( 't.transactionType = :type' )
					->andWhere( 't.status = :status' )
					->setParameter( 'from', $from )
					->setParameter( 'to', $to )
					->setParameter( 'type', Transaction::TRANSACTION_TYPE_INCOME )
					->setParameter( 'status', Transaction::TRANSACTION_STATUS_APPROVED )
					->groupBy( 't.transactionCategory' );

		if($client) {
			$query->leftJoin('t.quoteTransactions', 'qt')
				->andWhere('qt.client = :client')
				->setParameter('client', $client);
		}

		return $query->getQuery()->getResult();
	}
}
