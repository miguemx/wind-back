<?php

namespace App\Repository;

use App\Entity\Memorandum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Memorandum|null find( $id, $lockMode = null, $lockVersion = null )
 * @method Memorandum|null findOneBy( array $criteria, array $orderBy = null )
 * @method Memorandum[]    findAll()
 * @method Memorandum[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class MemorandumRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, Memorandum::class );
	}

	// /**
	//  * @return Memorandum[] Returns an array of Memorandum objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('m')
			->andWhere('m.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('m.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?Memorandum
	{
		return $this->createQueryBuilder('m')
			->andWhere('m.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
