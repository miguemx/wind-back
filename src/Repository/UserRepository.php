<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find( $id, $lockMode = null, $lockVersion = null )
 * @method User|null findOneBy( array $criteria, array $orderBy = null )
 * @method User[]    findAll()
 * @method User[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class UserRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, User::class );
	}

	/**
	 * get all users with entity general permissions by looking at the permissions related to their permissionGroups
	 *
	 * @param string $upperCaseEntityName : PROJECT, TASK, etc
	 * @param string $clientId
	 *
	 * @return User[] Returns an array of User objects
	 */
	public function findByDomainPermissionGroups( $upperCaseEntityName, $clientId ) {
		$entityGeneralPermissions = [
			$upperCaseEntityName . '_SHOW',
			$upperCaseEntityName . '_UPDATE',
			$upperCaseEntityName . '_DELETE',
			$upperCaseEntityName . '_CREATE',
		];

		return $this->createQueryBuilder( 'u' )
		            ->where( 'u.client = :clientId' )
		            ->join( 'u.permissionGroups', 'pg' )
		            ->join( 'pg.permissions', 'p' )
		            ->andWhere( 'p.name IN (:entityGeneralPermissions)' )
		            ->setParameter( 'clientId', $clientId )
		            ->setParameter( 'entityGeneralPermissions', $entityGeneralPermissions )
		            ->getQuery()
		            ->getResult();
	}
}
