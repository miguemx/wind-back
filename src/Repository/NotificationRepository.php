<?php

namespace App\Repository;

use App\Entity\Notification;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\RegistryInterface;


class NotificationRepository extends ServiceEntityRepository {
	public function __construct( ManagerRegistry $registry ) {
		parent::__construct( $registry, Notification::class );
	}

	public function getHeaderNotifications( User $user, $maxResults = 40 ) {

		$notifications = $this->createQueryBuilder( 'n' )
		                      ->select( 'n' )
		                      ->where( 'n.user = :user' )
		                      ->andWhere( 'n.readDate IS NULL' )
		                      ->addOrderBy( 'n.updatedDate', 'DESC' )
		                      ->getQuery()
		                      ->setParameter( 'user', $user )
		                      ->getResult();

		if ( ( is_countable( $notifications ) ? count( $notifications ) : 0 ) < $maxResults ) {
			$read = $this->createQueryBuilder( 'n' )
			             ->select( 'n' )
			             ->where( 'n.user = :user' )
			             ->andWhere( 'n.readDate IS NOT NULL' )
			             ->addOrderBy( 'n.updatedDate', 'DESC' )
			             ->setMaxResults( $maxResults - ( is_countable( $notifications ) ? count( $notifications ) : 0 ) )
			             ->getQuery()
			             ->setParameter( 'user', $user )
			             ->getResult();

			$notifications = array_merge( $notifications, $read );
		}

		return $notifications;
	}

	public function countAllByUser( User $user ) {
		return $this->createQueryBuilder( 'n' )
		            ->select( 'COUNT(n.id)as NUM' )
		            ->where( 'n.user = :user' )
		            ->getQuery()
		            ->setParameter( 'user', $user )
		            ->getResult()[0]['NUM'];
	}


}
