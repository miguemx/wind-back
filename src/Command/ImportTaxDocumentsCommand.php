<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Helpers\SatWsHelper;
use App\Entity\FiscalData;
use App\Entity\Tenant;
use App\Entity\TenantRfc;
use Psr\Log\LoggerInterface;

class ImportTaxDocumentsCommand extends Command {
	protected static $defaultName = 'app:import-tax-documents';

	public function __construct( private EntityManagerInterface $em, private SatWsHelper $satWsHelper, private LoggerInterface $logger ) {
		parent::__construct();
	}

	protected function configure() {
		$this->setDescription( 'Import tax documents from satws' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$io = new SymfonyStyle( $input, $output );

		$fiscalDatas = $this->em->getRepository( TenantRfc::class )->findAll();

		// foreach fiscal data get tax documents
		foreach ( $fiscalDatas as $fiscalData ) {
			try{
				$taxDocuments = $this->satWsHelper->getTaxDocuments( $fiscalData->getRfc(), $fiscalData->getTenant() );
				echo "Extracted Tenant: {$fiscalData->getTenant()->getId()} RFC: {$fiscalData->getRfc()} \n\n";
			} catch ( \Exception $e ) {
				$this->logger->critical( "Error extracting data tenant: {$fiscalData->getTenant()->getId()} RFC: {$fiscalData->getRfc()} \n\n" );
				$this->logger->critical( "Error details: {$e->getMessage()} \n\n" );
				//$io->error( $e->getMessage() );
				continue;
			}
		}

		return 0;
	}
}
