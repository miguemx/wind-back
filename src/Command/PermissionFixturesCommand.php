<?php

namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;


class PermissionFixturesCommand extends Command {

	public const INDENT = '  ';

	/**
	 * @var string $em
	 */
	private string $projectDir;

	/**
	 * PermissionsCommand constructor.
	 */
	public function __construct( KernelInterface $kernel ) {
		parent::__construct();
		$this->projectDir = $kernel->getProjectDir();
	}


	protected function configure() {
		$this->setName( 'permissions:fixtures' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$fileName = $this->projectDir . '/src/DataFixtures/ORM/fixtures/auto_permissions.yml';
		$output->writeln( 'Overwriting permissions fixtures in ' . $fileName );

		$data = $this->createPermissionsFixtures();

		$file = fopen( $fileName, 'w+' );
		fwrite( $file, $data );
		fclose( $file );

		$output->writeln( 'Done' );

		return 0;
	}

	private function createPermissionsFixtures() {
		$fixtures = "";
		$indent   = "";
		// --------- Create role fixtures
		$fixtures .= "App\\Entity\\Role:\n";
		$indent   .= self::INDENT;
		foreach ( PermissionsDefinition::ROLES as $role ) {
			$fixtures .= $indent . $role['name'] . ":\n";
			$indent   .= self::INDENT;
			$fixtures .= $indent . 'name: ' . $role['name'] . "\n";
			$fixtures .= $indent . 'title: ' . $role['title'] . "\n";
			$fixtures .= $indent . 'assignable: ' . ( $role['assignable'] ? 'true' : 'false' ) . "\n";
			$indent   = substr( $indent, strlen( self::INDENT ) );
		}

		$indent = substr( $indent, strlen( self::INDENT ) );

		// --------- Create permissions fixtures
		$fixtures .= "\n\n";
		$fixtures .= "App\\Entity\\Permission:\n";
		$indent   .= self::INDENT;
		foreach ( PermissionsDefinition::PERMISSIONS as $permission ) {
			$fixtures .= $indent . $permission['name'] . ":\n";
			$indent   .= self::INDENT;
			$fixtures .= $indent . 'name: ' . $permission['name'] . "\n";
			$indent   = substr( $indent, strlen( self::INDENT ) );
		}

		// --------- Create permission group fixtures
		$fixtures .= "\n\n";
		$fixtures .= "App\\Entity\\PermissionGroup:\n";
		$indent   .= self::INDENT;
		foreach ( PermissionsDefinition::PERMISSION_GROUPS as $permissionGroup ) {
			$fixtures .= $indent . $permissionGroup['code'] . ":\n";
			$indent   .= self::INDENT;
			$fixtures .= $indent . "id: '<uniqid()>'\n";
			$fixtures .= $indent . 'name: ' . $permissionGroup['name'] . "\n";
			$fixtures .= $indent . 'code: ' . $permissionGroup['code'] . "\n";
			$fixtures .= $indent . 'description: ' . $permissionGroup['description'] . "\n";
			$fixtures .= $indent . "permissions: [\n";
			$indent   .= self::INDENT;
			foreach ( $permissionGroup["permissions"] as $permission ) {
				$fixtures .= $indent . "'@$permission',\n";
			}
			$indent   = substr( $indent, strlen( self::INDENT ) );
			$fixtures .= $indent . "]\n";
			$indent   = substr( $indent, strlen( self::INDENT ) );
		}
		$indent = substr( $indent, strlen( self::INDENT ) );

		return $fixtures;
	}

}
