<?php

namespace App\Command;

use Aws\Ses\SesClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class TestEmailCommand extends Command {
	protected static $defaultName = 'test:email';

	public function __construct( protected SesClient $sesClient ) {
		parent::__construct();
	}

	protected function configure() {
		$this->setDescription( 'Send test email' )->addArgument( 'email', InputArgument::REQUIRED, 'email' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$io    = new SymfonyStyle( $input, $output );
		$email = $input->getArgument( 'email' );

		$subject          = 'Test email';
		$fromName         = 'test';
		$emailExtra       = null;
		$attachments      = [];
		$replyTo          = null;
		$toEmail          = $email;
		$textBody         = '';
		$renderedTemplate = '<div>Esto es una prueba desde Amazon</div>';

		if ( $fromName ) {
			$from = "$fromName <{$_ENV['MAILER_USER']}>";
		} else {
			$from = $_ENV['MAILER_USER'];
		}

		$config = [
			'Destination'      => [
				'ToAddresses' => [ $toEmail ],
			],
			'ReplyToAddresses' => [ $replyTo ?? $from ],
			'Source'           => $from,
			'Message'          => [
				'Body'    => [
					'Html' => [
						'Charset' => 'UTF-8',
						'Data'    => $renderedTemplate,
					],
					'Text' => [
						'Charset' => 'UTF-8',
						'Data'    => $textBody,
					],
				],
				'Subject' => [
					'Charset' => 'UTF-8',
					'Data'    => $subject
				],
			],
		];

		$this->sesClient->sendEmail( $config );

		return 0;
	}
}
