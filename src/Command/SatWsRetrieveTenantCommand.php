<?php

namespace App\Command;

use App\Helpers\ClientStatsHelper;
use App\Helpers\ProjectStatsHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Helpers\SatWsHelper;
use App\Entity\TenantRfc;

class SatWsRetrieveTenantCommand extends Command {
	protected static $defaultName = 'app:satws-retrieve-tenant';

	public function __construct( 
            private ClientStatsHelper $clientStatsHelper, 
            private EntityManagerInterface $em, 
            private SatWsHelper $satWsHelper ) {
		parent::__construct();
	}

	protected function configure() {
		$this->setDescription( 'Extracts all tax documents from an specific tenant; provide the tenant ID to arguments' );
        $this->addArgument( 'tenant', InputArgument::REQUIRED, 'No tenant provided' );
        $this->addArgument( 'from', InputArgument::OPTIONAL, 'Don\'t you need from date?' );
        $this->addArgument( 'to', InputArgument::OPTIONAL, 'Don\'t you need to date?' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ): int {
		$io = new SymfonyStyle( $input, $output );
        $tenantId = $input->getArgument('tenant');
        $from = $input->getArgument('from');
        $to = $input->getArgument('to');
        if ( is_null($from) ) $from = '2014-01-01';
        if ( is_null($to) ) $to = 'now';

		try {
            $fromDate = new \DateTime( $from, new \DateTimeZone('America/Mexico_City') );
            $toDate = new \DateTime( $to, new \DateTimeZone('America/Mexico_City') );
            $toDate->setTime( 23,59,59 );
            $retrieved  = $this->satWsHelper->retrieveTenantInvoices( $tenantId, $fromDate, $toDate );
            $output->writeln("<info>Retrieved correctly</info>");
			return 0;
		} catch ( \Exception $exception ) {
			$io->error( $exception->getMessage() );
			return 1;
		}
	}
}
