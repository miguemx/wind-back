<?php

namespace App\Command;
class PermissionsDefinition {
	public const ROLES = [
		[ 'name' => 'SUPER_ADMIN', 'title' => 'Súper administrador', 'assignable' => false ],
		[ 'name' => 'ROLE_GENERAL_ADMIN', 'title' => 'Administrador normal', 'assignable' => true ]
	];

	public const PERMISSIONS = [

		[ 'name' => 'USER_LIST', 'title' => 'Listar los usuarios' ],
		[ 'name' => 'USER_LIST_SIMPLE', 'title' => 'Listar los usuarios con información mínima' ],
		[ 'name' => 'USER_CREATE', 'title' => 'Crear un usuario' ],
		[ 'name' => 'USER_UPDATE', 'title' => 'Actualizar un usuario' ],
		[ 'name' => 'USER_DELETE', 'title' => 'Eliminar un usuario' ],
		[ 'name' => 'USER_SHOW', 'title' => 'Ver un usuario' ],

		[ 'name' => 'PERMISSION_LIST', 'title' => 'Listar los permisos' ],

		[ 'name' => 'ROLES_LIST', 'title' => 'Listar los roles' ],
		[ 'name' => 'ROLES_SHOW', 'title' => 'Ver un rol' ],

		// User permission groups
		[ 'name' => 'USER_PERMISSION_GROUP_UPDATE', 'title' => 'Actualizar un grupo de permisos de usuario' ],

		// account statement
		[ 'name' => 'ACCOUNT_STATEMENT_LIST', 'title' => 'Listar los estados de cuenta' ],
		[ 'name' => 'ACCOUNT_STATEMENT_CREATE', 'title' => 'Crear un estado de cuenta' ],
		[ 'name' => 'ACCOUNT_STATEMENT_UPDATE', 'title' => 'Actualizar un estado de cuenta' ],
		[ 'name' => 'ACCOUNT_STATEMENT_DELETE', 'title' => 'Eliminar un estado de cuenta' ],
		[ 'name' => 'ACCOUNT_STATEMENT_SHOW', 'title' => 'Ver un estado de cuenta' ],

		// bank
		[ 'name' => 'BANK_LIST', 'title' => 'Listar los bancos' ],
		[ 'name' => 'BANK_CREATE', 'title' => 'Crear un banco' ],
		[ 'name' => 'BANK_UPDATE', 'title' => 'Actualizar un banco' ],
		[ 'name' => 'BANK_DELETE', 'title' => 'Eliminar un banco' ],
		[ 'name' => 'BANK_SHOW', 'title' => 'Ver un banco' ],

		// quote product
		[ 'name' => 'QUOTE_PRODUCT_LIST', 'title' => 'Listar los productos de una cotización' ],
		[ 'name' => 'QUOTE_PRODUCT_CREATE', 'title' => 'Crear un producto de una cotización' ],
		[ 'name' => 'QUOTE_PRODUCT_UPDATE', 'title' => 'Actualizar un producto de una cotización' ],
		[ 'name' => 'QUOTE_PRODUCT_DELETE', 'title' => 'Eliminar un producto de una cotización' ],
		[ 'name' => 'QUOTE_PRODUCT_SHOW', 'title' => 'Ver un producto de una cotización' ],

		// bank account
		[ 'name' => 'BANK_ACCOUNT_LIST', 'title' => 'Listar las cuentas bancarias' ],
		[ 'name' => 'BANK_ACCOUNT_CREATE', 'title' => 'Crear cuenta bancaria' ],
		[ 'name' => 'BANK_ACCOUNT_UPDATE', 'title' => 'Actualizar una cuenta bancaria' ],
		[ 'name' => 'BANK_ACCOUNT_DELETE', 'title' => 'Eliminar una cuenta bancaria' ],
		[ 'name' => 'BANK_ACCOUNT_SHOW', 'title' => 'Ver una cuenta bancaria' ],

		// client
		[ 'name' => 'CLIENT_LIST', 'title' => 'Listar los clientes' ],
		[ 'name' => 'CLIENT_CREATE', 'title' => 'Crear un cliente' ],
		[ 'name' => 'CLIENT_UPDATE', 'title' => 'Actualizar un cliente' ],
		[ 'name' => 'CLIENT_DELETE', 'title' => 'Eliminar un cliente' ],
		[ 'name' => 'CLIENT_SHOW', 'title' => 'Ver un cliente' ],

		// recurring_transaction
		[ 'name' => 'RECURRING_PAYMENT_LIST', 'title' => 'Listar los pagos recurrentes' ],
		[ 'name' => 'RECURRING_PAYMENT_CREATE', 'title' => 'Crear un un pago recurrente' ],
		[ 'name' => 'RECURRING_PAYMENT_UPDATE', 'title' => 'Actualizar un pago recurrente' ],
		[ 'name' => 'RECURRING_PAYMENT_DELETE', 'title' => 'Eliminar un pago recurrente' ],
		[ 'name' => 'RECURRING_PAYMENT_SHOW', 'title' => 'Ver un pago recurrente' ],

		// project
		[ 'name' => 'PROJECT_LIST', 'title' => 'Listar los proyectos' ],
		[ 'name' => 'PROJECT_CREATE', 'title' => 'Crear un proyecto' ],
		[ 'name' => 'PROJECT_UPDATE', 'title' => 'Actualizar un proyecto' ],
		[ 'name' => 'PROJECT_DELETE', 'title' => 'Eliminar un proyecto' ],
		[ 'name' => 'PROJECT_SHOW', 'title' => 'Ver un proyecto' ],

		// provider
		[ 'name' => 'PROVIDER_LIST', 'title' => 'Listar los proveedores' ],
		[ 'name' => 'PROVIDER_CREATE', 'title' => 'Crear un proveedor' ],
		[ 'name' => 'PROVIDER_UPDATE', 'title' => 'Actualizar un proveedor' ],
		[ 'name' => 'PROVIDER_DELETE', 'title' => 'Eliminar un proveedor' ],
		[ 'name' => 'PROVIDER_SHOW', 'title' => 'Ver un proveedor' ],

		// state
		[ 'name' => 'STATE_LIST', 'title' => 'Listar los estados' ],
		[ 'name' => 'STATE_CREATE', 'title' => 'Crear un estado' ],
		[ 'name' => 'STATE_UPDATE', 'title' => 'Actualizar un estado' ],
		[ 'name' => 'STATE_DELETE', 'title' => 'Eliminar un estado' ],
		[ 'name' => 'STATE_SHOW', 'title' => 'Ver un estado' ],

		// quote transaction
		[ 'name' => 'QUOTE_TRANSACTION_LIST', 'title' => 'Listar las transacciones de una cotización' ],
		[ 'name' => 'QUOTE_TRANSACTION_CREATE', 'title' => 'Crear una transacción de una cotización' ],
		[ 'name' => 'QUOTE_TRANSACTION_UPDATE', 'title' => 'Actualizar una transacción de una cotización' ],
		[ 'name' => 'QUOTE_TRANSACTION_DELETE', 'title' => 'Eliminar una transacción de una cotización' ],
		[ 'name' => 'QUOTE_TRANSACTION_SHOW', 'title' => 'Ver una transacción de una cotización' ],

		// transaction
		[ 'name' => 'TRANSACTION_LIST', 'title' => 'Listar las transacciones' ],
		[ 'name' => 'TRANSACTION_CREATE', 'title' => 'Crear una transacción' ],
		[ 'name' => 'TRANSACTION_UPDATE', 'title' => 'Actualizar una transacción' ],
		[ 'name' => 'TRANSACTION_DELETE', 'title' => 'Eliminar una transacción' ],
		[ 'name' => 'TRANSACTION_SHOW', 'title' => 'Ver una transacción' ],

		// transaction_type
		[ 'name' => 'TRANSACTION_TYPE_LIST', 'title' => 'Listar los tipos de transacción' ],
		[ 'name' => 'TRANSACTION_TYPE_CREATE', 'title' => 'Crear un tipo de transacción' ],
		[ 'name' => 'TRANSACTION_TYPE_UPDATE', 'title' => 'Actualizar un tipo de transacción' ],
		[ 'name' => 'TRANSACTION_TYPE_DELETE', 'title' => 'Eliminar un tipo de transacción' ],
		[ 'name' => 'TRANSACTION_TYPE_SHOW', 'title' => 'Ver un tipo de transacción' ],

		[ 'name' => 'QUOTE_LIST', 'title' => 'Listar las cotizaciones' ],
		[ 'name' => 'QUOTE_CREATE', 'title' => 'Crear una cotización' ],
		[ 'name' => 'QUOTE_UPDATE', 'title' => 'Actualizar una cotización' ],
		[ 'name' => 'QUOTE_DELETE', 'title' => 'Eliminar una cotización' ],
		[ 'name' => 'QUOTE_SHOW', 'title' => 'Ver una cotización' ],

		[ 'name' => 'TIDE_ACL_LIST', 'title' => 'Listar las acls' ],
		[ 'name' => 'TIDE_ACL_CREATE', 'title' => 'Crear una acl' ],
		[ 'name' => 'TIDE_ACL_UPDATE', 'title' => 'Actualizar una acl' ],
		[ 'name' => 'TIDE_ACL_DELETE', 'title' => 'Eliminar una acl' ],
		[ 'name' => 'TIDE_ACL_SHOW', 'title' => 'Ver una acl' ],

		[ 'name' => 'MEMORANDUM_LIST', 'title' => 'Listar las minutas' ],
		[ 'name' => 'MEMORANDUM_CREATE', 'title' => 'Crear una minuta' ],
		[ 'name' => 'MEMORANDUM_UPDATE', 'title' => 'Actualizar una minuta' ],
		[ 'name' => 'MEMORANDUM_DELETE', 'title' => 'Eliminar una minuta' ],
		[ 'name' => 'MEMORANDUM_SHOW', 'title' => 'Ver una minuta' ],

		[ 'name' => 'INVOICE_LIST', 'title' => 'Listar las facturas' ],
		[ 'name' => 'INVOICE_CREATE', 'title' => 'Crear una factura' ],
		[ 'name' => 'INVOICE_UPDATE', 'title' => 'Actualizar una factura' ],
		[ 'name' => 'INVOICE_DELETE', 'title' => 'Eliminar una factura' ],
		[ 'name' => 'INVOICE_SHOW', 'title' => 'Ver una factura' ],


		[ 'name' => 'TRANSACTION_CATEGORY_LIST', 'title' => 'Listar las categorías de transacciones' ],
		[ 'name' => 'TRANSACTION_CATEGORY_CREATE', 'title' => 'Crear una categoría de transacción' ],
		[ 'name' => 'TRANSACTION_CATEGORY_UPDATE', 'title' => 'Actualizar una categoría de transacción' ],
		[ 'name' => 'TRANSACTION_CATEGORY_DELETE', 'title' => 'Eliminar una categoría de transacción' ],
		[ 'name' => 'TRANSACTION_CATEGORY_SHOW', 'title' => 'Ver una categoría de transacción' ],


        [ 'name' => 'FISCAL_DATA_LIST', 'title' => 'Listar datos fiscales del cliente' ],
        [ 'name' => 'FISCAL_DATA_CREATE', 'title' => 'Crear un dato fiscal para el ciente' ],
        [ 'name' => 'FISCAL_DATA_UPDATE', 'title' => 'Actualizar datos fiscales de los clientes' ],
        [ 'name' => 'FISCAL_DATA_DELETE', 'title' => 'Eliminar datos fiscales de los clientes' ],
        [ 'name' => 'FISCAL_DATA_SHOW', 'title' => 'Ver dato fiscal del cliente' ],

        [ 'name' => 'TAX_DOCUMENT_LIST', 'title' => 'Listar los documentos fiscales' ],
        [ 'name' => 'TAX_DOCUMENT_CREATE', 'title' => 'Crear un documento fiscal' ],
        [ 'name' => 'TAX_DOCUMENT_UPDATE', 'title' => 'Actualizar un documento fiscal' ],
        [ 'name' => 'TAX_DOCUMENT_DELETE', 'title' => 'Eliminar un documento fiscal' ],
        [ 'name' => 'TAX_DOCUMENT_SHOW', 'title' => 'Ver un documento fiscal' ],

		[ 'name' => 'TAX_DOCUMENT_ITEM_LIST', 'title' => 'Listar los elementos de los documentos fiscales' ],
        [ 'name' => 'TAX_DOCUMENT_ITEM_CREATE', 'title' => 'Crear un elemento de documento fiscal' ],
        [ 'name' => 'TAX DOCUMENT_ITEM_UPDATE', 'title' => 'Actualizar un elemento de documento fiscal' ],
        [ 'name' => 'TAX_DOCUMENT_ITEM_DELETE', 'title' => 'Eliminar un elemento de documento fiscal' ],
        [ 'name' => 'TAX_DOCUMENT_ITEM_SHOW', 'title' => 'Ver un elemento de documento fiscal' ],

		[ 'name' => 'APPLIED_TAX_LIST', 'title' => 'Listar los impuestos aplicados' ],
        [ 'name' => 'APPLIED_TAX_CREATE', 'title' => 'Crear un impuesto aplicado' ],
        [ 'name' => 'APPLIED_TAX_UPDATE', 'title' => 'Actualizar un impuesto aplicado' ],
        [ 'name' => 'APPLIED_TAX_DELETE', 'title' => 'Eliminar un impuesto aplicado' ],
        [ 'name' => 'APPLIED_TAX_SHOW', 'title' => 'Ver un impuesto aplicado' ],

		[ 'name' => 'TENANT_UPDATE', 'title' => 'Actualizar los datos de un tenant' ],
	];

	// -------- ROLE PERMISSION ------------

	public const ROLE_PERMISSIONS = [
		[
			"role"        => "ROLE_GENERAL_ADMIN",
			"permissions" => [
				'USER_LIST',
				'USER_CREATE',
				'USER_UPDATE',
				'USER_DELETE',
				'USER_SHOW',

				'PROJECT_LIST',
				'PROJECT_CREATE',
				'PROJECT_UPDATE',
				'PROJECT_DELETE',
				'PROJECT_SHOW',

				'CLIENT_LIST',
				'CLIENT_CREATE',
				'CLIENT_UPDATE',
				'CLIENT_DELETE',
				'CLIENT_SHOW',

				'QUOTE_PRODUCT_LIST',
				'QUOTE_PRODUCT_CREATE',
				'QUOTE_PRODUCT_UPDATE',
				'QUOTE_PRODUCT_DELETE',
				'QUOTE_PRODUCT_SHOW',

				'QUOTE_LIST',
				'QUOTE_CREATE',
				'QUOTE_UPDATE',
				'QUOTE_DELETE',
				'QUOTE_SHOW',

				'USER_LIST',
				'USER_CREATE',
				'USER_UPDATE',
				'USER_DELETE',
				'USER_SHOW'
			]
		]
	];

	public const PERMISSION_GROUPS = [
		[
			'name'        => 'Listar usuarios',
			'code'        => 'LIST_ALL_USERS',
			'description' => 'Permite listar usuarios del sisitema',
			'permissions' => [
				'USER_LIST',
				'USER_SHOW'
			]
		],
		[
			'name'        => 'Administrar proyectos',
			'code'        => 'MANAGE_PROJECTS',
			'description' => 'Permite administrar proyectos del sistema',
			'permissions' => [
				'PROJECT_LIST',
				'PROJECT_CREATE',
				'PROJECT_UPDATE',
				'PROJECT_DELETE',
				'PROJECT_SHOW'
			]
		],
		[
			'name'        => 'Administrar minutas',
			'code'        => 'MANAGE_MEMORANDUMS',
			'description' => 'Permite administrar minutas del sistema',
			'permissions' => [
				'MEMORANDUM_LIST',
				'MEMORANDUM_CREATE',
				'MEMORANDUM_UPDATE',
				'MEMORANDUM_DELETE',
				'MEMORANDUM_SHOW'
			]
		],
		[
			'name'        => 'Administrar facturas',
			'code'        => 'MANAGE_INVOICES',
			'description' => 'Permite administrar facturas del sistema',
			'permissions' => [
				'INVOICE_LIST',
				'INVOICE_CREATE',
				'INVOICE_UPDATE',
				'INVOICE_DELETE',
				'INVOICE_SHOW'
			]
		],
		[
			'name'        => 'Administrar clientes',
			'code'        => 'MANAGE_CLIENTS',
			'description' => 'Permite administrar clientes del sistema',
			'permissions' => [
				'CLIENT_LIST',
				'CLIENT_CREATE',
				'CLIENT_UPDATE',
				'CLIENT_DELETE',
				'CLIENT_SHOW'
			]
		],
		[
			'name'        => 'Administrar productos de cotización',
			'code'        => 'MANAGE_QUOTE_PRODUCTS',
			'description' => 'Permite administrar productos de cotización del sistema',
			'permissions' => [
				'QUOTE_PRODUCT_LIST',
				'QUOTE_PRODUCT_CREATE',
				'QUOTE_PRODUCT_UPDATE',
				'QUOTE_PRODUCT_DELETE',
				'QUOTE_PRODUCT_SHOW'
			]
		],
		[
			'name'        => 'Administrar cotizaciones',
			'code'        => 'MANAGE_QUOTES',
			'description' => 'Permite administrar cotizaciones del sistema',
			'permissions' => [
				'QUOTE_LIST',
				'QUOTE_CREATE',
				'QUOTE_UPDATE',
				'QUOTE_DELETE',
				'QUOTE_SHOW'
			]
		],
		[
			'name'        => 'Administrar usuarios',
			'code'        => 'MANAGE_USERS',
			'description' => 'Permite administrar usuarios del sistema',
			'permissions' => [
				'USER_LIST',
				'USER_CREATE',
				'USER_UPDATE',
				'USER_DELETE',
				'USER_SHOW'
			]
		],
		[
			'name'        => 'Administrar transacciones de cotización',
			'code'        => 'MANAGE_QUOTE_TRANSACTIONS',
			'description' => 'Permite administrar transacciones de cotización del sistema',
			'permissions' => [
				'QUOTE_TRANSACTION_LIST',
				'QUOTE_TRANSACTION_CREATE',
				'QUOTE_TRANSACTION_UPDATE',
				'QUOTE_TRANSACTION_DELETE',
				'QUOTE_TRANSACTION_SHOW'
			]
		],
		[
			'name'        => 'Administrar transacciones',
			'code'        => 'MANAGE_TRANSACTIONS',
			'description' => 'Permite administrar transacciones del sistema',
			'permissions' => [
				'TRANSACTION_LIST',
				'TRANSACTION_CREATE',
				'TRANSACTION_UPDATE',
				'TRANSACTION_DELETE',
				'TRANSACTION_SHOW'
			]
		],
		[
			'name'        => 'Administrar categorías de transacciones',
			'code'        => 'MANAGE_TRANSACTION_CATEGORIES',
			'description' => 'Permite administrar las categorías de transacciones del sistema',
			'permissions' => [
				'TRANSACTION_CATEGORY_LIST',
				'TRANSACTION_CATEGORY_CREATE',
				'TRANSACTION_CATEGORY_UPDATE',
				'TRANSACTION_CATEGORY_DELETE',
				'TRANSACTION_CATEGORY_SHOW'
			]
		],
		[
			'name'        => 'Administrar pagos recurrentes',
			'code'        => 'MANAGE_RECURRING_PAYMENTS',
			'description' => 'Permite administrar pagos recurrentes del sistema',
			'permissions' => [
				'RECURRING_PAYMENT_LIST',
				'RECURRING_PAYMENT_CREATE',
				'RECURRING_PAYMENT_UPDATE',
				'RECURRING_PAYMENT_DELETE',
				'RECURRING_PAYMENT_SHOW'
			]
		],
		[
			'name'        => 'Administrar grupos de permisos de un usuario',
			'code'        => 'MANAGE_USER_PERMISSION_GROUPS',
			'description' => 'Permite administrar grupos de permisos de un usuario',
			'permissions' => [
				'USER_PERMISSION_GROUP_UPDATE'
			]
		],
		[
			'name'        => 'Listar cuentas bancarias',
			'code'        => 'LIST_ALL_BANK_ACCOUNT_LIST',
			'description' => 'Permite listar las cuentas bancarias',
			'permissions' => [
				'BANK_ACCOUNT_LIST',
				'BANK_ACCOUNT_SHOW'
			]
		],
		[
			'name'        => 'Administrar cuentas bancarias',
			'code'        => 'MANAGE_BANK_ACCOUNTS',
			'description' => 'Permite administrar las cuentas bancarias del sistema',
			'permissions' => [
				'BANK_ACCOUNT_LIST',
				'BANK_ACCOUNT_CREATE',
				'BANK_ACCOUNT_UPDATE',
				'BANK_ACCOUNT_DELETE',
				'BANK_ACCOUNT_SHOW'
			]
		]
	];

}

