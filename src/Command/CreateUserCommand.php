<?php

namespace App\Command;

use App\Helpers\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateUserCommand extends Command {
	protected static $defaultName = 'user:create';

	public function __construct( private UserManager $userManager ) {
		parent::__construct();
	}

	protected function configure() {

		$this
			->setDescription( 'Create a user' )
			->addArgument( 'username', InputArgument::REQUIRED, 'Username' )
			->addArgument( 'email', InputArgument::REQUIRED, 'Email' )
			->addArgument( 'password', InputArgument::REQUIRED, 'Password' )
			->addArgument( 'role', InputArgument::REQUIRED, 'Role name' )
			->addArgument( 'tenantId', InputArgument::REQUIRED, 'Tenant Id' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$io       = new SymfonyStyle( $input, $output );
		$username = $input->getArgument( 'username' );
		$email    = $input->getArgument( 'email' );
		$password = $input->getArgument( 'password' );
		$role     = $input->getArgument( 'role' );
		$tenantId = $input->getArgument( 'tenantId' );


		try {
			$this->userManager->create( $username, $email, $password, $username, $role, $tenantId );
			$io->success( "User $username created correctly with role" );

			return 0;
		} catch ( \Exception $exception ) {
			$io->error( $exception->getMessage() );

			return 1;
		}


	}
}
