<?php

namespace App\Command;

use App\Helpers\RecurringPaymentsHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateRecurringTransactionsCommand extends Command {
	protected static $defaultName = 'create:recurringTransactions';

	public function __construct( private RecurringPaymentsHelper $recurringPaymentsHelper ) {
		parent::__construct();
	}

	protected function configure() {

	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$output->writeln( 'Start creating recurring transactions' );
		$transactionsCreated = $this->recurringPaymentsHelper->generateTransactions();
		$output->writeln( "Created $transactionsCreated transactions" );

		return 0;
	}
}
