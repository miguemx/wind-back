<?php

namespace App\Command;

use App\Helpers\ClientStatsHelper;
use App\Helpers\ProjectStatsHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Helpers\SatWsHelper;

class SatWsExtractCommand extends Command {
	protected static $defaultName = 'app:sat-ws-extract';

	public function __construct( private ClientStatsHelper $clientStatsHelper, private SatWsHelper $satWsHelper ) {
		parent::__construct();
	}

	protected function configure() {

		$this->setDescription( 'Create new extractions in satws for configured RFCs' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$io = new SymfonyStyle( $input, $output );

		try {
			// extract all rfc's
			$extractionsResult = $this->satWsHelper->generateExtractions();
			$io->success( "Extractions generated correctly" );
			return 0;
		} catch ( \Exception $exception ) {
			$io->error( $exception->getMessage() );

			return 1;
		}
	}
}
