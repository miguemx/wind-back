<?php

namespace App\Command;

use App\Entity\Quote;
use App\Helpers\ClientStatsHelper;
use App\Helpers\ProjectStatsHelper;
use App\Repository\QuoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ExpireQuotesCommand extends Command {
	protected static $defaultName = 'app:expire-quotes';

	public function __construct( private EntityManagerInterface $em ) {
		parent::__construct();
	}

	protected function configure() {
		$this->setDescription( 'Find all pending quotes that expired and change their status' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$io = new SymfonyStyle( $input, $output );

		try {
			$res = $this->em->getRepository(Quote::class)->setExpireStatusOfQuotesBefore(new \DateTime());
			$io->success( "$res Quotes expired correctly" );
			return 0;
		} catch ( \Exception $exception ) {
			$io->error( $exception->getMessage() );

			return 1;
		}


	}
}