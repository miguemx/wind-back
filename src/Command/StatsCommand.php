<?php

namespace App\Command;

use App\Helpers\ClientStatsHelper;
use App\Helpers\ProjectStatsHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class StatsCommand extends Command {
	protected static $defaultName = 'app:compute-stats';

	public function __construct( private ClientStatsHelper $clientStatsHelper, private ProjectStatsHelper $projectStatsHelper ) {
		parent::__construct();
	}

	protected function configure() {

		$this->setDescription( 'Compute and persist stats of all clients' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$io = new SymfonyStyle( $input, $output );

		try {
			$clientsCount = $this->clientStatsHelper->computeAllStatsForAllClients();
			$io->success( "Stats computed correctly for $clientsCount clients" );
			$projectsCount = $this->projectStatsHelper->computeAllStatsForAllProjects();
			$io->success( "Stats computed correctly for $projectsCount projects" );

			return 0;
		} catch ( \Exception $exception ) {
			$io->error( $exception->getMessage() );

			return 1;
		}


	}
}
