<?php

namespace App\Security;


use App\Helpers\TideAclHelper;
use App\Interfaces\AclEnabledInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TideAclVoter extends Voter {

	public function __construct( private TideAclHelper $aclHelper, private EntityManagerInterface $em ) {
	}

	/**
	 * @param $attribute
	 * @param null $subject
	 */
	protected function supports( $attribute, $subject = null ): bool {
		return $subject instanceof AclEnabledInterface;
	}

	/**
	 * @throws \Exception
	 */
	protected function voteOnAttribute( $attribute, $subject, TokenInterface $token ) {
		$user             = $token->getUser();
		$securityIdentity = $this->aclHelper->getSecurityIdentity( $user );

		return $this->aclHelper->checkObjectAclPermission( $subject, $securityIdentity, $attribute );
	}
}
