<?php

namespace App\Security;


use App\Entity\Permission;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PermissionVoter extends Voter {

	public function __construct( private AccessDecisionManagerInterface $decisionManager, private EntityManagerInterface $entityManager ) {
	}


	protected function supports( $attribute, $subject = null ) {
		if ( $subject !== null ) {
			false;
		}

		$permissionRepository = $this->entityManager->getRepository( Permission::class );
		if ( ! $permissionRepository->findOneBy( [ 'name' => $attribute ] ) ) {
			return false;
		}

		return true;
	}

	protected function voteOnAttribute( $attribute, $subject, TokenInterface $token ) {
		/**
		 * @var User $user
		 */
		$user     = $token->getUser();
		$userRole = $user->getRole();
		if ( $userRole && $userRole->getName() === 'SUPER_ADMIN' ) {
			return true;
		}

		// ROLE_SUPER_ADMIN can do anything! The power!
		if ( $this->decisionManager->decide( $token, array( 'SUPER_ADMIN' ) ) ) {
			return true;
		}

		/**
		 * @var User $user
		 */
		$user            = $token->getUser();
		$userPermissions = $user->getPermissionsArray();

		if ( ! in_array( $attribute, $userPermissions ) ) {
			return false;
		}

		return true;
	}
}
