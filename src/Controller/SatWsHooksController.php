<?php

namespace App\Controller;

use App\Repository\ProjectRepository;
use App\Repository\TenantRfcRepository;
use App\Repository\QuoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Helpers\SatWsHelper;
use Psr\Log\LoggerInterface;
use App\Entity\FiscalData;
use App\Entity\TenantRfc;

class SatWsHooksController extends AbstractController {

    const SATWS_EXTRACTION_UPDATED = 'extraction.updated';
    const SATWS_CREDENTIAL_UPDATED = 'credential.updated';

    public function __construct( 
        private EntityManagerInterface $em, 
        private SatWsHelper $satWsHelper, 
        private LoggerInterface $logger ) {

    }

	#[Route( path: '/satws/hooks/extraction_updated', name: 'satwshooks_extraction_updated' )]
	public function extractionUpdated(Request $request) {
        $content = $request->getContent();
        $hookData = json_decode( $content, true );
        $rfc = '';
        try {
            if( self::SATWS_EXTRACTION_UPDATED === $hookData['type'] && $hookData['data']['object']['finishedAt'] ) {
                $rfc = $hookData['data']['object']['taxpayer']['id'];
                $fiscalDataRepo = $this->em->getRepository( TenantRfc::class );
                $fiscalDatas = $fiscalDataRepo->findBy( [ 'rfc' => $rfc ] );
                $this->logger->info( "Webhook satwshooks_extraction_updated attempt to find  RFC {$rfc}" );
                foreach( $fiscalDatas as $fiscalData ) {
                    $tenant = $fiscalData->getTenant();
                    $tenantAlias = $tenant->getAlias();
                    $this->satWsHelper->getTaxDocuments( $rfc, $tenant );
                    $this->logger->info( "Webhook satwshooks_extraction_updated Extracted {$tenantAlias} with RFC {$rfc}" );
                }
                $this->logger->info( "Webhook satwshooks_extraction_updated Extraction event completed" );
            }
            else {
                $this->logger->warning( "Webhook satwshooks_extraction_updated not finnished yet" );
            }
        }
        catch ( \Doctrine\DBAL\Exception\NotNullConstraintViolationException $ex ) {
            $this->logger->error( "Error on Hook satwshooks_extraction_updated: CRITICAL {$ex->getMessage()}" );
        }
        catch (\Exception $ex) {
            $this->logger->error( "Error on Hook satwshooks_extraction_updated: {$ex->getMessage()}" );
        }
        
		return new JsonResponse( [
			'data' => [
				'message' => 'Hook received successfully',
			]
		] );
	}

    #[Route( path: '/satws/hooks/credential_updated', name: 'satwshooks_credential_updated' )]
	public function credentialUpdated(Request $request) {
        $content = $request->getContent();
        $hookData = json_decode( $content, true );
        try {
            if( self::SATWS_CREDENTIAL_UPDATED === $hookData['type'] && $hookData['data']['changes']['status']['new'] && $hookData['data']['object']['id'] ) {
                $tenantRfcRepo = $this->em->getRepository( TenantRfc::class );
                $tenantRfcs = $tenantRfcRepo->findBy( [ 'satWsId' => $hookData['data']['object']['id'] ] );
                foreach( $tenantRfcs as $tenantRfc ) {
                    $satWsId = $hookData['data']['object']['id'];
                    $rfc = $tenantRfc->getRfc();
                    $tenantRfc->setSatWsStatus( $hookData['data']['changes']['status']['new'] );
                    $tenantRfc->setIsSatWsConfigured( true );
                    $this->em->persist($tenantRfc);
                    $this->em->flush();
                    $this->logger->info( "Webhook satwshooks_cretendial_updated UPDATED {$satWsId} with RFC {$rfc}" );
                }
            }
        }
        catch (\ErrorException $ex) {
            $this->logger->error( "Error on Hook satwshooks_cretendial_updated: {$ex->getMessage()}" );
        }
		return new JsonResponse( [
			'data' => [
				'message' => 'Hook received successfully',
			]
		] );
	}

    #[Route(path: '/api/satwsconnect/{id}', methods: ['PUT'], name: 'satwshooks_connect_rfc')]
    public function connectRfc(int $id, Request $request) {
        $content = $request->getContent(); 
        $params = json_decode( $content, true );
        $credentialId = '';
        try {
            $tenantRfcRepo = $this->em->getRepository( TenantRfc::class );
            $tenantRfc = $tenantRfcRepo->find( $id );

            $fiscalData = [
                'rfc' => $tenantRfc->getRfc(),
                'type' => 'ciec',
                'password' => $params['params']['ciec']
            ];
            $credentials = $this->satWsHelper->findOrCreate($fiscalData);
            if ( isset($credentials['id']) ) {
                $credentialId = $credentials['id'];
                $tenantRfc->setSatWsId( $credentials['id'] );
                $tenantRfc->setSatWsStatus( $credentials['status'] );
                $this->em->persist($tenantRfc);
                $this->em->flush();
            }
            else {
                throw new HttpException(400, 'No se pudo crear la credencial correctamente.');
            }
        }
        catch (HttpException $ex) {
            throw new HttpException(400, $ex->getMessage());
        }
        catch (\ErrorException $ex) {
            throw new HttpException(400, $ex->getMessage());
        }
        return new JsonResponse( [
            'data' => [
                'id' => $id,
                'satWsId' => $credentialId,		
            ]
        ] );
    }

    #[Route(path: '/api/satwsdelete/{id}', methods: ['DELETE'], name: 'satwshooks_delete_rfc')]
    public function deleteRfc(int $id) {
        try {
            $tenantRfcRepo = $this->em->getRepository( TenantRfc::class );
            $tenantRfc = $tenantRfcRepo->find( $id );
            
            if ( $tenantRfc ) {
                $idSatws = $tenantRfc->getSatWsId();
                $rfc = $tenantRfc->getRfc();
                if ( $this->satWsHelper->deleteCredentials( $idSatws, $rfc ) ) {
                    $this->em->remove($tenantRfc);
                    $this->em->flush();
                }
            }
            else {
                throw new HttpException(400, 'No se puede eliminar el RFC. Es probable que ya haya sido eliminado.');
            }
        }
        catch (\ErrorException $ex) {
            throw new HttpException(400, $ex->getMessage());
        }
        return new JsonResponse( [
            'data' => 'Deleted successfully'
        ] );
    }

    
    
}
