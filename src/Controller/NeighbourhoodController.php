<?php

namespace App\Controller;

use App\Entity\Neighbourhood;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class NeighbourhoodController extends AbstractController {
	#[Route( path: '/neighbourhoods', name: 'get_public_neighbourhoods', methods: [ 'GET' ], defaults: [ '_api_resource_class'            => Neighbourhood::class,
	                                                                                                     '_api_collection_operation_name' => 'get_public'
	] )]
	function getPublic( $data ) {
		return $data;
	}
}
