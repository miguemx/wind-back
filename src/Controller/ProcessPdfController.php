<?php

namespace App\Controller;

use App\Helpers\PdfReader;
use App\Helpers\TransactionsHelper;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProcessPdfController extends AbstractController {
	public function __construct( private FileUploader $fileUploader, private PdfReader $pdfReader, private TransactionsHelper $transactionsHelper ) {
	}

	#[Route( path: '/process/pdf', name: 'process_pdf' )]
	public function index(): Response {
		return $this->render( 'process_pdf/index.html.twig', [ 'controller_name' => 'ProcessPdfController', ] );
	}

	#[Route( path: '/process/pdfup', name: 'process_pdf_and_insert', methods: [ 'GET', 'POST' ] )]
	public function processAndInsert( Request $request ): Response {
		$file     = $request->files->get( 'file' );
		$filename = $file->getClientOriginalName();
		$this->fileUploader->upload( "public", $file, $filename );
		$this->pdfReader->setPdfPath( "public/$filename" );
		//$bankAccount = $this->bankAccountRepository->findOne(['id' => 1]);
		//$provider = $this->providerRepository->findOne(['id' => 1]);
		//dd($bankAccount, $provider);
		$transactions = $this->pdfReader->getPdfData();
		dd( "inController", $transactions );
		$this->transactionsHelper->saveTransactionsFromPdfData( 1, $transactions );

		return $this->render( 'process_pdf/transactions.html.twig', [ 'data' => $transactions, 'saved' => true ] );
	}

	#[Route( path: '/process/pdfup', name: 'process_pdfup', methods: [ 'GET', 'POST' ] )]
	public function process( Request $request ): Response {
		$file     = $request->files->get( 'file' );
		$filename = $file->getClientOriginalName();
		$this->fileUploader->upload( "public", $file, $filename );
		$this->pdfReader->setPdfPath( "public/$filename" );
		$data = $this->pdfReader->getPdfData();

		return $this->render( 'process_pdf/transactions.html.twig', [ 'data' => $data ] );
	}
}

