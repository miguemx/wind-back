<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController {
	/**
	 * Get a users with general permissions to an object
	 */
	#[Route( name: 'get_users_domain_permissions', path: '/api/users/domain_permissions', methods: [ 'GET' ] )]
	public function getUsersWithDomainPermissionsAction( Request $request, EntityManagerInterface $em ): JsonResponse {
		//Get entity param from request
		$entityName = $request->query->get( 'entityName' );
		$clientId   = $request->query->get( 'clientId' );
		if ( ! $entityName ) {
			throw new HttpException( 400, 'Entidad no encontrada' );
		}
		if ( ! $clientId ) {
			throw new HttpException( 400, 'Cliente no encontrado' );
		}
		$upperCaseEntityName = strtoupper( $entityName );
		$repo                = $em->getRepository( User::class );
		$users               = $repo->findByDomainPermissionGroups( $upperCaseEntityName, $clientId );

		return $this->json( [ 'data' => $users ], 200, [], [
			'groups' => [
				'user_read',
				'user_read_avatar',
				'user_read_role',
				'role_read',
				'role_read_name',
				'app_file_read'
			]
		] );
	}

	//Utiliza la primer acción definida de tipo get para linkear los resources, ejemplo en employee

	/**
	 * @return User||null
	 */
	#[Route( name: 'user_get', path: '/api/users/{id}', methods: [ 'GET' ], defaults: [ '_api_resource_class'      => User::class,
	                                                                                    '_api_item_operation_name' => 'get'
	] )]
	public function getAction( $data ) {
		return $data;
	}

	#[Route( name: 'user_me', path: '/api/me', methods: [ 'GET' ] )]
	public function meAction() {
		return $this->json( [ 'data' => $this->getUser() ], 200, [], [
			'groups' => [
				'user_read',
				'user_read_avatar',
				'user_read_role',
				'role_read',
				'role_read_name',
				'user_read_client',
									'client_read',
									'client_read_name',
								'app_file_read'
			]
		] );
	}

	#[Route( name: 'user_logout', path: '/logout', methods: [ 'OPTIONS', 'GET' ] )]
	public function logoutAction() {
		$response = new Response();
		$response->headers->setCookie( new Cookie( 'token', '' ) );
		$response->headers->set( 'Access-Control-Allow-Credentials', 'true' );

		return $response;
	}
}
