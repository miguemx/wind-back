<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Quote;
use App\Repository\ProjectRepository;
use App\Repository\QuoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController {

	#[Route( path: '/api/projects/general_stats', name: 'projects_general_stats' )]
	public function generalStatsAction( EntityManagerInterface $em ) {
		/**
		 * @var $projectRepo ProjectRepository
		 * @var $quoteRepo QuoteRepository
		 */
		$projectRepo               = $em->getRepository( Project::class );
		$quoteRepo                 = $em->getRepository( Quote::class );
		$activeProjectsCount       = $projectRepo->getActiveProjectsCount();
		$pendingQuotesCount        = $quoteRepo->getPendingQuotesCount();
		$pendingPaymentQuotesCount = $quoteRepo->getPendingPaymentQuotesCount();

		return new JsonResponse( [
			"data" => [
				"activeProjectsCount"       => $activeProjectsCount,
				"pendingQuotesCount"        => $pendingQuotesCount,
				"pendingPaymentQuotesCount" => $pendingPaymentQuotesCount,
			]
		] );
	}

}
