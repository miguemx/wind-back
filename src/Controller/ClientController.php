<?php namespace App\Controller;

use App\Entity\User;
use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;


class ClientController extends AbstractController {

    public function __construct(private EntityManagerInterface $em)
    {
    }

    #[Route(
        name: 'client_put', 
        path: '/api/clients/{id}', 
        methods: ['PUT'], 
        defaults: [ '_api_resource_class' => Client::class, '_api_item_operation_name' => 'put']
    )]
    public function putClient(Client $data, Request $request) : Client
    {
        $requestData = json_decode( $request->getContent(), true );
        $companyOwnerId = ( isset($requestData["companyOwnerId"]) ? $requestData["companyOwnerId"] : null );

        if($companyOwnerId){
            // if the mainContact change, update it
            $companyOwner = $data->getCompanyOwner();

            if($companyOwner->getId()!=$companyOwnerId){
                $newCompanyOwner = $this->getDoctrine()->getRepository(User::class)->find($companyOwnerId);
                
                if(!$newCompanyOwner){
                    throw new HttpException(Response::HTTP_NOT_FOUND, "Company owner not found");
                }
                
                // Clean old owner
                $companyOwner->setIsCompanyOwner(false);

                // Set new owner
                $newCompanyOwner->setIsCompanyOwner(true);

                $this->em->persist($companyOwner);
                $this->em->persist($newCompanyOwner);
                $this->em->flush();
            }
        }

        return $data;
    }
}