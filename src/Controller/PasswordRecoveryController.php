<?php

namespace App\Controller;

use App\Entity\User;
use App\Helpers\Mailer;
use App\Helpers\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class PasswordRecoveryController extends AbstractController {
	public function __construct( private EntityManagerInterface $em, private Environment $twig, private Mailer $mailer, private UserManager $userManager, private LoggerInterface $logger ) {
	}

	/**
	 * @return Response
	 * @throws \Exception
	 */
	#[Route( name: '/recover_password_request', path: 'recover_password_request', methods: [ 'POST' ] )]
	public function passwordRecoveryRequest( Request $request ) {
		$data  = json_decode( $request->getContent(), true );
		$email = $data['email'] ?? null;
		if ( ! $email ) {
			return new JsonResponse( [ 'data' => [ 'message' => 'No email provided' ] ], 400 );
		}
		$userRepo = $this->em->getRepository( User::class );
		$user     = $userRepo->findOneBy( [ 'email' => $email ] );
		if ( $user ) {
			$now = new \DateTime();
			$user->setRecoveryToken( $this->generateRandomToken() );
			$user->setRecoveryTokenCreationDate( $now );
			$this->em->persist( $user );
			$this->em->flush();

			try {
				$this->sendRecoveryPasswordEmail( $user );
			} catch ( \Exception $e ) {
				$this->logger->error( "Error sending password recovery email to {$user->getEmail()}: {$e->getMessage()}" );
				throw  new HttpException( Response::HTTP_INTERNAL_SERVER_ERROR, 'Ocurrió un error al enviar el correo de recuperación de contraseña.' );
			}
		}

		return new JsonResponse( [ 'data' => [ 'message' => 'The verification email has been sent' ] ], 200 );
	}

	/**
	 * @return Response
	 * @throws \Exception
	 */
	#[Route( name: '/reset_password', path: 'reset_password', methods: [ 'POST' ] )]
	public function resetPassword( Request $request ) {
		$data     = json_decode( $request->getContent(), true );
		$password = $data['password'] ?? null;
		$token    = $data['token'] ?? null;
		if ( ! $password || ! $token ) {
			return new JsonResponse( [ 'data' => [ 'message' => 'Password and token not received' ] ], 400 );
		}
		$userRepo = $this->em->getRepository( User::class );
		/**
		 * @var User|null $user
		 */
		$user = $userRepo->findOneBy( [ 'recoveryToken' => $token ] );
		if ( ! $user ) {
			throw new HttpException( 400, 'Invalid or expired token' );
		}
		$user->setPassword( $this->userManager->encodePassword( $user, $password ) );
		$user->setRecoveryToken( null );
		$this->em->persist( $user );
		$this->em->flush();
		$this->sendRecoveryPasswordSuccessEmail( $user );

		return new JsonResponse( [ 'data' => [ 'message' => 'The password has been changed successfully' ] ], 200 );
	}

	/**
	 * @return string
	 */
	private function generateRandomToken() {
		$input         = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$input_length  = strlen( $input );
		$random_string = '';
		for ( $i = 0; $i < 200; $i ++ ) {
			$random_character = $input[ random_int( 0, $input_length - 1 ) ];
			$random_string    .= $random_character;
		}

		return $random_string;
	}

	private function sendRecoveryPasswordEmail( User $user ) {
		$renderMail = $this->twig->render( 'mail/passwordRecovery.html.twig', [ 'user' => $user ] );
		$this->mailer->sendEmailMessage( $renderMail, $user->getEmail(), 'Recuperación de contraseña', 'Sistema' );

	}

	private function sendRecoveryPasswordSuccessEmail( User $user ) {
		$renderMail = $this->twig->render( 'mail/passwordChanged.html.twig', [ 'user' => $user ] );
		$this->mailer->sendEmailMessage( $renderMail, $user->getEmail(), 'Contraseña restablecida', 'Sistema' );

	}
}