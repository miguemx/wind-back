<?php

namespace App\Controller;

use App\Entity\Quote;
use AppBundle\Entity\Crm\Quotation;
use AppBundle\Services\PdfRender;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;

class QuoteController extends AbstractController {

	/**
	 * @Route("api/quotes/pdf/{id}", name="quotation_render_pdf")
	 */
	public function renderPdf( Request $request, Pdf $knpSnappyPdf, KernelInterface $kernel, Quote $quote ) {

		$baseDir  = getenv( 'BACKEND_BASE_ROUTE' );
		$bodyHtml = $this->renderView( 'quote/pdf.html.twig', array(
			'quote'    => $quote,
			'base_dir' => $baseDir
		) );
		//return new Response($bodyHtml);

		$footerHtml = $this->renderView( 'quote/footer.html.twig' );
		$knpSnappyPdf->setOption( 'margin-top', 10 );
		$knpSnappyPdf->setOption( 'margin-left', 0 );
		$knpSnappyPdf->setOption( 'margin-right', 0 );
		$knpSnappyPdf->setOption( 'margin-bottom', 10 );
		$knpSnappyPdf->setOption( 'footer-html', $footerHtml );


		return new PdfResponse(
			$knpSnappyPdf->getOutputFromHtml( $bodyHtml ),
			$quote->getFolio() . '.pdf'
		);

		/*
		$footer = $this->renderView('@App/Crm/quotation/footer.html.twig',[
			'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath()
		]);
		$pdfOptions = [
			'footer-html' => $footer,
			'margin-top' => 10,
			'margin-bottom' => 10
		];

		$pdf = $this->get(PdfRender::class);
//
//        return $this->render("@App/Crm/quotation/pdf_email.html.twig",
//            [
//                "quotation"=>$quotation,
//                "project"=>$project,
//                'base_dir' => $request->getBasePath()
//            ]);


		return $pdf->render("@App/Crm/quotation/pdf_email.html.twig",
			[
				"quotation"=>$quotation,
				"project"=>$project,
				'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath()
			],
			$pdfOptions);
		*/

	}
}
