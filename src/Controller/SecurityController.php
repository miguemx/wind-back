<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SecurityController extends AbstractController {
	private $jwtManager;
	private $tokenStorage;

	public function __construct(JWTTokenManagerInterface $jwtManager, TokenStorageInterface $tokenStorage) {
		$this->jwtManager = $jwtManager;
		$this->tokenStorage = $tokenStorage;
	}

	#[Route( path: '/login', name: 'login' )]
	public function login() {
		// replace this line with your own code!
		return $this->render( 'login.html.twig' );
	}

	#[Route( path: '/api/login_check', name: 'login_check' )]
	public function loginCheck() {
		// Never enter here
		throw new \Exception( 'Missconfiguration in login' );
	}

	#[Route( path: '/api/refresh_token', name: 'login_check_refresh', methods: [ 'POST' ] )]
	public function refreshtoken() {
		$token = null;
		$user = $this->tokenStorage->getToken()->getUser();

		if($user)
			$token = $this->jwtManager->create($user);

		return $this->json( [ 'data' => ['token' => $token] ], 200, [], [] );
	}
}
