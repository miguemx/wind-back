<?php

namespace App\Controller;

use App\Entity\AppFile;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\DownloadHandler;


use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Helpers\TideAclHelper;
use App\Helpers\AppFileHelper;
use Symfony\Component\Security\Core\Security;

use App\Entity\TideAcl;

class UploadController extends AbstractController {

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var DownloadHandler
     */
    private $downloadHandler;

    /**
	 * @var TokenStorageInterface
	 */
	private $tokenStorage;

    public function __construct(
        EntityManagerInterface $em,
        DownloadHandler $downloadHandler,
        TokenStorageInterface $tokenStorage,
        TideAclHelper $aclHelper,
        Security $security,
        AppFileHelper $appFileHelper
    )
    {
        $this->em = $em;
        $this->downloadHandler = $downloadHandler;
        $this->tokenStorage = $tokenStorage;
        $this->aclHelper = $aclHelper;
        $this->security = $security;
        $this->appFileHelper = $appFileHelper;
    }

	#[Route( path: '/api/file/{id}', methods: [ 'GET' ] )]

    public function getFile(Request $request, $id){
        $appFile = $this->em->find(AppFile::class, $id);

        if($appFile){
            //TODO: Implement security when the owner is a FileContainer
            //Disabling file security for now
            $isAccessAllowed = true;//$this->appFileHelper->determineAllowedAccess($appFile);
        }

        if(!$appFile || !$isAccessAllowed) {
			throw new NotFoundHttpException( 'File not found' );
		}
		try {
			$response = $this->downloadHandler->downloadObject( $appFile, $fileField = 'file', $objectClass = null, $appFile->getOriginalName() );
			$response->headers->set( 'Content-Disposition', 'inline' );
			$response->headers->set( 'Content-Type', $appFile->getMimeType() );
			$response->headers->set( 'x-file-name', $appFile->getName() );

			return $response;
		} catch ( \Exception ) {
			throw new NotFoundHttpException( 'File not found in filesystem' );
		}
	}

	#[Route( path: '/api/file/download/{id}', methods: [ 'GET' ] )]
	public function downloadFile( $id, ParameterBagInterface $parameterBag ) {
		$appFile = $this->em->find( AppFile::class, $id );
		if ( ! $appFile ) {
			throw new NotFoundHttpException( 'File not found' );
		}
		$isAccessAllowed = $this->appFileHelper->determineAllowedAccess($appFile);

        // From https://ourcodeworld.com/articles/read/329/how-to-send-a-file-as-response-from-a-controller-in-symfony-3
        $uploadsFolder = $parameterBag->get('kernel.project_dir').'/uploads/';
        $filePath = $uploadsFolder.$appFile->getName();

        if(!file_exists($filePath) || !$isAccessAllowed) {
			throw new NotFoundHttpException( 'File not found' );
		}
		// This should return the file to the browser as response
		$response = new BinaryFileResponse( $filePath );
		$response->headers->set( 'Content-Type', $appFile->getMimeType() );
		// Set content disposition inline of the file
		$response->setContentDisposition( ResponseHeaderBag::DISPOSITION_ATTACHMENT, $appFile->getOriginalName() );

		return $response;
	}


}
