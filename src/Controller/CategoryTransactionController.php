<?php

namespace App\Controller;

use App\Helpers\{TransactionCategoryHelper, TransactionHelper};
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CategoryTransactionController {
	public function __construct( private TransactionCategoryHelper $transactionCategoryHelper, private TransactionHelper $transactionHelper ) {
	}

	#[Route( path: '/api/transaction_categories/expenses_by_category', name: 'transaction_categories_expenses_by_category' )]
	public function expensesByCategory( EntityManagerInterface $em, Request $request ) {
		$from = new \DateTime( 'first day of this month' );
		$to   = new \DateTime( 'last day of this month' );

		// start and end date
		if($request->query->has('from') && $request->query->has('to')){
			$from = new \DateTime($request->query->get('from'));
			$to = new \DateTime($request->query->get('to'));
		}

		return new JsonResponse( [
			'data' => $this->transactionCategoryHelper->getExpensesByCategory( $from, $to )
		] );
	}

	#[Route( path: '/api/transaction_categories/incomes_by_category', name: 'transaction_categories_incomes_by_category' )]
	public function incomesByCategory( EntityManagerInterface $em, Request $request ) {
		$from = new \DateTime( 'first day of this month' );
		$to   = new \DateTime( 'last day of this month' );
		$client = null;
		// start and end date
		if($request->query->has('from') && $request->query->has('to')){
			$from = new \DateTime($request->query->get('from'));
			$to = new \DateTime($request->query->get('to'));
		}

		// client
		if($request->query->has('client'))
			$client = $request->query->get('client');

		return new JsonResponse( [
			'data' => $this->transactionHelper->getIncomesByCategory( $from, $to, $client )
		] );
	}
}