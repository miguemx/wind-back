<?php

namespace App\Controller;

use App\Entity\TideAcl;
use App\Entity\User;
use App\Helpers\ControllerHelper;
use App\Helpers\TideAclHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class TideAclController extends AbstractController {
	/**
	 * Create or update an acl
	 */
	#[Route( name: 'post_tide_acl', path: '/api/tide_acls', methods: [ 'POST' ] )]
	public function createAction( Request $request, TideAclHelper $aclHelper, EntityManagerInterface $em, Security $security ): JsonResponse {
		//Get params from request
		[
			"domain"         => $domain,
			"objectId"       => $objectId,
			"user"           => $user,
			"permissionMask" => $permissionMask
		] = ControllerHelper::parseJsonRequest( $request, [ 'domain', 'objectId', 'user', 'permissionMask' ] );
		$permissionMask = (int) $permissionMask;
		//Check the actual user permission to update the acls of this object
		if ( ! $security->isGranted( 'TIDE_ACL_UPDATE' ) ) {
			$currentIdentity = $aclHelper->getSecurityIdentityFromUserId( $security->getToken()->getUser()->getId() );
			if ( ! $aclHelper->checkObjectIdAclPermissionMask( $domain, $objectId, $currentIdentity, TideAcl::ACL_PERMISSION_UPDATE_ACLS ) ) {
				throw new HttpException( 403, 'Access Denied' );
			}
		}
		//Find corresponding acl
		$aclRepo          = $em->getRepository( TideAcl::class );
		$securityIdentity = $aclHelper->getSecurityIdentityFromUserId( $user );
		$acl              = $aclRepo->findOneBy( [ 'domain'           => $domain,
		                                           'objectId'         => $objectId,
		                                           'securityIdentity' => $securityIdentity
		] );
		//If there's any permission set to the user, create or update the acl
		if ( $permissionMask ) {
			if ( ! $acl ) {
				$acl = new TideAcl( $domain, $objectId, $securityIdentity, $permissionMask );
			} else {
				$acl->setPermissionMask( $permissionMask );
			}
			$em->persist( $acl );
		}
		//If the permission is 0 delete de acl
		//Not sure if we should or shouldn't do this
		else if ( $acl ) {
			$em->remove( $acl );
			$acl = null;
		}
		$em->flush();

		return $this->json( [ 'data' => $acl ], 200, [], [ 'groups' => [ 'tide_acl_read' ] ] );
	}

	/**
	 * Get a list of users with permissions to an object
	 *
	 * @param string $domain
	 * @param string $objectId
	 *
	 * @throws HttpException
	 */
	#[Route( name: 'get_tide_acl_users', path: '/api/tide_acls/users', methods: [ 'GET' ] )]
	public function getUsersAction( Request $request, TideAclHelper $aclHelper, EntityManagerInterface $em ): JsonResponse {
		$domain   = $request->query->get( 'domain' );
		$objectId = $request->query->get( 'objectId' );
		//Check params and return 400 if any is missing
		if ( ! $domain ) {
			throw new HttpException( 400, 'No se encontró el dominio' );
		}
		if ( ! $objectId ) {
			throw new HttpException( 400, 'No se encontró el identificador del objeto' );
		}
		$aclRepo = $em->getRepository( TideAcl::class );
		$acls    = $aclRepo->findBy( [ "domain" => $domain, "objectId" => $objectId ] );
		// Get users id from acls
		$usersId = $aclHelper->getUsersIdFromAcls( $acls );
		// Get users from id
		$userRepo    = $em->getRepository( User::class );
		$users       = $userRepo->findBy( [ "id" => $usersId ] );
		$usersAndAcl = [];
		foreach ( $users as $user ) {
			$usersAndAcl[] = [
				"user" => $user,
				"acl"  => $aclHelper->getAclFromUserId( $user->getId(), $acls )
			];
		}

		return $this->json( [ 'data' => $usersAndAcl ], 200, [], [
			'groups' => [
				'user_read',
				'user_read_avatar',
				'user_read_role',
				'role_read',
				'role_read_name',
				'app_file_read',
				'tide_acl_read',
			]
		] );
	}
}
