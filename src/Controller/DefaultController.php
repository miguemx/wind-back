<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController {
	#[Route( path: '/', name: 'default' )]
	public function index() {
		return $this->render( 'login.html.twig' );
	}
}
