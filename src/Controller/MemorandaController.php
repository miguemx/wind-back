<?php

namespace App\Controller;

use App\Entity\Memorandum;
use AppBundle\Entity\Crm\Quotation;
use AppBundle\Services\PdfRender;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;

class MemorandaController extends AbstractController {

	/**
	 * @Route("api/memoranda/pdf/{id}", name="memorandum_render_pdf")
	 */
	public function renderPdf( Request $request, Pdf $knpSnappyPdf, KernelInterface $kernel, Memorandum $memoranda ) {

		$baseDir  = getenv( 'BACKEND_BASE_ROUTE' );
		$bodyHtml = $this->renderView( 'memoranda/pdf.html.twig', array(
			'memoranda'    => $memoranda,
			'base_dir' => $baseDir
		) );

		$footerHtml = $this->renderView( 'memoranda/footer.html.twig' );
		$knpSnappyPdf->setOption( 'margin-top', 10 );
		$knpSnappyPdf->setOption( 'margin-left', 0 );
		$knpSnappyPdf->setOption( 'margin-right', 0 );
		$knpSnappyPdf->setOption( 'margin-bottom', 10 );
		$knpSnappyPdf->setOption( 'footer-html', $footerHtml );


		return new PdfResponse(
			$knpSnappyPdf->getOutputFromHtml( $bodyHtml ),
			$memoranda->getFolio() . '.pdf'
		);
	}
}
