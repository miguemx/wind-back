<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Helpers\BelvoHelper;
use Symfony\Component\HttpFoundation\Request;

class BelvoController
{
    public function __construct(private BelvoHelper $belvoHelper)
    {
    }

    /**
     * @method("POST")
     */
    #[Route(path: '/api/belvo/generate_token', name: 'belvo_generate_token')]
    public function generateToken()
    {
        return new JsonResponse([
            'data' => $this->belvoHelper->generateToken()
        ]);
    }
    
    /**
     * @method("POST")
     */
    #[Route(path: '/api/belvo/get_accounts', name: 'belvo_get_accounts')]
    public function getAccounts(Request $request)
    {
        $linkId = $request->query->get('link_id');
        return new JsonResponse([
            'data' => $this->belvoHelper->getAccounts($linkId)
        ]);
    }

    /**
     * @method("POST")
     */
    #[Route(path: '/api/belvo/get_transactions', name: 'belvo_get_transactions')]
    public function getTransactions(Request $request)
    {
        $linkId = $request->query->get('link_id');
        $page = $request->query->get('page')??1;
        $trxs = $this->belvoHelper->getTransactions($linkId, $page);
        return new JsonResponse([
            'data' => $trxs["data"],
            'meta' => [
                'currentPage'=> $page,
                'itemsPerPage'=> 10,
                'totalItems'=> $trxs["totalItems"]
            ]
        ]);
    }
}