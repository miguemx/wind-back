<?php

namespace App\Helpers;

use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class InitialsHelper
{
    private $initialsLenght = 3;
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em ){

        $this->em = $em;
    }
    
    /**
     * Returns the initials of the client name
     *
     * @param  string $name
     * @return string $initials
     */
    public function generateInitials($name, $tenant){
        $name = strtoupper($name);

        // get first letters of the initials
        $initials = $this->leftString(strtoupper(preg_replace('/(?<=\w)./', '', $name)), $this->initialsLenght);
        
        // if initials not have minium configured lenght, add more letters
        if(strlen($initials)<$this->initialsLenght) 
            $initials = $this->completeInitials($name, $initials);

        // check if initials are already used
        if($this->isInitialsUsed($initials, $tenant)){
            $initials = $this->generateNewInitials($name, $tenant, $initials);
        }

        if($this->isInitialsUsed($initials, $tenant)){
            throw new HttpException(400, 'No fue posible generar la iniciales, por favor ingrésalas en el formulario');
        }

        return $initials;
    }
    
    /**
     * Generates the initials with the algoritm when a initials is already used
     *
     * @param  string $name
     * @param  string $initials
     * @return string $initials
     */
    private function generateNewInitials($name, $tenant, $initials){
        $lastInitialIndex=1;
        $secondInitialIndex=0;
        $lastWord = explode(" ", $name);
        $lastWord = $lastWord[count($lastWord)-1];
        $firstInitialIndex=0;

        // loop for first letter
        do{
            // loop for second letter
            do {
                // loop for last letter
                do {
                    $initials = $this->getNextInitialsLastLetter($lastWord, $initials, $lastInitialIndex);

                    if(!$this->isInitialsUsed($initials, $tenant))
                        continue;

                    $lastInitialIndex++; 
                } while ($this->isInitialsUsed($initials, $tenant) && $lastInitialIndex<=(strlen($lastWord)-1));

                if(!$this->isInitialsUsed($initials, $tenant))
                    continue;

                // change next letter
                $initials = $this->getNextInitialsSecondLetter($lastWord, $initials, $secondInitialIndex);
                $secondInitialIndex++;
                $lastInitialIndex=1;
            } while ($this->isInitialsUsed($initials, $tenant) && $secondInitialIndex<=(strlen($lastWord)-2));

            if(!$this->isInitialsUsed($initials, $tenant))
                continue;

            $initials = $this->getNextInitialsFirstLetter($lastWord, $initials, $firstInitialIndex);
            $secondInitialIndex=1;
            $lastInitialIndex=1;
            $firstInitialIndex++;
        } while(($firstInitialIndex<=strlen($lastWord)-$this->initialsLenght && $this->isInitialsUsed($initials, $tenant)));

        return $initials;
    }
    
    /**
     * Returns the next initials changing the first initial
     *
     * @param  string $lastWord
     * @param  string $initials
     * @param  string $index
     * @return void
     */
    private function getNextInitialsFirstLetter($lastWord, $initials, $index)
    {
        return $lastWord[$index] . $this->rightString($initials, $this->initialsLenght-1);
    }
    
    /**
     * Returns the next initials changing the second initial
     *
     * @param  string $lastWord
     * @param  string $initials
     * @param  string $index
     * @return void
     */
    private function getNextInitialsSecondLetter($lastWord, $initials, $index)
    {
        return $initials[0] . substr($lastWord, $index, 1) . $initials[strlen($initials)-1];
    }

    /**
     * Returns the next initials changing the last initial
     *
     * @param  string $lastWord
     * @param  string $initials
     * @param  string $index
     * @return string $initials
     */
    private function getNextInitialsLastLetter($lastWord, $initials, $index){
        $initials = $this->leftString($initials, $this->initialsLenght-1);
        return $initials . substr($lastWord, $index, 1);
    }
        
    /**
     * Add more letters to the initials
     *
     * @param  string $name
     * @param  string $initials
     * @return void
     */
    private function completeInitials($name, $initials){
        $lastName = explode(" ", $name)[strlen($initials)-1];
        
        // only one word
        if($name===$lastName)
            return $this->leftString($name, $this->initialsLenght);
        
        // more than one word
        $adjustsubString = strlen($initials)-1;
        return $initials . substr($lastName, ($adjustsubString), ($this->initialsLenght - $adjustsubString -1));
    }
    
    /**
     * Returns true if exists a client with the same initials
     *
     * @param  string $initials
     * @return boolean
     */
    private function isInitialsUsed($initials, $tenant){
        $clientRepository = $this->em->getRepository(Client::class);
        $clients = $clientRepository->findBy(['initials' => $initials, 'tenant' => $tenant]);
        return count($clients)>0;
    }
    
    /**
     * REturns the left part of a string
     *
     * @param  string $str
     * @param  integer $length
     * @return string
     */
    private function leftString($str, $length) {
        return substr($str, 0, $length);
    }

    /**
     * Returns the right part of a string
     *
     * @param  string $str
     * @param  integer $length
     * @return string
     */
    private function rightString($str, $length) {
        return substr($str, -$length);
    }
}
