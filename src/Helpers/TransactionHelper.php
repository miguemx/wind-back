<?php

namespace App\Helpers;

use App\Entity\{Transaction, TransactionCategory};
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class TransactionHelper {

	public function __construct( private EntityManagerInterface $em, private LoggerInterface $logger ) {
	}

	public function buildTransactionFromAccountStatementBankTransaction( $bankTransaction, $transaction ) {
		$newTransaction = new Transaction();
		$newTransaction->setTransactionDate( $transaction['date'] );

		$amount = $transaction['amount'];
		$amount = str_replace( ',', '', $amount );
		$amount = str_replace( '.', '', $amount );
		$amount = str_replace( '$', '', $amount );
		$amount = str_replace( ' ', '', $amount );

		$amount = (int) $amount;

		$newTransaction->setAmount( $amount );
		$newTransaction->setConcept( $transaction['concept'] );
		$newTransaction->setBankTransaction( $bankTransaction );
		$newTransaction->setTransactionType( $transaction['type'] );

		$this->em->persist( $newTransaction );
	}

	public function getIncomesByCategory( $from, $to, $client )
	{
		$data = [];
		$transactionsByCategory = $this->em->getRepository( Transaction::class )->getIncomesByCategoriesBetweenDates( $from, $to, $client );

		foreach ( $transactionsByCategory as $transactionByCategory ) {
			$categoryName = 'Sin categoria';

			if ( $transactionByCategory['trxCatId'] ) {
				// search category
				$category = $this->em->getRepository( TransactionCategory::class )->find( $transactionByCategory['trxCatId'] );
				$categoryName = $category->getName();
			}

			$data[] = [
				'name' => $categoryName,
				'total'   => $transactionByCategory['total'],
			];
		}

		return $data;
	}
}
