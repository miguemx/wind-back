<?php

namespace App\Helpers;
use App\Interfaces\FolioInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BelvoHelper
{
    /**
     * Belvo doc reference
     * https://developers.belvo.com/reference/using-the-api-reference
     */

    private $httpClient;
    private $apiURL = '';
    private $scopes = ['read_institutions','write_links','read_links'];

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;

        if(getenv('BELVO_IS_PROD') == 1) {
            $this->apiURL = 'https://api.belvo.com/api';
        } else {
            $this->apiURL = 'https://sandbox.belvo.com/api';
        }
    }
    
    /**
     * generate the token for the user in Belvo
     *
     * @return void
     */
    public function generateToken()
    {
        $response = null;

        $postParams = [
            'scopes' => implode(',', $this->scopes),
            'id' => getenv('BELVO_SECRET'),
            'password' => getenv('BELVO_PASSWORD')
        ];

        $response = $this->httpClient->request(
            "POST",
            "{$this->apiURL}/token",
            [
                'headers' => ['Content-Type' => 'application/json'],
                'json' => $postParams
            ],
        );

        $statusCode = $response->getStatusCode();

        if($statusCode != 200) {
            throw new HttpException(400, 'Error generating token');
        }

        $content = json_decode($response->getContent(false));

        return $content;
    }

    /**
     * Returns the institutions list: 
     *  https://developers.belvo.com/reference/retrieveaccounts
     */
    public function getAccounts($linkId)
    {
        $response = null;
        
        $response = $this->httpClient->request(
            "GET",
            "{$this->apiURL}/accounts?link=$linkId",
            [
                'auth_basic' => [getenv('BELVO_SECRET'), getenv('BELVO_PASSWORD')]
            ],
        );

        $statusCode = $response->getStatusCode();

        if($statusCode != 200) {
            throw new HttpException(400, 'Error getting accounts');
        }

        $content = json_decode($response->getContent(false), true);

        return $content["results"];
    }
    
    /**
     * Returns all transactions for the give linkId
     *
     * https://developers.belvo.com/reference/retrievetransactions
     * 
     * @param  string $linkId
     * @param  integer $page
     * @return array
     */
    public function getTransactions($linkId, $page=1)
    {
        $response = null;
        
        $response = $this->httpClient->request(
            "GET",
            "{$this->apiURL}/transactions?link={$linkId}&page={$page}",
            [
                'auth_basic' => [getenv('BELVO_SECRET'), getenv('BELVO_PASSWORD')]
            ],
        );

        $statusCode = $response->getStatusCode();

        if($statusCode != 200) {
            throw new HttpException(400, 'Error getting transactions');
        }

        $content = json_decode($response->getContent(false), true);

        return [
            'data' => (isset($content["results"]) ? $content["results"] : []),
            'totalItems' => (isset($content["count"]) ? $content["count"] : 0)
        ];
    }

    private function getErrorDescription($statusCode, $response)
    {
        $responseDescription = '';

        switch ($statusCode) {
            case 400:
                $responseDescription = 'Bad Request';
                break;
            
            case 401:
                $responseDescription = 'Unauthorized';
                break;

            case 403:
                $responseDescription = 'Forbidden';
                break;

            case 408:
                $responseDescription = 'Request Timeout';
                break;
            
            case 428:
                $responseDescription = 'MFA token Required';
                break;

            default:
                break;
        }

        $responseDescription .= ": {$response->getContent(false)}";

        return $responseDescription;
    }
}
