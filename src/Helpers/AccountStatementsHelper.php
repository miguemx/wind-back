<?php

namespace App\Helpers;

use App\Entity\AccountStatement;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class AccountStatementsHelper {

	public function __construct( private EntityManagerInterface $em, private PdfReader $pdfReader, private LoggerInterface $logger, private BankTransactionHelper $bankTransactionHelper ) {
	}

	public function buildAccountStatementFromPDF( AccountStatement $accountStatement, $pdf ) {
		$parsedPdf = $this->pdfReader->getPdfData( $pdf );

		if ( array_key_exists( "avg_balance", $parsedPdf ) ) {
			$averageBalance = $parsedPdf["avg_balance"];
			$averageBalance = str_replace( ",", "", $averageBalance );

			$accountStatement->setAverageBalance( $averageBalance );
		}
		$accountStatement->setTotalIncome( $parsedPdf["incomes"] );
		$accountStatement->setTotalExpense( $parsedPdf["expenses"] );
		$transactions = $parsedPdf['transactions'];

		foreach ( $transactions as $transaction ) {
			$this->bankTransactionHelper->buildBankTransactionFromAccountStatementTransaction( $accountStatement, $transaction );
		}
	}
}
