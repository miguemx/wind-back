<?php

namespace App\Helpers;

use App\Entity\RecurringPayment;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;

/**
 * This class creates the transactions from the recurring payments on the record's payment date
 */
class RecurringPaymentsHelper {
	public function __construct( protected EntityManagerInterface $em ) {
	}

	/**
	 * create transactions if the record's payment date is today
	 *
	 * @return int
	 */
	public function generateTransactions() {
		$now                 = ( new \DateTime() )->format( 'Y-m-d' );
		$recurringPayments   = $this->em->getRepository( \App\Entity\RecurringPayment::class )->getActive();
		$transactionsCreated = 0;

		foreach ( $recurringPayments as $recurringPayment ) {
			$isToday = $this->paymentDateIsToday( $recurringPayment );
			if ( $isToday ) {
				$this->createTransaction( $recurringPayment );
				$transactionsCreated ++;
			}
		}

		return $transactionsCreated;
	}

	/**
	 * creates a transaction from a recurring payment
	 *
	 * @param mixed $recurringPayment
	 *
	 * @return void
	 */
	private function createTransaction( RecurringPayment $recurringPayment ) {
		$transaction = new Transaction();
		$transaction->setRecurringPayment( $recurringPayment );
		$transaction->setAmount( $recurringPayment->getAmount() );
		$transaction->setTransactionDate( new \DateTime() );
		$transaction->setTransactionType( Transaction::TRANSACTION_TYPE_EXPENSE );
		$transaction->setStatus( Transaction::TRANSACTION_STATUS_PENDING );
		$transaction->setTenant( $recurringPayment->getTenant() );
		$transaction->setConcept( $recurringPayment->getConcept() );

		$this->em->persist( $transaction );
		$this->em->flush();
	}


	/**
	 * Methods for generate a list of dates
	 *
	 * @return bool
	 */
	public function paymentDateIsToday( RecurringPayment $recurringPayment ) {
		if ( $this->isValidConfig( $recurringPayment ) ) {
			$nextPaymentDate = $recurringPayment->getNextPaymentDate();

			return ( ( new \Datetime() )->format( 'Y-m-d' ) == $nextPaymentDate );
		}

		return false;
	}

	/**
	 * Check if the settings for the new recurring payment are valid.
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function isValidConfig( $recurringPayment ) {
		if ( $recurringPayment->getFrequencyInterval() <= 0 ) {
			throw new \Exception( "Frequency interval must be greater than 0" );
		}

		// validation for yearly recurring payments
		if ( $recurringPayment->getRepeatIn() && $recurringPayment->getFrequencyType() == RecurringPayment::FREQUENCY_TYPE_YEARLY ) {
			// Validate year format
			$date = date( "Y" ) . "-" . $recurringPayment->getRepeatIn();
			$d    = \DateTime::createFromFormat( 'Y-m-d', $date );

			if ( ! ( $d && $d->format( 'Y-m-d' ) === $date ) ) {
				throw new \Exception( "Invalid repeat format for yearly payments" );
			}
		}

		if ( $recurringPayment->getRepeatIn() && $recurringPayment->getFrequencyType() == RecurringPayment::FREQUENCY_TYPE_MONTHLY ) {
			// Validate month format
			if ( (int) $recurringPayment->getRepeatIn() < 1 || (int) $recurringPayment->getRepeatIn() > 31 ) {
				throw new \Exception( "Invalid repeat format for monthly payments" );
			}
		}

		if ( $recurringPayment->getRepeatIn() && $recurringPayment->getFrequencyType() == RecurringPayment::FREQUENCY_TYPE_WEEKLY ) {
			// Validate weekly format
			if ( ! in_array( $recurringPayment->getRepeatIn(), [
				'monday',
				'tuesday',
				'wednesday',
				'thursday',
				'friday'
			] ) ) {
				throw new \Exception( "Invalid repeat format for weekly payments" );
			}
		}

		return true;
	}
}
