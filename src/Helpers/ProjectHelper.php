<?php

namespace App\Helpers;

use App\Entity\Channel;
use App\Entity\Conversation;
use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;

class ProjectHelper {

	public function __construct( private EntityManagerInterface $em, private ProjectStatsHelper $projectStatsHelper, private ClientStatsHelper $clientStatsHelper ) {
	}

	/**
	 * Compute ProjectStats and ClientStats after some change in a project or quote.
	 * @throws \Exception
	 */
	public function computeStatsAfterChange( Project $project, $andFlush = false ) {
		$this->projectStatsHelper->computeAllStats( $project->getProjectStats() );
		$this->clientStatsHelper->computeAllStats( $project->getClient()->getClientStats() );
		$this->em->persist( $project->getProjectStats() );
		$this->em->persist( $project->getClient()->getClientStats() );

		if ( $andFlush ) {
			$this->em->flush();
		}

	}

	public function createConversation( $project ) {
		$conversation = new Conversation();
		$conversation->setClient( $project->getClient() );
		$conversation->setParentClass( Project::class );

		$channel = new Channel();
		$channel->setName( 'Default' );
		$channel->setConversation( $conversation );
		$channel->setEnableInternalUsers( 1 );
		$channel->setEnableExternalUsers( 0 );
		$channel->setClient( $project->getClient() );

		$project->setConversation( $conversation );

		$this->em->persist( $conversation );
		$this->em->persist( $channel );
		$this->em->persist( $project );

		return $conversation;
	}
}
