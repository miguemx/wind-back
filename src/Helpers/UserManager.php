<?php

namespace App\Helpers;

use App\Entity\Role;
use App\Entity\Tenant;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Twig\Environment;

class UserManager {
	private \App\Repository\UserRepository|\Doctrine\Common\Persistence\ObjectRepository $userRepo;

	public function __construct( private EntityManagerInterface $em, private UserPasswordHasherInterface $encoder, private Environment $twig, private Mailer $mailer ) {
		$this->userRepo = $em->getRepository( User::class );
	}

	public function create( string $username, string $email, string $password, $name, $role = null, $tenantId = null ) {
		if ( $this->userRepo->findOneBy( [ 'username' => $username ] ) ) {
			throw new \Exception( 'User alredy exists' );
		}


		$user = new User();
		$user->setUsername( $username );
		$user->setName( $name );
		$user->setEmail( $email );
		$user->setPassword( $this->encodePassword( $user, $password ) );
		$user->setIsActive( true );

		if ( $role ) {
			$role = $this->em->getRepository( Role::class )->findOneBy( [ 'name' => $role ] );
			if ( ! $role ) {
				throw new \Exception( 'Role not found' );
			}

			$user->setRole( $role );
		}


		if ( $tenantId ) {
			$tenant = $this->em->getRepository( Tenant::class )->find( $tenantId );
			if ( ! $tenant ) {
				throw new \Exception( 'Tenant not found' );
			}

			$user->setTenant( $tenant );
		}


		$this->em->persist( $user );
		$this->em->flush();

		return $user;
	}

	public function encodePassword( $user, $password ) {
		//return $this->encoder->encodePassword($user, $password);

		return $this->encoder->hashPassword( $user, $password );
	}

	/**
	 * Creates tokend and Send email to user with password change link
	 *
	 * @param mixed $user
	 *
	 * @return void
	 */
	public function sendCredentials( $user ) {
		if ( ! $user ) {
			throw new \Exception( 'User not found' );
		}

		$now = new \DateTime();
		$user->setRecoveryToken( $this->generateRandomToken() );
		$user->setRecoveryTokenCreationDate( $now );
		$this->em->persist( $user );
		$this->em->flush();
		$this->sendResetPasswordEmail( $user );
	}

	/**
	 * Send reset password email
	 *
	 * @param mixed $user
	 *
	 * @return void
	 */
	private function sendResetPasswordEmail( User $user ) {
		$renderMail = $this->twig->render( 'mail/resetPassword.html.twig', [ 'user' => $user ] );

		//try {
		$this->mailer->sendEmailMessage( $renderMail, $user->getEmail(), 'Se ha creado una nueva cuenta de cliente en Wind para usted.', $_ENV["MAILER_USER"] );
		//} catch (\Exception $ex) {

		//}
	}


	/**
	 * generateRandomToken
	 *
	 * @return void
	 */
	private function generateRandomToken() {
		$input         = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$input_length  = strlen( $input );
		$random_string = '';
		for ( $i = 0; $i < 200; $i ++ ) {
			$random_character = $input[ random_int( 0, $input_length - 1 ) ];
			$random_string    .= $random_character;
		}

		return $random_string;
	}
}
