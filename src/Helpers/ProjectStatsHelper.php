<?php

namespace App\Helpers;

use App\Entity\Project;
use App\Entity\ProjectStats;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProjectStatsHelper {

	public function __construct( private EntityManagerInterface $em ) {
	}

	public function computeAllStatsForAllProjects(): int {
		/**
		 * @var $projects Project[]
		 */
		$projects = $this->em->getRepository( Project::class )->findAll();
		foreach ( $projects as $project ) {
			if ( ! $project->getProjectStats() ) {
				$project->setProjectStats( new ProjectStats() );
			}
			$this->computeAllStats( $project->getProjectStats() );
			$this->em->persist( $project->getProjectStats() );
		}

		$this->em->flush();

		return count( $projects );
	}


	/**
	 * @throws \Exception
	 */
	public function computeAllStats( ProjectStats $projectStats ) {
		if ( ! $projectStats->getProject() ) {
			throw new \Exception( 'Trying to compute $projectStats without and assigned project' );
		}
		/**
		 * @var $projectRepo ProjectRepository
		 */
		$projectRepo = $this->em->getRepository( Project::class );

		$projectStats->setQuotesCount( $projectRepo->getQuotesCount( $projectStats->getProject() ) );
		$projectStats->setPendingQuotesCount( $projectRepo->getPendingQuotesCount( $projectStats->getProject() ) );
		$projectStats->setApprovedQuotesCount( $projectRepo->getApprovedQuotesCount( $projectStats->getProject() ) );
		$projectStats->setAllQuotesSum( $projectRepo->getAllQuotesSum( $projectStats->getProject() ) );
		$projectStats->setApprovedQuotesSum( $projectRepo->getApprovedQuotesSum( $projectStats->getProject() ) );
		$projectStats->setPaymentsSum( $projectRepo->getPaymentsSum( $projectStats->getProject() ) );

	}

}
