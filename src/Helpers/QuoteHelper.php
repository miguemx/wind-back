<?php

namespace App\Helpers;

use App\Entity\Quote;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class QuoteHelper {

	public function __construct( private EntityManagerInterface $em ) {
	}

	/**
	 * updateQuoteReviewFields(Quote $quote, User $user)
	 *
	 * @param User $approver
	 *
	 * @return void
	 */
	public function updateQuoteReviewFields( Quote $quote, User $user ) {
		$quote->setReviewedBy( $user );
		$quote->setReviewedDate( new \DateTime() );
		$this->em->persist( $quote );
	}
}
