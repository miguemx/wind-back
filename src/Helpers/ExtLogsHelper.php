<?php

namespace App\Helpers;

use App\Interfaces\FolioInterface;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Loggable\Entity\LogEntry;
use Symfony\Component\HttpFoundation\JsonResponse;

class ExtLogsHelper{
	public function __construct( protected EntityManagerInterface $em ) {
	}


	public function getLogsForEntity($entity){
		$logsRepo = $this->em->getRepository(LogEntry::class);
		$entityLogs = $logsRepo->getLogEntries($entity);
		return $entityLogs;
	}

}
