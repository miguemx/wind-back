<?php

namespace App\Helpers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ControllerHelper {
	/**
	 * @param array $mandatoryFields List of strings for the mandatory keys
	 */
	public static function parseJsonRequest( Request $request, array $mandatoryFields = [] ): array {
		$data = json_decode( $request->getContent(), true );
		if ( ! $data ) {
			throw new HttpException( 400, 'Missing parameters' );
		}
		foreach ( $mandatoryFields as $field ) {
			if ( ! isset( $data[ $field ] ) ) {
				throw new HttpException( 400, 'Missing parameter ' . $field );
			}
		}

		return $data;
	}
}
