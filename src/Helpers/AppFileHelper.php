<?php
namespace App\Helpers;

use App\Entity\User;
use App\Entity\AppFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Helpers\TideAclHelper;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;

class AppFileHelper{

	public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage, Security $security)
	{
		$this->em = $em;
		$this->tokenStorage = $tokenStorage;
		$this->security = $security;
	}

	public function createFromUploadedFile(UploadedFile $uploadedFile, $type=AppFile::GENERAL_FILE){
		$appFile = new AppFile();
		$appFile->setType($type);
		$appFile->setFile($uploadedFile);
		return $appFile;
	}

	public function determineAllowedAccess(AppFile $appFile){
		$objectId = $appFile->getObjectId();
		$ownerDomain = $appFile->getOwnerDomain();
		$ownerObject = $this->em->getRepository($ownerDomain)->find($objectId);

		$classParts = explode('\\', $ownerDomain);
		$objectClassName = strtoupper(preg_replace('/(?<!^)[A-Z]/', '_$0', end($classParts) )).'_';

		$currentUser = $this->tokenStorage->getToken()->getUser();

		$allowedAccess = false;

		if($this->security->isGranted($objectClassName.'SHOW', $ownerObject)){
			$allowedAccess = true;
		}
		
		if($ownerObject instanceof User){
			if($objectId == $currentUser->getId()){
				$allowedAccess = true;
			}
		}

		return $allowedAccess;
	}
}
