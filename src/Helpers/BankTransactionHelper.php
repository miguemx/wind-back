<?php

namespace App\Helpers;

use App\Entity\BankTransaction;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class BankTransactionHelper {

	public function __construct( private EntityManagerInterface $em, private LoggerInterface $logger ) {
	}

	public function buildBankTransactionFromAccountStatementTransaction( $accountStatement, $transaction ) {
		$bankTransaction = new BankTransaction();
		$bankTransaction->setDate( $transaction['date'] );

		if ( array_key_exists( "code", $transaction ) ) {
			$bankTransaction->setCode( $transaction['code'] );
		}

		$amount = $transaction['amount'];
		$amount = str_replace( ',', '', $amount );

		$bankTransaction->setAmount( $amount );
		$bankTransaction->setConcept( $transaction['concept'] );
		$bankTransaction->setAccountStatement( $accountStatement );

		$this->em->persist( $bankTransaction );
	}
}
