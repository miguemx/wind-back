<?php

namespace App\Helpers;

use App\Entity\Channel;
use App\Entity\ChannelUser;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class ConversationHelper {
	public function __construct( protected EntityManagerInterface $em ) {
	}

	public function canSendMessagesToConversation( User $user, Channel $channel ) {
		// if user is admin can send messages to any conversation
		if ( $user->getRole() && $user->getRole()->getName() === "SUPER_ADMIN" ) {
			return true;
		}

		// if user is not admin, check if he is the user belongs to client in the channel
		$channel = $this->em->getRepository( Channel::class )->find( $channel->getId() );
		$entity  = $this->em->getRepository( $channel->getConversation()->getParentClass() )->findBy( [ 'conversation' => $channel->getConversation() ] );

		// check if the user is in the list of users for the channel
		$channelUser = $this->em->getRepository( ChannelUser::class )->findBy( [
			'channel' => $channel,
			'user'    => $user
		] );

		if ( ! $channelUser || ! $entity ) {
			return false;
		}

		return true;
	}
}
