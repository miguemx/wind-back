<?php

namespace App\Helpers;

use App\Entity\AccountStatement;
use App\Entity\BankAccount;
use App\Entity\Quote;
use App\Entity\Transaction;
use App\Entity\TransactionType;
use Doctrine\ORM\EntityManagerInterface;

class TransactionsHelper {
	private $year;

	private $month;

	public function __construct( protected EntityManagerInterface $em, private ProjectHelper $projectHelper ) {
	}

	/**
	 * this function will update all the quotes out of the quote transactions if the pending amount is 0.
	 * The resulting quote status will be paid.
	 * @return void
	 */
	public function updateTransactionQuotesStatus( Transaction $transaction ) {
		$quoteTransactions = $transaction->getQuoteTransactions();

		foreach ( $quoteTransactions as $quoteTransaction ) {
			$quote                  = $quoteTransaction->getQuote();
			$quoteTransactionAmount = $quoteTransaction->getAmount();

			$pendingAmount = $quote->getPendingAmount();
			$pendingAmount = $pendingAmount - $quoteTransactionAmount;

			if ( $pendingAmount > 0 ) {
				$quote->setPaymentStatus( Quote::PAYMENT_STATUS_PAYING );
			} else {
				$quote->setPaymentStatus( Quote::PAYMENT_STATUS_PAID );
			}

			$this->em->persist( $quote );
		}
	}

	/**
	 * saveTransactionsFromPdfData
	 *
	 * @param mixed $bankAccountId
	 * @param mixed $pdfData
	 *
	 * @return void
	 */
	public function saveTransactionsFromPdfData( $bankAccountId, $pdfData ) {
		$accountStatementDate = $this->getMonthAndYearFromPdfData( $pdfData );
		$this->year           = $this->getMonthAndYearFromPdfData( $pdfData )->format( 'Y' );
		$this->month          = $this->getMonthAndYearFromPdfData( $pdfData )->format( 'm' );

		// search for account statement for current date, if does not existe create new
		$accountStatement = $this->getAccountStatementByAccount( $bankAccountId, $accountStatementDate, $pdfData );

		foreach ( $pdfData["transactions"] as $transaction ) {
			$this->findOrCreateTransaction( $transaction, $accountStatement );
		}

		return $this->em->getRepository( Transaction::class )->findBy( [ 'accountStatement' => $accountStatement ] );
	}

	/**
	 * findOrCreateTransaction
	 *
	 * @param mixed $transaction
	 * @param mixed $accountStatement
	 *
	 * @return void
	 */
	private function findOrCreateTransaction( $transaction, $accountStatement ) {
		$transactionType = $this->getTransactionType( $transaction );

		$filters = [
			'bankAccount'     => $accountStatement->getBankAccount(),
			'transactionType' => $transactionType,
			'transactionDate' => $transaction["date"],
			'amount'          => $transaction["amount1"],
		];

		if ( ! empty( $transaction["code"] ) ) {
			$filters["code"] = $transaction["code"];
		}

		$_transaction = $this->em->getRepository( Transaction::class )->findOneBy( $filters );

		if ( ! $_transaction ) {
			$_transaction = new Transaction();
			$_transaction->setBankAccount( $accountStatement->getBankAccount(), );
			$_transaction->setAccountStatement( $accountStatement );
			$_transaction->setTransactionType( $transactionType );
			$_transaction->setTransactionDate( $transaction["date"] );
			$_transaction->setAmount( floatval( $transaction["amount1"] ) );
			$_transaction->setConcept( $transaction["concept"] );
			$_transaction->setCode( $transaction["code"] );
			$_transaction->setOperation( floatval( $transaction["amount3"] ) );
			$_transaction->setValidated( true );

			$this->em->persist( $_transaction );
			$this->em->flush();

			$transaction = $_transaction;
		}

		return $transaction;
	}

	/**
	 * getTransactionType
	 *
	 * @param mixed $transaction
	 *
	 * @return void
	 */
	private function getTransactionType( $transaction ) {
		return $this->em->getRepository( TransactionType::class )->findOneBy( [ 'name' => $transaction["type"] ] );
	}

	/**
	 * getMonthAndYearFromPdfData
	 *
	 * @param mixed $pdfData
	 *
	 * @return void
	 */
	private function getMonthAndYearFromPdfData( $pdfData ) {
		if ( ! isset( $pdfData["period"] ) ) {
			return null;
		}

		$period = explode( " ", $pdfData["period"] );

		return \DateTime::createFromFormat( 'd/m/Y', $period[1] );
	}

	/**
	 * getAccountStatementByAccount
	 *
	 * @param mixed $bankAccountId
	 * @param mixed $year
	 * @param mixed $month
	 * @param mixed $file
	 *
	 * @return void
	 */
	public function getAccountStatementByAccount( $bankAccountId, $date, $pdfData ) {
		$bankAccount = $this->em->getRepository( BankAccount::class )->findOneBy( [ 'id' => $bankAccountId ] );

		$filters = [
			'bankAccount' => $bankAccount,
			'year'        => $date->format( 'Y' ),
			'month'       => $date->format( 'm' )
		];

		$accountStatementEntity = $this->em->getRepository( AccountStatement::class )->findOneBy( $filters );

		if ( ! $accountStatementEntity ) {
			$accountStatementEntity = new AccountStatement();
			$accountStatementEntity->setBankAccount( $bankAccount );
			$accountStatementEntity->setYear( $date->format( 'Y' ) );
			$accountStatementEntity->setMonth( $date->format( 'm' ) );
			$accountStatementEntity->setFile( $pdfData["pdf_path"] );
			$accountStatementEntity->setAverageBalance( floatval( $pdfData["pdf_path"] ) );

			$this->em->persist( $accountStatementEntity );
			$this->em->flush();
		}

		return $accountStatementEntity;
	}

	/**
	 * Update all stats for each project in de transaction
	 *
	 * @param mixed $transacion
	 *
	 * @return void
	 */
	public function updateProjectsStatsByTransaction( Transaction $transaction ) {
		foreach ( $transaction->getQuoteTransactions() as $quoteTransaction ) {
			$project = $quoteTransaction->getQuote()->getProject();
			$this->projectHelper->computeStatsAfterChange( $project, true );
		}
	}
}
