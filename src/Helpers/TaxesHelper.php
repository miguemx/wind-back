<?php

namespace App\Helpers;

class TaxesHelper {
    public const TAX_ISR_CODE = "001";
    public const TAX_IVA_CODE = "002";
    public const TAX_IEPS_CODE = "003";
}