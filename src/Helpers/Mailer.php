<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Helpers;

use App\Entity\Notification;
use App\Entity\User;
use Aws\Ses\SesClient;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class Mailer {
	protected $rootDir;

	public function __construct( protected UrlGeneratorInterface $router, protected Environment $templating, KernelInterface $kernel, private SesClient $sesClient ) {
		$this->rootDir = $kernel->getProjectDir();
	}

	/**
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function sendNotification( Notification $notification ) {

		/**
		 * @var User $user
		 */
		$rendered = $this->templating->render( 'mail/notification.html.twig', [ 'notification' => $notification ] );

		return $this->sendEmailMessage( $rendered, $notification->getUser()->getEmail(), 'Notificación nueva', 'Sistema' );
	}

	public function sendEmailMessage( $renderedTemplate, $toEmail, $subject, $fromName, $emailExtra = null, $attachments = [], $replyTo = null ) {
		$textBody = '';

		if ( $toEmail ) {
			if ( $_ENV['APP_ENV'] === "dev" ) {
				$toEmail = $_ENV['DEV_EMAIL_RECEIVER'];
			}

			if ( ! $toEmail ) {
				return false;
			}

			if ( $fromName ) {
				$from = "$fromName <{$_ENV['MAILER_USER']}>";
			} else {
				$from = $_ENV['MAILER_USER'];
			}

			$mailResponse = $this->sesClient->sendEmail( [
				'Destination'      => [
					'ToAddresses' => [ $toEmail ],
				],
				'ReplyToAddresses' => [ $replyTo ?? $from ],
				'Source'           => $from,
				'Message'          => [
					'Body'    => [
						'Html' => [
							'Charset' => 'UTF-8',
							'Data'    => $renderedTemplate,
						],
						'Text' => [
							'Charset' => 'UTF-8',
							'Data'    => $textBody,
						],
					],
					'Subject' => [
						'Charset' => 'UTF-8',
						'Data'    => $subject
					],
				],
			] );

			return $mailResponse;
		}

		return false;
	}

}
