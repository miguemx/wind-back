<?php

namespace App\Helpers;

use App\Entity\TransactionCategory;
use Doctrine\ORM\EntityManagerInterface;

class TransactionCategoryHelper {
	public function __construct( protected EntityManagerInterface $em ) {
	}

	public function getExpensesByCategory( $from, $to ) {
		return $this->em->getRepository( TransactionCategory::class )->getExpensesByCategoriesBetweenDates( $from, $to );
	}

	public function getIncomesByCategory( $from, $to, $client ) {
		return $this->em->getRepository( TransactionCategory::class )->getIncomesByCategoriesBetweenDates( $from, $to, $client );
	}
}
