<?php

namespace App\Helpers;

use Doctrine\ORM\EntityManagerInterface;

class BankAccountHelper
{
    const CLABE_LENGTH = 18;
    const CLABE_WEIGHTS = [3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7];

    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

        
    /**
     * Returns true if the given CLABE is valid
     *
     * @param  string $clabe
     * @return bool
     */
    public function isValidCLABE(string $clabe): bool
    {
        $clabe = $this->cleanString($clabe);

        return (
            $this->isAllNumbers($clabe) &&
            strlen($clabe)===self::CLABE_LENGTH &&
            ((int)substr($clabe, self::CLABE_LENGTH-1, 1)===$this->calculateControlDigit($clabe))
        );
    }
    
    /**
     * Calculate control digit for CLABE according on https://es.wikipedia.org/wiki/CLABE#D.C3.ADgito_control
     *
     * @param  mixed $clabe
     * @return void
     */
    private function calculateControlDigit($clabe)
    {
        $weighted = [];

        if (strlen($clabe) !== 18) {
            return false;
        }

        $digits = str_split($clabe); // convert clabe to array

        $digits = array_map(function ($digit) { // all digits to integer
            return (int) $digit;
        }, $digits);

        for($i = 0; $i<self::CLABE_LENGTH-1; $i++) {
            $weighted[] = ($digits[$i] * self::CLABE_WEIGHTS[$i] % 10);
        }

        $summed = array_reduce($weighted, function ($carry, $item) {
            return $carry + $item;
        }) % 10;

        return (10 - $summed) % 10;
    }

    /**
     * will return true only if characters in a string are digits    
     *
     * @param  string $value
     * @return bool
     */
    private function isAllNumbers($value)
    {
        return preg_match('/^([0-9]*)$/', $value);
    }
    
    /**
     * Returns true if the account number is valid
     *
     * @param  string $accountNumber
     * @return bool
     */
    public function isValidAccountNumber(string $accountNumber): bool
    {
        $accountNumber = $this->cleanString($accountNumber);

        return (
            $this->isAllNumbers($accountNumber) && !empty($accountNumber)
        );
    }
    
    /**
     * Removes all formatting characters from the string
     *
     * @param  string $string
     * @return string
     */
    public function cleanString($string)
    {
        $string = str_replace('-', '', $string);
        $string = str_replace(' ', '', $string);
        $string = str_replace('_', '', $string);

        return $string;
    }
}
