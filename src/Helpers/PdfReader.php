<?php

namespace App\Helpers;

use App\Helpers\PdfReader\PdfReaderBancomer;
use App\Helpers\PdfReader\PdfReaderSantander;

class PdfReader {
	public const BANCOMER = 'bancomer';
	public const SANTANDER = 'santander';

	private $type;

	private $pdfPath;

	private $reader;

	private $pdfText;

	private $parser;

	private $pdfData;

	private $provider;

	private $bankAcount;

	public function __construct( private PdfReaderBancomer $pdfReaderBancomer, private PdfReaderSantander $pdfReaderSantander ) {
		$this->parser = new \Smalot\PdfParser\Parser();
	}

	public function setPdfPath( $pdfPath ) {
		$this->pdfPath = $pdfPath;

		$parsed        = $this->parser->parseFile( $this->pdfPath );
		$this->pdfText = $parsed->getText();
		$bank          = $this->getBank();

		switch ( $bank ) {
			case PdfReader::BANCOMER:
				$this->pdfReaderBancomer->setParsed( $parsed );
				$this->pdfData = $this->pdfReaderBancomer->getPdfData();
				break;

			case PdfReader::SANTANDER:
				$this->pdfReaderSantander->setParsed( $parsed );
				$this->pdfData = $this->pdfReaderSantander->getPdfData();
				break;
		}

		$this->pdfData["pdf_path"] = $this->pdfPath;
	}

	public function getPdfData( $pdf ) {
		$parsed        = $this->parser->parseFile( $pdf );
		$this->pdfText = $parsed->getText();

		$bank = $this->getBank();

		switch ( $bank ) {
			case PdfReader::BANCOMER:
				$this->pdfReaderBancomer->setParsed( $parsed );
				$this->pdfData = $this->pdfReaderBancomer->getPdfData();
				break;

			case PdfReader::SANTANDER:
				$this->pdfReaderSantander->setParsed( $parsed );
				$this->pdfData = $this->pdfReaderSantander->getPdfData();
				break;
		}

		return $this->pdfData;
	}

	private function getBank() {
		if ( $this->isBancomer() ) {
			return PdfReader::BANCOMER;
		}

		if ( $this->isSantander() ) {
			return PdfReader::SANTANDER;
		}


		throw new \Exception( "Lectura no implementada para ese banco." );
	}

	private function isBancomer() {
		return ( strpos( $this->pdfText, "BBVA" ) > 0 );
	}

	private function isSantander() {
		return ( strpos( $this->pdfText, "SANTANDER" ) > 0 );
	}
}
