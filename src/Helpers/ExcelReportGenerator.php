<?php

namespace App\Helpers;

use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;

class ExcelReportGenerator {

	public function generate( $dataArray, $config = [] ) {
		$headers = [];
		$writer  = WriterEntityFactory::createXLSXWriter();
		$writer->openToFile( 'php://output' );

		if ( ! isset( $config['headers'] ) ) {
			foreach ( $dataArray[0] as $path => $value ) {
				$headers[ $path ] = $path;
			}
		} else {
			$headers = $config['headers'];
		}

		$transStrings = $config['transStrings'] ?? [];

		$this->renderHeaders( $headers, $writer );

		foreach ( $dataArray as $data ) {
			$rowData = [];
			foreach ( $headers as $path => $title ) {
				$rowData[] = $this->formatCell( $data[ $path ] ?? '', $transStrings );
			}
			$row = WriterEntityFactory::createRowFromArray( $rowData );
			$writer->addRow( $row );
		}

		return $writer;
	}

	private function renderHeaders( $headers, $writer ) {

		$style = ( new StyleBuilder() )
			->setFontSize( 12 )
			->setFontColor( "FF44ABE4" )
			->setBackgroundColor( "FF44ABE4" )
			->setShouldWrapText()
			->setFontBold()
			->build();

		$row = WriterEntityFactory::createRowFromArray( $headers, $style );
		$writer->addRow( $row );
	}

	private function formatCell( $content, $transStrings ) {
		if ( isset( $transStrings[ $content ] ) ) {
			$content = $transStrings[ $content ];
		}

		return $content;
	}

}
