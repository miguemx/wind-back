<?php

namespace App\Helpers\PdfReader;

class PdfReaderSantander {
	/**
	 * codes of transactions for Bancomer
	 */
	private array $codes = [
		'A15' => [
			'description' => 'Pago',
			'type'        => 'egreso'
		],
		'N06' => [
			'description' => 'Pago a cuenta de tercero',
			'type'        => 'egreso'
		],
		'S39' => [
			'description' => 'Servicio banca internet',
			'type'        => 'egreso'
		],
		'S40' => [
			'description' => 'IVA COM Servicio banca internet',
			'type'        => 'egreso'
		],
		'T20' => [
			'description' => 'SPEI Recibido',
			'type'        => 'ingreso'
		]
	];

	private string $pdfText = '';

	private $pdfPath = null;

	private array $pdfData = [];

	private $parsed;

	public function setParsed( $parsed ) {
		$this->parsed = $parsed;
	}

	public function setPdfPath( $pdfPath ) {
		$this->pdfPath = $pdfPath;
	}

	/**
	 * returns array with the data of PDF
	 *
	 * @return void
	 */
	public function getPdfData() {
		if ( $this->parsed ) {
			$this->processPdf();
		}

		return $this->pdfData;
	}

	private function processPdf() {
		//$pdf = $parser->parseFile($this->pdfPath);
		$this->pdfText = $this->parsed->getText();

		$data                        = [];
		$data["period"]              = $this->getPeriod();
		$data["avg_balance"]         = $this->getAvgBalance();
		$data["transactions"]        = $this->getTransactions();
		$data["days_period"]         = $this->getDaysPeriod();
		$data["liq_balance"]         = $this->getInitialSettlementBalance();
		$data["income_and_expenses"] = $this->getInmcomesAndExpenses();

		$this->pdfData = $data;
	}

	/**
	 * returns the period in the PDF
	 *
	 * @return void
	 */
	private function getPeriod() {
		$pattern = '/PERIODO : .{5,}/';
		preg_match( $pattern, $this->pdfText, $result );
		$stringDate = str_replace( "PERIODO : ", "", $result );
		$startDay   = '';


		dd( $stringDate );

		return ( $result[0] ?? '' );
	}

	/**
	 * returns average balance
	 *
	 * @param mixed $fullText
	 *
	 * @return void
	 */
	private function getAvgBalance() {
		$pattern = '/Saldo Promedio.{1}[0-9].{1,}/';
		preg_match( $pattern, $this->pdfText, $result );

		return ( isset( $result[0] ) ? explode( "Saldo Promedio", $result[0] )[1] : '' );
	}

	/**
	 * returns all transactions detected in the PDF
	 *
	 * @param mixed $fullText
	 *
	 * @return void
	 */
	private function getTransactions() {
		$trxs         = explode( "\n", $this->pdfText );
		$formatedTrxs = [];

		for ( $i = 0; $i < count( $trxs ) - 1; $i ++ ) {
			$trx          = $trxs[ $i ];
			$nextTrx      = '';
			$currentIndex = $i;

			if ( isset( $trxs[ $currentIndex + 1 ] ) && ! $this->isTrx( $trxs[ $currentIndex + 1 ] ) && $this->isText( $trxs[ $currentIndex + 1 ] ) ) {
				$nextTrx .= "<br/>" . $trxs[ $currentIndex + 1 ];
			}
			if ( isset( $trxs[ $currentIndex + 2 ] ) && ! $this->isTrx( $trxs[ $currentIndex + 2 ] ) && $this->isText( $trxs[ $currentIndex + 2 ] ) ) {
				$nextTrx .= "<br/>" . $trxs[ $currentIndex + 2 ];
			}

			if ( $this->isTrx( $trxs[ $currentIndex ] ) ) {
				$trxArray       = $this->parseTrx( $trx, $this->cleanTrx( $nextTrx ) );
				$formatedTrxs[] = $trxArray;
			}
		}

		return $formatedTrxs;
	}

	/**
	 * returns array with transaction data
	 *
	 * @param mixed $trx
	 * @param mixed $nextTrx
	 *
	 * @return void
	 */
	private function parseTrx( $trx, $nextTrx ) {
		$response = null;
		$dataRow  = explode( " ", $trx );

		if ( $this->isTrx( $trx ) ) {
			$date = $dataRow[0];

			$invoice   = substr( $dataRow[1], 0, 7 );
			$amountStr = $dataRow[ count( $dataRow ) - 1 ];
			$concept   = str_replace( $date, "", $trx );
			$concept   = str_replace( $invoice, "", $concept );
			$concept   = trim( str_replace( $amountStr, "", $concept ) ) . $nextTrx;

			$operationCode = substr( $concept, 0, 3 );
			$concept       = substr( $concept, - ( strlen( $concept ) - 3 ) );

			$pattern = "/\d{3}+(\,)\d+(\.\d{2})?$/"; //
			preg_match_all( $pattern, $amountStr, $result, PREG_SET_ORDER, 0 ); // balance
			$amount  = $this->getFirstAmount( $amountStr );
			$amount2 = $this->getSecondAmount( $amountStr, $amount );
			$amount3 = $this->getThirdAmount( $amountStr, $amount, $amount2 );

			try {
				$lastBalance = 0;
				$tmpAmount   = [];

				$response = [
					'date'        => $date,
					'invoice'     => $invoice,
					'amount'      => $amount,
					'concept'     => $concept,
					'amount2'     => $amount2,
					'amount3'     => $amount3,
					'lastBalance' => $lastBalance,
					'code'        => $operationCode,
					'type'        => $this->codes[ $operationCode ]["type"],
				];
			} catch ( \Exception $ex ) {
				dd( $ex->getMessage() );
				$amount = null;
			}
		}

		return $response;
	}

	/**
	 * returns if a text not contains transaction data
	 *
	 * @param mixed $textLine
	 *
	 * @return boolean
	 */
	private function isText( $textLine ) {
		return ( strpos( $textLine, "BANCOMER" ) <= 0 || strpos( $textLine, "RFC" ) >= 0 && strpos( $textLine, "n Financiera" ) > 0 );
	}

	/**
	 * Returns if text coontains transaction data
	 *
	 * @param mixed $textLine
	 *
	 * @return boolean
	 */
	private function isTrx( $textLine ) {
		$pattern = '/.{2}\/.{3} .{2}\/.{3}.{1,}/';
		preg_match( $pattern, $textLine, $result );

		return ( count( $result ) > 0 );
	}

	/**
	 * remove unnecesary text from $textLine
	 *
	 * @param mixed $textLine
	 *
	 * @return void
	 */
	private function cleanTrx( $textLine ) {
		return explode( "Información Financiera", explode( "Estado de Cuenta", $textLine )[0] )[0];
	}

	private function getFirstAmount( $amount ) {
		$posFirstDecimal = strpos( $amount, '.' );
		$firstAmount     = substr( $amount, 0, $posFirstDecimal + 3 );

		return $firstAmount;
	}

	private function getSecondAmount( $amount, $amount1 ) {
		$secondAmountStr = str_replace( $amount1, '', $amount );
		$posFirstDecimal = strpos( $secondAmountStr, '.' );
		$secondAmount    = substr( $secondAmountStr, 0, $posFirstDecimal + 3 );

		return $secondAmount;
	}

	private function getThirdAmount( $amount, $amount1, $amount2 ) {
		$secondAmountStr = str_replace( $amount1, '', $amount );
		$secondAmountStr = str_replace( $amount2, '', $secondAmountStr );

		$posFirstDecimal = strpos( $secondAmountStr, '.' );
		$secondAmount    = substr( $secondAmountStr, 0, $posFirstDecimal + 3 );

		return $secondAmount;
	}

	/**
	 * getInitialSettlementBalance
	 *
	 * @return void
	 */
	private function getInitialSettlementBalance() {
		$pattern = '/Saldo de Liquidación Inicial.{1}[0-9].{1,}/';
		preg_match( $pattern, $this->pdfText, $result );

		return ( isset( $result[0] ) ? explode( "Saldo de Liquidación Inicial", $result[0] )[1] : '' );
	}

	/**
	 * getDaysPeriod
	 *
	 * @return void
	 */
	private function getDaysPeriod() {
		$pattern = '/Días del Periodo.{1}[0-9][0-9]/';
		preg_match( $pattern, $this->pdfText, $result );

		return ( isset( $result[0] ) ? explode( "Días del Periodo", $result[0] )[1] : '' );
	}

	/**
	 * getInmcomesAndExpenses
	 *
	 * @param mixed $fullText
	 *
	 * @return void
	 */
	private function getInmcomesAndExpenses() {
		$pattern = '/Depósitos.{3,}Abonos.{1,}/';
		preg_match( $pattern, $this->pdfText, $result );

		return ( $result[0] ?? '' );
	}
}
