<?php

namespace App\Helpers\PdfReader;

use App\Entity\Transaction;

class PdfReaderBancomer {
	/**
	 * codes of transactions for Bancomer
	 */
	private array $codes = [
		'A15' => [
			'description' => 'Pago',
			'type'        => Transaction::TRANSACTION_TYPE_EXPENSE
		],
		'N06' => [
			'description' => 'Pago a cuenta de tercero',
			'type'        => Transaction::TRANSACTION_TYPE_EXPENSE
		],
		'S39' => [
			'description' => 'Servicio banca internet',
			'type'        => Transaction::TRANSACTION_TYPE_EXPENSE
		],
		'S40' => [
			'description' => 'IVA COM Servicio banca internet',
			'type'        => Transaction::TRANSACTION_TYPE_EXPENSE
		],
		'T20' => [
			'description' => 'SPEI Recibido',
			'type'        => Transaction::TRANSACTION_TYPE_INCOME
		]
	];

	private string $pdfText = '';

	private $pdfPath = null;

	private array $pdfData = [];

	private $parsed;

	public function setParsed( $parsed ) {
		$this->parsed = $parsed;
	}

	public function setPdfPath( $pdfPath ) {
		$this->pdfPath = $pdfPath;
	}

	/**
	 * returns array with the data of PDF
	 *
	 * @return void
	 */
	public function getPdfData() {
		if ( $this->parsed ) {
			$this->processPdf();
		}

		return $this->pdfData;
	}

	private function processPdf() {
		//$pdf = $parser->parseFile($this->pdfPath);
		$this->pdfText = $this->parsed->getText();

		$data                        = [];
		$data["period"]              = $this->getPeriod();
		$data["avg_balance"]         = $this->getAvgBalance();
		$data["transactions"]        = $this->getTransactions();
		$data["days_period"]         = $this->getDaysPeriod();
		$data["liq_balance"]         = $this->getInitialSettlementBalance();
		$data["income_and_expenses"] = $this->getIncomesAndExpenses();
		$data["incomes"]             = $this->getIncomes();
		$data["expenses"]            = $this->getExpenses();
		$data["pdf_path"]            = $this->pdfPath;

		$this->pdfData = $data;
	}

	/**
	 * returns the period in the PDF
	 *
	 * @return void
	 */
	private function getPeriod() {
		$pattern = '/DEL.{3}\/.{2}\/.{5}AL.{3}\/.{2}\/.{4}/';
		preg_match( $pattern, $this->pdfText, $result );

		return ( $result[0] ?? '' );
	}

	/**
	 * returns average balance
	 *
	 * @param mixed $fullText
	 *
	 * @return void
	 */
	private function getAvgBalance() {
		$pattern = '/Saldo Promedio.{1}[0-9].{1,}/';
		preg_match( $pattern, $this->pdfText, $result );

		return ( isset( $result[0] ) ? explode( "Saldo Promedio", $result[0] )[1] : '' );
	}

	/**
	 * returns all transactions detected in the PDF
	 *
	 * @param mixed $fullText
	 *
	 * @return void
	 */
	private function getTransactions() {
		$trxs         = explode( "\n", $this->pdfText );
		$formatedTrxs = [];

		for ( $i = 0; $i < count( $trxs ) - 1; $i ++ ) {
			$trx          = $trxs[ $i ];
			$nextTrx      = '';
			$currentIndex = $i;

			if ( isset( $trxs[ $currentIndex + 1 ] ) && ! $this->isTrx( $trxs[ $currentIndex + 1 ] ) && $this->isText( $trxs[ $currentIndex + 1 ] ) ) {
				$nextTrx .= "<br/>" . $trxs[ $currentIndex + 1 ];
			}
			if ( isset( $trxs[ $currentIndex + 2 ] ) && ! $this->isTrx( $trxs[ $currentIndex + 2 ] ) && $this->isText( $trxs[ $currentIndex + 2 ] ) ) {
				$nextTrx .= "<br/>" . $trxs[ $currentIndex + 2 ];
			}

			if ( $this->isTrx( $trxs[ $currentIndex ] ) ) {
				$trxArray       = $this->parseTrx( $trx, $this->cleanTrx( $nextTrx ) );
				$formatedTrxs[] = $trxArray;
			}
		}

		return $formatedTrxs;
	}

	/**
	 * returns array with transaction data
	 *
	 * @param mixed $trx
	 * @param mixed $nextTrx
	 *
	 * @return void
	 */
	private function parseTrx( $trx, $nextTrx ) {
		$response = null;
		$dataRow  = explode( " ", $trx );

		if ( $this->isTrx( $trx ) ) {
			$date = $dataRow[0];

			$invoice   = substr( $dataRow[1], 0, 7 );
			$amountStr = $dataRow[ count( $dataRow ) - 1 ];
			$concept   = str_replace( $date, "", $trx );
			$concept   = trim( str_replace( $amountStr, "", $concept ) );

			$type = ( $nextTrx != '' && str_contains( $nextTrx, 'IN' ) ) ? 'income' : 'expense';

			$concept = substr( $concept, - ( strlen( $concept ) ) );

			$pattern = "/\d{3}+(\,)\d+(\.\d{2})?$/"; //
			preg_match_all( $pattern, $amountStr, $result, PREG_SET_ORDER, 0 ); // balance
			$amount  = $this->getFirstAmount( $amountStr );
			$amount2 = $this->getSecondAmount( $amountStr, $amount );

			$amount3 = $this->extractAmount( $amountStr, [ $amount, $amount2 ] );
			//$amount3 = $this->getThirdAmount($amountStr, $amount, $amount2);

			try {
				$lastBalance = 0;
				$tmpAmount   = [];

				$response = [
					'date_string' => $date,
					'date'        => $this->transformToDate( $date ),
					'invoice'     => $invoice,
					'amount'      => $amount,
					'concept'     => $concept,
					'amount2'     => $amount2,
					'amount3'     => $amount3,
					'lastBalance' => $lastBalance,
					'code'        => $type,
					'type'        => $type,
				];
			} catch ( \Exception $ex ) {
				dd( $ex->getMessage() );
				$amount = null;
			}
		}

		return $response;
	}

	private function transformToDate( $date ) {
		$period        = explode( " ", $this->getPeriod() );
		$statementDate = \DateTime::createFromFormat( 'd/m/Y', $period[1] );
		$year          = $statementDate->format( 'Y' );
		$day           = explode( "/", $date )[0];
		$month         = $this->stringMonths[ explode( "/", $date )[1] ];

		return \DateTime::createFromFormat( "d/m/Y", "{$day}/{$month}/{$year}" );
	}

	private array $stringMonths = [
		'ENE' => '01',
		'FEB' => '02',
		'MAR' => '03',
		'ABR' => '04',
		'MAY' => '05',
		'JUN' => '06',
		'JUL' => '07',
		'AGO' => '08',
		'SEP' => '09',
		'OCT' => '10',
		'NOV' => '11',
		'DIC' => '12'
	];

	/**
	 * returns if a text not contains transaction data
	 *
	 * @param mixed $textLine
	 *
	 * @return boolean
	 */
	private function isText( $textLine ) {
		return ( strpos( $textLine, "BANCOMER" ) <= 0 || strpos( $textLine, "RFC" ) >= 0 && strpos( $textLine, "n Financiera" ) > 0 );
	}

	/**
	 * Returns if text coontains transaction data
	 *
	 * @param mixed $textLine
	 *
	 * @return boolean
	 */
	private function isTrx( $textLine ) {
		$pattern = '/.{2}\/.{3} .{2}\/.{3}.{1,}/';
		preg_match( $pattern, $textLine, $result );

		return ( count( $result ) > 0 );
	}

	/**
	 * remove unnecesary text from $textLine
	 *
	 * @param mixed $textLine
	 *
	 * @return void
	 */
	private function cleanTrx( $textLine ) {
		return explode( "Información Financiera", explode( "Estado de Cuenta", $textLine )[0] )[0];
	}

	private function extractAmount( $amountTxt, $amounts ) {
		$tempAmount = $amountTxt;

		foreach ( $amounts as $amount ) {
			$tempAmount = str_replace( $amount, '', $tempAmount );
		}

		$posFirstDecimal = strpos( $tempAmount, '.' );
		$extractedAmount = substr( $tempAmount, 0, $posFirstDecimal + 3 );

		return $extractedAmount;
	}

	private function getFirstAmount( $amount ) {
		$posFirstDecimal = strpos( $amount, '.' );
		$firstAmount     = substr( $amount, 0, $posFirstDecimal + 3 );

		return $firstAmount;
	}

	private function getSecondAmount( $amount, $amount1 ) {
		$secondAmountStr = str_replace( $amount1, '', $amount );
		$posFirstDecimal = strpos( $secondAmountStr, '.' );
		$secondAmount    = substr( $secondAmountStr, 0, $posFirstDecimal + 3 );

		return $secondAmount;
	}

	private function getThirdAmount( $amount, $amount1, $amount2 ) {
		$secondAmountStr = str_replace( $amount1, '', $amount );
		$secondAmountStr = str_replace( $amount2, '', $secondAmountStr );

		$posFirstDecimal = strpos( $secondAmountStr, '.' );
		$secondAmount    = substr( $secondAmountStr, 0, $posFirstDecimal + 3 );

		return $secondAmount;
	}

	/**
	 * getInitialSettlementBalance
	 *
	 * @return void
	 */
	private function getInitialSettlementBalance() {
		$pattern = '/Saldo de Liquidación Inicial.{1}[0-9].{1,}/';
		preg_match( $pattern, $this->pdfText, $result );

		return ( isset( $result[0] ) ? explode( "Saldo de Liquidación Inicial", $result[0] )[1] : '' );
	}

	/**
	 * getDaysPeriod
	 *
	 * @return void
	 */
	private function getDaysPeriod() {
		$pattern = '/Días del Periodo.{1}[0-9][0-9]/';
		preg_match( $pattern, $this->pdfText, $result );

		return ( isset( $result[0] ) ? explode( "Días del Periodo", $result[0] )[1] : '' );
	}

	/**
	 * getIncomesAndExpenses
	 *
	 * @param mixed $fullText
	 *
	 * @return void
	 */
	private function getIncomesAndExpenses() {
		$pattern = '/Depósitos.{3,}Abonos.{1,}/';
		preg_match( $pattern, $this->pdfText, $result );

		return ( $result[0] ?? '' );
	}

	/**
	 * getIncomes
	 *
	 * @return void
	 */
	private function getIncomes() {
		$pattern = '/TOTAL IMPORTE ABONOS.{3,}/';
		preg_match( $pattern, $this->pdfText, $result );
		if ( isset( $result[0] ) ) {
			$result = explode( "TOTAL MOVIMIENTOS ABONOS", $result[0] )[0];
			$result = preg_replace( '/[^0-9\.]/', '', $result );

			return $result;
		}

		return '';
	}

	/**
	 * getExpenses
	 *
	 * @return void
	 */
	private function getExpenses() {
		$pattern = '/TOTAL IMPORTE CARGOS.{3,}[0-9].{1,}/';
		preg_match( $pattern, $this->pdfText, $result );
		if ( isset( $result[0] ) ) {
			$result = explode( "TOTAL MOVIMIENTOS CARGOS", $result[0] )[0];
			$result = preg_replace( '/[^0-9\.]/', '', $result );

			return $result;
		}

		return '';
	}
}
