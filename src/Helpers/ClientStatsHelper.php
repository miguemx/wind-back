<?php

namespace App\Helpers;

use App\Entity\Client;
use App\Entity\ClientStats;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;

class ClientStatsHelper {

	public function __construct( private EntityManagerInterface $em ) {
	}

	public function computeAllStatsForAllClients() {
		/**
		 * @var $clients Client[]
		 */
		$clients = $this->em->getRepository( Client::class )->findAll();
		foreach ( $clients as $client ) {
			if ( ! $client->getClientStats() ) {
				$client->setClientStats( new ClientStats() );
			}
			$this->computeAllStats( $client->getClientStats() );
			$this->em->persist( $client->getClientStats() );
		}

		$this->em->flush();

		return count( $clients );
	}


	/**
	 * @throws \Exception
	 */
	public function computeAllStats( ClientStats $clientStats ) {
		if ( ! $clientStats->getClient() ) {
			throw new \Exception( 'Trying to compute ClientStats without and assigned client' );
		}
		/**
		 * @var $clientRepo ClientRepository
		 */
		$clientRepo = $this->em->getRepository( Client::class );

		$clientStats->setProjectsCount( $clientRepo->getProjectsCount( $clientStats->getClient() ) );
		$clientStats->setApprovedProjectsCount( $clientRepo->getApprovedProjectsCount( $clientStats->getClient() ) );
		$clientStats->setQuotesCount( $clientRepo->getQuotesCount( $clientStats->getClient() ) );
		$clientStats->setApprovedQuotesCount( $clientRepo->getApprovedQuotesCount( $clientStats->getClient() ) );
		$clientStats->setAllQuotesSum( $clientRepo->getAllQuotesSum( $clientStats->getClient() ) );
		$clientStats->setApprovedQuotesSum( $clientRepo->getApprovedQuotesSum( $clientStats->getClient() ) );
		$clientStats->setPendingQuotesCount( $clientRepo->getPendingQuotesCount( $clientStats->getClient() ) );
		$clientStats->setPaymentsSum( $clientRepo->getPaymentsSum( $clientStats->getClient() ) );
		$pendingAmount = $clientRepo->getAllPendingQuotesSum( $clientStats->getClient() ) - $clientStats->getPaymentsSum();
		$clientStats->setPendingPaymentsSum( $pendingAmount );
	}
}
