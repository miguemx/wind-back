<?php

namespace App\Helpers;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Psr\Log\LoggerInterface;
use App\Entity\TaxDocument;
use App\Entity\TaxDocumentItem;
use App\Entity\AppFile;
use App\Entity\Tenant;
use App\Entity\FiscalData;
use App\Entity\TenantRfc;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class for extract and save tax documents from satws
 */
class SatWsHelper {
    private $apiKey;
    private $apiUrl;
    private $itemsPerPage = 100;
    private $savePdf = true;

    private $message = null;

    const SATWS_CREDENTIAL_STATUS_VALID = 'valid';

    public function getMessage() {
        return $this->message;
    }

    private $transStatuses = [
        'VIGENTE' => 'ACTIVE',
        'CANCELADO' => 'CANCELLED',
    ];

	public function __construct( protected EntityManagerInterface $em, protected HttpClientInterface $httpClient, private LoggerInterface $logger ) {
        if(!isset($_ENV['SATWS_API_KEY'])) {
            throw new \Exception('SATWS_API_KEY not set');
        }

        if(!isset($_ENV['SATWS_API_URL'])) {
            throw new \Exception('SATWS_API_URL not set');
        }

        $this->apiKey = $_ENV['SATWS_API_KEY'];
        $this->apiUrl = $_ENV['SATWS_API_URL'];
	}

    /**
     * Method for extract and save all tax documents from satws
     * 
     * @param string $rfc the RFC to get tax documents
     * @param object $tenant object of Tenant::class 
     * @param array $period indicates range dates to download [ 'from'=>\DateTime object, 'to'=>\DateTime object ]
     * @param boolean $withLimit indicate if only will be extracted the first page
     * @return void
     */
    public function getTaxDocuments($rfc, $tenant, $period=[], $withLimit=false) {
        $page = 1;
        $maxPages = 1;
        $invoicesMethodUrl = "{$this->apiUrl}/taxpayers/{$rfc}/invoices?itemsPerPage={$this->itemsPerPage}&page={$page}&order[issuedAt]=asc";

        if ( array_key_exists('from',$period) ) $invoicesMethodUrl .= '&issuedAt[after]='.$period['from']->format("c");
        if ( array_key_exists('to',$period) ) $invoicesMethodUrl .= '&issuedAt[before]='.$period['to']->format("c");

        do {
            $response = $this->httpClient->request(
                "GET",
                $invoicesMethodUrl,
                [
                    'headers' => [
                        'Content-Type' => 'application/ld+json; charset=utf-8',
                        'X-API-Key' => $this->apiKey
                    ]
                ],
            );

            $statusCode = $response->getStatusCode();

            if($statusCode != 200) {
                throw new HttpException(400, 'Error getting tax documents for rfc: ' . $rfc . ' in tentant: '. $tenant->getId() .' - Status: ' . $response->getStatusCode() . ' Response: ' . $response->getContent(false));
            }

            // validate x-ratelimit-remaining header
            $rateLimitRemaining = $response->getHeaders()['x-ratelimit-remaining'][0];

            if($rateLimitRemaining < 1) {
                throw new HttpException(400, 'Rate limit exceeded');
            }

            $content = json_decode($response->getContent(false), true);
            $invoices = $content['hydra:member'];
            $total = count($invoices);
            $this->logger->info( "SATWS: Trying to DOWNLOAD {$total} documents for RFC {$rfc} with URL {$invoicesMethodUrl}" );
            // save invoices
            foreach($invoices as $invoice) {                
                //dd($invoice);
                $invoiceId = $invoice['id'];
                $uuidFolioFiscal = $invoice['uuid'];
                $rfc = $invoice["issuer"]["rfc"];

                $taxDocument = $this->em->getRepository(TaxDocument::class)->findOneBy([
                    'uuidFolioFiscal' => $uuidFolioFiscal,
                    'rfc' => $rfc
                ]);

                // if not exists, create it
                if(!$taxDocument) {
                    try {
                        $taxDocument = new TaxDocument();
                        $taxDocument->setDocumentType($invoice["type"]);
                        $taxDocument->setUuidFolioFiscal($uuidFolioFiscal);
                        $taxDocument->setFolio( $invoice["internalIdentifier"] );
                        $taxDocument->setStatus( isset( $this->transStatuses[ $invoice["status"] ] ) ?  $this->transStatuses[ $invoice["status"] ] : $invoice["status"] );
                        $taxDocument->setTenant($tenant);
                        $taxDocument->setPaymentType( isset($invoice["paymentType"]) ? $invoice["paymentType"] : '' );
                        $taxDocument->setPaymentMethod( isset($invoice["paymentMethod"]) ? $invoice["paymentMethod"] : '' );
                        $taxDocument->setIsIssuer( $invoice["isIssuer"]=="true" );
                        $taxDocument->setCurrency( $invoice["currency"]||'');
                        $taxDocument->setDiscount($invoice["discount"]);
                        $taxDocument->setTax( empty(strval($invoice["tax"])) ? '0' : strval($invoice["tax"]) );
                        $taxDocument->setRetainedTaxes(isset($invoice["retainedTaxes"]["total"]) ? $invoice["retainedTaxes"]["total"] : 0);
                        $taxDocument->setTransferredTaxes(isset($invoice["transferredTaxes"]["total"]) ? $invoice["transferredTaxes"]["total"] : 0);
                        $taxDocument->setSubtotal( empty(strval($invoice["subtotal"])) ? '0' : strval($invoice["subtotal"]) );
                        $taxDocument->setTotal( empty(strval($invoice["total"])) ? '0' : strval($invoice["total"]) );
                        $taxDocument->setPaidAmount( empty(strval($invoice["paidAmount"])) ? '0' : strval($invoice["paidAmount"]) );
                        $taxDocument->setDueAmount( empty(strval($invoice["dueAmount"])) ? '0' : strval($invoice["dueAmount"]) );
                        
                        if($invoice["fullyPaidAt"])
                            $taxDocument->setLastPaymentDate(new \DateTime($invoice["fullyPaidAt"]));
                        
                        if($invoice["fullyPaidAt"])
                            $taxDocument->setFullyPaidAt( new \DateTime($invoice["fullyPaidAt"]) );
                        
                        if($invoice["canceledAt"])
                            $taxDocument->setCanceledAt( new \DateTime($invoice["canceledAt"]) );

                        $taxDocument->setIssuedAt( new \DateTime($invoice["issuedAt"]) );
                        $taxDocument->setRfc($rfc);
                        
                        // save files
                        if($this->savePdf && $invoice['pdf']) {
                            $pdfFile = $this->saveTaxDocumentFile($invoiceId, $invoice["type"], $tenant, 'pdf');
                            $taxDocument->setPdfFile($pdfFile);
                        }

                        if($invoice['xml']) {
                            $xmlFile = $this->saveTaxDocumentFile($invoiceId, $invoice["type"], $tenant, 'xml');
                            $taxDocument->setXmlFile($xmlFile);
                        }
                        
                        // save tax document items
                        $taxDocumentItems = $invoice["items"];

                        foreach($taxDocumentItems as $taxDocumentItem) {
                            // save each item
                            $taxDocumentItemEntity = new TaxDocumentItem();
                            $taxDocumentItemEntity->setTaxDocument($taxDocument);
                            $taxDocumentItemEntity->setProductIdentification($taxDocumentItem["productIdentification"]);
                            $taxDocumentItemEntity->setDescription($taxDocumentItem["description"]);
                            $taxDocumentItemEntity->setUnitAmount( empty(strval($taxDocumentItem["quantity"])) ? '0' : strval($taxDocumentItem["quantity"]) );
                            $taxDocumentItemEntity->setUnitCode($taxDocumentItem["unitCode"]);
                            $taxDocumentItemEntity->setDiscountAmount( empty(strval($taxDocumentItem["discountAmount"])) ? '0' : strval($taxDocumentItem["discountAmount"]) );
                            $taxDocumentItemEntity->setTaxAmount( empty(strval($taxDocumentItem["taxAmount"])) ? '0' : strval($taxDocumentItem["taxAmount"]) );
                            $taxDocumentItemEntity->setPrice( empty(strval($taxDocumentItem["unitAmount"])) ? '0' : strval($taxDocumentItem["unitAmount"]) );
                            $taxDocumentItemEntity->setTenant($tenant);

                            if($this->getTaxRate($taxDocumentItem, TaxesHelper::TAX_IVA_CODE)) {
                                $taxDocumentItemEntity->setIva($this->getTaxRate($taxDocumentItem, TaxesHelper::TAX_IVA_CODE));
                            }

                            if($this->getTaxRate($taxDocumentItem, TaxesHelper::TAX_IEPS_CODE)) {
                                $taxDocumentItemEntity->setIeps($this->getTaxRate($taxDocumentItem, TaxesHelper::TAX_IEPS_CODE));
                            }

                            if($this->getTaxRate($taxDocumentItem, TaxesHelper::TAX_ISR_CODE)) {
                                $taxDocumentItemEntity->setIsr($this->getTaxRate($taxDocumentItem, TaxesHelper::TAX_ISR_CODE));
                            }

                            $taxDocument->addTaxDocumentItem($taxDocumentItemEntity);
                        }

                        $this->em->persist($taxDocument);
                        $this->em->flush();
                        $this->logger->info( "SATWS: Saved $uuidFolioFiscal for RFC $rfc " );
                    }
                    catch ( \Exception $ex ) {
                        $this->logger->error( "SATWS: Error while saving an invoices for RFC {$rfc}: ".$ex->getMessage() );
                        continue;
                    }
                }
            }
            
            $invoicesMethodUrl = isset($content["hydra:view"]["hydra:next"]) ? $this->apiUrl.$content["hydra:view"]["hydra:next"] : null;

            $page++;
        } while($invoicesMethodUrl && ($page <= $maxPages || !$withLimit));
        $this->em->clear();
    }

    private function getTaxRate($taxDocumentItem, $taxCode) 
    {
        $rate = null;

        if(isset($taxDocumentItem["taxes"])) {
            foreach($taxDocumentItem["taxes"] as $tax) {
                if($tax["tax"] == $taxCode) {
                    $rate = isset($tax["factor"]["rate"]) ? $tax["factor"]["rate"] : null;

                    if($tax["factor"]["amount"]==0)
                        $rate = 0;
                    
                    break;
                }
            }
        }

        return $rate;
    }

    /**
     * Method for save tax document file
     * 
     * @param string $invoiceId
     * @param string $documentType
     * @param string $tenant 
     * @param string $type
     * @return AppFile
     */
    private function saveTaxDocumentFile($invoiceId, $documentType, $tenant, $type='xml')
    {
        // download PDF file
        $fileUrl = "{$this->apiUrl}/invoices/{$invoiceId}/cfdi";
        
        switch($type) {
            case 'pdf':
                $acceptHeader = 'application/pdf';
                $fileUrl = $fileUrl.'.pdf';
                break;
            case 'xml':
                $acceptHeader = 'text/xml';
                break;
            default:
                throw new HttpException(400, 'Invalid file type');
        }

        $fileResponse = $this->httpClient->request(
            "GET",
            $fileUrl,
            [
                'headers' => [
                    'content-type' => $acceptHeader,
                    'X-API-Key' => $this->apiKey
                ]
            ],
        );

        $fileStatusCode = $fileResponse->getStatusCode();

        if($fileStatusCode != 200) {
            throw new HttpException(400, 'Error getting file for invoice '.$invoiceId);
        }

        $fileContent = $fileResponse->getContent(false);

        // save file
        $fileName = "{$invoiceId}_{$documentType}.{$type}";
        
        $this->checkTaxDocumentsFolder();

        //Store in the filesystem.
        $fp = fopen( __DIR__ . "/../../uploads/{$fileName}", "w");
        fwrite($fp, $fileContent);
        fclose($fp);

        $appFile = $this->createAppFile($fileName, $tenant);

        $this->em->persist($appFile);

        return $appFile;
    }

    /**
     * Method for create new AppFile with the file path
     *
     * @param  string $fileName
     * @return AppFile
     */
    private function createAppFile($fileName, $tenant)
    {
        $file = new File(__DIR__ . "/../../uploads/{$fileName}");

        $appFile = new AppFile();
		$appFile->setType(AppFile::GENERAL_FILE);
		$appFile->setFile($file);
        $appFile->setUpdatedDate(new \DateTime());
        $appFile->setName("{$fileName}");
        $appFile->setSize($file->getSize());
        $appFile->setMimeType($file->getMimeType());
        $appFile->setOriginalName($fileName);
        $appFile->setTenant($tenant);

        return $appFile;
    }

    /**
     * Method for check if tax documents folder exists, if not, create it
     *
     * @return string
     */
    private function checkTaxDocumentsFolder()
    {
        $folder = __DIR__ . "/../../uploads/";

        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        return $folder;
    }


    public function configureRfcsInSatWs( $fiscalDatas, $tenant )
    {
        foreach($fiscalDatas as $fiscalData) {
            if(isset($fiscalData["rfc"]) && isset($fiscalData["type"]) ) {
                // get fiscal data from tenant with fiscalData rfc
                $parameters = ["rfc" => $fiscalData["rfc"], "tenant" => $tenant->getId() ];
                $fiscalDataObject = $this->em->getRepository(TenantRfc::class)->findOneBy($parameters);

                if($fiscalDataObject) {
                    // create credentials in satws
                    $credentials = $this->sendRfcToSatWs($fiscalData);
                }
            }
        }
    }

    public function createInitialExtraction($fiscalDataObject)
    {
        if(!$_ENV["SATWS_INITIAL_DAYS_TO_EXTRACT"] || !$_ENV["SATWS_EXTRACTION_TYPES"]) {
            throw new HttpException(400, "SATWS_INITIAL_DAYS_TO_EXTRACT or SATWS_EXTRACTION_TYPES not configured");
        }

        // calculate date from using configured env value SATWS_INITIAL_DAYS_TO_EXTRACT
        $now = new \DateTime();
        $dateFrom = new \DateTime( '2014-01-01' ); 
        $dateFrom->setTimeZone( new \DateTimeZone('America/Mexico_City') );
        $now->setTimeZone( new \DateTimeZone('America/Mexico_City') );

        // get types from env value SATWS_EXTRACTION_TYPES
        $types = explode(",", $_ENV["SATWS_EXTRACTION_TYPES"]);

        $params = [
            "taxpayer" => "/taxpayers/{$fiscalDataObject->getRfc()}",
            "extractor" => "invoice",
            "options" => [
                "types" => $types,
                "period" => [
                    "from" => $dateFrom->format("c"),
                    "to" => $now->format("c")
                ]
            ]
        ];
        $this->logger->info( "SATWS: Initial extraction for  {$fiscalDataObject->getRfc()} with data ".json_encode( $params ) );
        $extraction = $this->createExtractionInSatWs($params);

        return $extraction;
    }

    public function createRecurrentExtraction($fiscalData)
    {
        if(!$_ENV["SATWS_EXTRACTION_TYPES"]) {
            throw new HttpException(400, "SATWS_INITIAL_DAYS_TO_EXTRACT or SATWS_EXTRACTION_TYPES not configured");
        }

        // calculate date from using configured env value SATWS_DAYS_TO_EXTRACT
        $dateFrom = $fiscalData->getLastExtractionDate();
        $now = new \DateTime();
        $dateFrom->setTimeZone( new \DateTimeZone('America/Mexico_City') );
        $now->setTimeZone( new \DateTimeZone('America/Mexico_City') );

        // get types from env value SATWS_EXTRACTION_TYPES
        $types = explode(",", $_ENV["SATWS_EXTRACTION_TYPES"]);

        $params = [
            "taxpayer" => "/taxpayers/{$fiscalData->getRfc()}",
            "extractor" => "invoice",
            "options" => [
                "types" => $types,
                "period" => [
                    "from" => $dateFrom->format("c"),
                    "to" => $now->format("c")
                ]
            ]
        ];
        $this->logger->info( "SATWS: Creating recurrent extraction for  {$fiscalData->getRfc()} with data ".json_encode($params) );
        $extraction = $this->createExtractionInSatWs($params);

        return $extraction;
    }

    /**
     * creates an extraction in SATWS based on a tenant
     * @param tenantId a string with tenantId
     * @param from (optional) date with format YYYY-MM-DD with initial range; defailt current date
     * @param to (optional) date with format YYYY-MM-DD with final range; default current date
     */
    public function createExtractionForTenant( $tenantId, $from=null, $to=null ) {
        if ( is_null($from) ) {
            $from = new \DateTime('2014-01-01', new \DateTimeZone('America/Mexico_City') );
        }
        if ( is_null($to) ) $to = new \DateTime( null, new \DateTimeZone('America/Mexico_City') );
        $extractions = [];
        $rfcs = $this->em->getRepository(TenantRfc::class)->findBy([ 'tenant' => $tenantId ]);
        if ( count( $rfcs ) > 0 ) {
            foreach( $rfcs as $rfc ) {
                try {
                    $types = explode(",", $_ENV["SATWS_EXTRACTION_TYPES"]);
                    $params = [
                        "taxpayer" => "/taxpayers/{$rfc->getRfc()}",
                        "extractor" => "invoice",
                        "options" => [
                            "types" => $types,
                            "period" => [
                                "from" => $from->format("c"),
                                "to" => $to->format("c")
                            ]
                        ]
                    ];
                    $extraction = $this->createExtractionInSatWs( $params );
                    if ( $extraction ) $extractions[ $rfc->getRfc() ] = $extraction;
                }
                catch ( \Exception $ex ) {
                    $this->logger->error( "SATWS: could´t create extraction for tenant: {$rfc->getRfc()} with data ".json_encode($params) );
                }
            }
        }
        else {
            throw new \Exception("Tenant not found");
        }
        
        return $extractions;
    }

    public function createExtractionInSatWs($params)
    {
        $extractionsUrl = "{$this->apiUrl}/extractions";

        $response = $this->httpClient->request(
            "POST",
            $extractionsUrl,
            [
                'headers' => [
                    'Content-Type' => 'application/json;',
                    'X-API-Key' => $this->apiKey
                ],
                'body' => json_encode($params)
            ],
        );

        $statusCode = $response->getStatusCode();

        if($statusCode != 202) {
            switch ( $statusCode ) {
                case 400: throw new HttpException(400, 'Validation error. Some values could be wrong; pelase check your request.'); break;
                case 401: throw new HttpException(400, 'Unauthorized.'); break;
                case 409: throw new HttpException(400, 'Extraction already created.'); break;
                default: throw new HttpException(400, 'Error creating extraction.'); break;
            }
        }

        $content = $response->getContent(false);

        return json_decode($content, true);
    }

    public function getCredentialsByRfc($rfc, $throwable=true)
    {
        $credentialsUrl = "{$this->apiUrl}/credentials?rfc={$rfc}";

        $response = $this->httpClient->request(
            "GET",
            $credentialsUrl,
            [
                'headers' => [
                    'Content-Type' => 'application/json;',
                    'X-API-Key' => $this->apiKey
                ],
            ],
        );

        $statusCode = $response->getStatusCode();

        if($statusCode != 200) {
            if ( $throwable ) throw new HttpException(400, 'Error getting credentials for RFC '.$rfc);
            else return null;
        }

        $content = $response->getContent(false);

        return json_decode($content, true);
    }

    public function existsRfcInSatWs($rfc)
    {
        $credentials = $this->getCredentialsByRfc($rfc);

        if(isset($credentials["hydra:member"]) && count($credentials["hydra:member"]) > 0) {
            return true;
        }

        return false;
    }

    public function sendRfcToSatWs( $fiscalData, $throwable=true )
    {
        $rfc = $fiscalData["rfc"];
        $type = $fiscalData["type"];
        $parameters = null;

        // validate type, only ciec or efirma is allowed
        if($type != "ciec" && $type != "efirma") {
            if ( $throwable ) throw new HttpException(400, 'Invalid type for RFC '.$rfc);
            else {
                $this->message = 'Invalid type for RFC '.$rfc;
                return null;
            }
        }

        // validate parameters for ciec
        if($type == "ciec") {
            if(!isset($fiscalData["password"])) {
                if ( $throwable ) throw new HttpException(400, 'Password is required for RFC '.$rfc);
                else {
                    $this->message = 'Password is required for RFC '.$rfc;
                    return null;
                }
            }

            $parameters = [
                'rfc' => $rfc,
                'type' => 'ciec',
                'password' => $fiscalData["password"]
            ];
        }

        // validate parameters for efirma
        if($type == "efirma") {
            if(!isset($fiscalData["certificate"]) || !isset($fiscalData["privateKey"]) || !isset($fiscalData["password"])) {
                if ( $throwable ) throw new HttpException(400, 'Certificate, private key and password are required for RFC '.$rfc);
                else {
                    $this->message = 'Certificate, private key and password are required for RFC '.$rfc;
                    return null;
                }
            }

            $parameters = [
                'rfc' => $rfc,
                'type' => 'efirma',
                'certificate' => $fiscalData["certificate"],
                'privateKey' => $fiscalData["privateKey"],
                'password' => $fiscalData["password"]
            ];
        }
        // @todo verificar si la existencia de esta sección tiene implicaciones en otra sección
        if($this->existsRfcInSatWs($rfc))
            return null;

        $credentialsUrl = "{$this->apiUrl}/credentials";

        $response = $this->httpClient->request(
            "POST",
            $credentialsUrl,
            [
                'headers' => [
                    'Content-Type' => 'application/json;',
                    'X-API-Key' => $this->apiKey
                ],
                'json' => $parameters,
            ],
        );

        $statusCode = $response->getStatusCode();

        if($statusCode != 202) {
            if ( $throwable ) throw new HttpException(400, 'Error creating credentials for RFC '.$rfc);
            else {
                $this->message = 'Error creating credentials for RFC '.$rfc;
                return null;
            }
        }

        $content = $response->getContent(false);

        $credentials = json_decode($content, true);

        return $credentials;    
    }

    /**
     * Finds a credential on SatWS or create a new one
     * using RFC as criteria
     * @param fiscalData an array with data requered by SatWs
     * @return credentials an object with credentials created or found
     * @return null if can't find and create credentials
     */
    public function findOrCreate($fiscalData) {
        $credentials = null;
        $credentialsSearch = $this->getCredentialsByRfc( $fiscalData['rfc'], false );
        if ( isset($credentialsSearch["hydra:member"]) && count($credentialsSearch["hydra:member"]) > 0 ) {
            $credentials = $credentialsSearch['hydra:member'][0];
            if ( !$this->deleteCredentials( $credentials['id'], $fiscalData['rfc'], false ) ) {
                throw new HttpException(400, 'Error trying update for credential '.$rfc);
            }
        }
        
        $credentials = $this->sendRfcToSatWs( $fiscalData, false );
        
        return $credentials;
    }

    private function getTaxPayerSatWs($taxPayerRfc)
    {
        $taxPayerSatWsStatusUrl = "{$this->apiUrl}/credentials?rfc={$taxPayerRfc}";

        $response = $this->httpClient->request(
            "GET",
            $taxPayerSatWsStatusUrl,
            [
                'headers' => [
                    'Content-Type' => 'application/json;',
                    'X-API-Key' => $this->apiKey
                ],
            ],
        );

        $statusCode = $response->getStatusCode();

        if($statusCode != 200) {
            throw new HttpException(400, 'Error getting taxpayer status for RFC '.$taxPayerRfc);
        }

        $content = json_decode($response->getContent(false), true);

        $taxPayer = null;

        if(isset($content["hydra:member"]) && count($content["hydra:member"]) > 0) {
            $taxPayer = $content["hydra:member"][0];
        }

        return $taxPayer;
    }

    public function generateExtractions()
    {
        $fiscalDatas = $this->em->getRepository( TenantRfc::class )->findAll();

        foreach($fiscalDatas as $fiscalDataObject) {
            if ( is_null( $fiscalDataObject->getSatWsId() ) ) {
                $this->logger->info( "SATWS: Detected an RFC not connected. {$fiscalDataObject->getRfc()} " );
                continue;
            }
            $now = new \DateTime(null, new \DateTimeZone('America/Mexico_City'));
            try {
                $taxPayerSatWs = $this->getTaxPayerSatWs($fiscalDataObject->getRfc());
                $this->logger->info( "SATWS: Checking {$fiscalDataObject->getRfc()} " );
                // if taxpayer is not valid in SAT WS, skip it
                if($taxPayerSatWs["status"] != self::SATWS_CREDENTIAL_STATUS_VALID || !$taxPayerSatWs) {
                    $this->logger->info( "SATWS: Status not valid for {$fiscalDataObject->getRfc()}. Skipped." );
                    $fiscalDataObject->setSatWsStatus($taxPayerSatWs["status"]);
                    $this->em->persist($fiscalDataObject);
                    $this->em->flush();
                    continue;
                }
                $this->logger->info( "SATWS: Status VALID for {$fiscalDataObject->getRfc()}." );
                // if first extraction is not created, create it
                if( !$fiscalDataObject->getIsFirstDataExtracted() || is_null($fiscalDataObject->getLastExtractionDate()) ) {
                    $extraction = $this->createInitialExtraction($fiscalDataObject);
                    $this->logger->info( "SATWS: Created initial extraction with data  ".json_encode($extraction) );
                    // update fiscal data object
                    $fiscalDataObject->setIsFirstDataExtracted(true);
                } else {
                    $extraction = $this->createRecurrentExtraction($fiscalDataObject);
                    $this->logger->info( "SATWS: Created recurrent extraction with data  ".json_encode($extraction) );
                }
                $fiscalDataObject->setSatWsStatus(self::SATWS_CREDENTIAL_STATUS_VALID);
                $fiscalDataObject->setLastExtractionDate($now);
                $this->em->persist($fiscalDataObject);
                $this->em->flush();
            }
            catch ( \Exception $ex ) {
                $this->logger->error( "SATWS: Process for {$fiscalDataObject->getRfc()} : ".$ex->getMessage() );
            }
        }
    }

    /**
     * removes a credential from SATWS based on their ID; rfc is just received for messaging purposes
     * @param id the SATWs Id to delete
     * @param rfc the RFC to show in messages
     * @param throwable a flag indicating if errors reported are shown as exception
     * @return true on success
     * @return null on failure
     */
    public function deleteCredentials($id, $rfc, $throwable=true)
    {
        $credentialsUrl = "{$this->apiUrl}/credentials/{$id}";

        $response = $this->httpClient->request(
            "DELETE",
            $credentialsUrl,
            [
                'headers' => [
                    'Content-Type' => 'application/json;',
                    'X-API-Key' => $this->apiKey
                ],
            ],
        );

        $statusCode = $response->getStatusCode();

        if($statusCode != 204) {
            if ( $throwable ) throw new HttpException(400, 'Error deleting credentials for '.$rfc);
            else return false;
        }

        return true;
    }

    /**
     * retrieves all invoices from a tenant in a periodo defined, which is optional
     * @param tenantId numeric with tenant ID to find
     * @param from (optional) a DateTime object with initial date to download invoices
     * @param to (optional) a DateTime object with enddate to download invoices
     */
    public function retrieveTenantInvoices( $tenantId, $from, $to ) {
        $rfcs = $this->em->getRepository( TenantRfc::class )->findBy([ 'tenant'=>$tenantId ]);
        if ( count( $rfcs ) > 0 ) {
            foreach( $rfcs as $rfc ) {
                $tenant = $rfc->getTenant();
                $this->getTaxDocuments($rfc->getRfc(), $tenant, [ 'from'=>$from, 'to'=>$to ], false);
            }
        }
        else {
            throw new \Exception("Tenant not found");
        }
    }
}