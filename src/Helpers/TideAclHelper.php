<?php

namespace App\Helpers;

use App\Entity\TideAcl;
use App\Entity\User;
use App\Interfaces\AclEnabledInterface;
use Doctrine\ORM\EntityManagerInterface;

class TideAclHelper {

	public function __construct( private EntityManagerInterface $em ) {
	}

	/**
	 * @throws \Exception
	 */
	public function getSecurityIdentity( $subject ) {
		if ( $subject instanceof User ) {
			return $this->getSecurityIdentityFromUserId( $subject->getId() );
		}
		throw new \Exception( 'Security identity type not implemented.' );
	}

	public function getSecurityIdentityFromUserId( $userId ): string {
		return 'U:' . $userId;
	}

	public function checkObjectAclPermission( AclEnabledInterface $object, $securityIdentity, $permission ): bool {
		/**
		 * @var User $user
		 * @var TideAcl $acl
		 */

		$domain   = $object->getAclDomain();
		$objectId = $object->getObjectId();
		// Find if acl exists
		$acl = $this->em->getRepository( TideAcl::class )->findOneBy( [
			'domain'           => $domain,
			'objectId'         => $objectId,
			'securityIdentity' => $securityIdentity
		] );
		// if it's not defined, the user has no permissions over the entity
		if ( ! $acl ) {
			return false;
		}
		//We find the mask that corresponds to the permission asked
		$permissionMask = $this->getMaskForObjectPermission( $object, $permission );

		//If the permission is not defined, we don't give access (this is a weird case, I think this shouldn't happen)
		if ( ! $permissionMask ) {
			return false;
		}

		//Apply the mask and test if every bit in the mask is set.
		return ( $acl->getPermissionMask() & $permissionMask ) === $permissionMask;
	}

	public function checkObjectIdAclPermissionMask( $domain, $objectId, $securityIdentity, $permissionMask ): bool {

		// Find if acl exists
		$acl = $this->em->getRepository( TideAcl::class )->findOneBy( [
			'domain'           => $domain,
			'objectId'         => $objectId,
			'securityIdentity' => $securityIdentity
		] );
		// if it's not defined, the user has no permissions over the entity
		if ( ! $acl ) {
			return false;
		}

		return ( $acl->getPermissionMask() & $permissionMask ) === $permissionMask;
	}

	/**
	 * We get a permission of an object and search for the definition (a mask) of the permission.
	 * There are 4 ways
	 *
	 * @param $object
	 * @param $permission
	 *
	 * @return mixed|null
	 */
	public function getMaskForObjectPermission( $object, $permission ) {

		//Search the mask in the object's class
		$class          = new \ReflectionClass( $object::class );
		$permissionMask = $class->getConstant( 'ACL_PERMISSION_' . $permission );
		if ( $permissionMask ) {
			return $permissionMask;
		}

		//Get class name without namespace
		$classParts = explode( '\\', $object::class );
		//Convert came case to snake case in caps
		$objectClassName = strtoupper( preg_replace( '/(?<!^)[A-Z]/', '_$0', end( $classParts ) ) ) . '_';

		//Check if the permission has the class name as prefix, if it does, we remove it
		$permissionPrefix     = substr( $permission, 0, strlen( $objectClassName ) );
		$permissionPrefixless = false;
		if ( $permissionPrefix === $objectClassName ) {
			$permissionPrefixless = substr( $permission, strlen( $objectClassName ) );
		}
		//If we removed the class prefix, we try again to find the permission in object's class
		if ( $permissionPrefixless ) {
			$permissionMask = $class->getConstant( 'ACL_PERMISSION_' . $permissionPrefixless );
			if ( $permissionMask ) {
				return $permissionMask;
			}
		}
		//We search if it's a default permission in the acl
		$aclClass       = new \ReflectionClass( TideAcl::class );
		$permissionMask = $aclClass->getConstant( 'ACL_PERMISSION_' . $permission );
		if ( $permissionMask ) {
			return $permissionMask;
		}

		//We search without the prefix if it's a default permission in the acl
		if ( $permissionPrefixless ) {
			$permissionMask = $aclClass->getConstant( 'ACL_PERMISSION_' . $permissionPrefixless );
			if ( $permissionMask ) {
				return $permissionMask;
			}
		}

		//Not found :(
		return null;
	}

	public function getUserIdFromAcl( TideAcl $acl ) {
		$securityIdentity      = $acl->getSecurityIdentity();
		$securityIdentityArray = array_map( 'trim', explode( ':', $securityIdentity ) );

		if ( $securityIdentityArray[0] === 'U' ) {
			return $securityIdentityArray[1];
		}

		return null;
	}

	public function getUsersIdFromAcls( array $acls ) {
		$usersId = [];
		foreach ( $acls as $acl ) {
			$userId = $this->getUserIdFromAcl( $acl );
			if ( $userId ) {
				$usersId[] = $userId;
			}
		}

		return $usersId;
	}

	public function getAclFromUserId( string $id, array $acls ) {
		foreach ( $acls as $acl ) {
			if ( $this->getUserIdFromAcl( $acl ) === $id ) {
				return $acl;
			}
		}
	}


	public function getAllowedUserEntitiesByPermissionMask( User $user, $entityClass, $permissionMask, $obtainScalarIdsArray = false ) {
		$securityIdentity = $this->getSecurityIdentityFromUserId( $user->getId() );
		$entities         = $this->em->getRepository( TideAcl::class )->getAllowedUserEntitiesByPermissionMask( $securityIdentity, $entityClass, $permissionMask );

		if ( $obtainScalarIdsArray ) {
			$ids = [];
			foreach ( $entities as $entity ) {
				$ids[] = $entity->getId();
			}

			return $ids;
		}

		return $entities;

	}


}
