<?php

namespace App\Helpers;

use App\Interfaces\FolioInterface;
use Doctrine\ORM\EntityManagerInterface;

class FolioHelper {
	public function __construct( protected EntityManagerInterface $em ) {
	}

	/**
	 * generateFolio
	 * {EMPRESA-INICIALES}-{TIPO (C|P|M|S)}{SEQ}
	 * @return void
	 */
	public function generateFolio( FolioInterface $entity ) {
		return "{$entity->getFolioBaseInitials()}-{$entity->getFolioTypeIdentifier()}{$entity->getFolioSequential()}";
	}

	/**
	 * getNextFolioSequential
	 *
	 * @return void
	 */
	public function getNextFolioSequential( FolioInterface $entity ) {
		$entityType = $entity::class;

		$max = $this->em->createQueryBuilder()
		                ->select( 'e' )
		                ->from( $entityType, 'e' )
		                ->orderBy( 'e.folioSequential', 'DESC' );

		$max = ( $entity->addDQLFolioSequential( $max ) )
			->setMaxResults( 1 )
			->getQuery()->getOneOrNullResult();

		return ( $max ? $max->getFolioSequential() + 1 : 1 );
	}

	/**
	 * This searches for the next folio of the entity and sets all the required properties
	 */
	public function createAndSetNextFolio( FolioInterface $entity ) {
		$entity->setFolioSequential( $this->getNextFolioSequential( $entity ) );
		$entity->setFolio( $this->generateFolio( $entity ) );
	}
}
