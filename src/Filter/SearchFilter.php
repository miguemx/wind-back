<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

#[\Attribute( \Attribute::TARGET_CLASS )]
final class SearchFilter extends AbstractContextAwareFilter {
	//https://github.com/api-platform/core/issues/398

	/**
	 * @param string $property
	 * @param $value
	 * @param QueryBuilder $queryBuilder
	 * @param QueryNameGeneratorInterface $queryNameGenerator
	 * @param string $resourceClass
	 * @param string|null $operationName
	 *
	 * @throws \Doctrine\Common\Annotations\AnnotationException
	 * @throws \HttpInvalidParamException
	 * @throws \ReflectionException
	 */
	protected function filterProperty( string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null ) {
		if ( $property !== 'search' ) {
			return;
		}

		$value = str_replace('"','', $value);

		$reflection      = new \ReflectionClass( get_class( new $resourceClass ) );
		$classAttributes = $reflection->getAttributes();
		$fields          = null;


		foreach ( $classAttributes as $attribute ) {

			if ( $attribute->getName() === SearchAnnotation::class ) {
				if ( isset( $attribute->getArguments()[0] ) ) {
					$fields = $attribute->getArguments()[0];
				}
			}
		}

		if ( ! $fields ) {
			throw new HttpException( Response::HTTP_INTERNAL_SERVER_ERROR, 'No Search implemented.' );
		}

		$this->createJoinsQueryBuilder( $queryBuilder, $fields );

		if ( is_string( $value ) ) {
			$orX = $this->findByString( $queryBuilder, $value, $fields );
			$queryBuilder->andWhere( $orX );
		}

		if ( is_array( $value ) ) {
			$conds       = [];
			$multipleOrX = $queryBuilder->expr()->orX();
			foreach ( $value as $string ) {
				$orX     = $this->findByString( $queryBuilder, $string, $fields );
				$conds[] = $queryBuilder->expr()->orX( $orX );
			}
			$multipleOrX->addMultiple( $conds );
			$queryBuilder->andWhere( $multipleOrX );
		}
		$queryBuilder->setParameter("searchValue", "%".$value."%");
	}


	/**
	 * @param string $resourceClass
	 *
	 * @return array
	 */
	public function getDescription( string $resourceClass ): array {
		$reflection =  new \ReflectionClass( new $resourceClass );
		$attributes = $reflection->getAttributes(SearchAnnotation::class);

		if(count($attributes)===0)
			return [];

		$fields = $attributes[0]->getArguments()[0];

		$description['search'] = [
			'property' => 'search',
			'type'     => 'string',
			'required' => false,
			'swagger'  => [ 'description' => 'FullTextFilter on ' . implode( ', ', $fields) ],
		];

		return $description;
	}


	private function createJoinsQueryBuilder( QueryBuilder $dql, array $fields ) {
		$select             = $dql->getDQLPart( 'select' )[0]->getParts()[0];
		$insertedJoinsAlias = [];
		foreach ( $fields as $field ) {
			$fieldParts = explode( '.', $field );
			$numParts   = count( $fieldParts );
			$parent     = $select;
			for ( $i = 0; $i < $numParts - 1; $i ++ ) {
				if ( $i != $numParts ) {
					$exists = false;
					if ( count( $dql->getDQLPart( "join" ) ) > 0 ) {
						$joinParts = $dql->getDQLPart( "join" )[ $select ];
						foreach ( $joinParts as $joinPart ) {
							if ( $joinPart->getAlias() == $fieldParts[ $i ] ) {
								$exists = true;
							}
						}
					}

					if ( ! $exists ) {
						$insertedJoinsAlias[ $fieldParts[ $i ] ] = 1 + isset( $insertedJoinsAlias[ $fieldParts[ $i ] ] ) ?: $insertedJoinsAlias[ $fieldParts[ $i ] ];
						$postfix                                 = "";
						if ( $insertedJoinsAlias[ $fieldParts[ $i ] ] > 1 ) {
							$postfix = "_" . ( $insertedJoinsAlias[ $fieldParts[ $i ] ] - 1 );
						}
						$dql->leftJoin( $parent . '.' . $fieldParts[ $i ], $fieldParts[ $i ] . $postfix );
					}
					$parent = $fieldParts[ $i ];
				}
			}
		}

		return $dql;
	}

	private function findByString( QueryBuilder $dql, $string, $searchFields ) {
		$orX          = $dql->expr()->orX();
		$conditions   = [];
		$selectEntity = $dql->getDQLPart( 'select' )[0]->getParts()[0];


		$composedLike = "CONCAT (";
		foreach ( $searchFields as $field ) {
			if ( strpos( $field, "." ) == false ) {
				$fieldFormatted = "$selectEntity." . $field;
			} else {
				$fieldParts = explode( '.', $field );
				$points     = count( $fieldParts );
				if ( $points < 3 ) {
					$fieldFormatted = $field;
				} else {
					$fieldFormatted = $fieldParts[ $points - 2 ] . "." . $fieldParts[ $points - 1 ];
				}
			}

			$conditions[] = $dql->expr()->like( $fieldFormatted, ":searchValue" );
			$composedLike .= "IFNULL($fieldFormatted,'')";
			if ( $field != end( $searchFields ) ) {
				$composedLike .= ",' ',";
			}
		}
		$composedLike .= ",'')";
		$conditions[] = $dql->expr()->like( $composedLike, ":searchValue" );

		$orX->addMultiple( $conditions );

		return $orX;
	}


}
