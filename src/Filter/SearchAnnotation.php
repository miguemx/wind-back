<?php

namespace App\Filter;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\Annotation\Target;
use Doctrine\Common\Annotations\AnnotationException;

/**
 * @Annotation
 * @Target("CLASS")
 */
#[\Attribute( \Attribute::TARGET_CLASS )]
final class SearchAnnotation
{
	public $fields = [];


	/**
	 * Constructor.
	 *
	 * @param array $fieds
	 * @throws AnnotationException
	 */
	public function __construct(array $fields)
	{
		foreach ($fields as  $value) {
			if (is_string($value)) {
				$this->fields[] = $value;
			} else {
				throw new AnnotationException('Fields must be a array of strings.');
			}
		}
	}
}